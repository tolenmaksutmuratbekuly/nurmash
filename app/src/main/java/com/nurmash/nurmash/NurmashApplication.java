package com.nurmash.nurmash;

import android.app.Application;
import android.net.Uri;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.timber.StethoTree;
import com.nurmash.nurmash.config.DebugConfig;
import com.nurmash.nurmash.util.leakcanary.GlobalRefWatcher;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.nurmash.nurmash.util.stetho.StethoCustomConfigBuilder;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.DefaultLogger;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.SilentLogger;
import timber.log.CrashlyticsTree;
import timber.log.Timber;

public class NurmashApplication extends Application {
    @Override
    public void onCreate() {
        MultiDex.install(getApplicationContext());
        super.onCreate();
        setupStetho();
        setupTimber();
        setupPicasso();
        setupFabric();
        setupLeakCanary();
        setupVK();
    }

    private void setupStetho() {
        if (DebugConfig.STETHO_ENABLED) {
            Stetho.Initializer initializer = new StethoCustomConfigBuilder(this)
                    .viewHierarchyInspectorEnabled(false)
                    .build();
            Stetho.initialize(initializer);
        }
    }

    private void setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsTree());
        }

        if (DebugConfig.STETHO_ENABLED) {
            Timber.plant(new StethoTree());
        }
    }

    private void setupPicasso() {
        Picasso.Builder builder = new Picasso.Builder(getApplicationContext());
        builder.listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                Timber.tag("Picasso");
                Timber.e(exception, "uri: %s", uri);
            }
        });
        //noinspection ConstantConditions,PointlessBooleanExpression
        if (DebugConfig.PICASSO_DEBUG_ENABLED) {
            builder.indicatorsEnabled(true);
            builder.loggingEnabled(true);
        }
        Picasso.setSingletonInstance(builder.build());
        Transformations.initialize(this);
    }

    private void setupFabric() {
        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Crashlytics crashlytics = new Crashlytics.Builder().core(crashlyticsCore).build();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));
        TwitterCore twitterCore = new TwitterCore(authConfig);
        TweetComposer tweetComposer = new TweetComposer();
        Fabric fabric = new Fabric.Builder(this)
                .kits(crashlytics, twitterCore, tweetComposer)
                .logger(BuildConfig.DEBUG ? new DefaultLogger() : new SilentLogger()) // don't log anything to Logcat in release
                .build();
        Fabric.with(fabric);
    }

    private void setupLeakCanary() {
        if (DebugConfig.LEAK_CANARY_ENABLED) {
            GlobalRefWatcher.setInstance(LeakCanary.install(this));
        } else {
            GlobalRefWatcher.setInstance(RefWatcher.DISABLED);
        }
    }

    private void setupVK() {
        VKSdk.initialize(this);
    }
}
