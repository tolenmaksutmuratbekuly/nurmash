package com.nurmash.nurmash.model;

import android.support.annotation.NonNull;
import android.util.Pair;

import com.nurmash.nurmash.config.NurmashConfig.SocialNetwork;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.Survey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.text.TextUtils.isEmpty;

public class ParticipationData {
    public static final String VIDEO_STEP = "video";
    public static final String SHARE_STEP = "sharing";
    public static final String SURVEY_STEP = "survey";
    public static final String LAST_STEP = "last";
    public static final String ALL_STEPS_DONE = "done";
    // All possible steps in predefined order.
    public static final String[] ALL_STEPS = {VIDEO_STEP, SHARE_STEP, SURVEY_STEP};

    private final Promotion promotion;
    private final Competitor competitor;

    public ParticipationData(Promotion promotion, Competitor competitor) {
        if (promotion == null) {
            throw new NullPointerException("Promotion object cannot be null.");
        }
        if (competitor != null && competitor.promo_id != promotion.id) {
            throw new IllegalArgumentException(String.format("promotion.id=%d and competitor.promo_id=%d must be equal.",
                    promotion.id, competitor.promo_id));
        }
        this.promotion = promotion;
        this.competitor = competitor;
    }

    @NonNull
    public Promotion getPromotion() {
        return promotion;
    }

    public long getPromoId() {
        return promotion.id;
    }

    public Competitor getCompetitor() {
        return competitor;
    }

    public long getCompetitorId() {
        return competitor.id;
    }

    public String getCompetitorPhotoHash() {
        return competitor != null && competitor.sharing != null && !isEmpty(competitor.sharing.photo)
                ? competitor.sharing.photo
                : null;
    }

    public int getPromoState() {
        return promotion.getState(System.currentTimeMillis());
    }

    ///////////////////////////////////////////////////////////////////////////
    // Steps
    ///////////////////////////////////////////////////////////////////////////

    /**
     * @return A list of pairs ({step name}, {is step completed}) for the current promotion and competitor.<br />
     * The steps in the list are arranged in predefined order which is defined in {@link ParticipationData#ALL_STEPS}
     * constant. For example,<br />
     * - if there is a VIDEO_STEP it will go before SHARE_STEP and SURVEY_STEP if any.<br />
     * - if there is a SHARE_STEP it will go before SURVEY_STEP if any.<br />
     * - if there is a SURVEY_STEP it will be last in the list.<br />
     */
    public List<Pair<String, Boolean>> getSteps() {
        List<Pair<String, Boolean>> result = new ArrayList<>(promotion.steps.length);
        List<String> promoSteps = Arrays.asList(promotion.steps);
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < ALL_STEPS.length; ++i) {
            String step = ALL_STEPS[i];
            if (promoSteps.contains(step)) {
                result.add(Pair.create(step, isStepComplete(step)));
            }
        }
        return result;
    }

    public boolean promoHasStep(String step) {
        return promotion.steps != null && Arrays.asList(promotion.steps).contains(step);
    }

    public boolean isStepComplete(String step) {
        Integer value = competitor != null && competitor.steps != null ? competitor.steps.get(step) : null;
        return value != null && value == 1;
    }

    public String getPromoVideoId() {
        return promotion.steps_options.video.code;
    }

    public String getNextStep() {
        List<String> steps = Arrays.asList(promotion.steps);
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < ALL_STEPS.length; ++i) {
            String step = ALL_STEPS[i];
            if (steps.contains(step) && !isStepComplete(step)) return step;
        }
        return competitor.hasCompletedAllSteps() ? ALL_STEPS_DONE : LAST_STEP;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Survey
    ///////////////////////////////////////////////////////////////////////////

    public Survey getSurvey() {
        return promotion.steps_options.survey;
    }

    public String getSavedAnswerId() {
        return competitor != null && competitor.survey != null ? competitor.survey.answer : null;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Share
    ///////////////////////////////////////////////////////////////////////////

    public boolean hasSharedOn(@SocialNetwork String socialNetwork) {
        return competitor != null && competitor.hasSharedOn(socialNetwork);
    }
}
