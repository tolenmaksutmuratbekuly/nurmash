package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

@Parcel
public class Company {
    public long id;
    public String legal_name;
    public String logo;
}
