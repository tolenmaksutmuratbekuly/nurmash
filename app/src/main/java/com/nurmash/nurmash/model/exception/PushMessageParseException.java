package com.nurmash.nurmash.model.exception;

public class PushMessageParseException extends Exception {
    public PushMessageParseException(String detailMessage) {
        super(detailMessage);
    }

    public PushMessageParseException(Throwable throwable) {
        super(throwable);
    }
}
