package com.nurmash.nurmash.model.json.prizes;

import com.nurmash.nurmash.model.Money;

public class PrizeClaim {
    public long id;
    public String method;
    public Money amount;
}
