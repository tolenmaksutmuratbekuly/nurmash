package com.nurmash.nurmash.model.push.promo;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.CheckResult;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.promotion.PromoDetailActivity;

import java.util.Collection;

public class PromoNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    public PromoNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
    }

    @CheckResult
    public Notification build(Collection<PromoPushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.drawable.ic_new_promo_notification);
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);

        if (messages.size() == 1) {
            return buildSingleItemNotification(builder, messages.iterator().next());
        } else {
            return buildMultiItemNotification(builder, messages);
        }
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, PromoPushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        taskStack.addNextIntent(PromoDetailActivity.newIntent(context, msg.contest_id));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentTitle(context.getString(R.string.notification_title_new_promo_single));
        builder.setContentText(msg.contest_title);
        builder.setStyle(new Notification.BigTextStyle().bigText(msg.contest_title));
        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder, Collection<PromoPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentTitle(context.getString(R.string.notification_title_new_promo_multi));
        builder.setContentText(context.getString(R.string.notification_text_new_promo_multi, messages.size()));
        builder.setNumber(messages.size());

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (PromoPushMessage msg : messages) {
            inboxStyle.addLine(msg.contest_title);
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }
}
