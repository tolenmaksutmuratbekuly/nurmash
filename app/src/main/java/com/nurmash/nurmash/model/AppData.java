package com.nurmash.nurmash.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Holds information parsed from application's HTML page on Google Play Market.
 */
public class AppData {
    @NonNull public final String appId;
    @NonNull public final String title;
    @Nullable public final String description;
    @Nullable public final String imageUrl;

    AppData(@NonNull String appId, @NonNull String title, @Nullable String description, @Nullable String imageUrl) {
        this.appId = appId;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public static AppData parse(@NonNull String appId, Document document) {
        if (document == null) {
            throw new NullPointerException("document == null");
        }
        String title = findTitle(document);
        if (title == null || title.isEmpty()) {
            // Don't bother parsing other data if we couldn't even parse the application title.
            return null;
        }
        String description = findDescription(document);
        String imageUrl = findImageUrl(document);
        return new AppData(appId, title, description, imageUrl);
    }

    private static String findTitle(@NonNull Document doc) {
        Element mainTitle = doc.getElementsByClass("id-app-title").first();
        return mainTitle == null ? null : mainTitle.text();
    }

    private static String findDescription(@NonNull Document doc) {
        for (Element meta : doc.getElementsByTag("meta")) {
            if ("description".equalsIgnoreCase(meta.attr("name")) && meta.hasAttr("content")) {
                return meta.attr("content");
            }
        }
        return null;
    }

    private static String findImageUrl(@NonNull Document doc) {
        // There should be exactly one main-content element in the document.
        Element mainContent = doc.getElementsByClass("main-content").first();
        if (mainContent == null) {
            return null;
        }
        // There can be multiple cover-image elements in the document, but the one we need should be the descendant of
        // main-content element.
        Element coverImage = mainContent.getElementsByClass("cover-image").first();
        if (coverImage == null) {
            return null;
        }
        String imageUrl = coverImage.absUrl("src");
        return imageUrl == null || imageUrl.isEmpty() ? null : imageUrl;
    }
}
