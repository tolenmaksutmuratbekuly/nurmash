package com.nurmash.nurmash.model.push.unbanned;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;

import java.util.Collection;

public class UnBannedUserNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    public UnBannedUserNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
    }

    public Notification build(Collection<UnBannedUserPushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.drawable.ic_disqual_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
        builder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        builder.setContentTitle(context.getString(R.string.app_name));
        return messages.size() == 1 ? buildSingleItemNotification(builder, messages.iterator().next()) : buildMultiItemNotification(builder, messages);
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, UnBannedUserPushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContentText(getContentText(msg));
        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder,
                                                    Collection<UnBannedUserPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContentText(context.getString(R.string.app_name));

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (UnBannedUserPushMessage msg : messages) {
            inboxStyle.addLine(getContentText(msg));
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }

    private String getContentText(UnBannedUserPushMessage msg) {
        String contentText = "";
        if (msg.type != null && msg.type.equals("user_baned")) {
            contentText = context.getString(R.string.error_message_user_push_banned);
        } else if (msg.type != null && msg.type.equals("user_unbaned")) {
            contentText = context.getString(R.string.error_message_user_push_unbanned);
        }
        return contentText;
    }
}
