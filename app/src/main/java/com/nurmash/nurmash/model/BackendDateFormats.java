package com.nurmash.nurmash.model;

public class BackendDateFormats {
    public static final String DATE_TIME_TZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE = "yyyy-MM-dd";

    /**
     * Known date formats which can be returned by Nurmash API.
     * The order in which they are defined in this list matters, because we want to parse with the more precise format
     * first and fallback to less precise format if it fails.
     */
    public static final String[] ALL = {DATE_TIME_TZ, DATE_TIME, DATE};
}
