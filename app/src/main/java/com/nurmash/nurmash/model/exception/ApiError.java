package com.nurmash.nurmash.model.exception;

import java.util.Arrays;
import java.util.List;

public class ApiError extends RuntimeException {
    private static final String USER_BANNED_ERROR = "USER_BANNED";
    private static final String INVALID_GOOGLE_TOKEN_ERROR = "INVALID_GOOGLE_TOKEN";
    private static final String PROMO_NOT_FOUND_ERROR = "PROMO_NOT_FOUND";
    private static final String PROMO_HAS_ENDED_ERROR = "PROMO_HAS_ENDED";
    private static final String USER_PHOTO_COMPLAINT_EXISTS_ERROR = "USER_PHOTO_COMPLAINT_EXISTS";
    private static final String USER_COMMENT_COMPLAINT_EXISTS_ERROR = "USER_COMMENT_COMPLAINT_EXISTS";
    private static final String CANNOT_LINK_EXISTING_USER_ERROR = "CANNOT_LINK_EXISTING_USER";
    private static final String USER_INVITE_INVALID_QR_CODE = "INVALID_QR_CODE";
    private static final String USER_INVITE_YOU_ALREADY_INVITED = "YOU_ALREADY_INVITED";

    private static final List<String> COMMON_ERRORS = Arrays.asList(
            USER_PHOTO_COMPLAINT_EXISTS_ERROR,
            USER_COMMENT_COMPLAINT_EXISTS_ERROR,
            CANNOT_LINK_EXISTING_USER_ERROR
    );

    protected final List<String> errors;

    public ApiError(String... errors) {
        if (errors != null) {
            this.errors = Arrays.asList(Arrays.copyOf(errors, errors.length));
        } else {
            this.errors = null;
        }
    }

    @Override
    public String getMessage() {
        return "errors = " + errors;
    }

    public boolean shouldReport() {
        if (errors == null || errors.isEmpty()) {
            // This is weird, report it!
            return true;
        }
        // Search for an uncommon error.
        for (int i = 0; i < errors.size(); ++i) {
            if (!COMMON_ERRORS.contains(errors.get(i))) {
                return true;
            }
        }
        return false;
    }

    public boolean hasInvalidGoogleTokenError() {
        return errors != null && errors.contains(INVALID_GOOGLE_TOKEN_ERROR);
    }

    public boolean hasPromoNotFoundError() {
        return errors != null && errors.contains(PROMO_NOT_FOUND_ERROR);
    }

    public boolean hasPromoEndedError() {
        return errors != null && errors.contains(PROMO_HAS_ENDED_ERROR);
    }

    public boolean hasComplaintExistsError() {
        return errors != null
                && (errors.contains(USER_PHOTO_COMPLAINT_EXISTS_ERROR) || errors.contains(USER_COMMENT_COMPLAINT_EXISTS_ERROR));
    }

    public boolean hasCannotLinkExistingUserError() {
        return errors != null && errors.contains(CANNOT_LINK_EXISTING_USER_ERROR);
    }

    public boolean hasUserBannedError() {
        return errors != null && errors.contains(USER_BANNED_ERROR);
    }

    public boolean isQRCodeInvalidError() {
        return errors != null && errors.contains(USER_INVITE_INVALID_QR_CODE);
    }

    public boolean hasInvitedAlready() {
        return errors != null && errors.contains(USER_INVITE_YOU_ALREADY_INVITED);
    }
}
