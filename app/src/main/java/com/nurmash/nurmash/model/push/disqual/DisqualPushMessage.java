package com.nurmash.nurmash.model.push.disqual;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class DisqualPushMessage extends PushMessage {
    public final long contest_id;
    public final String contest_title;
    public final long competitor_id;

    public DisqualPushMessage(Bundle data) throws PushMessageParseException {
        super(data);
        contest_id = strictGetLong(data, "contest_id");
        contest_title = strictGetString(data, "contest_title");
        competitor_id = strictGetLong(data, "competitor_id");
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof DisqualPushMessage) && ((DisqualPushMessage) o).contest_id == contest_id;
    }

    @Override
    public int hashCode() {
        return longHashCode(contest_id);
    }
}
