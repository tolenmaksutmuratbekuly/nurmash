package com.nurmash.nurmash.model.withdraw;

import java.math.BigDecimal;
import java.util.Map;

@Deprecated
public interface WithdrawForm {
    ValidationError validate(BigDecimal amount);

    Map<String, String> buildParamsMap(BigDecimal amount);
}
