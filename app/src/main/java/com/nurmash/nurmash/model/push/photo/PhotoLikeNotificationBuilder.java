package com.nurmash.nurmash.model.push.photo;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.CheckResult;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;

import java.util.Collection;

public class PhotoLikeNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    public PhotoLikeNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
    }

    @CheckResult
    public Notification build(Collection<PhotoLikePushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.drawable.ic_photo_like_notification);
        builder.setContentTitle(context.getString(R.string.notification_title_photo_like));
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);

        if (messages.size() == 1) {
            return buildSingleItemNotification(builder, messages.iterator().next());
        } else {
            return buildMultiItemNotification(builder, messages);
        }
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, PhotoLikePushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        taskStack.addNextIntent(PhotoDetailActivity.newIntent(context, msg.competitor_id));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        String text = context.getString(R.string.notification_text_photo_like_single, msg.author_display_name, msg.contest_title);
        builder.setContentText(text);
        builder.setStyle(new Notification.BigTextStyle().bigText(text));

        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder, Collection<PhotoLikePushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        // TODO: 3/28/16 MainActivity should open with Profile tab selected.
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentText(context.getString(R.string.notification_text_photo_like_multi, messages.size()));

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (PhotoLikePushMessage msg : messages) {
            inboxStyle.addLine(context.getString(R.string.notification_text_photo_like_single, msg.author_display_name,
                    msg.contest_title));
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }
}
