package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

import static com.nurmash.nurmash.R.id.question;

@Parcel
public class Survey {
    public static final String SURVEY_TYPE_LIST = "list";
    public static final String SURVEY_TYPE_TREE = "tree";

    public String type;
    public Node[] nodes_list; // LIST
    public Node nodes_tree; // TREE

    @Deprecated public String question; // needed for backwards compatibility
    @Deprecated public Answer[] answers; // needed for backwards compatibility

    @Parcel
    public static class Node {
        public String question;
        public Answer[] answers;
    }

    @Parcel
    public static class Answer {
        public String id;
        public String title;
        public Children[] children;
    }

    @Parcel
    public static class Children {
        public String question;
        public String id;
        public Answer[] answers;
    }
}
