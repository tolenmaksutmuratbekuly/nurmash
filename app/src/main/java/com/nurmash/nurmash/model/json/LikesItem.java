package com.nurmash.nurmash.model.json;

import java.util.Date;

public class LikesItem {
    public long id;
    public User user;
    public Date created_at;
}
