package com.nurmash.nurmash.model;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.nurmash.nurmash.config.NurmashConfig.IMAGES_BASE_URL;

/**
 * Provides methods to generate photo urls of various sizes according to the following table:
 * The "gr" prefix means Golden Ratio.
 * <pre>
 * image_size = {
 *      'xs': (32, 32),
 *      's': (64, 64),
 *      'm': (128, 128),
 *      'l': (256, 256),
 *      'xl': (512, 512),
 *      'xxl': (1024, 1024),
 *      'xxxl': (2048, 2048),
 *
 *      'orig': (0, 0),
 *
 *      'grx': (32, 20),
 *      'grs': (64, 40),
 *      'grm': (128, 80),
 *      'grl': (256, 160),
 *      'grxl': (512, 320),
 *      'grxxl': (1024, 640),
 *      'grxxxl': (2048, 1280)
 * }
 * </pre>
 */
@SuppressWarnings({"SpellCheckingInspection", "unused"})
public class PhotoUrl {
    /**
     * Original size.
     */
    public static final String ORIGINAL = "orig";
    /**
     * 32 x 32
     */
    public static final String XS = "xs";
    /**
     * 64 x 64
     */
    public static final String S = "s";
    /**
     * 128 x 128
     */
    public static final String M = "m";
    /**
     * 256 x 256
     */
    public static final String L = "l";
    /**
     * 512 x 512
     */
    public static final String XL = "xl";
    /**
     * 1024 x 1024
     */
    public static final String XXL = "xxl";
    /**
     * 2048 x 2048
     */
    public static final String XXXL = "xxxl";
    /*
     * 32 x 20
     */
    public static final String GRX = "grx";
    /*
     * 64 x 40
     */
    public static final String GRS = "grs";
    /*
     * 128 x 80
     */
    public static final String GRM = "grm";
    /*
     * 256 x 160
     */
    public static final String GRL = "grl";
    /*
     * 512 x 320
     */
    public static final String GRXL = "grxl";
    /*
     * 1024 x 640
     */
    public static final String GRXXL = "grxxl";
    /*
     * 2048 x 1280
     */
    public static final String GRXXXL = "grxxxl";

    @StringDef({ORIGINAL, XS, S, M, L, XL, XXL, XXXL, GRX, GRS, GRM, GRL, GRXL, GRXXL, GRXXXL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PhotoSize {
    }

    /**
     * @return Url to the original photo.
     */
    public static String orig(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/orig/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>32 x 32</b> pixels.
     */
    public static String xs(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/xs/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>64 x 64</b> pixels.
     */
    public static String s(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/s/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>128 x 128</b> pixels.
     */
    public static String m(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/m/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>256 x 256</b> pixels.
     */
    public static String l(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/l/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>512 x 512</b> pixels.
     */
    public static String xl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/xl/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>1024 x 1024</b> pixels.
     */
    public static String xxl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/xxl/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>2048 x 2048</b> pixels.
     */
    public static String xxxl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/xxxl/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>32 x 20</b> pixels.
     */
    public static String grx(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grx/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>64 x 40</b> pixels.
     */
    public static String grs(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grs/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>128 x 80</b> pixels.
     */
    public static String grm(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grm/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>256 x 160</b> pixels.
     */
    public static String grl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grl/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>512 x 320</b> pixels.
     */
    public static String grxl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grxl/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>1024 x 640</b> pixels.
     */
    public static String grxxl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grxxl/" + hash : null;
    }

    /**
     * @return Url to a photo of size <b>2048 x 1280</b> pixels.
     */
    public static String grxxxl(String hash) {
        return (hash != null && !hash.isEmpty()) ? IMAGES_BASE_URL + "/grxxxl/" + hash : null;
    }
}
