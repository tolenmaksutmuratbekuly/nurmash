package com.nurmash.nurmash.model.json;

import android.net.UrlQuerySanitizer;

import com.google.gson.annotations.SerializedName;
import com.nurmash.nurmash.model.Money;

import org.parceler.Parcel;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import static android.text.TextUtils.isEmpty;

@Parcel
public class Promotion {
    /**
     * Indicates that determining the exact state of the promotion was not possible.
     * For example, because {@link #start_date} or {@link #stop_date} is {@code null}.
     */
    public static final int STATE_UNKNOWN = -1;
    /**
     * The promotions hasn't started yet.
     */
    public static final int STATE_BEFORE_START = 0;
    /**
     * The promotion has started. Users can participate, as well as like, comment and report other users' entries.
     */
    public static final int STATE_RUNNING = 1;
    /**
     * The promotion has ended less than 24h ago. Participation is not possible anymore, but users can still like,
     * comment and report other users' entries.
     */
    public static final int STATE_ENDED_RECENTLY = 2;
    /**
     * The promotion has ended. Participation and other actions are not possible anymore.
     */
    public static final int STATE_ENDED = 3;

    /**
     * The period after the end of the promotion for which users are still allowed to perform actions on other users'
     * entries.
     */
    public static final int USER_ACTIONS_ALLOWED_PERIOD = 24 * 60 * 60 * 100; // 24 hours in milliseconds.

    /**
     * Checks whether the given promotion state is active, i.e. user are allowed to like, comment and report other
     * competitors.
     *
     * @param state The promotion state to check.
     */
    public static boolean isActiveState(int state) {
        return state == STATE_RUNNING || state == STATE_ENDED_RECENTLY;
    }

    public static final String DEADLINE_TYPE_PARTICIPANT = "participant";
    public static final String DEADLINE_TYPE_DATE = "date";

    public long id;
    public String title;
    public String photo;
    public String description;
    public String promo_type;
    public Date start_date;
    public Date stop_date;
    public boolean can_participate;
    public String tz;
    public Prizes prizes;
    public Company company;
    public MobileApps mobile_apps;
    public int participants_amount;
    public RecentCompetitor[] recent_competitors;
    public String[] sample_photos;
    public String[] steps;
    public StepsOptions steps_options;
    public Winners winners;
    public boolean incompleted;
    public String deadline_type;
    public Integer participants_boundary;
    public Currency currency;

    public boolean isPhotoSharingPromo() {
        return "sharing".equals(promo_type) || (steps != null && Arrays.asList(steps).contains("sharing"));
    }

    public boolean hasWinners() {
        return winners != null;
    }

    /**
     * Returns the state of the promotion relative to the given point of time.
     *
     * @param currentTimeMillis The point of time relative to which the state of the promotion should be returned.
     *                          Given in milliseconds since January 1, 1970 00:00:00.0 UTC.
     */
    public int getState(long currentTimeMillis) {
        if (start_date == null || stop_date == null) {
            return STATE_UNKNOWN;
        }
        long startTime = start_date.getTime();
        long stopTime = stop_date.getTime();
        if (stopTime < startTime) {
            throw new IllegalStateException(String.format(Locale.US, "The stop_date=%s is before the start_date=%s.",
                    stop_date, start_date));
        }
        if (currentTimeMillis < startTime) {
            return STATE_BEFORE_START;
        } else if (currentTimeMillis < stopTime) {
            return STATE_RUNNING;
        } else if (currentTimeMillis < stopTime + USER_ACTIONS_ALLOWED_PERIOD) {
            return STATE_ENDED_RECENTLY;
        } else {
            return STATE_ENDED;
        }
    }

    public String getLinkedAppId() {
        if (mobile_apps == null || isEmpty(mobile_apps.android)) {
            return null;
        }
        // Extract application id parameter from query part of the url.
        // For example:
        //  com.nurmash.nurmash will be extracted from https://play.google.com/store/apps/details?id=com.nurmash.nurmash
        UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
        sanitizer.registerParameter("id", UrlQuerySanitizer.getAllButNulLegal());
        sanitizer.parseUrl(mobile_apps.android);
        String appId = sanitizer.getValue("id");
        return isEmpty(appId) ? null : appId;
    }

    public String getCurrencyCode() {
        if (currency != null && currency.code != null && currency.code.length() > 0) {
            return currency.code;
        } else {
            return null;
        }
    }

    @Parcel
    public static class Prizes {
        public Money place_1;
        public Money place_2;
        public Money place_3;
        public Money random_each;
        @SerializedName("1st_each") public Money first_each;
        public Money random;
        @SerializedName("1st") public Money first;
        public Money jp;
        Money totalPrize;

        public Integer random_pool;
        public Integer first_pool;

        public synchronized Money getTotalPrize() {
            if (totalPrize == null) {
                BigDecimal total = BigDecimal.ZERO;
                if (place_1 != null) {
                    total = total.add(place_1.getAmount());
                }
                if (place_2 != null) {
                    total = total.add(place_2.getAmount());
                }
                if (place_3 != null) {
                    total = total.add(place_3.getAmount());
                }

                final BigDecimal TWENTY = new BigDecimal(20);

                if (random != null) {
                    total = total.add(random.getAmount());
                } else if (random_each != null) {
                    total = total.add(random_each.getAmount().multiply(TWENTY));
                }

                if (first != null) {
                    total = total.add(first.getAmount());
                } else if (first_each != null) {
                    total = total.add(first_each.getAmount().multiply(TWENTY));
                }

                if (jp != null) {
                    total = total.add(jp.getAmount());
                }

                totalPrize = Money.getTypeMoney(total);
            }

            return totalPrize;
        }

        public int getFirstPoolSize() {
            if (first_pool == null)
                return 20;
            else
                return first_pool;
        }

        public int getRandomPoolSize() {
            if (random_pool == null)
                return 20;
            else
                return random_pool;
        }
    }

    @Parcel
    public static class MobileApps {
        public String android;
    }

    @Parcel
    public static class StepsOptions {
        public Video video;
        public Survey survey;
    }

    @Parcel
    public static class Video {
        public String code;
    }

    @Parcel
    public static class Winners {
        public Winner place_1;
        public Winner place_2;
        public Winner place_3;
        public Winner[] random_each;
        @SerializedName("1st_each") public Winner[] first_each;
    }

    @Parcel
    public static class Winner {
        public long id;
        public String photo;
        public User user;
    }

    @Parcel
    public static class RecentCompetitor {
        public String photo;
        public User user;
    }

    @Parcel
    public static class Currency {
        public String code;
        public Integer id;
    }
}
