package com.nurmash.nurmash.model;

public enum Gender {
    FEMALE(0), MALE(1);

    public final int intValue;

    Gender(int intValue) {
        this.intValue = intValue;
    }

    public static Gender fromInteger(Integer integerValue) {
        if (integerValue == null) {
            return null;
        }
        switch (integerValue) {
            case 0:
                return FEMALE;
            case 1:
                return MALE;
            default:
                return null;
        }
    }
}
