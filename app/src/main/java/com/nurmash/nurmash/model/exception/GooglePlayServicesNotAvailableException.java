package com.nurmash.nurmash.model.exception;

public class GooglePlayServicesNotAvailableException extends RuntimeException {
    private final int errorCode;

    public GooglePlayServicesNotAvailableException(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return "errorCode = " + errorCode;
    }
}
