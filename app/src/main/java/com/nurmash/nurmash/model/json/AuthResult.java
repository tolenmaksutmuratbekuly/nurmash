package com.nurmash.nurmash.model.json;

public class AuthResult {
    public String token;
    public User user;
}
