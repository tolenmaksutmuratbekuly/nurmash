package com.nurmash.nurmash.model.json.prizes;

import com.nurmash.nurmash.model.Money;

import org.parceler.Parcel;

@Parcel
public class WonPrizes {
    public String promoImg;
    public String promoTitle;
    public String promoDesc;
    public Money amount;
}

