package com.nurmash.nurmash.model.json.geo;

import org.parceler.Parcel;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.StringUtils.trim;

@Parcel
public class City {
    public long id;
    public String title;
    public String area;

    public String getTitleWithArea() {
        return isEmpty(area) ? trim(title) : trim(title) + " - " + trim(area);
    }
}
