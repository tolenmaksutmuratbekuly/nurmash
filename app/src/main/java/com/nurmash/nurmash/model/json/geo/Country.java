package com.nurmash.nurmash.model.json.geo;

import org.parceler.Parcel;

@Parcel
public class Country {
    public long id;
    public String title;
}