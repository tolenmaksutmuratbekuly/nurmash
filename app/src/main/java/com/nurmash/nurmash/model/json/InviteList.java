package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.Money;
import java.util.Date;

public class InviteList {
    public long referral_user_id;
    public Money bonus;
    public int is_fulfilled;
    public int is_withdrawn;
    public long referer_country_id;
    public long referral_country_id;
    public Date invitation_datetime;
    public long id;
    public long referer_user_id;
    public ReferralUser referral_user;
    MetaInfo meta_info;

    public class ReferralUser {
        public String first_name;
        public String last_name;
        public String display_name;
        public int is_approved;
        public String photo;
        public long id;
    }

    public String getCurrencyCode() {
        if (meta_info != null && meta_info.currency_code != null) {
            return meta_info.currency_code;
        } else {
            return null;
        }
    }
}