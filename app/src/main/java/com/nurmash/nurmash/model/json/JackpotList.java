package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.Money;

import java.math.BigDecimal;
import java.util.List;

public class JackpotList {
    public boolean display_winners;
    public List<JackpotListItem> jackpot_list;
    public List<Winners> winners;
    // Time left in seconds
    public double jackpot_time_left;

    public class JackpotListItem {
        public Money jp_amount;
        public Currency currency;

        public String getCurrencyCode() {
            if (currency != null && currency.code != null) {
                return currency.code;
            } else {
                return null;
            }
        }

        public Money getJackPotAmount() {
            return jp_amount == null ? Money.getTypeMoney(BigDecimal.ZERO) : jp_amount;
        }
    }

    public class Winners {
        public String atyear;
        public Money balance;
        public int country_id;
        public MetaInfo currency_info;
        public User user;

        public String getCurrencyCode() {
            if (currency_info != null && currency_info.currency_code != null) {
                return currency_info.currency_code;
            } else {
                return null;
            }
        }
    }

    public class Currency {
        public String code;
        public long id;
        public String title;
    }

    public class User {
        public String photo;
        public String display_name;
        public long id;
        public int is_approved;
    }
}
