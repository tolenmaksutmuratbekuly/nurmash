package com.nurmash.nurmash.model;


import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeLeft {
    private static final int SECONDS_IN_MINUTE = 60;
    private static final int SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE;
    private static final int SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR;

    public final long endTime;
    private int days;
    private int hours;
    private int minutes;
    private int seconds;
    private boolean noTimeLeft;

    public TimeLeft(int totalSecondsLeft) {
        endTime = System.currentTimeMillis() / 1000 + totalSecondsLeft;
        update(totalSecondsLeft);
    }

    public TimeLeft(Date endDate) {
        endTime = endDate.getTime() / 1000;
        update();
    }

    @NonNull
    public TimeLeft update() {
        update((int) (endTime - System.currentTimeMillis() / 1000));
        return this;
    }

    private void update(int secondsLeft) {
        if (secondsLeft <= 0) {
            noTimeLeft = true;
        } else {
            days = secondsLeft / SECONDS_IN_DAY;
            hours = (secondsLeft % SECONDS_IN_DAY) / SECONDS_IN_HOUR;
            minutes = (secondsLeft % SECONDS_IN_HOUR) / SECONDS_IN_MINUTE;
            seconds = secondsLeft % SECONDS_IN_MINUTE;
        }
    }

    public int getDays() {
        return days;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public boolean isNoTimeLeft() {
        return noTimeLeft;
    }

    public String getJackpotCardEndDate() {
        DateFormat objFormatter = new SimpleDateFormat("dd MMM");
        Calendar calendar = Calendar.getInstance();
        objFormatter.setTimeZone(calendar.getTimeZone());
        calendar.setTimeInMillis(endTime * 1000);
        String result = objFormatter.format(calendar.getTime());
        calendar.clear();
        return result;
    }
}
