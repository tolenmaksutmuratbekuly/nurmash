package com.nurmash.nurmash.model.json.prizes;

import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.TimeLeft;

import java.math.BigDecimal;

public class PrizeInfo {
    public Money user_balance;
    public Money jackpot;
    public int prize_count;

    // Time left in seconds
    public double jackpot_time_left;

    public Money getJackPotAmount() {
        return jackpot == null ? Money.getTypeMoney(BigDecimal.ZERO) : jackpot;
    }

    public TimeLeft getJackpotCountdown() {
        return new TimeLeft((int) jackpot_time_left);
    }

    public Money getUserBalanceAmount() {
        return user_balance == null ? Money.getTypeMoney(BigDecimal.ZERO) : user_balance;
    }

    public int getPrizeCount() {
        return prize_count;
    }
}
