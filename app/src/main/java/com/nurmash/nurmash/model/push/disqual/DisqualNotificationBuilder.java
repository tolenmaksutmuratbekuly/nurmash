package com.nurmash.nurmash.model.push.disqual;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.promotion.PromoDetailActivity;

import java.util.Collection;

public class DisqualNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    public DisqualNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
    }

    public Notification build(Collection<DisqualPushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.drawable.ic_disqual_notification);
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);

        if (messages.size() == 1) {
            return buildSingleItemNotification(builder, messages.iterator().next());
        } else {
            return buildMultiItemNotification(builder, messages);
        }
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, DisqualPushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        taskStack.addNextIntent(PromoDetailActivity.newIntent(context, msg.contest_id));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        String text = context.getString(R.string.notification_text_disqual_single, msg.contest_title);
        builder.setContentTitle(context.getString(R.string.notification_title_disqual));
        builder.setContentText(text);
        builder.setStyle(new Notification.BigTextStyle().bigText(text));

        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder, Collection<DisqualPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentTitle(context.getString(R.string.notification_title_disqual));
        builder.setContentText(context.getString(R.string.notification_text_disqual_multi, messages.size()));

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (DisqualPushMessage msg : messages) {
            inboxStyle.addLine(msg.contest_title);
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }
}
