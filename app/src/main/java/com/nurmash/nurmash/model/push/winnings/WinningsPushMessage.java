package com.nurmash.nurmash.model.push.winnings;

import android.os.Bundle;

import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

import java.math.BigDecimal;

public class WinningsPushMessage extends PushMessage {
    private static final String CONTEST_TYPE_SHARING = "sharing";

    public final long contest_id;
    public final String contest_title;
    public final String contest_type;
    public final long competitor_id;
    public final String win_type;
    public final Money win_amount;

    public WinningsPushMessage(Bundle data) throws PushMessageParseException {
        super(data);
        contest_id = strictGetLong(data, "contest_id");
        contest_title = strictGetString(data, "contest_title");
        contest_type = strictGetString(data, "contest_type");
        win_type = strictGetString(data, "win_type");
        win_amount = Money.getTypeMoney(new BigDecimal(strictGetString(data, "win_amount")));
        competitor_id = strictGetLong(data, "competitor_id");
    }

    public boolean isPhotoSharingContest() {
        return CONTEST_TYPE_SHARING.equals(contest_type);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof WinningsPushMessage)
                && ((WinningsPushMessage) o).contest_id == contest_id
                && ((WinningsPushMessage) o).competitor_id == competitor_id;
    }

    @Override
    public int hashCode() {
        return longHashCode(contest_id) ^ longHashCode(competitor_id);
    }
}
