package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

@Parcel
public class MetaInfo {
    public String currency_title;
    public long country_id;
    public String title_en;
    public long currency_id;
    public String title_ru;
    public String currency_code;
}