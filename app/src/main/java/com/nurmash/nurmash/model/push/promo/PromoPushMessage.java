package com.nurmash.nurmash.model.push.promo;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class PromoPushMessage extends PushMessage {
    public final long contest_id;
    public final String contest_title;

    public PromoPushMessage(Bundle data) throws PushMessageParseException {
        super(data);
        contest_id = strictGetLong(data, "contest_id");
        contest_title = strictGetString(data, "contest_title");
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof PromoPushMessage) && ((PromoPushMessage) o).contest_id == contest_id;
    }

    @Override
    public int hashCode() {
        return longHashCode(contest_id);
    }
}
