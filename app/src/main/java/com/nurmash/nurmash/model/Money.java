package com.nurmash.nurmash.model;

import android.support.annotation.NonNull;

import org.parceler.Parcel;

import java.math.BigDecimal;
import java.text.NumberFormat;

import timber.log.Timber;

import static java.math.BigDecimal.ZERO;

@Parcel
public class Money {
    BigDecimal amount;

    public static Money getTypeMoney(@NonNull BigDecimal amount) {
        return new Money(amount);
    }

    Money() {
        this(BigDecimal.ZERO);
    }

    Money(@NonNull BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isZeroAmount() {
        return ZERO.compareTo(amount) == 0;
    }

    @NonNull
    public BigDecimal getAmount() {
        return amount;
    }

    public String toString(@NonNull NumberFormat amountFormat) {
        return toString(amountFormat, " ");
    }

    public String toString(@NonNull NumberFormat amountFormat, String separator) {
        return amountFormat.format(amount);
    }

    public BigDecimal substractBigDecimal(BigDecimal substractValue) {
        return amount.subtract(substractValue);
    }

    public BigDecimal multiplyBigDecimal(BigDecimal multiplyValue) {
        return amount.multiply(multiplyValue);
    }

    public BigDecimal divideBigDecimal(BigDecimal amountValue, BigDecimal divideValue) {
        return amountValue.divide(divideValue);
    }

    public boolean canBeWithdrawn() {
        BigDecimal minWithdrawAmount = new BigDecimal(500);
        return (amount.compareTo(minWithdrawAmount) >= 0);
    }
}
