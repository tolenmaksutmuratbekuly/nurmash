package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.Money;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class PartnerBalance {
    public Bonuses bonuses;
    public MetaInfo meta_info;
    public LastClaim last_claim;

    @Parcel
    public static class Bonuses {
        public Money available;
        public Money invited;
        public Money withdrawn;
        public Money pending;
    }

    @Parcel
    public static class LastClaim {
        public int status;
        public Date created_at;
        public Money amount;
        public long id;
    }

    public String getCurrencyCode() {
        if (meta_info != null && meta_info.currency_code != null) {
            return meta_info.currency_code;
        } else {
            return null;
        }
    }
}
