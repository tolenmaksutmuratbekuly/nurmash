package com.nurmash.nurmash.model.withdraw;

import com.nurmash.nurmash.model.json.User;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.config.NurmashConfig.WIRE_TRANSFER;

@Deprecated
public class WireTransferForm implements WithdrawForm {
    public static final int ERROR_AMOUNT_LOW = 0;
    public static final int ERROR_FIRST_NAME_EMPTY = 1;
    public static final int ERROR_LAST_NAME_EMPTY = 2;
    public static final int ERROR_STREET1_EMPTY = 3;
    public static final int ERROR_ZIP_EMPTY = 4;
    public static final int ERROR_CITY_EMPTY = 5;
    public static final int ERROR_COUNTRY_EMPTY = 6;
    public static final int ERROR_BANK_NAME_EMPTY = 7;
    public static final int ERROR_SWIFT_EMPTY = 8;
    public static final int ERROR_IBAN_EMPTY = 9;

    private final BigDecimal MIN_AMOUNT = new BigDecimal(100);

    public String first_name;
    public String last_name;
    public String street1;
    public String street2;
    public String zip;
    public String city;
    public String region;
    public String country;
    public String bank_name;
    public String swift;
    public String iban;

    public void fillWithUser(User user) {
        if (user != null) {
            if (isEmpty(first_name) && isEmpty(last_name) &&
                    !isEmpty(user.first_name) && !isEmpty(user.last_name)) {
                first_name = user.first_name;
                last_name = user.last_name;
            }
            String countryTitle = user.country == null ? null : user.country.title;
            String regionTitle = user.region == null ? null : user.region.title;
            String cityTitle = user.city == null ? null : user.city.title;
            if (isEmpty(country) && isEmpty(region) && isEmpty(city)) {
                country = countryTitle;
                region = regionTitle;
                city = cityTitle;
            }
        }
    }

    @Override
    public ValidationError validate(BigDecimal amount) {
        if (amount == null) {
            throw new NullPointerException("amount == null");
        }
        Set<Integer> errors = new LinkedHashSet<>();
        if (amount.compareTo(MIN_AMOUNT) < 0) {
            errors.add(ERROR_AMOUNT_LOW);
        }
        if (isEmpty(first_name)) {
            errors.add(ERROR_FIRST_NAME_EMPTY);
        }
        if (isEmpty(last_name)) {
            errors.add(ERROR_LAST_NAME_EMPTY);
        }
        if (isEmpty(street1)) {
            errors.add(ERROR_STREET1_EMPTY);
        }
        if (isEmpty(zip)) {
            errors.add(ERROR_ZIP_EMPTY);
        }
        if (isEmpty(city)) {
            errors.add(ERROR_CITY_EMPTY);
        }
        if (isEmpty(country)) {
            errors.add(ERROR_COUNTRY_EMPTY);
        }
        if (isEmpty(bank_name)) {
            errors.add(ERROR_BANK_NAME_EMPTY);
        }
        if (isEmpty(swift)) {
            errors.add(ERROR_SWIFT_EMPTY);
        }
        if (isEmpty(iban)) {
            errors.add(ERROR_IBAN_EMPTY);
        }
        return errors.isEmpty() ? null : new ValidationError(errors);
    }

    @Override
    public Map<String, String> buildParamsMap(BigDecimal amount) {
        if (validate(amount) != null) {
            throw new IllegalStateException("Form validation failed.");
        }
        Map<String, String> params = new LinkedHashMap<>();
        params.put("amount", amount.toString());
        params.put("method", WIRE_TRANSFER);
        params.put("first_name", first_name);
        params.put("last_name", last_name);
        params.put("street1", street1);
        params.put("street2", street2);
        params.put("zip", zip);
        params.put("city", city);
        params.put("region", region);
        params.put("country", country);
        params.put("bank_name", bank_name);
        params.put("swift", swift);
        params.put("iban", iban);
        return params;
    }
}
