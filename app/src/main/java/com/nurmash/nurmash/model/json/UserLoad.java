package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

@Parcel
public class UserLoad extends User {
    public Flags flags;

    @Parcel
    public static class Flags {
        public int is_verified;
    }
}
