package com.nurmash.nurmash.model.push.verify;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class UserVerifyPushMessage extends PushMessage {
    public final String type;

    public UserVerifyPushMessage(Bundle data, String type) throws PushMessageParseException {
        super(data);
        this.type = type;
    }
}
