package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.Gender;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;

import org.parceler.Parcel;

import java.util.Arrays;
import java.util.Date;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.config.NurmashConfig.FACEBOOK;
import static com.nurmash.nurmash.config.NurmashConfig.GOOGLE;
import static com.nurmash.nurmash.config.NurmashConfig.VK;

@Parcel
public class User {
    public long id;
    public String first_name;
    public String last_name;
    public String first_name_native;
    public String last_name_native;
    public String display_name;
    public Integer sex;
    public String email;
    public Date birth_date;
    public String photo;
    public long country_id;
    public Country country;
    public long region_id;
    public Region region;
    public long city_id;
    public City city;
    public Extra extra;
    public double balance;
    public String[] providers;

    // Assume that profile is complete by default. If not, the backend will send the false value.
    public boolean is_profile_complete = true;

    public Gender getGender() {
        return Gender.fromInteger(sex);
    }

    public String getDisplayName() {
        if (!isEmpty(display_name)) {
            return display_name;
        } else {
            if (isEmpty(first_name)) {
                if (isEmpty(last_name)) {
                    return "";
                } else {
                    return last_name;
                }
            } else {
                if (isEmpty(last_name)) {
                    return first_name;
                } else {
                    return first_name + " " + last_name.substring(0, 1);
                }
            }
        }
    }

    public boolean hasLinkedProvider(String provider) {
        return providers != null && Arrays.asList(providers).contains(provider);
    }

    public boolean hasLinkedFacebookAccount() {
        return hasLinkedProvider(FACEBOOK);
    }

    public boolean hasLinkedGoogleAccount() {
        return hasLinkedProvider(GOOGLE);
    }

    public boolean hasLinkedVKAccount() {
        return hasLinkedProvider(VK);
    }

    public boolean hasFullName() {
        return !isEmpty(first_name) && !isEmpty(last_name);
    }
}
