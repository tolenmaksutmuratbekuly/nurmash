package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.Money;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class PrizeItem {
    public int status;
    public long user_id;
    public Contest contest;
    public Date created_at;
    public long competitor_id;

    public long contest_id;
    public long currency_id;
    public Currency currency;
    public Money amount;
    public Details details;
    public String processed_at;
    public long id;

    @Parcel
    public static class Contest {
        public String photo;
        public long id;
        public String brand;
        public String title;
    }

    @Parcel
    public static class Currency {
        public String code;
        public long id;
        public String title;
    }

    @Parcel
    public static class Details {
    }

    public String getCurrencyCode() {
        if (currency != null && currency.code != null) {
            return currency.code;
        } else {
            return null;
        }
    }
}
