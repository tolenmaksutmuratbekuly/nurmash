package com.nurmash.nurmash.model.json;

public class PrizeItemPojo {
    public static final int ITEM_PROMO_PRIZE = 0;
    public static final int ITEM_DISCOUNT = 1;
    public static final int ITEM_PRIZES = 2;
    public static final int ITEM_JACKPOT = 3;
    public static final int ITEM_PARTNER_PROGRAM = 4;
    public static final int ITEM_PAYMENT_HISTORY = 5;

    public String title;
    public int imgResource;
    public int notificationCount;
    public int itemType;

    public PrizeItemPojo(String title, int imgResource, int notificationCount, int itemType) {
        this.title = title;
        this.imgResource = imgResource;
        this.notificationCount = notificationCount;
        this.itemType = itemType;
    }
}
