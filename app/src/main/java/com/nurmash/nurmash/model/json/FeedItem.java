package com.nurmash.nurmash.model.json;

import java.util.Date;

public class FeedItem {
    private static final String ACTION_COMMENT = "comment";
    private static final String ACTION_COMMENT_REPLY = "comment_reply";
    private static final String ACTION_LIKE = "like";

    public long competitor_id;
    public User author;
    public String photo;
    public Date datetime;
    public String action;
    public ActionData action_data;

    public boolean isComment() {
        return ACTION_COMMENT.equals(action);
    }

    public boolean isCommentReply() {
        return  ACTION_COMMENT_REPLY.equals(action);
    }

    public boolean isLike() {
        return ACTION_LIKE.equals(action);
    }

    public class ActionData {
        public String text;
    }
}
