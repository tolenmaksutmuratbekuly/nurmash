package com.nurmash.nurmash.model.json;

import com.google.gson.annotations.SerializedName;
import com.nurmash.nurmash.config.NurmashConfig;
import com.nurmash.nurmash.config.NurmashConfig.SocialNetwork;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static android.text.TextUtils.isEmpty;

@Parcel
public class Competitor {
    public long id;
    @SerializedName("contest_id") public long promo_id;
    public Promotion promo;
    public long user_id;
    public User user;
    public int age;
    public long country_id;
    public long region_id;
    public long city_id;
    public Sharing sharing;
    public Date completed_at;
    public SurveyAnswer survey;
    public Map<String, Integer> steps;
    public Comment[] last_comments;
    public int like_count;
    public int comment_count;
    public boolean is_liked;
    public boolean has_user_complaint;
    public String url_hash;
    public int[] flags;

    public boolean isDisqualified() {
        return flags != null && flags.length > 1 && flags[1] == 1;
    }

    public boolean hasPhoto() {
        return sharing != null && !isEmpty(sharing.photo);
    }

    public String getPhotoHash() {
        return sharing != null ? sharing.photo : null;
    }

    public String getPhotoDescription() {
        return sharing != null ? sharing.description : null;
    }

    public long getUserId() {
        return user != null ? user.id : user_id;
    }

    public String getUserPhotoHash() {
        return user != null ? user.photo : null;
    }

    public boolean hasDescription() {
        return !isEmpty(getPhotoDescription());
    }

    public boolean hasComments() {
        return comment_count > 0;
    }

    public int getCommentCountIncludingDescription() {
        return hasDescription() ? comment_count + 1 : comment_count;
    }

    public String getVoteUrl(@SocialNetwork String socialNetwork) {
        if (isEmpty(url_hash)) {
            return null;
        }
        return NurmashConfig.GO_BASE_URL + "/" + socialNetwork + "/" + url_hash;
    }

    public boolean hasCompletedAllSteps() {
        return completed_at != null;
    }

    public boolean canLike() {
        return !isDisqualified() && !is_liked;
    }

    public boolean canComment() {
        return true;
    }

    public boolean canReport() {
        return !isDisqualified() && !has_user_complaint;
    }

    public long getPromoId() {
        return promo != null ? promo.id : promo_id;
    }

    public boolean hasSharedOn(@SocialNetwork String socialNetwork) {
        return sharing != null && sharing.share != null && sharing.share.contains(socialNetwork);
    }

    public boolean hasSharedOnAllNetworks() {
        return hasSharedOn(NurmashConfig.FACEBOOK) && hasSharedOn(NurmashConfig.TWITTER)
                && hasSharedOn(NurmashConfig.GOOGLE) && hasSharedOn(NurmashConfig.VK);
    }

    @Parcel
    public static class SurveyAnswer {
        public String answer;
    }

    @Parcel
    public static class Steps {
        public int video;
        public int survey;
        public int sharing;
    }

    @Parcel
    public static class Sharing {
        public String photo;
        public List<String> share;
        public String description;
    }
}
