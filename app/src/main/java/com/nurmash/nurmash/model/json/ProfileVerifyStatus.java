package com.nurmash.nurmash.model.json;

public class ProfileVerifyStatus {
    public int user_verified_status;
    public int status;
    public boolean pending_verification;

    public boolean isUserVerified() {
        return user_verified_status == 1;
    }

    public boolean needToShowUploadButtons() {
        return (!pending_verification && status == 0);
    }

    public boolean isPendingRequest() {
        return pending_verification;
    }

    public boolean isRejected() {
        // Previously sent request on verification, Document is NOT verified (rejected)
        return !pending_verification && status < 0;
    }

    public boolean isPhotoQualityBad() {
        // Previously sent request on verification, Document is NOT verified (photo's quality is bad)
        return !pending_verification && status == -2;
    }
}

