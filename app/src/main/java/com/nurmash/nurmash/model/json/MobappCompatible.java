package com.nurmash.nurmash.model.json;

import java.util.Date;
import java.util.List;

public class MobappCompatible {
    public List<AppInfoItem> requested_app;
    public List<AppInfoItem> on_store_app;

    public class AppInfoItem {
        public boolean on_store;
        public boolean is_compatible;
        public String version;
        public String os;
        public Date published_date;
    }

    public boolean isRequestedAppCompatible() {
        if (requested_app != null && requested_app.size() > 0) {
            return !requested_app.get(0).is_compatible ? true : false; // IF returns true need to update application
        } else {
            return false;
        }
    }

    public String getVersion() {
        if (on_store_app != null && on_store_app.size() > 0) {
            if (on_store_app.get(0).version != null) {
                return on_store_app.get(0).version;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
}