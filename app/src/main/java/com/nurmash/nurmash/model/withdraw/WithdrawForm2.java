package com.nurmash.nurmash.model.withdraw;

import java.util.Map;

public interface WithdrawForm2 {
    ValidationError validate();

    Map<String, String> buildParamsMap();
}
