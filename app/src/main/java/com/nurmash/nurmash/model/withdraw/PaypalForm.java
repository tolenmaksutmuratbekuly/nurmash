package com.nurmash.nurmash.model.withdraw;

import com.nurmash.nurmash.model.json.User;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.config.NurmashConfig.PAYPAL;

@Deprecated
public class PaypalForm implements WithdrawForm {
    public static final int ERROR_AMOUNT_ZERO = 0;
    public static final int ERROR_ACCOUNT_EMPTY = 1;
    public static final int ERROR_FIRST_NAME_EMPTY = 2;
    public static final int ERROR_LAST_NAME_EMPTY = 3;

    public String account;
    public String first_name;
    public String last_name;

    public void fillWithUser(User user) {
        if (user != null) {
            if (isEmpty(account) && !isEmpty(user.email)) {
                account = user.email;
            }
            if (isEmpty(first_name) && isEmpty(last_name) &&
                    (!isEmpty(user.first_name) || !isEmpty(user.last_name))) {
                first_name = user.first_name;
                last_name = user.last_name;
            }
        }
    }

    @Override
    public ValidationError validate(BigDecimal amount) {
        if (amount == null) {
            throw new NullPointerException("amount == null");
        }
        Set<Integer> errors = new LinkedHashSet<>();
        if (amount.compareTo(BigDecimal.ZERO) == 0) {
            errors.add(ERROR_AMOUNT_ZERO);
        }
        if (isEmpty(account)) {
            errors.add(ERROR_ACCOUNT_EMPTY);
        }
        if (isEmpty(first_name)) {
            errors.add(ERROR_FIRST_NAME_EMPTY);
        }
        if (isEmpty(last_name)) {
            errors.add(ERROR_LAST_NAME_EMPTY);
        }
        return errors.isEmpty() ? null : new ValidationError(errors);
    }

    @Override
    public Map<String, String> buildParamsMap(BigDecimal amount) {
        if (validate(amount) != null) {
            throw new IllegalStateException("Form validation failed.");
        }
        Map<String, String> params = new LinkedHashMap<>();
        params.put("amount", amount.toString());
        params.put("method", PAYPAL);
        params.put("account", account);
        params.put("first_name", first_name);
        params.put("last_name", last_name);
        return params;
    }
}
