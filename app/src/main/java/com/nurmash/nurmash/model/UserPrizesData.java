package com.nurmash.nurmash.model;

import android.support.annotation.NonNull;

import com.nurmash.nurmash.model.json.prizes.PrizeInfo;

import java.math.BigDecimal;

public class UserPrizesData {
    @NonNull public final Money jackpotAmount;
    @NonNull public final TimeLeft jackpotCountdown;
    @NonNull public final Money userBalanceAmount;

    public UserPrizesData(PrizeInfo prizeInfo) {
        this.jackpotAmount = prizeInfo == null ? Money.getTypeMoney(BigDecimal.ZERO) : prizeInfo.jackpot;
        this.jackpotCountdown = new TimeLeft(prizeInfo == null ? 0 : (int) prizeInfo.jackpot_time_left);
        this.userBalanceAmount = prizeInfo == null ? Money.getTypeMoney(BigDecimal.ZERO) : prizeInfo.user_balance;
    }
}
