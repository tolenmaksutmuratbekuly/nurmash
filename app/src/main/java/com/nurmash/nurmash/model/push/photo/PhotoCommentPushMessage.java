package com.nurmash.nurmash.model.push.photo;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class PhotoCommentPushMessage extends PushMessage {
    public final String contest_title;
    public final long competitor_id;
    public final String author_display_name;
    public final String comment_body;

    public PhotoCommentPushMessage(Bundle data) throws PushMessageParseException {
        super(data);
        contest_title = strictGetString(data, "contest_title");
        competitor_id = strictGetLong(data, "competitor_id");
        author_display_name = strictGetString(data, "author_display_name");
        comment_body = strictGetString(data, "comment_body");

    }
}
