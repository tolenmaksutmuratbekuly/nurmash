package com.nurmash.nurmash.model.withdraw;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static android.text.TextUtils.isEmpty;

public class CardTransferForm implements WithdrawForm2 {
    public static final int ERROR_INVALID_CARD_NUMBER = 1;
    public static final int ERROR_INVALID_CARD_CONFIRMATION_NUMBER = 2;

    private final long prize_id;
    private final boolean isInviteBonus;
    private final String[] card_number = new String[4];
    private final String[] card_number_confirm = new String[4];

    public CardTransferForm(boolean isInviteBonus, long prize_id) {
        this.prize_id = prize_id;
        this.isInviteBonus = isInviteBonus;
    }

    @Override
    public ValidationError validate() {
        Set<Integer> errors = new LinkedHashSet<>();
        if (!validateCardNumber()) {
            errors.add(ERROR_INVALID_CARD_NUMBER);
        }

        if (!validateCardConfirmation()) {
            errors.add(ERROR_INVALID_CARD_CONFIRMATION_NUMBER);
        }
        if (errors.isEmpty()) {
            return null;
        } else {
            return new ValidationError(errors);
        }
    }

    @Override
    public Map<String, String> buildParamsMap() {
        if (validate() != null) {
            throw new IllegalStateException("Form validation failed.");
        }
        Map<String, String> params = new LinkedHashMap<>();
        params.put("method", "card");
        if (!isInviteBonus) {
            params.put("prize_id", String.valueOf(prize_id));
        }
        params.put("card_number", card_number[0] + card_number[1] + card_number[2] + card_number[3]);
        return params;
    }

    public void setCardNumberPart(int part, String value, String confirmValue) {
        if (part < 0 || part > 3) {
            throw new IllegalArgumentException("part must lie inside [0..3] range");
        }
        card_number[part] = value;
        card_number_confirm[part] = confirmValue;
    }

    private boolean validateCardNumber() {
        for (int part = 0; part < 4; ++part) {
            if (isEmpty(card_number[part]) || card_number[part].length() != 4) {
                return false;
            }
            for (int i = 0; i < 4; ++i) {
                if (!Character.isDigit(card_number[part].charAt(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateCardConfirmation() {
        boolean result = true;
        for (int part = 0; part < 4; ++part) {
            if (!(card_number[part].equals(card_number_confirm[part]))) {
                result = false;
            }
        }
        return result;
    }
}
