package com.nurmash.nurmash.model.push.unbanned;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class UnBannedUserPushMessage extends PushMessage {
    public final String type;
    public UnBannedUserPushMessage(Bundle data, String type) throws PushMessageParseException {
        super(data);
        this.type = type;
    }
}
