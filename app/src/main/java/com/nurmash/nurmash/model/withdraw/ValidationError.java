package com.nurmash.nurmash.model.withdraw;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.Set;

public class ValidationError {
    @NonNull
    public final Set<Integer> errorCodes;

    public ValidationError(@NonNull Set<Integer> errors) {
        if (errors.isEmpty()) {
            throw new IllegalArgumentException("errors is empty");
        }
        errorCodes = Collections.unmodifiableSet(errors);
    }

    public boolean hasErrorCode(int errorCode) {
        return errorCodes.contains(errorCode);
    }
}
