package com.nurmash.nurmash.model.push;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.disqual.DisqualPushMessage;
import com.nurmash.nurmash.model.push.photo.CommentAnswerPushMessage;
import com.nurmash.nurmash.model.push.photo.PhotoCommentPushMessage;
import com.nurmash.nurmash.model.push.photo.PhotoLikePushMessage;
import com.nurmash.nurmash.model.push.promo.PromoPushMessage;
import com.nurmash.nurmash.model.push.unbanned.UnBannedUserPushMessage;
import com.nurmash.nurmash.model.push.verify.UserVerifyPushMessage;
import com.nurmash.nurmash.model.push.winnings.WinningsPushMessage;

import timber.log.Timber;

public abstract class PushMessage {
    public final long user_id;
    public final String type;

    protected PushMessage(Bundle data) throws PushMessageParseException {
        user_id = strictGetLong(data, "user_id");
        type = strictGetString(data, "type");
    }

    @Nullable
    public static PushMessage fromBundle(Bundle data) {
        try {
            return fromBundleStrict(data);
        } catch (PushMessageParseException e) {
            Timber.e(e, null);
            return null;
        }
    }

    @NonNull
    private static PushMessage fromBundleStrict(Bundle data) throws PushMessageParseException {
        String type = strictGetString(data, "type");
        switch (type) {
            case "contest_new":
                return new PromoPushMessage(data);
            case "contest_win":
                return new WinningsPushMessage(data);
            case "participant_disqualified":
                return new DisqualPushMessage(data);
            case "photo_like":
                return new PhotoLikePushMessage(data);
            case "photo_comment":
                return new PhotoCommentPushMessage(data);
            case "photo_comment_reply":
                return new CommentAnswerPushMessage(data);
            case "user_baned":
                return new UnBannedUserPushMessage(data, type);
            case "user_unbaned":
                return new UnBannedUserPushMessage(data, type);
            case "user_verify_approved":
                return new UserVerifyPushMessage(data, type);
            case "user_verify_rejected":
                return new UserVerifyPushMessage(data, type);
            default:
                throw new PushMessageParseException("Unrecognized push message type: " + type);
        }
    }

    protected static long strictGetLong(Bundle data, String key) throws PushMessageParseException {
        try {
            return Long.parseLong(strictGetString(data, key));
        } catch (NumberFormatException e) {
            throw new PushMessageParseException(e);
        }
    }

    protected static boolean strictGetBool(Bundle data, String key) throws PushMessageParseException {
        try {
            return Boolean.parseBoolean(strictGetString(data, key));
        } catch (Exception e) {
            throw new PushMessageParseException(e);
        }
    }


    @NonNull
    protected static String strictGetString(Bundle data, String key) throws PushMessageParseException {
        if (data == null) {
            throw new PushMessageParseException("Null data");
        }
        String value = data.getString(key);
        if (value == null) {
            throw new PushMessageParseException("Null value for key: " + key);
        }
        return value;
    }

    protected static int longHashCode(long id) {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "PushMessage{user_id=" + user_id
                + ", type=" + type
                + "}";
    }
}
