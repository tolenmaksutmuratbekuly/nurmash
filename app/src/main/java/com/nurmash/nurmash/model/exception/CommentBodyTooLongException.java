package com.nurmash.nurmash.model.exception;

public class CommentBodyTooLongException extends Throwable {
    public final int commentBodyLength;
    public final int commentBodyLengthLimit;

    public CommentBodyTooLongException(int length, int limit) {
        this.commentBodyLength = length;
        this.commentBodyLengthLimit = limit;
    }

    @Override
    public String getMessage() {
        return "length = " + commentBodyLength + ", limit = " + commentBodyLengthLimit;
    }
}
