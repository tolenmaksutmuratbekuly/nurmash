package com.nurmash.nurmash.model.push.verify;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.prizes.verification.DocsVerifiyResultActivity;

import java.util.Collection;

public class UserVerifyNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    public UserVerifyNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
    }

    public Notification build(Collection<UserVerifyPushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.drawable.ic_disqual_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
        builder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        builder.setContentTitle(context.getString(R.string.app_name));
        return messages.size() == 1 ? buildSingleItemNotification(builder, messages.iterator().next()) : buildMultiItemNotification(builder, messages);
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, UserVerifyPushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        if (msg.type != null && msg.type.equals("user_verify_approved")) {
            taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        } else if (msg.type != null && msg.type.equals("user_verify_rejected")) {
            taskStack.addNextIntent(DocsVerifiyResultActivity.newIntent(context));
        }
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContentText(getContentText(msg));
        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder,
                                                    Collection<UserVerifyPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContentText(context.getString(R.string.app_name));

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (UserVerifyPushMessage msg : messages) {
            inboxStyle.addLine(getContentText(msg));
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }

    private String getContentText(UserVerifyPushMessage msg) {
        String contentText = "";
        if (msg.type != null && msg.type.equals("user_verify_approved")) {
            contentText = context.getString(R.string.notification_text_user_verify_approved);
        } else if (msg.type != null && msg.type.equals("user_verify_rejected")) {
            contentText = context.getString(R.string.notification_text_user_verify_rejected);
        }
        return contentText;
    }
}
