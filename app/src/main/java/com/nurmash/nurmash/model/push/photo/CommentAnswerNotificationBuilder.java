package com.nurmash.nurmash.model.push.photo;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;

import java.util.Collection;

public class CommentAnswerNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    public CommentAnswerNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
    }

    public Notification build(Collection<CommentAnswerPushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.drawable.ic_photo_comment_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
        builder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        if (messages.size() == 1) {
            builder.setContentTitle(context.getString(R.string.notification_title_comment_answer_1));
            return buildSingleItemNotification(builder, messages.iterator().next());

        } else {
            builder.setContentTitle(context.getString(R.string.notification_title_comment_answers));
            long competitorId = getCompetitorId(messages);
            if (competitorId != -1) {
                return buildSinglePhotoNotification(builder, competitorId, messages);
            } else {
                return buildMultiItemNotification(builder, messages);
            }
        }
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, CommentAnswerPushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        taskStack.addNextIntent(PhotoDetailActivity.newIntent(context, msg.competitor_id));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentText(context.getString(R.string.notification_text_photo_comment_single, msg.author_display_name,
                msg.comment_body));

        return builder.build();
    }

    private Notification buildSinglePhotoNotification(Notification.Builder builder,
                                                      long competitorId,
                                                      Collection<CommentAnswerPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        taskStack.addNextIntent(PhotoDetailActivity.newIntent(context, competitorId));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentText(context.getString(R.string.notification_text_photo_comment_multi, messages.size()));
        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (CommentAnswerPushMessage msg : messages) {
            inboxStyle.addLine(context.getString(R.string.notification_text_photo_comment_single,
                    msg.author_display_name, msg.comment_body));
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder,
                                                    Collection<CommentAnswerPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        // TODO: 3/28/16 MainActivity should open with Profile tab selected.
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        builder.setContentText(context.getString(R.string.notification_text_photo_comment_multi, messages.size()));

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (CommentAnswerPushMessage msg : messages) {
            inboxStyle.addLine(context.getString(R.string.notification_text_photo_comment_single, msg.author_display_name,
                    msg.comment_body));
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }

    private static long getCompetitorId(Collection<CommentAnswerPushMessage> messages) {
        long competitorId = -1;
        for (CommentAnswerPushMessage msg : messages) {
            if (competitorId == -1) {
                competitorId = msg.competitor_id;
            } else if (competitorId != msg.competitor_id) {
                return -1;
            }
        }
        return competitorId;
    }
}
