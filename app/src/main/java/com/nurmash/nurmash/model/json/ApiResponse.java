package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.exception.ApiError;

public class ApiResponse<T> {
    public boolean success;
    public T data;
    public String[] errors;

    public T extractData() throws ApiError {
        if (!success) {
            throw new ApiError(errors);
        }
        return data;
    }
}
