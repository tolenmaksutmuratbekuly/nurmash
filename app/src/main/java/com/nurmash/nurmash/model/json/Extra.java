package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

@Parcel
public class Extra {
    public String avatar;
    public String profile_url;
}

