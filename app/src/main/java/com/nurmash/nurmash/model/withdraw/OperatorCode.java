package com.nurmash.nurmash.model.withdraw;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({OperatorCode.ACTIV, OperatorCode.KCELL, OperatorCode.TELE2, OperatorCode.BEELINE, OperatorCode.ALTEL})
@Retention(RetentionPolicy.SOURCE)
public @interface OperatorCode {
    String ACTIV = "activ";
    String KCELL = "kcell";
    String TELE2 = "tele2";
    String BEELINE = "beeline";
    String ALTEL = "altel";
}
