package com.nurmash.nurmash.model.push.photo;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class PhotoLikePushMessage extends PushMessage {
    public final String contest_title;
    public final long competitor_id;
    public final String author_display_name;

    public PhotoLikePushMessage(Bundle data) throws PushMessageParseException {
        super(data);
        contest_title = strictGetString(data, "contest_title");
        competitor_id = strictGetLong(data, "competitor_id");
        author_display_name = strictGetString(data, "author_display_name");
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof PhotoLikePushMessage)
                && ((PhotoLikePushMessage) o).competitor_id == competitor_id
                && ((PhotoLikePushMessage) o).author_display_name.equals(author_display_name);
    }

    @Override
    public int hashCode() {
        return longHashCode(competitor_id) ^ author_display_name.hashCode();
    }
}
