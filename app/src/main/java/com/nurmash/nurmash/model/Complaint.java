package com.nurmash.nurmash.model;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.R;

import org.parceler.Parcel;

import java.util.Arrays;
import java.util.List;

@Parcel
public class Complaint {
    private static final String SPAM = "spam";
    private static final String PORNO = "porno";
    private static final String VIOLENCE_SCENES = "violence_scenes";
    private static final String VIOLENCE_PROMOTION = "violence_promotion";
    private static final String PROFANITY = "profanity";
    private static final String PROVOCATIVE_CONTENT = "provocative_content";

    public static List<Complaint> getPhotoComplaints() {
        return Arrays.asList(
                new Complaint(SPAM, R.string.label_complaint_spam),
                new Complaint(PORNO, R.string.label_complaint_porno),
                new Complaint(VIOLENCE_SCENES, R.string.label_complaint_violence_scenes),
                new Complaint(VIOLENCE_PROMOTION, R.string.label_complaint_violence_promotion),
                new Complaint(PROFANITY, R.string.label_complaint_profanity),
                new Complaint(PROVOCATIVE_CONTENT, R.string.label_complaint_provocative_content)
        );
    }

    public static List<Complaint> getCommentComplaints() {
        return Arrays.asList(
                new Complaint(SPAM, R.string.label_complaint_spam),
                new Complaint(VIOLENCE_SCENES, R.string.label_complaint_violence_scenes),
                new Complaint(PROFANITY, R.string.label_complaint_profanity),
                new Complaint(PROVOCATIVE_CONTENT, R.string.label_complaint_provocative_content)
        );
    }

    String code;
    @StringRes int descriptionStringId;

    Complaint() {
    }

    public Complaint(String code, int descriptionStringId) {
        this.code = code;
        this.descriptionStringId = descriptionStringId;
    }

    public String getCode() {
        return code;
    }

    @StringRes
    public int getDescriptionStringId() {
        return descriptionStringId;
    }
}
