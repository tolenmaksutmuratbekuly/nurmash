package com.nurmash.nurmash.model;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.util.Patterns;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;

import org.parceler.Parcel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.StringUtils.length;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.YEAR;

@Parcel
public class ProfileEditForm {
    private static final int DEFAULT_MAX_LENGTH = 255;
    private static final int DISPLAY_NAME_MAX_LENGTH = 32;
    private static final int MIN_AGE = 18;

    public String displayName;
    public String firstName;
    public String lastName;
    public String nativeFirstName;
    public String nativeLastName;
    public String email;
    public Date birthdate;
    public Gender gender;
    public Country country;
    public Region region;
    public City city;
    public String photoHash;

    public String getBirthdateStr(@NonNull String dateFormatStr) {
        return birthdate == null ? null : getBirthdateStr(new SimpleDateFormat(dateFormatStr, Locale.US));
    }

    public String getBirthdateStr(@NonNull DateFormat dateFormat) {
        return birthdate == null ? null : dateFormat.format(birthdate);
    }

    public long getCountryId() {
        return country == null ? 0 : country.id;
    }

    public String getCountryTitle() {
        return country == null ? null : country.title;
    }

    public long getRegionId() {
        return region == null ? 0 : region.id;
    }

    public String getRegionTitle() {
        return region == null ? null : region.title;
    }

    public long getCityId() {
        return city == null ? 0 : city.id;
    }

    public String getCityTitle() {
        return city == null ? null : city.title;
    }

    @StringRes
    public int validateFirstName() {
        if (isEmpty(firstName)) {
            return R.string.error_message_form_first_name_empty;
        } else if (length(firstName) > DEFAULT_MAX_LENGTH) {
            return R.string.error_message_form_input_too_long;
        } else {
            return 0;
        }
    }

    @StringRes
    public int validateLastName() {
        if (isEmpty(lastName)) {
            return R.string.error_message_form_last_name_empty;
        } else if (length(lastName) > DEFAULT_MAX_LENGTH) {
            return R.string.error_message_form_input_too_long;
        } else {
            return 0;
        }
    }

    @StringRes
    public int validateDisplayName() {
        if (isEmpty(displayName)) {
            return R.string.error_message_form_display_name_empty;
        } else if (displayName.length() > DISPLAY_NAME_MAX_LENGTH) {
            return R.string.error_message_form_input_too_long;
        } else {
            return 0;
        }
    }

    @StringRes
    public int validateNativeFirstName() {
        return length(nativeFirstName) > DEFAULT_MAX_LENGTH ? R.string.error_message_form_input_too_long : 0;
    }

    @StringRes
    public int validateNativeLastName() {
        return length(nativeLastName) > DEFAULT_MAX_LENGTH ? R.string.error_message_form_input_too_long : 0;
    }

    @StringRes
    public int validateEmail() {
        if (isEmpty(email)) {
            return R.string.error_message_form_email_empty;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return R.string.error_message_form_email_not_valid;
        } else {
            return 0;
        }
    }

    @StringRes
    public int validateBirthdate() {
        if (birthdate == null) {
            return R.string.error_message_form_birthdate_not_selected;
        } else {
            Calendar birthdateCal = new GregorianCalendar();
            birthdateCal.setTime(birthdate);
            Calendar todayCal = new GregorianCalendar();
            todayCal.setTimeInMillis(System.currentTimeMillis());
            int years = todayCal.get(YEAR) - birthdateCal.get(YEAR);
            if (years < MIN_AGE || (years == MIN_AGE && todayCal.get(DAY_OF_YEAR) < birthdateCal.get(DAY_OF_YEAR))) {
                return R.string.error_message_form_birthdate_too_young;
            } else {
                return 0;
            }
        }
    }

    @StringRes
    public int validateGender() {
        return gender == null ? R.string.error_message_form_gender_not_selected : 0;
    }

    @StringRes
    public int validateCountry() {
        return country == null || country.id == 0 ? R.string.error_message_form_country_not_selected : 0;
    }

    @StringRes
    public int validateRegion() {
        return 0;
    }

    @StringRes
    public int validateCity() {
        return city == null || city.id == 0 ? R.string.error_message_form_city_not_selected : 0;
    }

    @StringRes
    public int validatePhoto() {
        return isEmpty(photoHash) ? R.string.error_message_form_photo_not_selected : 0;
    }

    public boolean allFieldsValid(boolean isRegistration) {
        // Check that all of the validators return 0.
        int all = isRegistration
                ?
                validateFirstName()
                        | validateLastName()
                        | validateEmail()
                        | validateBirthdate()
                        | validateGender()
                        | validateCountry()
                        | validateRegion()
                        | validateCity()
                        | validatePhoto()
                :
                validateDisplayName()
                        | validateFirstName()
                        | validateLastName()
                        | validateNativeFirstName()
                        | validateNativeLastName()
                        | validateEmail()
                        | validateBirthdate()
                        | validateGender()
                        | validateCountry()
                        | validateRegion()
                        | validateCity()
                        | validatePhoto();

        return all == 0;
    }

    public void fillWithUser(User user) {
        if (user != null) {
            setOptFirstName(user.first_name);
            setOptLastName(user.last_name);
            setOptNativeFirstName(user.first_name_native);
            setOptNativeLastName(user.last_name_native);
            setOptDisplayName(user.getDisplayName());
            setOptEmail(user.email);
            setOptBirthdate(user.birth_date);
            setOptGender(user.getGender());
            setOptLocation(user.country, user.region, user.city);
            setOptPhotoHash(user.photo);
        }
    }

    private void setOptFirstName(String optFirstName) {
        if (isEmpty(firstName)) {
            firstName = optFirstName;
        }
    }

    private void setOptLastName(String optLastName) {
        if (isEmpty(lastName)) {
            lastName = optLastName;
        }
    }

    private void setOptNativeFirstName(String optNativeFirstName) {
        if (isEmpty(nativeFirstName)) {
            nativeFirstName = optNativeFirstName;
        }
    }

    private void setOptNativeLastName(String optNativeLastName) {
        if (isEmpty(nativeLastName)) {
            nativeLastName = optNativeLastName;
        }
    }

    private void setOptDisplayName(String optDisplayName) {
        if (isEmpty(displayName)) {
            displayName = optDisplayName;
        }
    }

    private void setOptEmail(String optEmail) {
        if (isEmpty(email)) {
            email = optEmail;
        }
    }

    private void setOptBirthdate(Date optBirthdate) {
        if (birthdate == null) {
            birthdate = optBirthdate;
        }
    }

    private void setOptGender(Gender newGender) {
        if (gender == null) {
            gender = newGender;
        }
    }

    private void setOptLocation(Country newCountry, Region newRegion, City newCity) {
        if (country == null) {
            country = newCountry;
            region = newRegion;
            city = newCity;
        }
    }

    private void setOptPhotoHash(String newPhotoHash) {
        if (isEmpty(photoHash)) {
            photoHash = newPhotoHash;
        }
    }
}
