package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class Comment {
    public long id;
    public User user;
    public String body;
    public Date created_at;

    @Override
    public String toString() {
        return "Comment(" + id + ")";
    }
}
