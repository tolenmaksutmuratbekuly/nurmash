package com.nurmash.nurmash.model.withdraw;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static android.text.TextUtils.isEmpty;

public class MobileBalanceTransferForm implements WithdrawForm2 {
    public static final int ERROR_OPERATOR_NOT_SELECTED = 1;
    public static final int ERROR_INVALID_PHONE_NUMBER = 2;
    public static final int ERROR_PHONE_NUMBERS_DO_NOT_MATCH = 3;

    private final boolean isInviteBonus;
    private final long prize_id;
    private String operator_code;
    private String phone_number;
    private String phoneNumberCopy;
    private boolean saveNumber;

    public MobileBalanceTransferForm(boolean isInviteBonus, long prize_id) {
        this.prize_id = prize_id;
        this.isInviteBonus = isInviteBonus;
    }

    @Override
    public ValidationError validate() {
        Set<Integer> errors = new LinkedHashSet<>();
        if (!isOperatorSelected()) {
            errors.add(ERROR_OPERATOR_NOT_SELECTED);
        }
        if (!isPhoneNumberValid()) {
            errors.add(ERROR_INVALID_PHONE_NUMBER);
        }
        if (!isPhoneNumbersMatch()) {
            errors.add(ERROR_PHONE_NUMBERS_DO_NOT_MATCH);
        }
        if (errors.isEmpty()) {
            return null;
        } else {
            return new ValidationError(errors);
        }
    }

    @Override
    public Map<String, String> buildParamsMap() {
        if (validate() != null) {
            throw new IllegalStateException("Form validation failed.");
        }
        Map<String, String> params = new LinkedHashMap<>();
        params.put("method", "mobile_balance");
        if (!isInviteBonus) {
            params.put("prize_id", String.valueOf(prize_id));
        }
        params.put("operator_code", operator_code);
        params.put("phone_number", phone_number);
        return params;
    }

    public void setPhoneNumber(String new_number) {
        if (new_number == null) {
            phone_number = null;
            return;
        }
        // Remove non-digit characters and only allow a '+' sign if it's first.
        phone_number = removeNonDigitCharacters(new_number);
    }

    public void setPhoneNumberCopy(String newPhoneNumberCopy) {
        if (newPhoneNumberCopy == null) {
            phoneNumberCopy = null;
            return;
        }
        // Remove non-digit characters and only allow a '+' sign if it's first.
        phoneNumberCopy = removeNonDigitCharacters(newPhoneNumberCopy);
    }

    public String removeNonDigitCharacters(String phone_number) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < phone_number.length(); ++i) {
            char c = phone_number.charAt(i);
            if ((c == '+' && sb.length() == 0) || Character.isDigit(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public boolean isPhoneNumberValid() {
        if (isEmpty(phone_number) || phone_number.length() != 12 || !phone_number.startsWith("+7")) {
            return false;
        }
        for (int i = 1; i < phone_number.length(); ++i) {
            if (!Character.isDigit(phone_number.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean isPhoneNumbersMatch() {
        if (isEmpty(phoneNumberCopy) || phoneNumberCopy.length() != 12 || !phoneNumberCopy.startsWith("+7")) {
            return false;
        }
        if (!(phone_number.equals(phoneNumberCopy))) {
            return false;
        }
        return true;
    }

    public void setOperatorCode(@OperatorCode String code) {
        operator_code = code;
    }

    public boolean isOperatorSelected() {
        return operator_code != null;
    }

    public void setSaveNumberCheck(boolean saveNumber) {
        this.saveNumber = saveNumber;
    }

    public boolean hasSaveNumberChecked() {
        return saveNumber;
    }
}
