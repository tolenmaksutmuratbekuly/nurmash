package com.nurmash.nurmash.model.push.winnings;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.CheckResult;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.promotion.PastPromoDetailActivity;
import com.nurmash.nurmash.util.FormatUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Collection;

public class WinningsNotificationBuilder {
    private final Context context;

    // Request code for PendingIntent
    private final int requestCode;

    private final NumberFormat moneyAmountFormat;

    public WinningsNotificationBuilder(Context context, int requestCode) {
        this.context = context;
        this.requestCode = requestCode;
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
    }

    @CheckResult
    public Notification build(Collection<WinningsPushMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return null;
        }

        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.drawable.ic_winnings_notification);
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);

        if (messages.size() == 1) {
            return buildSingleItemNotification(builder, messages.iterator().next());
        } else {
            return buildMultiItemNotification(builder, messages);
        }
    }

    private Notification buildSingleItemNotification(Notification.Builder builder, WinningsPushMessage msg) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        taskStack.addNextIntent(PastPromoDetailActivity.newIntent(context, msg.contest_id, msg.isPhotoSharingContest()));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        String fullText = formatText(msg);
        builder.setContentTitle(context.getString(R.string.notification_title_winnings));
        builder.setContentText(fullText);
        builder.setStyle(new Notification.BigTextStyle().bigText(fullText));
        return builder.build();
    }

    private Notification buildMultiItemNotification(Notification.Builder builder, Collection<WinningsPushMessage> messages) {
        TaskStackBuilder taskStack = TaskStackBuilder.create(context);
        // TODO: 3/28/16 MainActivity should open with Prizes tab selected
        taskStack.addNextIntent(MainActivity.newIntentAsRoot(context));
        builder.setContentIntent(taskStack.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT));

        String totalAmountStr = calcTotalAmount(messages).toString(moneyAmountFormat);
        builder.setContentTitle(context.getString(R.string.notification_title_winnings));
        builder.setContentText(context.getString(R.string.notification_text_winnings_total, totalAmountStr, messages.size()));

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        for (WinningsPushMessage msg : messages) {
            inboxStyle.addLine(formatText(msg));
        }
        builder.setStyle(inboxStyle);

        return builder.build();
    }

    private static Money calcTotalAmount(Collection<WinningsPushMessage> messages) {
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (WinningsPushMessage msg : messages) {
            totalAmount = totalAmount.add(msg.win_amount.getAmount());
        }
        return Money.getTypeMoney(totalAmount);
    }

    private String formatText(WinningsPushMessage msg) {
        String promoTitle = msg.contest_title;
        String amount = msg.win_amount.toString(moneyAmountFormat);
        return context.getString(R.string.notification_text_you_won, promoTitle);
    }
}
