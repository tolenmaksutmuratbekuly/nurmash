package com.nurmash.nurmash.model.json;

import org.parceler.Parcel;

@Parcel
public class InviteAvailable {
    public boolean can_scan;
    public boolean can_invite;
    public boolean can_view;

}
