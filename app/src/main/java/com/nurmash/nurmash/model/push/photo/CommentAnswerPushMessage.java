package com.nurmash.nurmash.model.push.photo;

import android.os.Bundle;

import com.nurmash.nurmash.model.exception.PushMessageParseException;
import com.nurmash.nurmash.model.push.PushMessage;

public class CommentAnswerPushMessage extends PushMessage {
    public final String contest_title;
    public final long competitor_id;
    public final String author_display_name;
    public final String comment_body;

    public CommentAnswerPushMessage(Bundle data) throws PushMessageParseException {
        super(data);
        competitor_id = strictGetLong(data, "competitor_id");
        contest_title = strictGetString(data, "contest_title");
        author_display_name = strictGetString(data, "author_display_name");
        comment_body = strictGetString(data, "comment_body");

    }
}
