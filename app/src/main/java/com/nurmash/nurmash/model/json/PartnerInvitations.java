package com.nurmash.nurmash.model.json;

import com.nurmash.nurmash.model.Money;

import java.util.List;

public class PartnerInvitations {
    public int invite_count;
    public Money sum;
    public List<InviteList> invite_list;
    MetaInfo meta_info;

    public String getCurrencyCode() {
        if (meta_info != null && meta_info.currency_code != null) {
            return meta_info.currency_code;
        } else {
            return null;
        }
    }
}
