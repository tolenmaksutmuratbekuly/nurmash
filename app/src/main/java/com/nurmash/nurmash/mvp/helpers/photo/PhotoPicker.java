package com.nurmash.nurmash.mvp.helpers.photo;

import android.net.Uri;

public interface PhotoPicker {
    void setPhotoPickerCallback(PhotoPickerCallback callback);

    void startCroppedPhotoPicker();

    void disposeOfPhotoFile(Uri fileUri);
}
