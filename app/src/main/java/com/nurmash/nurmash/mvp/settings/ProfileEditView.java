package com.nurmash.nurmash.mvp.settings;

import android.net.Uri;
import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.Gender;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.Date;

public interface ProfileEditView extends MvpView {
    void showProfileLoadingIndicator(boolean show);

    void showBirthdatePickerView(Date currentBirthdate);

    void showCountryPickerView();

    void showRegionPickerView(long countryId);

    void showCityPickerView(long countryId, long regionId);

    void showCityPickerViewWithoutRegion(long countryId);

    void setTitle(@StringRes int titleRes);

    void setSaveButtonTitle(@StringRes int titleRes);

    void setDisplayName(String displayName);

    void setFirstName(String firstName);

    void setLastName(String lastName);

    void setNativeFirstName(String nativeFirstName);

    void setNativeLastName(String nativeLastName);

    void setEmail(String email);

    void setBirthdate(String birthdate);

    void setGender(Gender gender);

    void setCountryTitle(String country);

    void setRegionTitle(String region);

    void setCityTitle(String city);

    void showProfileSavingIndicator(boolean show);

    void updateProfileData();

    void finish();

    void showDisplayNameError(int errorRes, boolean scrollToView);

    void showFirstNameError(@StringRes int errorRes, boolean scrollToView);

    void showLastNameError(@StringRes int errorRes, boolean scrollToView);

    void showNativeFirstNameError(int errorRes, boolean scrollToView);

    void showNativeLastNameError(int errorRes, boolean scrollToView);

    void showEmailError(@StringRes int errorRes, boolean scrollToView);

    void showBirthdateError(@StringRes int errorRes, boolean scrollToView);

    void showGenderError(@StringRes int errorRes, boolean scrollToView);

    void showCountryError(@StringRes int errorRes, boolean scrollToView);

    void showRegionError(@StringRes int errorRes, boolean scrollToView);

    void showCityError(@StringRes int errorRes, boolean scrollToView);

    void showPhotoError(@StringRes int errorRes, boolean scrollToView);

    void showSelectedPhoto(Uri photoFileUri);

    void showSelectedPhoto(String photoHash);

    void showPhotoUploadOverlay(boolean show);

    void showPhotoUploadingIndicator(boolean show);

    void setPhotoUploadProgress(int progress);

    void showPhotoUploadError();

    void showTempPhotoStorageError();

    void showCropActivityNotAvailable();

    void openMainScreen();

    void logout();

    void setFacebookLinkActive(boolean active);

    void setGoogleLinkActive(boolean active);

    void setVKLinkActive(boolean active);

    void showLinkProgressIndicator(boolean show);

    void onFacebookSdkError();

    void onEmailPermissionNotGranted();

    void onGoogleApiConnectingError();

    void onGoogleApiNotConnectedError();

    void onGoogleSdkError();

    void onVKSdkError();

    void onAccountAlreadyLinkedError();

    void needToVerifyAgainMessage(Date birthdate);
}
