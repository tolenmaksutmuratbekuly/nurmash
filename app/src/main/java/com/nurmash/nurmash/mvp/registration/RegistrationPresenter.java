package com.nurmash.nurmash.mvp.registration;

import android.content.ActivityNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Gender;
import com.nurmash.nurmash.model.ProfileEditForm;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.FilesProvider;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPicker;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPickerCallback;
import com.nurmash.nurmash.util.FileUtils;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

// TODO: 3/3/16 Refactor this class because it is responsible for too many things: Presenting, Validation, Uploading, Linking accounts
public class RegistrationPresenter implements MvpPresenter<RegistrationView>, PhotoPickerCallback {
    private static final String TEMP_FILE_NAME = "socava_temp.jpg";
    private final String FORM_KEY = "PROFILE_EDIT_FORM";
    private final DateFormat BIRTHDATE_DISPLAY_FORMAT = new SimpleDateFormat("MMM d, yyyy", Locale.getDefault());

    private final DataLayer dataLayer;
    private String socialNetworkAvatar;
    private File tempPhotoFile;
    private FilesProvider filesProvider;
    private final boolean registration;
    private ProfileEditForm form;
    private PhotoPicker photoPicker;
    private ErrorHandler errorHandler;
    private RegistrationView registrationView;
    private Subscription profileLoadSubscription;
    private Subscription profileSaveSubscription;
    private Subscription photoUploadSubscription;
    private Subscription photoUploadProgressSubscription;
    private Uri uploadPhotoUri;

    public RegistrationPresenter(DataLayer dataLayer, User user) {
        this.dataLayer = dataLayer;
        this.socialNetworkAvatar = (user != null && user.extra != null && user.extra.avatar != null)
                ? user.extra.avatar : null;
        this.registration = !dataLayer.authPreferences().isUserProfileComplete();
    }

    public void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putParcelable(FORM_KEY, Parcels.wrap(form));
        }
    }

    public void onRestoreInstanceState(Bundle savedState) {
        if (savedState != null && savedState.containsKey(FORM_KEY)) {
            form = Parcels.unwrap(savedState.getParcelable(FORM_KEY));
        } else {
            form = new ProfileEditForm();
        }
    }

    @Override
    public void attachView(RegistrationView view) {
        if (form == null) {
            throw new NullPointerException("form == null. Did you forget to call onRestoreInstanceState() ?");
        }
        registrationView = view;
        registrationView.showProfileLoadingIndicator(isActive(profileLoadSubscription));
        if (registration) {
            registrationView.setTitle(R.string.label_activity_registration);
            registrationView.setSaveButtonTitle(R.string.action_continue);
        }
        if (isUploadingPhoto()) {
            registrationView.showPhotoUploadOverlay(true);
            registrationView.showPhotoUploadingIndicator(true);
        }

        /*
            If user has social network avatar,
                1) Set photo by default to photoView
                2) Download photo from social link and save it inside external storage for uploading later.
          */
        if (socialNetworkAvatar != null) {
            downloadSocialAvatar();
        }

        updateFormViews();
        loadMyProfile();
    }

    @Override
    public void detachView() {
        if (profileLoadSubscription != null) {
            profileLoadSubscription.unsubscribe();
            profileLoadSubscription = null;
        }
        if (profileSaveSubscription != null) {
            profileSaveSubscription.unsubscribe();
            profileSaveSubscription = null;
        }
        FileUtils.deleteFileQuietly(tempPhotoFile);
        registrationView = null;
    }

    public void setPhotoPicker(PhotoPicker newPicker) {
        if (newPicker == null) {
            // Dispose
            cancelPhotoUpload();
            photoPicker.setPhotoPickerCallback(null);
            photoPicker = null;
        } else {
            photoPicker = newPicker;
            photoPicker.setPhotoPickerCallback(this);
        }
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setFilesProvider(FilesProvider filesProvider) {
        this.filesProvider = filesProvider;
    }

    public void onBackButtonClick() {
        if (registrationView != null) {
            registrationView.finish();
        }
    }

    public void onPhotoClick() {
        if (registrationView == null) {
            return;
        }
        if (isUploadingPhoto()) {
            cancelPhotoUpload();
            // Restore the preview of the last successfully uploaded photo.
            registrationView.showSelectedPhoto(form.photoHash, true);
            registrationView.showPhotoUploadOverlay(false);
            registrationView.showPhotoUploadingIndicator(false);
        } else if (photoPicker != null) {
            photoPicker.startCroppedPhotoPicker();
        }
    }

    public void onPhotoUploadRetryClick() {
        if (isUploadingPhoto()) {
            cancelPhotoUpload();
        }
        setPhotoUri(uploadPhotoUri);
    }

    public void onBirthdateFieldClick() {
        if (registrationView != null) {
            registrationView.showBirthdatePickerView(form.birthdate);
        }
    }

    public void onCountryFieldClick() {
        if (registrationView != null) {
            registrationView.showCountryPickerView();
        }
    }

    public void onRegionFieldClick() {
        if (registrationView != null && form.country != null) {
            registrationView.showRegionPickerView(form.getCountryId());
        }
    }

    public void onCityFieldClick() {
        if (registrationView != null) {
            if (form.country != null && form.getCountryId() != 0) {
                if (form.region == null || form.getRegionId() == 0) {
                    registrationView.showCityPickerViewWithoutRegion(form.getCountryId());
                } else {
                    registrationView.showCityPickerView(form.getCountryId(), form.getRegionId());
                }
            } else {
                registrationView.showErrorMessage(R.string.error_message_form_country_not_selected);
            }
        }
    }

    public void setFirstName(String firstName) {
        form.firstName = trim(firstName);
        if (registrationView != null) {
            registrationView.setFirstName(form.firstName);
        }
        validateFirstName(false);
    }

    public void setLastName(String lastName) {
        form.lastName = trim(lastName);
        if (registrationView != null) {
            registrationView.setLastName(form.lastName);
        }
        validateLastName(false);
    }

    public void setEmail(String email) {
        form.email = trim(email);
        if (registrationView != null) {
            registrationView.setEmail(form.email);
        }
        validateEmail(false);
    }

    public void setBirthdate(Date birthdate) {
        form.birthdate = birthdate;
        if (registrationView != null) {
            registrationView.setBirthdate(form.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT));
        }
        validateBirthdate(false);
    }

    public void setGender(Gender gender) {
        form.gender = gender;
    }

    public void setCountry(Country country) {
        form.country = country;
        form.region = null;
        form.city = null;
        if (registrationView != null) {
            registrationView.setCountryTitle(form.getCountryTitle());
            registrationView.setRegionTitle(form.getRegionTitle());
            registrationView.setCityTitle(form.getCityTitle());
        }
    }

    public void setRegion(Region region) {
        form.region = region;
        form.city = null;
        if (registrationView != null) {
            registrationView.setRegionTitle(form.getRegionTitle());
            registrationView.setCityTitle(form.getCityTitle());
        }
    }

    public void setCity(City city) {
        form.city = city;
        if (registrationView != null) {
            registrationView.setCityTitle(form.getCityTitle());
        }
    }

    public void checkGeneralInfo() {
        boolean ok = true;
        ok &= validateFirstName(ok);
        ok &= validateLastName(ok);
        ok &= validateGender(ok);
        ok &= validateEmail(ok);
        ok &= validateBirthdate(ok);
        ok &= validatePhoto(ok);
        if (ok) {
            if (registrationView != null) {
                registrationView.nextStepButtonEnable(false);
                registrationView.showErrorMessage(R.string.error_message_upload_photo_not_finished_yet);
                restartPhotoUpload();
            }
        }
    }

    public void saveUser() {
        boolean ok = true;
        //noinspection ConstantConditions
        ok &= validateCountry(ok);
        ok &= validateRegion(ok);
        ok &= validateCity(ok);
        if (ok) {
            saveInternal();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoPickerCallback interface
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onCroppedPhotoPicked(@NonNull Uri croppedPhotoUri) {
        if (registrationView != null) {
            registrationView.showSelectedPhoto(croppedPhotoUri);
        }
        setPhotoUri(croppedPhotoUri);
    }

    @Override
    public void onPhotoPickCanceled() {
    }

    @Override
    public void onPhotoPickError(Throwable e) {
        if (registrationView == null) {
            return;
        }
        if (e instanceof IOException) {
            registrationView.showTempPhotoStorageError();
        } else if (e instanceof ActivityNotFoundException) {
            registrationView.showCropActivityNotAvailable();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Private implementation
    ///////////////////////////////////////////////////////////////////////////

    private void loadMyProfile() {
        if (isActive(profileLoadSubscription)) {
            return;
        }
        if (registrationView != null) {
            registrationView.showProfileLoadingIndicator(true);
        }
        profileLoadSubscription = dataLayer.getMyProfile(false)
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        setUser(user);
                        if (registrationView != null) {
                            registrationView.showProfileLoadingIndicator(false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (registrationView != null) {
                            registrationView.showProfileLoadingIndicator(false);
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void setUser(User user) {
        form.fillWithUser(user);
        updateFormViews();
    }

    private void updateFormViews() {
        if (registrationView != null) {
            registrationView.setFirstName(form.firstName);
            registrationView.setLastName(form.lastName);
            registrationView.setDisplayName(form.displayName);
            registrationView.setEmail(form.email);
            registrationView.setBirthdate(form.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT));
            registrationView.setGender(form.gender);
            registrationView.setCountryTitle(form.getCountryTitle());
            registrationView.setRegionTitle(form.getRegionTitle());
            registrationView.setCityTitle(form.getCityTitle());
            if (isUploadingPhoto()) {
                registrationView.showSelectedPhoto(uploadPhotoUri);
            } else {
                registrationView.showSelectedPhoto(form.photoHash, true);
            }
        }
    }

    private void saveInternal() {
        if (isActive(profileSaveSubscription)) {
            return;
        }
        if (registrationView != null) {
            registrationView.showProfileSavingIndicator(true);
        }
        profileSaveSubscription = dataLayer.saveProfile(form, true)
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        setUser(user);
                        if (registrationView != null) {
                            if (registration) {
                                registrationView.openMainScreen();
                                registrationView.finish();
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (registrationView != null) {
                            registrationView.showProfileSavingIndicator(false);
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private boolean validateFirstName(boolean scrollToView) {
        int errorRes = form.validateFirstName();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showFirstNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateLastName(boolean scrollToView) {
        int errorRes = form.validateLastName();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showLastNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateEmail(boolean scrollToView) {
        int errorRes = form.validateEmail();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showEmailError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateBirthdate(boolean scrollToView) {
        int errorRes = form.validateBirthdate();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showBirthdateError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateGender(boolean scrollToView) {
        int errorRes = form.validateGender();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showGenderError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateCountry(boolean scrollToView) {
        int errorRes = form.validateCountry();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showCountryError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateRegion(boolean scrollToView) {
        int errorRes = form.validateRegion();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showRegionError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateCity(boolean scrollToView) {
        int errorRes = form.validateCity();
        if (errorRes != 0 && registrationView != null) {
            registrationView.showCityError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validatePhoto(boolean scrollToView) {
        if (uploadPhotoUri != null) {
            return true;
        } else {
            if (registrationView != null) {
                registrationView.showPhotoError(R.string.error_message_form_photo_not_selected, scrollToView);
            }
            return false;
        }
    }

    private void downloadSocialAvatar() {
        if (registrationView != null) {
            Target target = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                    File file = filesProvider.getExternalCacheDir();
                    if (file != null) {
                        tempPhotoFile = new File(file, TEMP_FILE_NAME);
                        try {
                            tempPhotoFile.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(tempPhotoFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                            ostream.close();

                            registrationView.showSelectedPhoto(socialNetworkAvatar, false);
                            setPhotoUri(Uri.fromFile(tempPhotoFile));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            };
            if (socialNetworkAvatar.trim().length() > 0) {
                registrationView.downloadSocialNetworkAvatar(target, socialNetworkAvatar);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Photo upload
    ///////////////////////////////////////////////////////////////////////////

    private boolean isUploadingPhoto() {
        return isActive(photoUploadSubscription);
    }

    private void setPhotoUri(Uri photoUri) {
        if (photoUri == null) {
            return;
        }
        if (!photoUri.equals(uploadPhotoUri)) {
            // Dispose of the previous upload.
            cancelPhotoUpload();
        }
        uploadPhotoUri = photoUri;
    }

    private void restartPhotoUpload() {
        if (registrationView != null) {
            registrationView.showPhotoUploadOverlay(true);
            registrationView.showPhotoUploadingIndicator(true);
            registrationView.setPhotoUploadProgress(0);
        }
        final ObservableTypedFile observableFile = ObservableTypedFile.withPhotoUri(uploadPhotoUri);
        photoUploadProgressSubscription = observableFile.getProgressObservable()
                .sample(100, TimeUnit.MILLISECONDS, Schedulers.computation())
                .concatWith(Observable.just(100))
                .observeOn(mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer progress) {
                        if (registrationView != null) {
                            registrationView.setPhotoUploadProgress(progress);
                        }
                    }
                });
        photoUploadSubscription = dataLayer.uploadPhoto(observableFile)
                .observeOn(mainThread())
                .subscribe(new Subscriber<FileUploadResult>() {
                    @Override
                    public void onNext(FileUploadResult result) {
                        form.photoHash = result.hash;
                        if (photoPicker != null) {
                            photoPicker.disposeOfPhotoFile(uploadPhotoUri);
                        }
                        if (registrationView != null) {
                            registrationView.showPhotoUploadOverlay(false);
                            registrationView.showPhotoUploadingIndicator(false);
                        }
                        registrationView.generalInfoCompleted(true);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (registrationView != null) {
                            registrationView.showPhotoUploadingIndicator(false);
                            registrationView.showPhotoUploadError();
                        }
                    }
                });
    }

    private void cancelPhotoUpload() {
        if (isActive(photoUploadProgressSubscription)) {
            photoUploadProgressSubscription.unsubscribe();
            photoUploadProgressSubscription = null;
        }
        if (isActive(photoUploadSubscription)) {
            photoUploadSubscription.unsubscribe();
            photoUploadSubscription = null;
        }
        if (photoPicker != null && uploadPhotoUri != null) {
            photoPicker.disposeOfPhotoFile(uploadPhotoUri);
        }
        uploadPhotoUri = null;
    }

    public void showPrivacyDialog() {
        if (registrationView != null) {
            registrationView.showPrivacyDialog();
        }
    }
}
