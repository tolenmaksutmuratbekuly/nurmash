package com.nurmash.nurmash.mvp.prizes.withdraw;

import android.support.annotation.NonNull;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.model.withdraw.CardTransferForm;
import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.math.BigDecimal;
import java.text.NumberFormat;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class CardTransferPresenter implements MvpPresenter<CardTransferView> {
    private final DataLayer dataLayer;
    private PrizeItem prizeItem;
    private PartnerBalance partnerBalance;
    private final CardTransferForm cardTransferForm;
    private final NumberFormat moneyAmountFormat;
    private CardTransferView cardTransferView;
    private ErrorHandler errorHandler;
    private Subscription withdrawSubscription;

    public CardTransferPresenter(DataLayer dataLayer, PrizeItem prizeItem) {
        if (prizeItem == null) {
            throw new NullPointerException("prizeItem == null");
        }
        this.dataLayer = dataLayer;
        this.prizeItem = prizeItem;
        this.cardTransferForm = new CardTransferForm(false, prizeItem.id);
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
    }

    public CardTransferPresenter(DataLayer dataLayer, PartnerBalance partnerBalance) {
        if (partnerBalance == null) {
            throw new NullPointerException("partnerBalance == null");
        }
        this.dataLayer = dataLayer;
        this.partnerBalance = partnerBalance;
        this.cardTransferForm = new CardTransferForm(true, 0);
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
    }

    @Override
    public void attachView(CardTransferView view) {
        cardTransferView = view;
        if (prizeItem != null) {
            setPrizeViews(prizeItem.amount, prizeItem.getCurrencyCode());
        } else if (partnerBalance != null) {
            setPrizeViews(partnerBalance.bonuses.available, partnerBalance.getCurrencyCode());
        }
    }

    private void setPrizeViews(Money money, String currencyCode) {
        int pitPercent = 10;
        BigDecimal pitAmount = money.divideBigDecimal((money.multiplyBigDecimal(new BigDecimal(pitPercent))), new BigDecimal(100));
        BigDecimal commissionAmount = new BigDecimal(200);
        BigDecimal creditingAmount = (money.substractBigDecimal(pitAmount))
                .subtract(commissionAmount);

        cardTransferView.showAmountPrize(money.toString(moneyAmountFormat) + " " + currencyCode);
        cardTransferView.showTaxAmount(Money.getTypeMoney(pitAmount).toString(moneyAmountFormat) + " " + currencyCode);
        cardTransferView.showComission(Money.getTypeMoney(commissionAmount).toString(moneyAmountFormat) + " " + currencyCode);
        cardTransferView.showAmountCrediting(Money.getTypeMoney(creditingAmount).toString(moneyAmountFormat) + " " + currencyCode);
    }

    @Override
    public void detachView() {
        cardTransferView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setCardNumberPart(int part, String value, String confirmValue) {
        cardTransferForm.setCardNumberPart(part, value, confirmValue);
    }

    public void onClaimPrizeClick() {
        if (prizeItem != null) {
            performClaimPrize();
        } else if (partnerBalance != null) {
            performInviteBonus();
        }
    }

    private void performClaimPrize() {
        final ValidationError error = cardTransferForm.validate();
        if (error != null) {
            handleValidationError(error);
            return;
        }
        if (cardTransferView == null || withdrawSubscription != null) {
            return;
        }
        cardTransferView.showLoadingIndicator(true);
        withdrawSubscription = dataLayer.withdrawMoney2(cardTransferForm)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        cardTransferView.finishWithSuccess();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void performInviteBonus() {
        final ValidationError error = cardTransferForm.validate();
        if (error != null) {
            handleValidationError(error);
            return;
        }
        if (cardTransferView == null || withdrawSubscription != null) {
            return;
        }
        cardTransferView.showLoadingIndicator(true);
        withdrawSubscription = dataLayer.withdrawInviteBonus(cardTransferForm)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        cardTransferView.finishWithSuccess();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void handleValidationError(@NonNull ValidationError error) {
        if (error.hasErrorCode(CardTransferForm.ERROR_INVALID_CARD_NUMBER)) {
            cardTransferView.showInvalidCardNumberErrorToast();
        } else if (error.hasErrorCode(CardTransferForm.ERROR_INVALID_CARD_CONFIRMATION_NUMBER)) {
            cardTransferView.showInvalidCardConfirmationErrorToast();
        }
    }
}
