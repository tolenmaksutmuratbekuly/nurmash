package com.nurmash.nurmash.mvp.prizes.jackpot;

import android.support.annotation.NonNull;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.TimeLeft;
import com.nurmash.nurmash.model.json.JackpotList;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class JackpotListPresenter implements MvpPresenter<JackpotListView> {
    private final DataLayer dataLayer;
    private JackpotListView jackpotView;
    private ErrorHandler errorHandler;
    private Subscription jackpotSubscription;
    private Subscription countdownSubscription;
    private JackpotList bindedPrizeList;
    private TimeLeft jackpotCountdown;
    private StringsProvider strings;
    private final NumberFormat moneyAmountFormat;

    public JackpotListPresenter(DataLayer dataLayer, NumberFormat moneyAmountFormat) {
        this.dataLayer = dataLayer;
        this.moneyAmountFormat = moneyAmountFormat;
    }

    @Override
    public void attachView(JackpotListView view) {
        jackpotView = view;
        if (jackpotView != null) {
            loadJackpot();
        }
    }

    @Override
    public void detachView() {
        if (jackpotSubscription != null) {
            jackpotSubscription.unsubscribe();
            jackpotSubscription = null;
        }

        if (countdownSubscription != null) {
            countdownSubscription.unsubscribe();
            countdownSubscription = null;
        }
        jackpotView = null;
    }

    public void setStringsProvider(StringsProvider strings) {
        this.strings = strings;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    private void loadJackpot() {
        jackpotView.showLoadingIndicator(true);
        jackpotSubscription = dataLayer.getJackpotList()
                .observeOn(mainThread())
                .subscribe(new Subscriber<JackpotList>() {
                    @Override
                    public void onNext(JackpotList prizeList) {
                        jackpotView.showLoadingIndicator(false);
                        if (prizeList != null) {
                            bindedPrizeList = prizeList;
                            jackpotView.viewHoldersState(prizeList.display_winners);
                            if (prizeList.display_winners) {
                                jackpotView.setWinners(prizeList.winners);
                            } else {
                                jackpotCountdown = new TimeLeft((int) prizeList.jackpot_time_left);
                                updateAllViewState();
                                scheduleCountdownUpdate();
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        jackpotView.showLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void updateAllViewState() {
        if (jackpotView == null) {
            return;
        }
        if (bindedPrizeList == null) {
            jackpotView.showJackpotTitle(strings.getString(R.string.label_jackpot_not_raffled));
            jackpotView.showJackpotTimeLeft(null);
            jackpotView.showJackpotMoneyList(null);
        } else {
            updateJackpotCountdownView(jackpotCountdown.update());

            if (bindedPrizeList.jackpot_list != null) {
                for (int i = 0; i < bindedPrizeList.jackpot_list.size(); i++) {
                    jackpotView.showJackpotMoneyList(bindedPrizeList.jackpot_list.get(i).getJackPotAmount().toString(moneyAmountFormat)
                            + " " + bindedPrizeList.jackpot_list.get(i).getCurrencyCode() + "\n");
                }
            }
        }
    }

    private void updateJackpotCountdownView(@NonNull TimeLeft timeLeft) {
        if (timeLeft.isNoTimeLeft()) {
            jackpotView.showJackpotTitle(strings.getString(R.string.label_jackpot_raffled));
            jackpotView.showJackpotTimeLeft(null);
        } else {
            jackpotView.showJackpotTitle(strings.getString(R.string.label_jackpot_not_raffled));
            jackpotView.showJackpotTimeLeft(formatTimeLeft(timeLeft));
        }
    }

    private String formatTimeLeft(@NonNull TimeLeft timeLeft) {
        StringBuilder builder = new StringBuilder(64);
        if (timeLeft.getDays() > 0) {
            builder.append(strings.getQuantityString(R.plurals.time_left_days_format,
                    timeLeft.getDays(), timeLeft.getDays()));
            builder.append(' ');
        }
        if (timeLeft.getHours() > 0 || builder.length() > 0) {
            builder.append(strings.getString(R.string.time_left_hours_format, timeLeft.getHours()));
            builder.append(' ');
        }
        if (timeLeft.getMinutes() > 0 || builder.length() > 0) {
            builder.append(strings.getString(R.string.time_left_minutes_format, timeLeft.getMinutes()));
            builder.append(' ');
        }
        builder.append(strings.getString(R.string.time_left_seconds_format, timeLeft.getSeconds()));
        return builder.toString();
    }

    private void scheduleCountdownUpdate() {
        if (isActive(countdownSubscription)) {
            countdownSubscription.unsubscribe();
        }
        if (jackpotCountdown.update().isNoTimeLeft()) {
            return;
        }
        countdownSubscription = Observable.interval(1, 1, TimeUnit.SECONDS, Schedulers.newThread())
                .onBackpressureLatest()
                .observeOn(mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onNext(Long tick) {
                        if (jackpotCountdown == null) {
                            unsubscribe();
                            return;
                        }
                        TimeLeft timeLeft = jackpotCountdown.update();
                        updateJackpotCountdownView(timeLeft);
                        if (timeLeft.isNoTimeLeft()) {
                            unsubscribe();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }
                });
    }
}
