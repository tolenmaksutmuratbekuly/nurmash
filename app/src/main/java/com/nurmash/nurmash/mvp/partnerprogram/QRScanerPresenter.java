package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class QRScanerPresenter implements MvpPresenter<QRScanerView> {
    private final DataLayer dataLayer;
    private User user;
    private QRScanerView qrScanerView;
    private ErrorHandler errorHandler;
    private Subscription inviteUserSubscription;

    public QRScanerPresenter(DataLayer dataLayer, User user) {
        this.dataLayer = dataLayer;
        this.user = user;
    }

    @Override
    public void attachView(QRScanerView view) {
        qrScanerView = view;
    }

    @Override
    public void detachView() {
        if (inviteUserSubscription != null) {
            inviteUserSubscription.unsubscribe();
            inviteUserSubscription = null;
        }
        qrScanerView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void inviteUser(final String scannedUserId) {
        if (qrScanerView == null) {
            return;
        }
        if (scannedUserId.equals(user.id + "")) {
            qrScanerView.canNotInviteYourself();
        } else {
            inviteUserSubscription = dataLayer.inviteUserQRCode(scannedUserId)
                    .observeOn(mainThread())
                    .subscribe(new Subscriber<Void>() {
                        @Override
                        public void onStart() {
                            super.onStart();
                            qrScanerView.showLoadingIndicator(true);
                        }

                        @Override
                        public void onNext(Void aVoid) {
                            qrScanerView.showLoadingIndicator(false);
                            qrScanerView.onSuccessUserInvited();

                        }

                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            qrScanerView.showLoadingIndicator(false);
                            if (e instanceof ApiError && ((ApiError) e).isQRCodeInvalidError()) {
                                qrScanerView.invalidQRCodeError();
                            } else if (e instanceof ApiError && ((ApiError) e).hasInvitedAlready()) {
                                qrScanerView.invitedAlreadyError();
                            } else {
                                // Delegate displaying error message to the default error handler.
                                Timber.e(new SubscriberErrorWrapper(e), null);
                                if (errorHandler != null) errorHandler.handleError(e);
                            }
                        }
                    });
        }
    }
}

