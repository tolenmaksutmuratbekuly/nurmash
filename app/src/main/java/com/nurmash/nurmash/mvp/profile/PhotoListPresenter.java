package com.nurmash.nurmash.mvp.profile;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PhotoListPresenter implements MvpPresenter<PhotoListView> {
    private static final int ITEMS_PER_PAGE = 20;

    private final DataLayer dataLayer;
    private final long userId;
    private final List<Competitor> loadedPhotos = new ArrayList<>();
    private PhotoListView photoListView;
    private ErrorHandler errorHandler;
    private Subscription photosLoadSubscription;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private boolean isLoadingItems;

    public PhotoListPresenter(DataLayer dataLayer) {
        this(dataLayer, 0);
    }

    public PhotoListPresenter(DataLayer dataLayer, long userId) {
        this.dataLayer = dataLayer;
        this.userId = userId;
    }

    private void resetLoadedData() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedPhotos.clear();
    }

    @Override
    public void attachView(PhotoListView view) {
        photoListView = view;
        if (isFinalListEmpty()) {
            showEmptyView();
        } else {
            photoListView.setPhotoItems(loadedPhotos);
            photoListView.showPhotosLoadingIndicator(isLoadingItems);
            if (loadedPhotos.isEmpty()) {
                loadNextPage();
            }
        }
    }

    @Override
    public void detachView() {
        if (photosLoadSubscription != null) {
            photosLoadSubscription.unsubscribe();
            photosLoadSubscription = null;
        }
        photoListView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onRefreshAction() {
        reload();
    }

    public void onRequestMorePhotos() {
        loadNextPage();
    }

    public void onPhotoClick(Competitor competitor) {
        if (competitor != null && competitor.hasPhoto() && photoListView != null) {
            photoListView.openPhotoDetailActivity(competitor.id);
        }
    }

    public void onEmptyPlaceholderButtonClick() {
        if (photoListView != null) {
            photoListView.finishActivity();
        }
    }

    private boolean isMyProfile() {
        return userId == 0 || userId == dataLayer.authPreferences().getUserId();
    }

    private void setLoadingItems(boolean isLoading) {
        isLoadingItems = isLoading;
        if (photoListView != null) {
            photoListView.showPhotosLoadingIndicator(isLoading);
        }
    }

    private Observable<List<Competitor>> getPhotos(int page) {
        if (isMyProfile()) {
            return dataLayer.getMyPhotos(page, ITEMS_PER_PAGE);
        } else {
            return dataLayer.getUserPhotos(userId, page, ITEMS_PER_PAGE);
        }
    }

    private void loadNextPage() {
        if (isLoadingItems || noMoreItems) {
            return;
        }
        setLoadingItems(true);
        photosLoadSubscription = getPhotos(numPagesLoaded + 1)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<Competitor>>() {
                    @Override
                    public void onNext(List<Competitor> competitors) {
                        setLoadingItems(false);
                        onNextPageLoaded(competitors);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void reload() {
        if (photosLoadSubscription != null) {
            photosLoadSubscription.unsubscribe();
        }
        setLoadingItems(true);
        photosLoadSubscription = getPhotos(1)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<Competitor>>() {
                    @Override
                    public void onNext(List<Competitor> competitors) {
                        setLoadingItems(false);
                        resetLoadedData();
                        onNextPageLoaded(competitors);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void onNextPageLoaded(List<Competitor> competitors) {
        if (competitors == null || competitors.isEmpty()) {
            noMoreItems = true;
            if (isFinalListEmpty()) {
                showEmptyView();
            }
        } else {
            numPagesLoaded = numPagesLoaded + 1;
            List<Competitor> newItems = filterCompetitors(competitors);
            loadedPhotos.addAll(newItems);
            if (photoListView != null) {
                if (numPagesLoaded == 1) {
                    photoListView.setPhotoItems(newItems);
                } else {
                    photoListView.addPhotoItems(newItems);
                }
            }
        }
    }

    private List<Competitor> filterCompetitors(List<Competitor> competitors) {
        List<Competitor> result = new ArrayList<>(competitors.size());
        for (Competitor c : competitors) {
            if (isMyProfile()) {
                if (c.hasPhoto() || c.isDisqualified()) {
                    // We'd like to show disqualified entries in user's own profile.
                    result.add(c);
                }
            } else {
                if (c.hasPhoto() && !c.isDisqualified()) {
                    // We'd like to hide disqualified entries in other users' profiles.
                    result.add(c);
                }
            }
        }
        return result;
    }

    private boolean isFinalListEmpty() {
        return noMoreItems && loadedPhotos.isEmpty();
    }

    private void showEmptyView() {
        if (isMyProfile()) {
            photoListView.showPhotoListPlaceholder(R.string.info_text_my_profile_photos_empty, R.string.action_participate);
        } else {
            photoListView.showPhotoListPlaceholder(R.string.info_text_user_profile_photos_empty, 0);
        }
    }
}
