package com.nurmash.nurmash.mvp.prizes.withdraw;

import android.support.annotation.NonNull;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.model.withdraw.MobileBalanceTransferForm;
import com.nurmash.nurmash.model.withdraw.OperatorCode;
import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.math.BigDecimal;
import java.text.NumberFormat;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class MobileBalanceTransferPresenter implements MvpPresenter<MobileBalanceTransferView> {
    private final DataLayer dataLayer;
    private PrizeItem prizeItem;
    private PartnerBalance partnerBalance;
    private final MobileBalanceTransferForm form;
    private final NumberFormat moneyAmountFormat;
    private MobileBalanceTransferView view;
    private ErrorHandler errorHandler;
    private Subscription withdrawSubscription;
    private String tempPhoneNumber;

    public MobileBalanceTransferPresenter(DataLayer dataLayer, PrizeItem prizeItem) {
        if (prizeItem == null) {
            throw new NullPointerException("prizeItem == null");
        }
        this.dataLayer = dataLayer;
        this.prizeItem = prizeItem;
        this.form = new MobileBalanceTransferForm(false, prizeItem.id);
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
    }

    public MobileBalanceTransferPresenter(DataLayer dataLayer, PartnerBalance partnerBalance) {
        if (partnerBalance == null) {
            throw new NullPointerException("partnerBalance == null");
        }
        this.dataLayer = dataLayer;
        this.partnerBalance = partnerBalance;
        this.form = new MobileBalanceTransferForm(true, 0);
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
    }

    @Override
    public void attachView(MobileBalanceTransferView view) {
        this.view = view;
        if (prizeItem != null) {
            setPrizeViews(prizeItem.amount, prizeItem.getCurrencyCode());
        } else if (partnerBalance != null) {
            setPrizeViews(partnerBalance.bonuses.available, partnerBalance.getCurrencyCode());
        }
        this.view.setLocalPhoneNumber(dataLayer.authPreferences().getPhoneNumber());
    }

    private void setPrizeViews(Money money, String currencyCode) {
        int pitPercent = 10;
        BigDecimal pitAmount = money.divideBigDecimal((money.multiplyBigDecimal(new BigDecimal(pitPercent))), new BigDecimal(100));
        BigDecimal commissionAmount = new BigDecimal(0);
        BigDecimal creditingAmount = (money.substractBigDecimal(pitAmount))
                .subtract(commissionAmount);
        this.view.showAmountPrize(money.toString(moneyAmountFormat) + " " + currencyCode);
        this.view.showTaxAmount(Money.getTypeMoney(pitAmount).toString(moneyAmountFormat) + " " + currencyCode);
        this.view.showComission(Money.getTypeMoney(commissionAmount).toString(moneyAmountFormat) + " " + currencyCode);
        this.view.showAmountCrediting(Money.getTypeMoney(creditingAmount).toString(moneyAmountFormat) + " " + currencyCode);
    }

    @Override
    public void detachView() {
        view = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setPhoneNumber(String phoneNumber, String phoneNumberCopy) {
        form.setPhoneNumber(phoneNumber);
        form.setPhoneNumberCopy(phoneNumberCopy);
    }

    public void setPhoneNumberErrorHandle(String phoneNumber, String phoneNumberCopy) {
        form.setPhoneNumber(phoneNumber);
        form.setPhoneNumberCopy(phoneNumberCopy);
        final ValidationError error = form.validate();
        handleValidationErrorCheckboxState(error);
    }

    public void setOperatorCode(@OperatorCode String operatorCode) {
        form.setOperatorCode(operatorCode);
    }

    public void setOperatorCodeErrorHandle(@OperatorCode String operatorCode) {
        form.setOperatorCode(operatorCode);
        final ValidationError error = form.validate();
        handleValidationErrorCheckboxState(error);
    }

    public void savePhoneNumber(boolean saveNumber, String phoneNumber) {
        tempPhoneNumber = phoneNumber;
        form.setSaveNumberCheck(saveNumber);
    }

    public void onClaimPrizeClick() {
        if (prizeItem != null) {
            performClaimPrize();
        } else if (partnerBalance != null) {
            performInviteBonus();
        }
    }

    private void performClaimPrize() {
        final ValidationError error = form.validate();
        if (error != null) {
            handleValidationErrorToast(error);
            return;
        }
        if (view == null || withdrawSubscription != null) {
            return;
        }

        view.showLoadingIndicator(true);
        withdrawSubscription = dataLayer.withdrawMoney2(form)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        finishWithSuccess();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        view.showLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void performInviteBonus() {
        final ValidationError error = form.validate();
        if (error != null) {
            handleValidationErrorToast(error);
            return;
        }
        if (view == null || withdrawSubscription != null) {
            return;
        }

        view.showLoadingIndicator(true);
        withdrawSubscription = dataLayer.withdrawInviteBonus(form)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        finishWithSuccess();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        view.showLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void finishWithSuccess() {
        dataLayer.authPreferences().setPhoneNumber(form.hasSaveNumberChecked() ? tempPhoneNumber : "");
        view.finishWithSuccess();
    }

    private void handleValidationErrorToast(@NonNull ValidationError error) {
        if (error.hasErrorCode(MobileBalanceTransferForm.ERROR_OPERATOR_NOT_SELECTED)) {
            view.showOperatorNotSelectedErrorToast();
        } else if (error.hasErrorCode(MobileBalanceTransferForm.ERROR_INVALID_PHONE_NUMBER)) {
            view.showInvalidPhoneNumberErrorToast();
        } else if (error.hasErrorCode(MobileBalanceTransferForm.ERROR_PHONE_NUMBERS_DO_NOT_MATCH)) {
            view.showInvalidPhoneNumbersAraNotSameErrorToast();
        }
    }

    private void handleValidationErrorCheckboxState(@NonNull ValidationError error) {
        if (error != null) {
            view.carrierCheckState(error.hasErrorCode(MobileBalanceTransferForm.ERROR_OPERATOR_NOT_SELECTED));
            view.phoneNumberCheckState(error.hasErrorCode(MobileBalanceTransferForm.ERROR_INVALID_PHONE_NUMBER));
            view.phoneNumberConfirmationCheckState(error.hasErrorCode(MobileBalanceTransferForm.ERROR_PHONE_NUMBERS_DO_NOT_MATCH));
        } else {
            view.carrierCheckState(false);
            view.phoneNumberCheckState(false);
            view.phoneNumberConfirmationCheckState(false);
        }
    }
}
