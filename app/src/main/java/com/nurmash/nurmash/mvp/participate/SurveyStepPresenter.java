package com.nurmash.nurmash.mvp.participate;

import android.os.Handler;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.json.Survey;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import timber.log.Timber;

import static com.nurmash.nurmash.model.ParticipationData.SURVEY_STEP;

public class SurveyStepPresenter implements MvpPresenter<SurveyStepView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private String surveyType;
    private SurveyStepView surveyStepView;
    private ParticipationData participationData;
    private List<Survey.Node> nodeList;
    private ArrayList<String> answerIdsList;
    private HashMap<String, String> treeAnswerIdsList;

    private int questionIterator;
    private Subscription participationDataLoadSubscription;
    private Subscription commitSubscription;
    private ErrorHandler errorHandler;

    public SurveyStepPresenter(DataLayer dataLayer, long promoId) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
        this.answerIdsList = new ArrayList<>();
        this.treeAnswerIdsList = new HashMap<>();
        this.questionIterator = 0;
    }

    @Override
    public void attachView(SurveyStepView view) {
        surveyStepView = view;
    }

    @Override
    public void detachView() {
        if (participationDataLoadSubscription != null) {
            participationDataLoadSubscription.unsubscribe();
            participationDataLoadSubscription = null;
        }
        if (commitSubscription != null) {
            commitSubscription.unsubscribe();
            commitSubscription = null;
        }
        surveyStepView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void reloadParticipationData() {
        if (surveyStepView != null) surveyStepView.showLoadingIndicator(true);
        if (participationDataLoadSubscription != null) participationDataLoadSubscription.unsubscribe();
        participationDataLoadSubscription = dataLayer.getParticipationData(promoId, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        setSurvey(data.getSurvey());
                        setParticipationData(data);
                    }

                    @Override
                    public void onCompleted() {
                        if (surveyStepView != null) surveyStepView.showLoadingIndicator(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (surveyStepView != null) surveyStepView.showLoadingIndicator(false);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }

    public void onAnswerClick(String answerId) {
        if (surveyStepView == null || participationData.isStepComplete(SURVEY_STEP)) return;
        addAnswerIds(answerId);
        surveyStepView.setSelectedAnswerId(answerId);
        surveyStepView.showNextButtonState(questionIterator == nodeList.size() - 1);
        if (nodeList.size() - 1 > questionIterator) questionIterator++;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                setNode();
            }
        }, 200);
    }

    public void onAnswerClick(final Survey.Answer answer, String childrenId) {
        if (surveyStepView == null || participationData.isStepComplete(SURVEY_STEP)) return;
        // Add answer IDs
        treeAnswerIdsList.put(childrenId, answer.id);
        surveyStepView.setSelectedAnswerId(answer.id);

        if (answer.children != null && answer.children.length > 0) {
            surveyStepView.showNextButtonState(false);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    surveyStepView.setChildren(answer.children);
                }
            }, 200);
        } else {
            surveyStepView.showNextButtonState(true);
        }
    }

    private void addAnswerIds(String answerId) {
        if (answerIdsList.size() > questionIterator && questionIterator >= 0) {
            answerIdsList.set(questionIterator, answerId);
        } else {
            answerIdsList.add(answerId);
        }
    }

    public void onNextClick() {
        if (surveyStepView == null || (answerIdsList.size() == 0 && treeAnswerIdsList.size() == 0)) return;
        surveyStepView.showCommitLoadingIndicator(true);
        if (commitSubscription != null) commitSubscription.unsubscribe();
        Observable<ParticipationData> commitStepObs = null;
        if (surveyType == null) {
            // Ensure backwards compatibility with an old type of survey without nodes.
            commitStepObs = dataLayer.commitSurveyStepOld(participationData, answerIdsList.get(0));
        } else if (surveyType.equals(Survey.SURVEY_TYPE_LIST)) {
            String strAnswerIds = "";
            for (int i = 0; i < answerIdsList.size(); i++) {
                if (i == 0) {
                    strAnswerIds += "[";
                }
                strAnswerIds += "'" + answerIdsList.get(i) + "'";

                if (i == answerIdsList.size() - 1) {
                    strAnswerIds += "]";
                } else {
                    strAnswerIds += ", ";
                }
            }
            commitStepObs = dataLayer.commitSurveyStep(participationData, strAnswerIds);
        } else if (surveyType.equals(Survey.SURVEY_TYPE_TREE)) {
            String strAnswerIds = "";
            int i = 0;
            for (String answerId : treeAnswerIdsList.values()) {
                if (i == 0) {
                    strAnswerIds += "[";
                }
                strAnswerIds += "'" + answerId + "'";

                if (i == treeAnswerIdsList.size() - 1) {
                    strAnswerIds += "]";
                } else {
                    strAnswerIds += ", ";
                }
                i++;
            }
            commitStepObs = dataLayer.commitSurveyStep(participationData, strAnswerIds);
        }

        if (commitStepObs != null) {
            commitSubscription = commitStepObs
                    .flatMap(new Func1<ParticipationData, Observable<ParticipationData>>() {
                        @Override
                        public Observable<ParticipationData> call(ParticipationData data) {
                            return dataLayer.commitLastStep(data);
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ParticipationData>() {
                        @Override
                        public void onNext(ParticipationData data) {
                            setParticipationData(data);
                            if (surveyStepView != null) {
                                if (data.getPromotion().isPhotoSharingPromo()) {
                                    surveyStepView.openMyPhotoActivity(data.getCompetitorId());
                                }
                                surveyStepView.finish();
                            }
                        }

                        @Override
                        public void onCompleted() {
                            if (surveyStepView != null) surveyStepView.showCommitLoadingIndicator(false);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(new SubscriberErrorWrapper(e), null);
                            if (surveyStepView != null) surveyStepView.showCommitLoadingIndicator(false);
                            if (errorHandler != null) errorHandler.handleError(e);
                        }
                    });
        }
    }

    private void setParticipationData(ParticipationData data) {
        participationData = data;
    }

    private void setSurvey(Survey survey) {
        if (surveyStepView == null) return;
        this.surveyType = survey.type;
        surveyStepView.setSurveyType(survey.type);
        if (survey.type == null) {
            // Ensure backwards compatibility with an old type of survey without nodes.
            Survey.Node singleNode = new Survey.Node();
            singleNode.question = survey.question;
            singleNode.answers = survey.answers;
            nodeList = new ArrayList<>(Collections.singleton(singleNode));
            setNode();
        } else if (survey.type.equals(Survey.SURVEY_TYPE_LIST)) {
            if (survey.nodes_list != null && survey.nodes_list.length > 0) {
                Survey.Node[] nodes = survey.nodes_list;
                nodeList = new ArrayList<>(Arrays.asList(nodes));
                setNode();
            }
        } else if (survey.type.equals(Survey.SURVEY_TYPE_TREE)) {
            if (survey.nodes_tree != null) {
                surveyStepView.setNode(survey.nodes_tree);
            }
        }
    }

    private void setNode() {
        if (nodeList.size() > questionIterator && questionIterator >= 0) {
            surveyStepView.setNode(nodeList.get(questionIterator));
        }
    }
}
