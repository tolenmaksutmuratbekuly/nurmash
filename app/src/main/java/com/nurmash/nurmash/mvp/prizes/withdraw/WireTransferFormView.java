package com.nurmash.nurmash.mvp.prizes.withdraw;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.mvp.MvpView;

public interface WireTransferFormView extends MvpView {
    void showFirstName(String firstName);

    void showLastName(String lastName);

    void showCountry(String country);

    void showRegion(String region);

    void showCity(String city);

    void showAddressLine1(String addressLine1);

    void showAddressLine2(String addressLine2);

    void showZipCode(String zipCode);

    void showBankName(String bankName);

    void showBankSwift(String bankSwift);

    void showBankAccountNumber(String bankAccountNumber);

    void showFirstNameError(@StringRes int errorRes, boolean scrollTo);

    void showLastNameError(@StringRes int errorRes, boolean scrollTo);

    void showAddressError(@StringRes int errorRes, boolean scrollTo);

    void showZipCodeError(@StringRes int errorRes, boolean scrollTo);

    void showCityError(@StringRes int errorRes, boolean scrollTo);

    void showRegionError(@StringRes int errorRes, boolean scrollTo);

    void showCountryError(@StringRes int errorRes, boolean scrollTo);

    void showBankNameError(@StringRes int errorRes, boolean scrollTo);

    void showBankSwiftError(@StringRes int errorRes, boolean scrollTo);

    void showBankAccountNumberError(@StringRes int errorRes, boolean scrollTo);

    void showLowAmountError(@StringRes int errorRes);
}
