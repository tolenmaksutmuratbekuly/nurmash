package com.nurmash.nurmash.mvp.helpers;

public interface ClipboardHelper {
    void copyText(CharSequence text);
}
