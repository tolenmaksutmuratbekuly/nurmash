package com.nurmash.nurmash.mvp.prizes.withdraw;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.mvp.MvpView;

public interface PaypalFormView extends MvpView {
    void showAccount(String account);

    void showFirstName(String firstName);

    void showLastName(String lastName);

    void showAccountError(@StringRes int errorRes, boolean scrollTo);

    void showFirstNameError(@StringRes int errorRes, boolean scrollTo);

    void showLastNameError(@StringRes int errorRes, boolean scrollTo);
}
