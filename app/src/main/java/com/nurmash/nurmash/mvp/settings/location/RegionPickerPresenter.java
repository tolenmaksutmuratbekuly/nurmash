package com.nurmash.nurmash.mvp.settings.location;

import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.util.rx.RxUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class RegionPickerPresenter extends LocationPickerPresenter<Region> {
    public RegionPickerPresenter(DataLayer dataLayer, long countryId) {
        super(dataLayer, countryId, LOCATION_ID_NONE);
    }

    @Override
    protected void loadItems() {
        if (RxUtils.isActive(itemsLoadSubscription)) {
            return;
        }
        showLoadingIndicator(true);
        itemsLoadSubscription = dataLayer.getRegionList(countryId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Region>>() {
                    @Override
                    public void onNext(List<Region> regions) {
                        List<Pair<Region, String>> regionsWithTitles = new ArrayList<>(regions.size());
                        for (Region region : regions) {
                            regionsWithTitles.add(Pair.create(region, region.title));
                        }
                        onItemsLoaded(regionsWithTitles);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        onItemsLoadFailed(e);
                    }
                });
    }
}
