package com.nurmash.nurmash.mvp.photo;

import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.mvp.helpers.CommentsLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

// TODO: 2/25/16 Rewrite. Preferably on the backend side. At the very least test and document this class.
public class ReverseCommentsLoader implements CommentsLoader {
    private static final int COMMENTS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private final long competitorId;
    private final int totalPageCount;
    private int loadedPages;

    public ReverseCommentsLoader(DataLayer dataLayer, long competitorId, int totalCommentsCount) {
        this.dataLayer = dataLayer;
        this.competitorId = competitorId;
        this.totalPageCount = totalCommentsCount / COMMENTS_PER_PAGE;
        boolean hasPartialPage = (totalCommentsCount % COMMENTS_PER_PAGE) != 0;
        if (hasPartialPage) {
            this.loadedPages = -1;
        }
    }

    @Override
    public void setLoadedPages(int page) {
        if (loadedPages > totalPageCount) {
            throw new IllegalArgumentException(
                    String.format("Argument is larger than total page count: totalPageCount=%d, page=%d",
                            totalPageCount, page));
        }
        loadedPages = page;
    }

    @Override
    public boolean hasMorePages() {
        return loadedPages < totalPageCount;
    }

    @Override
    public Observable<Pair<Integer, List<Comment>>> loadNextPage() {
        if (loadedPages == -1) {
            if (totalPageCount == 0) {
                return getPageLoader(0)
                        .map(new Func1<List<Comment>, Pair<Integer, List<Comment>>>() {
                            @Override
                            public Pair<Integer, List<Comment>> call(List<Comment> comments) {
                                return Pair.create(0, reversedArrayList(comments));
                            }
                        });
            } else {
                Observable<Comment> partialPageLoaderFlat = getPageLoader(0)
                        .flatMapIterable(new Func1<List<Comment>, Iterable<? extends Comment>>() {
                            @Override
                            public Iterable<? extends Comment> call(List<Comment> comments) {
                                return comments;
                            }
                        });
                Observable<Comment> firstPageLoaderFlat = getPageLoader(1)
                        .flatMapIterable(new Func1<List<Comment>, Iterable<? extends Comment>>() {
                            @Override
                            public Iterable<? extends Comment> call(List<Comment> comments) {
                                return comments;
                            }
                        });
                return Observable.concat(firstPageLoaderFlat, partialPageLoaderFlat)
                        .toList()
                        .map(new Func1<List<Comment>, Pair<Integer, List<Comment>>>() {
                            @Override
                            public Pair<Integer, List<Comment>> call(List<Comment> comments) {
                                return Pair.create(1, reversedArrayList(comments));
                            }
                        });
            }
        } else {
            final int nextPage = loadedPages + 1;
            return getPageLoader(nextPage)
                    .map(new Func1<List<Comment>, Pair<Integer, List<Comment>>>() {
                        @Override
                        public Pair<Integer, List<Comment>> call(List<Comment> comments) {
                            return Pair.create(nextPage, reversedArrayList(comments));
                        }
                    });
        }
    }

    private static <T> List<T> reversedArrayList(List<T> list) {
        List<T> result = new ArrayList<>(list);
        Collections.reverse(result);
        return result;
    }

    private Observable<List<Comment>> getPageLoader(int page) {
        return dataLayer.getCommentList(competitorId, totalPageCount - page + 1, COMMENTS_PER_PAGE);
    }
}
