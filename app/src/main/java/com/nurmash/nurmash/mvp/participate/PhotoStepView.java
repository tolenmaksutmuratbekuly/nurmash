package com.nurmash.nurmash.mvp.participate;

import android.net.Uri;

import com.nurmash.nurmash.mvp.MvpView;

public interface PhotoStepView extends MvpView {
    boolean openCameraApp(Uri tempPhotoUri);

    void openGalleryApp();

    boolean openCropApp(Uri inFileUri, Uri outFileUri);

    void openShareStepActivity(long promoId, Uri photoFileUri);

    void finish();

    void showTempStorageError();

    void showCameraAppError();

    void showCropAppError();
}
