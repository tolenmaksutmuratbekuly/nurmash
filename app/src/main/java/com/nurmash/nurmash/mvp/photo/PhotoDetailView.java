package com.nurmash.nurmash.mvp.photo;

import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

public interface PhotoDetailView extends MvpView {
    void showLoadingIndicator(boolean show);

    void onCompetitorLoaded(Competitor competitor, long authUserId);

    void showReportLoadingIndicator(boolean show);

    void showPhotoReportedMessage();

    void showPhotoComplaintExistsError();

    void openCommentListActivity(Competitor competitor, boolean focusInput);

    void openUserProfileActivity(User user);

    void openPromoDetailActivity(long promoId);
}
