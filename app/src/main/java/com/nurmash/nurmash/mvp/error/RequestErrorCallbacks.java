package com.nurmash.nurmash.mvp.error;

public interface RequestErrorCallbacks {
    void onAuthTokenInvalidated();

    void onUserBannedError();

    void onNetworkError();

    void onRequestFailed();

    void onInternalServerError();

    void onPromoHasEndedError();

    void onPromoNotFoundError();
}
