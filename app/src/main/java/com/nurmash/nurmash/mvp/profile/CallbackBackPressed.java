package com.nurmash.nurmash.mvp.profile;

public interface CallbackBackPressed {
    boolean isPhotoOpenOnBackPressed();
}
