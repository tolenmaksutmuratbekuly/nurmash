package com.nurmash.nurmash.mvp.prizes.withdraw;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.model.withdraw.WireTransferForm;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.Set;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_AMOUNT_LOW;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_BANK_NAME_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_CITY_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_COUNTRY_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_FIRST_NAME_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_IBAN_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_LAST_NAME_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_STREET1_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_SWIFT_EMPTY;
import static com.nurmash.nurmash.model.withdraw.WireTransferForm.ERROR_ZIP_EMPTY;
import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class WireTransferFormPresenter implements MvpPresenter<WireTransferFormView> {
    private final WireTransferForm form;
    private DataLayer dataLayer;
    private User userProfile;
    private WireTransferFormView wireTransferFormView;
    private Subscription userProfileLoadSubscription;

    public WireTransferFormPresenter() {
        this.form = new WireTransferForm();
    }

    public void attachView(WireTransferFormView view, DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        attachView(view);
    }

    @Override
    public void attachView(WireTransferFormView view) {
        wireTransferFormView = view;
        if (userProfile == null) {
            loadUserProfile();
        }
        updateFormView();
    }

    @Override
    public void detachView() {
        if (userProfileLoadSubscription != null) {
            userProfileLoadSubscription.unsubscribe();
            userProfileLoadSubscription = null;
        }
        wireTransferFormView = null;
        dataLayer = null;
    }

    public WireTransferForm getForm(String firstName, String lastName,
                                    String country, String region, String city,
                                    String addressLine1, String addressLine2, String zipCode,
                                    String bankName, String bankSwift, String bankAccountNumber) {
        form.first_name = trim(firstName);
        form.last_name = trim(lastName);
        form.country = trim(country);
        form.region = trim(region);
        form.city = trim(city);
        form.street1 = trim(addressLine1);
        form.street2 = trim(addressLine2);
        form.zip = trim(zipCode);
        form.bank_name = trim(bankName);
        form.swift = trim(bankSwift);
        form.iban = trim(bankAccountNumber);
        return form;
    }

    public void onValidationError(ValidationError e) {
        if (wireTransferFormView == null) {
            return;
        }
        if (e == null) {
            throw new NullPointerException("e == null");
        }
        Set<Integer> errors = e.errorCodes;
        if (errors.contains(ERROR_AMOUNT_LOW)) {
            wireTransferFormView.showLowAmountError(R.string.error_message_wire_transfer_low_amount);
            return;
        }
        int scrollToError = getNextScrollError(errors);
        if (errors.contains(ERROR_FIRST_NAME_EMPTY)) {
            wireTransferFormView.showFirstNameError(R.string.error_message_form_first_name_empty, ERROR_FIRST_NAME_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_LAST_NAME_EMPTY)) {
            wireTransferFormView.showLastNameError(R.string.error_message_form_last_name_empty, ERROR_LAST_NAME_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_STREET1_EMPTY)) {
            wireTransferFormView.showAddressError(R.string.error_message_form_address_empty, ERROR_STREET1_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_ZIP_EMPTY)) {
            wireTransferFormView.showZipCodeError(R.string.error_message_form_zip_empty, ERROR_ZIP_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_CITY_EMPTY)) {
            wireTransferFormView.showCityError(R.string.error_message_form_city_empty, ERROR_CITY_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_COUNTRY_EMPTY)) {
            wireTransferFormView.showCountryError(R.string.error_message_form_country_empty, ERROR_COUNTRY_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_BANK_NAME_EMPTY)) {
            wireTransferFormView.showBankNameError(R.string.error_message_form_bank_name_empty, ERROR_BANK_NAME_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_SWIFT_EMPTY)) {
            wireTransferFormView.showBankSwiftError(R.string.error_message_form_swift_empty, ERROR_SWIFT_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_IBAN_EMPTY)) {
            wireTransferFormView.showBankAccountNumberError(R.string.error_message_form_iban_empty, ERROR_IBAN_EMPTY == scrollToError);
        }
    }

    private static int getNextScrollError(Set<Integer> errors) {
        if (errors.contains(ERROR_FIRST_NAME_EMPTY)) return ERROR_FIRST_NAME_EMPTY;
        if (errors.contains(ERROR_LAST_NAME_EMPTY)) return ERROR_LAST_NAME_EMPTY;
        if (errors.contains(ERROR_COUNTRY_EMPTY)) return ERROR_COUNTRY_EMPTY;
        if (errors.contains(ERROR_CITY_EMPTY)) return ERROR_CITY_EMPTY;
        if (errors.contains(ERROR_STREET1_EMPTY)) return ERROR_STREET1_EMPTY;
        if (errors.contains(ERROR_ZIP_EMPTY)) return ERROR_ZIP_EMPTY;
        if (errors.contains(ERROR_BANK_NAME_EMPTY)) return ERROR_BANK_NAME_EMPTY;
        if (errors.contains(ERROR_SWIFT_EMPTY)) return ERROR_SWIFT_EMPTY;
        return ERROR_IBAN_EMPTY;
    }

    private void updateFormView() {
        if (wireTransferFormView != null) {
            wireTransferFormView.showFirstName(form.first_name);
            wireTransferFormView.showLastName(form.last_name);
            wireTransferFormView.showCountry(form.country);
            wireTransferFormView.showRegion(form.region);
            wireTransferFormView.showCity(form.city);
            wireTransferFormView.showAddressLine1(form.street1);
            wireTransferFormView.showAddressLine2(form.street2);
            wireTransferFormView.showZipCode(form.zip);
            wireTransferFormView.showBankName(form.bank_name);
            wireTransferFormView.showBankSwift(form.swift);
            wireTransferFormView.showBankAccountNumber(form.iban);
        }
    }

    private void loadUserProfile() {
        if (isActive(userProfileLoadSubscription)) {
            return;
        }
        userProfileLoadSubscription = dataLayer.getMyProfile(true)
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        userProfile = user;
                        form.fillWithUser(user);
                        updateFormView();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }
                });
    }
}
