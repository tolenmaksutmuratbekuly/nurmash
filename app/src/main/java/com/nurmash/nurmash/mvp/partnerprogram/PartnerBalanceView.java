package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.mvp.MvpView;

public interface PartnerBalanceView extends MvpView {
    void showLoadingIndicator(boolean show);

    void onBalanceLoaded(PartnerBalance partnerBalance);

    void isZeroBalanceActive(boolean isZeroBalance);
}
