package com.nurmash.nurmash.mvp.main;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface PromoListView extends MvpView {
    void showLoadingIndicator(boolean show);

    void showRefreshingIndicator(boolean show);

    void setPromoListItems(List<Promotion> promotions);

    void addPromoListItems(List<Promotion> promotions);

    void setSoonPromoListItems(List<Promotion> promotions);

    void addSoonPromoListItems(List<Promotion> promotions);

    void showListPlaceholder(@StringRes int textRes);

    void hideListPlaceholder();

    void openPromoDetailActivity(Promotion promo);

    void openPastPromoDetailActivity(Promotion promo);
}
