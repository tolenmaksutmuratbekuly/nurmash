package com.nurmash.nurmash.mvp.prizes.verification;

import com.nurmash.nurmash.mvp.MvpPresenter;

public class PhotoDocsPresenter implements MvpPresenter<PhotoDocsView> {
    private PhotoDocsView photoDocsView;

    public PhotoDocsPresenter() {
    }

    @Override
    public void attachView(PhotoDocsView view) {
        photoDocsView = view;
    }

    @Override
    public void detachView() {
        photoDocsView = null;
    }

    public void onIDCardClick() {
        if (photoDocsView != null) {
            photoDocsView.onIDCardButtonClick();
        }
    }

    public void onPassportClick() {
        if (photoDocsView != null) {
            photoDocsView.onPassportButtonClick();
        }
    }

    public void onBackEventClick() {
        if (photoDocsView != null) {
            photoDocsView.onBackEventClick();
        }
    }
}

