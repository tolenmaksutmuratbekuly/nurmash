package com.nurmash.nurmash.mvp.participate;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface StepsSummaryView extends MvpView {
    void showLoadingIndicator(boolean show);

    void openVideoStepActivity(long promoId);

    void openShareStepActivity(long promoId);

    void openSurveyStepActivity(long promoId);

    void openMyPhotoActivity(long competitorId);

    void setChecklistItems(List<ChecklistItem> checklistItems);

    void setNextActionTitle(@StringRes int resId);

    void finish();
}
