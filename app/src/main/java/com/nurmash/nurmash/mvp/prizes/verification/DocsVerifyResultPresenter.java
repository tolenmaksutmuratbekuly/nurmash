package com.nurmash.nurmash.mvp.prizes.verification;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class DocsVerifyResultPresenter implements MvpPresenter<DocsVerifyResultView> {
    private final DataLayer dataLayer;
    private ErrorHandler errorHandler;
    private DocsVerifyResultView docsVerifyResultView;
    private Subscription profileVerifySubscription;

    public DocsVerifyResultPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void attachView(DocsVerifyResultView view) {
        docsVerifyResultView = view;
    }

    @Override
    public void detachView() {
        docsVerifyResultView = null;
    }

    public void profileVerifyStatus() {
        if (profileVerifySubscription != null) {
            profileVerifySubscription.unsubscribe();
        }
        docsVerifyResultView.showLoadingIndicator(true);
        profileVerifySubscription = dataLayer.getProfileVerifyStatus()
                .observeOn(mainThread())
                .subscribe(new Subscriber<ProfileVerifyStatus>() {
                    @Override
                    public void onNext(ProfileVerifyStatus profileVerifyStatus) {
                        docsVerifyResultView.showLoadingIndicator(false);
                        docsVerifyResultView.profileVerificationStatus(profileVerifyStatus);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        docsVerifyResultView.showLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}

