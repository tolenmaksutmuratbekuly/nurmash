package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.InviteList;
import com.nurmash.nurmash.model.json.PartnerInvitations;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PartnerInvitationsPresenter implements MvpPresenter<PartnerInvitationsView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private final List<InviteList> loadedItems = new ArrayList<>();
    private PartnerInvitationsView invitationsView;
    private ErrorHandler errorHandler;
    private Subscription feedLoadSubscription;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private boolean isLoadingItems;
    private long myUserId;

    public PartnerInvitationsPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    private void resetLoadedItems() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void attachView(PartnerInvitationsView view) {
        invitationsView = view;
        if (isFinalListEmpty()) {
            showEmptyView();
        } else {
            invitationsView.setInvitationItems(loadedItems);
            invitationsView.showLoadingIndicator(isLoadingItems);
            invitationsView.invitedUsersSum(0);
            if (loadedItems.isEmpty()) {
                loadNextPage();
            }
        }
    }

    @Override
    public void detachView() {
        if (feedLoadSubscription != null) {
            feedLoadSubscription.unsubscribe();
            feedLoadSubscription = null;
        }
        invitationsView = null;
    }

    public void setMyUserId(long myUserId) {
        this.myUserId = myUserId;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onRefreshAction() {
        reload();
    }

    public void onRequestMoreItems() {
        loadNextPage();
    }

    private void setLoadingItems(boolean isLoading) {
        isLoadingItems = isLoading;
        if (invitationsView != null) {
            invitationsView.showLoadingIndicator(isLoadingItems);
        }
    }

    private void loadNextPage() {
        if (noMoreItems || isLoadingItems) {
            return;
        }
        setLoadingItems(true);
        feedLoadSubscription = dataLayer.getPartnerInvitations(numPagesLoaded + 1, ITEMS_PER_PAGE, 0)
                .observeOn(mainThread())
                .subscribe(new Subscriber<PartnerInvitations>() {
                    @Override
                    public void onNext(PartnerInvitations invitation) {
                        setLoadingItems(false);
                        onNextPageLoaded(invitation);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void reload() {
        if (feedLoadSubscription != null) {
            feedLoadSubscription.unsubscribe();
        }
        setLoadingItems(true);
        feedLoadSubscription = dataLayer.getPartnerInvitations(1, ITEMS_PER_PAGE, 0)
                .observeOn(mainThread())
                .subscribe(new Subscriber<PartnerInvitations>() {
                    @Override
                    public void onNext(PartnerInvitations invitation) {
                        setLoadingItems(false);
                        resetLoadedItems();
                        onNextPageLoaded(invitation);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void onNextPageLoaded(PartnerInvitations invitation) {
        if (invitation.invite_list == null || invitation.invite_list.isEmpty()) {
            noMoreItems = true;
            if (isFinalListEmpty()) {
                showEmptyView();
            }
        } else {
            numPagesLoaded = numPagesLoaded + 1;
            loadedItems.addAll(invitation.invite_list);
            if (invitationsView != null) {
                if (numPagesLoaded == 1) {
                    invitationsView.setInvitationItems(invitation.invite_list);
                } else {
                    invitationsView.addInvitationItems(invitation.invite_list);
                }

                int invite_count = invitation.invite_count;
                if (invite_count > 0) {
                    for (int i = 0; i < invitation.invite_list.size(); i++) {
                        if (myUserId == invitation.invite_list.get(i).referer_user_id) {
                            invite_count--;
                        }
                    }
                    invitationsView.invitedUsersSum(invite_count);
                }
            }
        }
    }

    private boolean isFinalListEmpty() {
        return noMoreItems && loadedItems.isEmpty();
    }

    private void showEmptyView() {
        if (invitationsView != null) {
            invitationsView.showEmptyView();
        }
    }
}
