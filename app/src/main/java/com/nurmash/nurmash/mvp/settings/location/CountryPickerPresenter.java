package com.nurmash.nurmash.mvp.settings.location;

import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;

public class CountryPickerPresenter extends LocationPickerPresenter<Country> {
    public CountryPickerPresenter(DataLayer dataLayer) {
        super(dataLayer, LOCATION_ID_NONE, LOCATION_ID_NONE);
    }

    @Override
    protected void loadItems() {
        if (isActive(itemsLoadSubscription)) {
            return;
        }
        showLoadingIndicator(true);
        itemsLoadSubscription = dataLayer.getCountryList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Country>>() {
                    @Override
                    public void onNext(List<Country> countries) {
                        List<Pair<Country, String>> countriesWithTitles = new ArrayList<>(countries.size());
                        for (Country country : countries) {
                            countriesWithTitles.add(Pair.create(country, country.title));
                        }
                        onItemsLoaded(countriesWithTitles);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        onItemsLoadFailed(e);
                    }
                });
    }
}
