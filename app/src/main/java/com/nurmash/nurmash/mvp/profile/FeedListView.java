package com.nurmash.nurmash.mvp.profile;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.json.FeedItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface FeedListView extends MvpView {
    void showFeedLoadingIndicator(boolean show);

    void setFeedItems(List<FeedItem> items);

    void addFeedItems(List<FeedItem> items);

    void showFeedListPlaceholder(@StringRes int textRes, @StringRes int btnRes);

    void openPhotoDetailActivity(long competitorId);

    void openUserProfileActivity(User user);

    void finishActivity();
}
