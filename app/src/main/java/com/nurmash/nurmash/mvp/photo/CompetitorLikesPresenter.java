package com.nurmash.nurmash.mvp.photo;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.LikesItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class CompetitorLikesPresenter implements MvpPresenter<CompetitorLikesView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private long competitorId;
    private final List<LikesItem> loadedItems = new ArrayList<>();
    private CompetitorLikesView competitorLikesView;
    private ErrorHandler errorHandler;
    private Subscription competitorLikesLoadSubscription;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private boolean isLoadingItems;

    public CompetitorLikesPresenter(DataLayer dataLayer, long competitorId) {
        this.dataLayer = dataLayer;
        this.competitorId = competitorId;
    }

    private void resetLoadedItems() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void attachView(CompetitorLikesView view) {
        competitorLikesView = view;
        if (!isFinalListEmpty()) {
            competitorLikesView.setLikeItems(loadedItems);
            competitorLikesView.showLoadingIndicator(isLoadingItems);
            if (loadedItems.isEmpty()) {
                loadNextPage();
            }
        }
    }

    @Override
    public void detachView() {
        if (competitorLikesLoadSubscription != null) {
            competitorLikesLoadSubscription.unsubscribe();
            competitorLikesLoadSubscription = null;
        }
        competitorLikesView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onRefreshAction() {
        reload();
    }

    public void onRequestMoreItems() {
        loadNextPage();
    }

    public void onUserClick(User user) {
        if (competitorLikesView != null && user != null && user.id != dataLayer.authPreferences().getUserId()) {
            competitorLikesView.openUserProfileActivity(user);
        }
    }

    private void setLoadingItems(boolean isLoading) {
        isLoadingItems = isLoading;
        if (competitorLikesView != null) {
            competitorLikesView.showLoadingIndicator(isLoadingItems);
        }
    }

    private void loadNextPage() {
        if (noMoreItems || isLoadingItems) {
            return;
        }

        setLoadingItems(true);
        competitorLikesLoadSubscription = dataLayer.getCompetitorLikes(numPagesLoaded + 1, ITEMS_PER_PAGE, competitorId)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<LikesItem>>() {
                    @Override
                    public void onNext(List<LikesItem> feedItems) {
                        setLoadingItems(false);
                        onNextPageLoaded(feedItems);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void reload() {
        if (competitorLikesLoadSubscription != null) {
            competitorLikesLoadSubscription.unsubscribe();
        }
        setLoadingItems(true);
        competitorLikesLoadSubscription = dataLayer.getCompetitorLikes(1, ITEMS_PER_PAGE, competitorId)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<LikesItem>>() {
                    @Override
                    public void onNext(List<LikesItem> likesItems) {
                        setLoadingItems(false);
                        resetLoadedItems();
                        onNextPageLoaded(likesItems);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void onNextPageLoaded(List<LikesItem> likesItems) {
        if (likesItems == null || likesItems.isEmpty()) {
            noMoreItems = true;
        } else {
            numPagesLoaded = numPagesLoaded + 1;
            loadedItems.addAll(likesItems);
            if (competitorLikesView != null) {
                if (numPagesLoaded == 1) {
                    competitorLikesView.setLikeItems(likesItems);
                } else {
                    competitorLikesView.addLikeItems(likesItems);
                }
            }
        }
    }

    private boolean isFinalListEmpty() {
        return noMoreItems && loadedItems.isEmpty();
    }
}
