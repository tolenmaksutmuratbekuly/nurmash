package com.nurmash.nurmash.mvp.error;

import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.mvp.ErrorHandler;

import retrofit.RetrofitError;

import static com.nurmash.nurmash.util.retrofit.RetrofitUtils.isHttpError;
import static com.nurmash.nurmash.util.retrofit.RetrofitUtils.isHttpForbiddenError;

public class RequestErrorHandler implements ErrorHandler {
    private RequestErrorCallbacks callbacks;

    public RequestErrorHandler() {
    }

    public RequestErrorHandler(RequestErrorCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void setCallbacks(RequestErrorCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void handleError(Throwable e) {
        if (e instanceof ApiError) {
            handleGenericApiError((ApiError) e);
            return;
        }

        if (e instanceof RetrofitError) {
            switch (((RetrofitError) e).getKind()) {
                case HTTP:
                    if (isHttpForbiddenError(e)) {
                        handleAuthTokenError();
                    } else if (isHttpError(e, 500) || isHttpError(e, 502)) {
                        handleInternalServerError();
                    } else {
                        handleGenericRequestError();
                    }
                    break;
                case NETWORK:
                    handleNetworkError();
                    break;
                default:
                    handleGenericRequestError();
            }
            return;
        }

        handleGenericRequestError();
    }

    private void handleGenericApiError(ApiError e) {
        if (e.hasPromoEndedError()) {
            if (callbacks != null) {
                callbacks.onPromoHasEndedError();
            }
        } else if (e.hasPromoNotFoundError()) {
            if (callbacks != null) {
                callbacks.onPromoNotFoundError();
            }
        } else if (e.hasUserBannedError()) {
            if (callbacks != null) {
                callbacks.onUserBannedError();
            }
        } else {
            handleGenericRequestError();
        }
    }

    private void handleNetworkError() {
        if (callbacks != null) callbacks.onNetworkError();
    }

    private void handleGenericRequestError() {
        if (callbacks != null) callbacks.onRequestFailed();
    }

    private void handleInternalServerError() {
        if (callbacks != null) callbacks.onInternalServerError();
    }

    private void handleAuthTokenError() {
        if (callbacks != null) callbacks.onAuthTokenInvalidated();
    }
}
