package com.nurmash.nurmash.mvp.prizes.discount;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

public class CurrentDiscountsPresenter implements MvpPresenter<CurrentDiscountsView> {
    private static final int ITEMS_PER_PAGE = 20;

    private final DataLayer dataLayer;
    private final List<PrizeItem> loadedPrizes = new ArrayList<>();
    private CurrentDiscountsView currentDiscountsView;
    private ErrorHandler errorHandler;
    private Subscription prizesLoadSubscription;

    public CurrentDiscountsPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    private void resetLoadedData() {
        loadedPrizes.clear();
    }

    @Override
    public void attachView(CurrentDiscountsView view) {
        currentDiscountsView = view;
    }

    @Override
    public void detachView() {
        if (prizesLoadSubscription != null) {
            prizesLoadSubscription.unsubscribe();
            prizesLoadSubscription = null;
        }
        currentDiscountsView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onRefreshAction() {

    }

    public void onRequestMore() {

    }

    public void onEmptyPlaceholderButtonClick() {
        if (currentDiscountsView != null) {
            currentDiscountsView.finishActivity();
        }
    }

    private void setLoadingItems(boolean isLoading) {
        if (currentDiscountsView != null) {
            currentDiscountsView.showPhotosLoadingIndicator(isLoading);
        }
    }

    private void showEmptyView() {
        currentDiscountsView.showPhotoListPlaceholder(R.string.info_text_my_profile_photos_empty, R.string.action_participate);
    }
}
