package com.nurmash.nurmash.mvp.promotion;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class PastPromoDetailPresenter implements MvpPresenter<PastPromoDetailView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private Promotion promo;
    private ErrorHandler errorHandler;
    private PastPromoDetailView pastPromoDetailView;
    private Subscription promoLoadSubscription;
    private boolean isLoadingPromo;

    public PastPromoDetailPresenter(DataLayer dataLayer, Promotion promo) {
        this(dataLayer, promo.id);
        this.promo = promo;
    }

    public PastPromoDetailPresenter(DataLayer dataLayer, long promoId) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
    }

    @Override
    public void attachView(PastPromoDetailView view) {
        pastPromoDetailView = view;
        if (promo != null) {
            pastPromoDetailView.showTitle(promo.title);
            pastPromoDetailView.showPhoto(getPromoPhotoUrl(promo));
        } else {
            loadPromo();
        }
    }

    @Override
    public void detachView() {
        if (promoLoadSubscription != null) {
            promoLoadSubscription.unsubscribe();
            promoLoadSubscription = null;
        }
        pastPromoDetailView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onResume() {
        dataLayer.notificationsHelper().clearWinningsNotification(promoId);
    }

    private static String getPromoPhotoUrl(Promotion promo) {
        return PhotoUrl.grxl(promo.photo);
    }

    private void setLoadingPromo(boolean isLoading) {
        isLoadingPromo = isLoading;
        if (pastPromoDetailView != null) {
            pastPromoDetailView.showLoadingIndicator(isLoadingPromo);
        }
    }

    private void setPromo(Promotion promo) {
        this.promo = promo;
        if (pastPromoDetailView != null) {
            pastPromoDetailView.showTitle(promo.title);
            pastPromoDetailView.showPhoto(getPromoPhotoUrl(promo));
        }
    }

    private void loadPromo() {
        if (isLoadingPromo) {
            return;
        }
        setLoadingPromo(true);
        promoLoadSubscription = dataLayer.getPromotion(promoId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Promotion>() {
                    @Override
                    public void onNext(Promotion promo) {
                        setLoadingPromo(false);
                        setPromo(promo);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingPromo(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
