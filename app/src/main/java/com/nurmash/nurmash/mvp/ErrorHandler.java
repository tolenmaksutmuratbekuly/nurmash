package com.nurmash.nurmash.mvp;

public interface ErrorHandler {
    void handleError(Throwable e);
}
