package com.nurmash.nurmash.mvp.prizes;

import com.nurmash.nurmash.model.json.PrizeItemPojo;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.mvp.MvpView;

public interface PrizesView extends MvpView {
    void showLoadingIndicator(boolean show);

    void onPrizeInfoLoadingFailed();

    void onInviteBonusLoadingFailed();

    void openPrizesInfoActivity();

    void showClaimPrizeSuccessMessage();

    void addPrize(PrizeItemPojo wonPrizes);

    void profileVerificationStatus(ProfileVerifyStatus profileVerifyStatus);
}
