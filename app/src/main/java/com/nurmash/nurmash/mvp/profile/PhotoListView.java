package com.nurmash.nurmash.mvp.profile;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface PhotoListView extends MvpView {
    void showPhotosLoadingIndicator(boolean show);

    void setPhotoItems(List<Competitor> competitors);

    void addPhotoItems(List<Competitor> competitors);

    void showPhotoListPlaceholder(@StringRes int textRes, @StringRes int btnRes);

    void openPhotoDetailActivity(long competitorId);

    void finishActivity();
}
