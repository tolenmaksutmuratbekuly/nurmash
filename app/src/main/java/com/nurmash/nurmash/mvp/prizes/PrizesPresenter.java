package com.nurmash.nurmash.mvp.prizes;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItemPojo;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.model.json.prizes.PrizeInfo;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PrizesPresenter implements MvpPresenter<PrizesView> {
    private final DataLayer dataLayer;
    private PrizeInfo prizesData;
    private PrizesView prizesView;
    private StringsProvider strings;
    private Subscription prizesLoadSubscription;
    private Subscription inviteBonusSubscription;
    private ErrorHandler errorHandler;
    private Subscription profileVerifySubscription;

    public PrizesPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(PrizesView view) {
        prizesView = view;
        prizesView.showLoadingIndicator(isActive(prizesLoadSubscription));
        updateAllViewState();
        loadInviteBonus();
        loadPrizeInfo();
        profileVerifyStatus();
    }

    @Override
    public void detachView() {
        prizesView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onDestroy() {
        if (prizesLoadSubscription != null) {
            prizesLoadSubscription.unsubscribe();
            prizesLoadSubscription = null;
        }
        if (inviteBonusSubscription != null) {
            inviteBonusSubscription.unsubscribe();
            inviteBonusSubscription = null;
        }
        if (profileVerifySubscription != null) {
            profileVerifySubscription.unsubscribe();
            profileVerifySubscription = null;
        }
    }

    public void setStringsProvider(StringsProvider strings) {
        this.strings = strings;
    }

    public void onResume() {
        dataLayer.notificationsHelper().clearAllWinningsNotifications();
    }

    public void onRetryClick() {
        loadPrizeInfo();
    }

    public void onInviteBonusRetry() {
        loadInviteBonus();
    }

    public void onInfoClick() {
        if (prizesView != null) {
            prizesView.openPrizesInfoActivity();
        }
    }

    public void onClaimPrizeSuccess() {
        if (prizesView != null) {
            prizesView.showClaimPrizeSuccessMessage();
        }
        prizesData = null;
        updateAllViewState();
        loadInviteBonus();
        loadPrizeInfo();
    }

    private void loadPrizeInfo() {
        if (isActive(prizesLoadSubscription)) {
            return;
        }
        if (prizesData == null && prizesView != null) {
            prizesView.showLoadingIndicator(true);
        }
        prizesLoadSubscription = dataLayer.getPrizesData()
                .observeOn(mainThread())
                .subscribe(new Subscriber<PrizeInfo>() {
                    @Override
                    public void onNext(PrizeInfo data) {
                        prizesView.showLoadingIndicator(false);
                        prizesData = data;
                        updateAllViewState();
                        profileVerifyStatus();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (prizesView != null) {
                            prizesView.showLoadingIndicator(false);
                        }
                        // Silently fail if prizesData != null
                        if (prizesData == null) {
                            if (prizesView != null) {
                                prizesView.onPrizeInfoLoadingFailed();
                            }
                        }
                    }
                });
    }

    private void updateAllViewState() {
        if (prizesView == null) {
            return;
        }
        if (prizesData != null) {
            prizesView.addPrize(new PrizeItemPojo(
                    strings.getString(R.string.label_prize_item_you_won, prizesData.getPrizeCount()),
                    R.drawable.prize_item_prizes, 0, PrizeItemPojo.ITEM_PROMO_PRIZE
            ));
        }
    }

    private void loadInviteBonus() {
        if (prizesView == null) {
            return;
        }
        prizesView.showLoadingIndicator(true);
        inviteBonusSubscription = dataLayer.getPartnerProgramBalance()
                .observeOn(mainThread())
                .subscribe(new Subscriber<PartnerBalance>() {
                    @Override
                    public void onNext(PartnerBalance partnerBalance) {
                        prizesView.showLoadingIndicator(false);
                        if (partnerBalance.last_claim != null ||
                                (partnerBalance.bonuses != null && partnerBalance.bonuses.available != null && !partnerBalance.bonuses.available.isZeroAmount())) {
                            String title = "";
                            if (partnerBalance.last_claim != null) {
                                title = strings.getString(R.string.label_prize_item_partner_program, partnerBalance.last_claim.amount.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + partnerBalance.getCurrencyCode());

                            } else if (!(partnerBalance.bonuses.available.isZeroAmount())) {
                                title = strings.getString(R.string.label_prize_item_partner_program, partnerBalance.bonuses.available.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + partnerBalance.getCurrencyCode());
                            }
                            prizesView.addPrize(new PrizeItemPojo(
                                    title,
                                    R.drawable.prize_item_partner, 0, PrizeItemPojo.ITEM_PARTNER_PROGRAM
                            ));
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        prizesView.showLoadingIndicator(false);
                        // Silently fail if prizesData != null
                        if (prizesData == null) {
                            prizesView.onInviteBonusLoadingFailed();
                        }
                    }
                });
    }

    private void profileVerifyStatus() {
        if (profileVerifySubscription != null) {
            profileVerifySubscription.unsubscribe();
        }

        prizesView.showLoadingIndicator(true);
        profileVerifySubscription = dataLayer.getProfileVerifyStatus()
                .observeOn(mainThread())
                .subscribe(new Subscriber<ProfileVerifyStatus>() {
                    @Override
                    public void onNext(ProfileVerifyStatus profileVerifyStatus) {
                        prizesView.showLoadingIndicator(false);
                        prizesView.profileVerificationStatus(profileVerifyStatus);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        prizesView.showLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
