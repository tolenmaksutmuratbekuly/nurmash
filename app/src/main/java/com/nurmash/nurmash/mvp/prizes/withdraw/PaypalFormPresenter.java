package com.nurmash.nurmash.mvp.prizes.withdraw;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.withdraw.PaypalForm;
import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.Set;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static com.nurmash.nurmash.model.withdraw.PaypalForm.ERROR_ACCOUNT_EMPTY;
import static com.nurmash.nurmash.model.withdraw.PaypalForm.ERROR_FIRST_NAME_EMPTY;
import static com.nurmash.nurmash.model.withdraw.PaypalForm.ERROR_LAST_NAME_EMPTY;
import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PaypalFormPresenter implements MvpPresenter<PaypalFormView> {
    private final PaypalForm form;
    private DataLayer dataLayer;
    private User userProfile;
    private PaypalFormView paypalFormView;
    private Subscription userProfileLoadSubscription;

    public PaypalFormPresenter() {
        this.form = new PaypalForm();
    }

    public void attachView(PaypalFormView view, DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        attachView(view);
    }

    @Override
    public void attachView(PaypalFormView view) {
        paypalFormView = view;
        if (userProfile == null) {
            loadUserProfile();
        }
        updateFormView();
    }

    @Override
    public void detachView() {
        if (userProfileLoadSubscription != null) {
            userProfileLoadSubscription.unsubscribe();
            userProfileLoadSubscription = null;
        }
        paypalFormView = null;
        dataLayer = null;
    }

    public PaypalForm getForm(String firstName, String lastName, String account) {
        form.first_name = trim(firstName);
        form.last_name = trim(lastName);
        form.account = trim(account);
        return form;
    }

    public void onValidationError(ValidationError e) {
        if (paypalFormView == null) {
            return;
        }
        if (e == null) {
            throw new NullPointerException("e == null");
        }
        Set<Integer> errors = e.errorCodes;
        int scrollToError = getNextScrollError(errors);
        if (errors.contains(ERROR_ACCOUNT_EMPTY)) {
            paypalFormView.showAccountError(R.string.error_message_paypal_account_empty, ERROR_ACCOUNT_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_FIRST_NAME_EMPTY)) {
            paypalFormView.showFirstNameError(R.string.error_message_form_first_name_empty, ERROR_FIRST_NAME_EMPTY == scrollToError);
        }
        if (errors.contains(ERROR_LAST_NAME_EMPTY)) {
            paypalFormView.showLastNameError(R.string.error_message_form_last_name_empty, ERROR_LAST_NAME_EMPTY == scrollToError);
        }
    }

    private static int getNextScrollError(Set<Integer> errors) {
        if (errors.contains(ERROR_ACCOUNT_EMPTY)) return ERROR_ACCOUNT_EMPTY;
        if (errors.contains(ERROR_FIRST_NAME_EMPTY)) return ERROR_FIRST_NAME_EMPTY;
        return ERROR_LAST_NAME_EMPTY;
    }

    private void updateFormView() {
        if (paypalFormView != null) {
            paypalFormView.showAccount(form.account);
            paypalFormView.showFirstName(form.first_name);
            paypalFormView.showLastName(form.last_name);
        }
    }

    private void loadUserProfile() {
        if (isActive(userProfileLoadSubscription)) {
            return;
        }
        userProfileLoadSubscription = dataLayer.getMyProfile(true)
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        userProfile = user;
                        form.fillWithUser(user);
                        updateFormView();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }
                });
    }
}
