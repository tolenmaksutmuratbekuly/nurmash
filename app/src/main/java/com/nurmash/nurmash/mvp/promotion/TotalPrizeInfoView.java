package com.nurmash.nurmash.mvp.promotion;

import android.view.View;

import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.MvpView;

public interface TotalPrizeInfoView extends MvpView {
    void showLoadingIndicator(boolean show);

    void showNextButton(boolean show);

    void setNextButtonText(String text);

    void setNextButtonCallback(View.OnClickListener callback);

    void openStepsSummaryActivity(long promoId);

    void openShareStepActivity(long promoId);

    void openPhotoDetailActivity(long competitorId);

    void openPastPromoDetailActivity(Promotion promo);
}
