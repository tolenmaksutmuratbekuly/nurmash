package com.nurmash.nurmash.mvp.helpers;

import java.io.File;

public interface FilesProvider {
    File getExternalCacheDir();
}
