package com.nurmash.nurmash.mvp.main;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.ApiUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PromoListPresenter implements MvpPresenter<PromoListView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private final String filter;

    private PromoListView promoListView;
    private Subscription promotionsLoadSubscription;
    private ErrorHandler errorHandler;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private List<Promotion> loadedItems;
    private int numSoonPagesLoaded;
    private boolean noMoreSoonItems;
    private List<Promotion> loadedSoonItems;
    private boolean isLoading;

    public PromoListPresenter(DataLayer dataLayer, String filter) {
        this.dataLayer = dataLayer;
        this.filter = filter;
        this.loadedItems = new ArrayList<>();
        this.loadedSoonItems = new ArrayList<>();
        reset();
        loadMoreItems();
    }

    private void reset() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
        numSoonPagesLoaded = 0;
        noMoreSoonItems = !ApiUtils.FILTER_NEW_PROMOTIONS.equals(filter);
        loadedSoonItems.clear();
    }

    @Override
    public void attachView(PromoListView view) {
        promoListView = view;
        promoListView.setPromoListItems(loadedItems);
        promoListView.setSoonPromoListItems(loadedSoonItems);
        promoListView.showLoadingIndicator(isLoading);
        if (noMoreItems && noMoreSoonItems) {
            checkIfEmpty();
        } else {
            if (numPagesLoaded == 0) {
                loadMoreItems();
            }
        }
    }

    @Override
    public void detachView() {
        promoListView = null;
    }

    public void onDestroy() {
        if (promotionsLoadSubscription != null) {
            promotionsLoadSubscription.unsubscribe();
            promotionsLoadSubscription = null;
        }
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onPromoClick(Promotion promo) {
        if (promoListView != null && promo != null) {
            int state = promo.getState(System.currentTimeMillis());
            switch (state) {
                case Promotion.STATE_UNKNOWN:
                case Promotion.STATE_RUNNING:
                case Promotion.STATE_BEFORE_START:
                    promoListView.openPromoDetailActivity(promo);
                    break;
                case Promotion.STATE_ENDED_RECENTLY:
                case Promotion.STATE_ENDED:
                    promoListView.openPastPromoDetailActivity(promo);
                    break;
                default:
                    throw new IllegalStateException("Unsupported promotion state: " + state);
            }
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
        if (promoListView != null) {
            promoListView.showLoadingIndicator(isLoading);
        }
    }

    public void loadMoreItems() {
        if (noMoreItems) {
            loadMoreSoonItems();
            return;
        }
        if (isLoading) {
            return;
        }
        setLoading(true);
        promotionsLoadSubscription = dataLayer.getPromoList(filter, numPagesLoaded + 1, ITEMS_PER_PAGE)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<Promotion>>() {
                    @Override
                    public void onNext(List<Promotion> promotions) {
                        setLoading(false);
                        numPagesLoaded = numPagesLoaded + 1;
                        if (promotions == null || promotions.isEmpty()) {
                            noMoreItems = true;
                            onPrimaryListFinished();
                        } else {
                            loadedItems.addAll(promotions);
                            if (promoListView != null) {
                                promoListView.addPromoListItems(promotions);
                                promoListView.hideListPlaceholder();
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        handleLoadError(e);
                    }
                });
    }

    private void loadMoreSoonItems() {
        if (isLoading || noMoreSoonItems) {
            return;
        }
        setLoading(true);
        promotionsLoadSubscription = dataLayer.getSoonPromoList(numSoonPagesLoaded + 1, ITEMS_PER_PAGE)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<Promotion>>() {
                    @Override
                    public void onNext(List<Promotion> promotions) {
                        setLoading(false);
                        numSoonPagesLoaded = numSoonPagesLoaded + 1;
                        if (promotions == null || promotions.isEmpty()) {
                            noMoreSoonItems = true;
                            checkIfEmpty();
                        } else {
                            loadedSoonItems.addAll(promotions);
                            if (promoListView != null) {
                                if (numSoonPagesLoaded == 1) {
                                    promoListView.setSoonPromoListItems(promotions);
                                } else {
                                    promoListView.addSoonPromoListItems(promotions);
                                }
                                promoListView.hideListPlaceholder();
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        handleLoadError(e);
                    }
                });
    }

    public void refreshItems() {
        if (promotionsLoadSubscription != null) {
            promotionsLoadSubscription.unsubscribe();
        }
        setLoading(true);
        promotionsLoadSubscription = dataLayer.getPromoList(filter, 1, ITEMS_PER_PAGE)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<Promotion>>() {
                    @Override
                    public void onNext(List<Promotion> promotions) {
                        setLoading(false);
                        if (promoListView != null) {
                            promoListView.showRefreshingIndicator(false);
                        }

                        reset();
                        numPagesLoaded = 1;
                        if (promotions == null || promotions.isEmpty()) {
                            noMoreItems = true;
                            onPrimaryListFinished();
                        } else {
                            loadedItems.addAll(promotions);
                            if (promoListView != null) {
                                promoListView.hideListPlaceholder();
                                promoListView.setPromoListItems(loadedItems);
                                promoListView.setSoonPromoListItems(loadedSoonItems);
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        handleLoadError(e);
                    }
                });
    }

    private void onPrimaryListFinished() {
        if (noMoreSoonItems) {
            checkIfEmpty();
        } else {
            loadMoreSoonItems();
        }
    }

    private void checkIfEmpty() {
        if (loadedItems.isEmpty() && loadedSoonItems.isEmpty() && promoListView != null) {
            switch (filter) {
                case ApiUtils.FILTER_NEW_PROMOTIONS:
                    promoListView.showListPlaceholder(R.string.info_text_on_empty_new_promo_list);
                    break;
                case ApiUtils.FILTER_MY_PROMOTIONS:
                    promoListView.showListPlaceholder(R.string.info_text_on_empty_my_promo_list);
                    break;
                case ApiUtils.FILTER_PAST_PROMOTIONS:
                    promoListView.showListPlaceholder(R.string.info_text_on_empty_past_promo_list);
                    break;
            }
        }
    }

    private void handleLoadError(Throwable e) {
        if (promoListView != null) {
            promoListView.showLoadingIndicator(false);
            promoListView.showRefreshingIndicator(false);
        }
        if (errorHandler != null) {
            errorHandler.handleError(e);
        }
    }
}
