package com.nurmash.nurmash.mvp.promotion;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.MvpView;

public interface WinnerListView extends MvpView {
    void showLoadingIndicator(boolean show);

    void showWinners(Promotion promo);

    void showNoWinnersPlaceholder(@StringRes int textRes, @StringRes int buttonTextRes);

    void openPhotoDetailActivity(long competitorId);

    void openProfileDetailActivity(long userId);
}
