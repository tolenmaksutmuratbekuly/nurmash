package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PartnerQRCodePresenter implements MvpPresenter<PartnerQRCodeView> {
    private final DataLayer dataLayer;
    private PartnerQRCodeView partnerListView;
    private ErrorHandler errorHandler;
    private Subscription inviteAvailableSubscription;

    public PartnerQRCodePresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(PartnerQRCodeView view) {
        partnerListView = view;
        loadInviteAvailable();
    }

    @Override
    public void detachView() {
        if (inviteAvailableSubscription != null) {
            inviteAvailableSubscription.unsubscribe();
            inviteAvailableSubscription = null;
        }
        partnerListView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    private void loadInviteAvailable() {
        partnerListView.showLoadingIndicator(true);
        inviteAvailableSubscription = dataLayer.isInviteAvailable()
                .observeOn(mainThread())
                .subscribe(new Subscriber<InviteAvailable>() {
                    @Override
                    public void onNext(InviteAvailable inviteAvailable) {
                        if (partnerListView != null) {
                            partnerListView.showLoadingIndicator(false);
                            partnerListView.inviteAvailableLoaded(inviteAvailable);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        partnerListView.showLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
