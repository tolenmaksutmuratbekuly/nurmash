package com.nurmash.nurmash.mvp.main;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.MobappCompatible;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class MainPresenter implements MvpPresenter<MainView> {
    private final DataLayer dataLayer;
    private MainView mainView;
    private ErrorHandler errorHandler;
    private Subscription compatibleSubscription;

    public MainPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(MainView view) {
        mainView = view;
        if (mainView != null) {
            loadMobappCompatibility();
        }
    }

    @Override
    public void detachView() {
        if (compatibleSubscription != null) {
            compatibleSubscription.unsubscribe();
            compatibleSubscription = null;
        }
        mainView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    private void loadMobappCompatibility() {
        compatibleSubscription = dataLayer.getMobappCompatibility()
                .observeOn(mainThread())
                .subscribe(new Subscriber<MobappCompatible>() {
                    @Override
                    public void onNext(MobappCompatible mobappCompatible) {
                        if (mainView != null) {
                            mainView.onMobCompatibilityLoaded(mobappCompatible);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}


