package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PartnerBalancePresenter implements MvpPresenter<PartnerBalanceView> {
    private final DataLayer dataLayer;
    private PartnerBalanceView partnerListView;
    private ErrorHandler errorHandler;
    private Subscription partnerLoadSubscription;
    private boolean isLoadingItems;

    public PartnerBalancePresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(PartnerBalanceView view) {
        partnerListView = view;
        loadBalance(false);
    }

    @Override
    public void detachView() {
        if (partnerLoadSubscription != null) {
            partnerLoadSubscription.unsubscribe();
            partnerLoadSubscription = null;
        }

        partnerListView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onRefreshAction() {
        loadBalance(true);
    }

    private void setLoading(boolean isLoading) {
        isLoadingItems = isLoading;
        if (partnerListView != null) {
            partnerListView.showLoadingIndicator(isLoadingItems);
        }
    }

    private void loadBalance(boolean isReload) {
        if (isReload) {
            if (partnerLoadSubscription != null) {
                partnerLoadSubscription.unsubscribe();
            }
        } else {
            if (isLoadingItems) {
                return;
            }
        }

        setLoading(true);
        partnerLoadSubscription = dataLayer.getPartnerProgramBalance()
                .observeOn(mainThread())
                .subscribe(new Subscriber<PartnerBalance>() {
                    @Override
                    public void onNext(PartnerBalance partnerBalance) {
                        setLoading(false);
                        if (partnerListView != null) {
                            partnerListView.onBalanceLoaded(partnerBalance);
                            if (partnerBalance.last_claim != null || !partnerBalance.bonuses.available.isZeroAmount()) {
                                partnerListView.isZeroBalanceActive(false);
                            } else {
                                partnerListView.isZeroBalanceActive(true);
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoading(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
