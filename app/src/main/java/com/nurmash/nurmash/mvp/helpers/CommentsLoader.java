package com.nurmash.nurmash.mvp.helpers;

import android.util.Pair;

import com.nurmash.nurmash.model.json.Comment;

import java.util.List;

import rx.Observable;

public interface CommentsLoader {
    void setLoadedPages(int page);

    boolean hasMorePages();

    Observable<Pair<Integer, List<Comment>>> loadNextPage();
}
