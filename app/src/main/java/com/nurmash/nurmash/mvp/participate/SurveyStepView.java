package com.nurmash.nurmash.mvp.participate;

import com.nurmash.nurmash.model.json.Survey;
import com.nurmash.nurmash.mvp.MvpView;

public interface SurveyStepView extends MvpView {
    void showLoadingIndicator(boolean show);

    void setNode(Survey.Node node);

    void setChildren(Survey.Children[] children);

    void setSelectedAnswerId(String answerId);

    void showNextButtonState(boolean show);

    void setSurveyType(String type);

    void showCommitLoadingIndicator(boolean show);

    void openMyPhotoActivity(long competitorId);

    void finish();
}
