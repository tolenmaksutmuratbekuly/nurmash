package com.nurmash.nurmash.mvp.promotion;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class CompetitorListPresenter implements MvpPresenter<CompetitorListView> {
    private static final int ITEMS_PER_PAGE = 40;
    private static final String COMPETITOR_LIST_DISPLAY = "full";
    private final DataLayer dataLayer;
    private final long promoId;
    private final boolean isPhotoSharingPromo;
    private final List<Competitor> loadedCompetitors = new ArrayList<>();
    private final Promotion promo;
    private ErrorHandler errorHandler;
    private CompetitorListView competitorListView;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private Subscription competitorsLoadSubscription;
    private Subscription likePhotoSubscription;
    private Subscription reportPhotoSubscription;

    public CompetitorListPresenter(DataLayer dataLayer, Promotion promo) {
        this(dataLayer, promo.id, promo.isPhotoSharingPromo(), promo);
    }

    public CompetitorListPresenter(DataLayer dataLayer, long promoId, boolean isPhotoSharingPromo, Promotion promo) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
        this.isPhotoSharingPromo = isPhotoSharingPromo;
        this.promo = promo;
        loadMoreItemsInternal();
    }

    @Override
    public void attachView(CompetitorListView view) {
        competitorListView = view;
        competitorListView.setCompetitors(loadedCompetitors, dataLayer.authPreferences().getUserId());
        competitorListView.showLoadingIndicator(isLoading(competitorsLoadSubscription));
    }

    @Override
    public void detachView() {
        if (competitorsLoadSubscription != null) {
            competitorsLoadSubscription.unsubscribe();
            competitorsLoadSubscription = null;
        }
        competitorListView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void loadMoreItems() {
        loadMoreItemsInternal();
    }

    public void onCompetitorClick(Competitor competitor, int competitorIndex) {
        if (competitorListView == null) {
            return;
        }
        if (isPhotoSharingPromo) {
            competitorListView.openListDetailFragment(competitor.id, competitorIndex);
        } else {
            competitorListView.openProfileDetailActivity(competitor.getUserId());
        }
    }

    private boolean isLoading(Subscription s) {
        return s != null && !s.isUnsubscribed();
    }

    private void loadMoreItemsInternal() {
        if (noMoreItems || isLoading(competitorsLoadSubscription)) {
            return;
        }
        if (competitorListView != null) {
            competitorListView.showLoadingIndicator(true);
        }
        competitorsLoadSubscription = dataLayer.getCompetitorList(promoId, COMPETITOR_LIST_DISPLAY, numPagesLoaded + 1, ITEMS_PER_PAGE)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<Competitor>>() {
                    @Override
                    public void onNext(List<Competitor> competitors) {
                        if (competitorListView != null) {
                            competitorListView.showLoadingIndicator(false);
                        }
                        if (competitors == null || competitors.isEmpty()) {
                            noMoreItems = true;
                        } else {
                            numPagesLoaded = numPagesLoaded + 1;
                            List<Competitor> photoCompetitors = filterCompetitors(competitors);
                            loadedCompetitors.addAll(photoCompetitors);
                            if (competitorListView != null) {
                                for (int i = 0; i < photoCompetitors.size(); i++) {
                                    Competitor competitor = photoCompetitors.get(i);
                                    competitor.promo = promo;

                                }
                                competitorListView.addCompetitors(photoCompetitors);
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (competitorListView != null) {
                            competitorListView.showLoadingIndicator(false);
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private List<Competitor> filterCompetitors(List<Competitor> competitors) {
        List<Competitor> result = new ArrayList<>(competitors.size());
        for (int i = 0; i < competitors.size(); ++i) {
            Competitor c = competitors.get(i);
            if (!c.isDisqualified() && (!isPhotoSharingPromo || c.hasPhoto())) {
                result.add(c);
            }
        }
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    // List Detail implementation
    ///////////////////////////////////////////////////////////////////////////

    public void onLikeClick(Competitor competitor, int competitorPosition) {
        likePhoto(competitor, competitorPosition);
    }

    public void onCommentButtonClick(Competitor competitor) {
        if (competitorListView != null) {
            competitorListView.openCommentListActivity(competitor, true);
        }
    }

    public void onCompetitorLikesButtonClick(Competitor competitor) {
        if (competitorListView != null) {
            competitorListView.openCompetitorLikesActivity(competitor);
        }
    }

    public void onMoreCommentsButtonClick(Competitor competitor) {
        if (competitorListView != null) {
            competitorListView.openCommentListActivity(competitor, false);
        }
    }

    public void onUserClick(User user) {
        if (competitorListView != null) {
            competitorListView.openUserProfileActivity(user);
        }
    }

    public void onOpenPromoButtonClick(Competitor competitor) {
        if (competitorListView != null && competitor != null) {
            competitorListView.openPromoDetailActivity(competitor.getPromoId());
        }
    }

    public void onComplaintSelected(Competitor competitor, Complaint complaint, int competitorPosition) {
        reportPhoto(competitor, complaint, competitorPosition);
    }

    private void updatePhotoDetail(int competitorPosition) {
        if (competitorListView == null) {
            return;
        }

        competitorListView.updatePhotoDetail(competitorPosition);

    }

    private void likePhoto(final Competitor competitor, final int competitorPosition) {
        if (competitor == null || !competitor.canLike() || isActive(likePhotoSubscription)) {
            return;
        }

        likePhotoSubscription = dataLayer.likePhoto(competitor)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        competitor.like_count += 1;
                        competitor.is_liked = true;
                        updatePhotoDetail(competitorPosition); // Refresh item without waiting request response.
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private boolean isMyPhoto(Competitor competitor) {
        return competitor != null && competitor.getUserId() == dataLayer.authPreferences().getUserId();
    }

    private void reportPhoto(final Competitor competitor, Complaint complaint, final int competitorPosition) {
        if (complaint == null || isMyPhoto(competitor) || competitor == null || !competitor.canReport()
                || isActive(reportPhotoSubscription)) {
            return;
        }
        if (competitorListView != null) {
            competitorListView.showReportLoadingIndicator(true);
        }
        reportPhotoSubscription = dataLayer.reportPhoto(competitor.id, complaint)
                .observeOn(mainThread())
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        if (competitorListView != null) {
                            competitorListView.showReportLoadingIndicator(false);
                        }
                    }
                })
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        competitor.has_user_complaint = true;
                        updatePhotoDetail(competitorPosition);
                        if (competitorListView != null) {
                            competitorListView.showPhotoReportedMessage();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);

                        if (e instanceof ApiError && ((ApiError) e).hasComplaintExistsError()) {
                            if (competitorListView != null) {
                                competitorListView.showPhotoComplaintExistsError();
                            }
                            return;
                        }

                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
