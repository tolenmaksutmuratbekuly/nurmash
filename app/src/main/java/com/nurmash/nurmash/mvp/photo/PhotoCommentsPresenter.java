package com.nurmash.nurmash.mvp.photo;

import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.exception.CommentBodyTooLongException;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.ClipboardHelper;
import com.nurmash.nurmash.mvp.helpers.CommentsLoader;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import timber.log.Timber;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.StringUtils.trim;
import static java.lang.Character.isSpaceChar;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PhotoCommentsPresenter implements MvpPresenter<PhotoCommentsView> {
    private final DataLayer dataLayer;
    private final long competitorId;
    private Competitor competitor;
    private boolean isPromoActive;
    private ErrorHandler errorHandler;
    private ClipboardHelper clipboardHelper;
    private CommentsLoader commentsLoader;
    private PhotoCommentsView photoCommentsView;
    private Subscription commentsLoadSubscription;
    private Subscription commentPostSubscription;
    private Subscription competitorLoadSubscription;
    private Subscription commentReportSubscription;
    private Comment origComment;
    private List<Comment> comments = new ArrayList<>();
    private String commentInputText;
    private boolean isLoadingCompetitor;
    private boolean isLoadingComments;
    private boolean isPostingComment;
    private boolean isReportingComment;

    private Comment selectedComment;
    private Comment replyComment;

    private long reportCommentId = -1;

    public PhotoCommentsPresenter(DataLayer dataLayer, long competitorId) {
        this.dataLayer = dataLayer;
        this.competitorId = competitorId;
    }

    public PhotoCommentsPresenter(DataLayer dataLayer, Competitor competitor) {
        this(dataLayer, competitor.id);
        setCompetitor(competitor);
    }

    @Override
    public void attachView(PhotoCommentsView view) {
        photoCommentsView = view;
        photoCommentsView.showLoadingIndicator(isLoadingCompetitor || isReportingComment, !isLoadingCompetitor);
        photoCommentsView.showCommentsLoadingIndicator(isLoadingComments);
        photoCommentsView.setSendButtonEnabled(canSend());
        photoCommentsView.setCommentInputText(commentInputText);
        if (competitor == null) {
            loadCompetitor();
        } else {
            photoCommentsView.showOriginalComment(origComment);
        }
        if (comments != null) {
            photoCommentsView.setComments(comments);
        }
    }

    @Override
    public void detachView() {
        if (commentsLoadSubscription != null) {
            commentsLoadSubscription.unsubscribe();
            commentsLoadSubscription = null;
        }
        if (commentPostSubscription != null) {
            commentPostSubscription.unsubscribe();
            commentPostSubscription = null;
        }
        if (competitorLoadSubscription != null) {
            competitorLoadSubscription.unsubscribe();
            competitorLoadSubscription = null;
        }
        if (commentReportSubscription != null) {
            commentReportSubscription.unsubscribe();
            commentReportSubscription = null;
        }
        photoCommentsView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setClipboardHelper(ClipboardHelper clipboardHelper) {
        this.clipboardHelper = clipboardHelper;
    }

    public void onUserClick(User user) {
        if (user != null && photoCommentsView != null) {
            photoCommentsView.openUserProfileActivity(user);
        }
    }

    public void onRequestMoreComments() {
        loadMoreComments();
    }

    public void onCommentInputTextChanged(String text) {
        commentInputText = trim(text);
        if (isEmpty(commentInputText)) {
            replyComment = null;
        }
        if (photoCommentsView != null) {
            photoCommentsView.setSendButtonEnabled(canSend());
        }
    }

    public void onSendClick() {
        postComment();
    }

    public void onCommentSelected(Comment comment) {
        if (photoCommentsView != null) {
            if (selectedComment != null && comment != null && selectedComment.id == comment.id) {
                // Hide context menu, if the same item is selected twice in a row.
                setSelectedComment(null);
            } else {
                setSelectedComment(comment);
            }
        }
    }

    public void onCommentContextMenuDestroy() {
        setSelectedComment(null);
    }

    public boolean isCopyActionEnabled() {
        return selectedComment != null;
    }

    public boolean isReplyActionEnabled() {
        return selectedComment != null && !isMyComment(selectedComment);
    }

    public boolean isReportActionEnabled() {
        return selectedComment != null && !isMyComment(selectedComment) &&
                (origComment == null || origComment.id != selectedComment.id);
    }

    public void onCommentCopyAction() {
        if (clipboardHelper != null && selectedComment != null) {
            clipboardHelper.copyText(selectedComment.body);
            if (photoCommentsView != null) {
                photoCommentsView.showCommentCopiedToastMessage();
            }
        }
        setSelectedComment(null);
    }

    public void onCommentReplyAction() {
        if (photoCommentsView == null || selectedComment == null) {
            return;
        }
        String displayName = selectedComment.user == null ? null : selectedComment.user.getDisplayName();
        if (!isEmpty(displayName)) {
            StringBuilder builder = new StringBuilder(commentInputText);
            if (builder.length() > 0 && !isSpaceChar(builder.charAt(builder.length() - 1))) {
                builder.append(' ');
            }
            builder.append(displayName);
            builder.append(", ");
            commentInputText = builder.toString();
            photoCommentsView.setCommentInputText(commentInputText);
            photoCommentsView.focusOnCommentInput();
            if (replyComment == null && selectedComment != origComment) {
                replyComment = selectedComment;
            }
        }
        setSelectedComment(null);
    }

    public void onCommentReportAction() {
        if (photoCommentsView == null || selectedComment == null) {
            return;
        }
        reportCommentId = selectedComment.id;
        photoCommentsView.openCommentReportDialog();
        setSelectedComment(null);
    }

    public void onReportReasonSelected(Complaint complaint) {
        if (reportCommentId == -1) {
            return;
        }
        if (complaint == null) {
            reportCommentId = -1;
            return;
        }
        reportComment(reportCommentId, complaint);
        reportCommentId = -1;
    }

    private boolean isMyComment(Comment comment) {
        return comment != null && comment.user != null && comment.user.id == dataLayer.authPreferences().getUserId();
    }

    private void setLoadingCompetitor(boolean isLoading) {
        isLoadingCompetitor = isLoading;
        if (photoCommentsView != null) {
            photoCommentsView.setSendButtonEnabled(canSend());
            photoCommentsView.showLoadingIndicator(isLoadingCompetitor, false);
        }
    }

    private void setLoadingComments(boolean isLoading) {
        isLoadingComments = isLoading;
        if (photoCommentsView != null) {
            photoCommentsView.setSendButtonEnabled(canSend());
            photoCommentsView.showCommentsLoadingIndicator(isLoadingComments);
        }
    }

    private void setPostingComment(boolean isPosting) {
        isPostingComment = isPosting;
        if (photoCommentsView != null) {
            photoCommentsView.setSendButtonEnabled(canSend());
        }
    }

    private void setReportingComment(boolean isReporting) {
        isReportingComment = isReporting;
        if (photoCommentsView != null) {
            photoCommentsView.setSendButtonEnabled(canSend());
            photoCommentsView.showLoadingIndicator(isReportingComment, true);
        }
    }

    private void setSelectedComment(Comment comment) {
        selectedComment = comment;
        if (photoCommentsView != null) {
            if (selectedComment != null) {
                photoCommentsView.showCommentContextMenu(true);
            } else {
                photoCommentsView.showCommentContextMenu(false);
                photoCommentsView.clearCommentSelection();
            }
            photoCommentsView.setSendButtonEnabled(canSend());
        }
    }

    private boolean canSend() {
        return !isLoadingComments && !isLoadingCompetitor && !isPostingComment
                && competitor != null && competitor.canComment() && isPromoActive
                && selectedComment == null
                && !isEmpty(commentInputText);
    }

    private void setCompetitor(Competitor newCompetitor) {
        if (commentsLoader == null && newCompetitor != null) {
            commentsLoader = new ReverseCommentsLoader(dataLayer, competitorId, newCompetitor.comment_count);
            loadMoreComments();
        }

        competitor = newCompetitor;
        int promoState = competitor == null ? Promotion.STATE_UNKNOWN : competitor.promo.getState(System.currentTimeMillis());
        isPromoActive = Promotion.isActiveState(promoState);
        String description = competitor == null ? null : competitor.getPhotoDescription();
        if (!isEmpty(description)) {
            origComment = new Comment();
            origComment.body = description;
            origComment.user = competitor.user;
            origComment.created_at = competitor.completed_at;
        } else {
            origComment = null;
        }
        if (photoCommentsView != null) {
            photoCommentsView.showOriginalComment(origComment);
        }
    }

    private void addOldComments(List<Comment> oldComments) {
        if (oldComments == null || oldComments.isEmpty()) {
            return;
        }
        comments.addAll(oldComments);
        if (photoCommentsView != null) {
            photoCommentsView.addOldComments(oldComments);
        }
    }

    private void addNewComment(Comment comment) {
        if (comment == null) {
            throw new NullPointerException("Adding null comment is unsupported.");
        }
        comments.add(0, comment);
        if (photoCommentsView != null) {
            photoCommentsView.addNewComment(comment);
        }
    }

    private void loadCompetitor() {
        if (isLoadingCompetitor) {
            return;
        }
        setLoadingCompetitor(true);
        competitorLoadSubscription = dataLayer.getCompetitor(competitorId)
                .flatMap(new Func1<Competitor, Observable<Competitor>>() {
                    @Override
                    public Observable<Competitor> call(final Competitor c) {
                        // Fetch more detailed promotion data.
                        return dataLayer.getPromotion(c.getPromoId())
                                .map(new Func1<Promotion, Competitor>() {
                                    @Override
                                    public Competitor call(Promotion detailedPromotion) {
                                        c.promo = detailedPromotion;
                                        return c;
                                    }
                                });
                    }
                })
                .observeOn(mainThread())
                .subscribe(new Subscriber<Competitor>() {
                    @Override
                    public void onNext(Competitor competitor) {
                        setLoadingCompetitor(false);
                        setCompetitor(competitor);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingCompetitor(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void loadMoreComments() {
        if (isLoadingComments || commentsLoader == null || !commentsLoader.hasMorePages()) {
            return;
        }
        setLoadingComments(true);
        commentsLoadSubscription = commentsLoader.loadNextPage()
                .observeOn(mainThread())
                .subscribe(new Subscriber<Pair<Integer, List<Comment>>>() {
                    @Override
                    public void onNext(Pair<Integer, List<Comment>> commentsPage) {
                        setLoadingComments(false);
                        commentsLoader.setLoadedPages(commentsPage.first);
                        addOldComments(commentsPage.second);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingComments(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void postComment() {
        if (!canSend()) {
            return;
        }
        setPostingComment(true);
        commentPostSubscription = dataLayer.postComment(competitorId, commentInputText, replyComment)
                .flatMap(new Func1<Comment, Observable<Comment>>() {
                    @Override
                    public Observable<Comment> call(final Comment comment) {
                        return dataLayer.getMyProfile(true).map(new Func1<User, Comment>() {
                            @Override
                            public Comment call(User user) {
                                comment.user = user;
                                return comment;
                            }
                        });
                    }
                })
                .observeOn(mainThread())
                .subscribe(new Subscriber<Comment>() {
                    @Override
                    public void onNext(Comment comment) {
                        setPostingComment(false);
                        replyComment = null;
                        addNewComment(comment);
                        if (photoCommentsView != null) {
                            photoCommentsView.clearCommentInputText();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setPostingComment(false);
                        if (e instanceof CommentBodyTooLongException) {
                            if (photoCommentsView != null) {
                                int length = ((CommentBodyTooLongException) e).commentBodyLength;
                                int limit = ((CommentBodyTooLongException) e).commentBodyLengthLimit;
                                photoCommentsView.showCommentBodyTooLongErrorMessage(length, limit);
                            }
                            return;
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void reportComment(long commentId, Complaint complaint) {
        if (isReportingComment || commentId == -1 || complaint == null) {
            return;
        }
        setReportingComment(true);
        commentReportSubscription = dataLayer.reportComment(competitorId, commentId, complaint)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        setReportingComment(false);
                        photoCommentsView.showCommentReportedToastMessage();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setReportingComment(false);
                        if (e instanceof ApiError && ((ApiError) e).hasComplaintExistsError()) {
                            if (photoCommentsView != null) {
                                photoCommentsView.showCommentComplaintExistsError();
                            }
                            return;
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
