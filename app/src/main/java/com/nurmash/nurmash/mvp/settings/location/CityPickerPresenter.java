package com.nurmash.nurmash.mvp.settings.location;

import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.nurmash.nurmash.util.rx.RxUtils.isActive;

public class CityPickerPresenter extends LocationPickerPresenter<City> {
    public CityPickerPresenter(DataLayer dataLayer, long countryId) {
        this(dataLayer, countryId, LOCATION_ID_NONE);
    }

    public CityPickerPresenter(DataLayer dataLayer, long countryId, long regionId) {
        super(dataLayer, countryId, regionId);
    }

    private Observable<List<City>> getCityListLoader() {
        if (countryId == LOCATION_ID_NONE) {
            return Observable.error(new IllegalStateException("No country id."));
        } else if (regionId == LOCATION_ID_NONE) {
            return dataLayer.getNoRegionCityList(countryId);
        } else {
            return dataLayer.getCityList(regionId);
        }
    }

    private Map<String, Integer> countTitles(List<City> cities) {
        Map<String, Integer> counts = new HashMap<>();
        for (City city : cities) {
            String title = trim(city.title);
            if (!counts.containsKey(title)) {
                counts.put(title, 1);
            } else {
                counts.put(title, counts.get(title) + 1);
            }
        }
        return counts;
    }

    @Override
    protected void loadItems() {
        if (isActive(itemsLoadSubscription)) {
            return;
        }
        showLoadingIndicator(true);
        itemsLoadSubscription = getCityListLoader()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<City>>() {
                    @Override
                    public void onNext(List<City> cities) {
                        List<Pair<City, String>> citiesWithTitles = new ArrayList<>(cities.size());
                        if (regionId == LOCATION_ID_NONE) {
                            // Always try to include area name for cities without a region.
                            for (City city : cities) {
                                citiesWithTitles.add(Pair.create(city, city.getTitleWithArea()));
                            }
                        } else {
                            Map<String, Integer> titleCounts = countTitles(cities);
                            for (City city : cities) {
                                String title = trim(city.title);
                                if (titleCounts.get(title) > 1) {
                                    // Try to disambiguate cities with equal titles by including area name.
                                    title = city.getTitleWithArea();
                                }
                                citiesWithTitles.add(Pair.create(city, title));
                            }
                        }
                        onItemsLoaded(citiesWithTitles);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        onItemsLoadFailed(e);
                    }
                });
    }
}
