package com.nurmash.nurmash.mvp.promotion;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;

public class WinnerListPresenter implements MvpPresenter<WinnerListView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private ErrorHandler errorHandler;
    private ParticipationData participationData;
    private WinnerListView winnerListView;
    private Subscription winnersLoadSubscription;

    public WinnerListPresenter(DataLayer dataLayer, long promoId) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
    }

    @Override
    public void attachView(WinnerListView view) {
        winnerListView = view;
        if (participationData != null) {
            updateView(participationData);
        } else {
            winnerListView.showLoadingIndicator(isActive(winnersLoadSubscription));
            loadWinners();
        }
    }

    @Override
    public void detachView() {
        if (winnersLoadSubscription != null) {
            winnersLoadSubscription.unsubscribe();
            winnersLoadSubscription = null;
        }
        winnerListView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onWinnerClick(Promotion.Winner winner) {
        if (winnerListView == null || winner == null) {
            return;
        }
        if (participationData.getPromotion().isPhotoSharingPromo()) {
            winnerListView.openPhotoDetailActivity(winner.id);
        } else if (winner.user != null) {
            winnerListView.openProfileDetailActivity(winner.user.id);
        }
    }

    public void onNoWinnersPlaceholderButtonClick() {
        if (winnerListView != null && participationData.getCompetitor() != null) {
            winnerListView.openPhotoDetailActivity(participationData.getCompetitorId());
        }
    }

    private void setParticipationData(ParticipationData data) {
        participationData = data;
        updateView(data);
    }

    private void updateView(ParticipationData data) {
        if (data.getPromotion().hasWinners()) {
            winnerListView.showWinners(data.getPromotion());
        } else {
            Competitor competitor = data.getCompetitor();
            if (competitor != null && competitor.hasPhoto() && competitor.hasCompletedAllSteps()) {
                winnerListView.showNoWinnersPlaceholder(R.string.info_text_prizes_not_drafted, R.string.action_see_my_photo);
            } else {
                winnerListView.showNoWinnersPlaceholder(R.string.info_text_prizes_not_drafted, 0);
            }
        }
    }

    private void loadWinners() {
        if (isActive(winnersLoadSubscription)) {
            return;
        }
        if (winnerListView != null) {
            winnerListView.showLoadingIndicator(true);
        }
        winnersLoadSubscription = dataLayer.getParticipationData(promoId, false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        if (winnerListView != null) {
                            winnerListView.showLoadingIndicator(false);
                        }
                        setParticipationData(data);
                        updateView(data);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (winnerListView != null) {
                            winnerListView.showLoadingIndicator(false);
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
