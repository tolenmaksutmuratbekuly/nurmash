package com.nurmash.nurmash.mvp.promotion;

import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;

public interface CompetitorListFragmentCallbacks {
    void loadMoreItems();

    void onCompetitorClick(Competitor competitor, int position);

    void onUserClick(User user);

    void onLikeClick(Competitor competitor, int competitorPosition);

    void onCommentButtonClick(Competitor competitor);

    void onCompetitorLikesButtonClick(Competitor competitor);

    void onOpenPromoButtonClick(Competitor competitor);

    void onMoreCommentsButtonClick(Competitor competitor);

    void hideDetailFragment(int competitorIndex);

    void openPhotoReportDialog(int competitorPosition);

    void setReportCompetitor(Competitor competitor);

}
