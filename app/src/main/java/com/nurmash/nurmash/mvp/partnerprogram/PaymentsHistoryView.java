package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.json.InviteList;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface PaymentsHistoryView extends MvpView {
    void showLoadingIndicator(boolean show);

    void setInvitationItems(List<InviteList> items);

    void addInvitationItems(List<InviteList> items);

    void showEmptyView();

    void setTotalWithDrawnSum(String totalWithDrawnSum);
}
