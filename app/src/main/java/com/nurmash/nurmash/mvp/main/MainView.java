package com.nurmash.nurmash.mvp.main;

import com.nurmash.nurmash.model.json.MobappCompatible;
import com.nurmash.nurmash.mvp.MvpView;

public interface MainView extends MvpView {
    void onMobCompatibilityLoaded(MobappCompatible mobappCompatible);
}