package com.nurmash.nurmash.mvp.main;

import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface HotPromoSlideView extends MvpView {
    void openPromoDetailActivity(Promotion promo);

}

