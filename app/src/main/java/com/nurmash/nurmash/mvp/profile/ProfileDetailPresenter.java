package com.nurmash.nurmash.mvp.profile;

import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPicker;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPickerCallback;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class ProfileDetailPresenter implements MvpPresenter<ProfileDetailView>, PhotoPickerCallback {
    private final DataLayer dataLayer;
    private final long userId;
    private User user;
    private ErrorHandler errorHandler;
    private ProfileDetailView profileDetailView;
    private PhotoPicker photoPicker;
    private Subscription profileLoadSubscription;
    private Subscription inviteAvailableSubscription;
    private Subscription photoUploadSubscription;
    private Subscription photoUploadProgressSubscription;
    private Uri uploadPhotoUri;
    private boolean isLoadingProfile;

    public ProfileDetailPresenter(DataLayer dataLayer) {
        this(dataLayer, dataLayer.authPreferences().getUserId(), dataLayer.getCachedUserProfile());
    }

    public ProfileDetailPresenter(DataLayer dataLayer, @NonNull User user) {
        this(dataLayer, user.id, user);
    }

    public ProfileDetailPresenter(DataLayer dataLayer, long userId) {
        this(dataLayer, userId, null);
    }

    private ProfileDetailPresenter(DataLayer dataLayer, long userId, User user) {
        if (userId < 0) {
            throw new IllegalArgumentException("Invalid user id: " + userId);
        }
        this.dataLayer = dataLayer;
        this.userId = userId;
        this.user = user;
    }

    @Override
    public void attachView(ProfileDetailView view) {
        profileDetailView = view;
        if (user != null) {
            profileDetailView.showProfileLoadingIndicator(false);
            profileDetailView.onProfileLoaded(user);
        }
        loadProfileData();
        if (isMyProfile()) {
            loadInviteAvailable();
        }
    }

    @Override
    public void detachView() {
        profileDetailView = null;
    }

    public void onDestroy() {
        if (profileLoadSubscription != null) {
            profileLoadSubscription.unsubscribe();
            profileLoadSubscription = null;
        }
        if (inviteAvailableSubscription != null) {
            inviteAvailableSubscription.unsubscribe();
            inviteAvailableSubscription = null;
        }
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setPhotoPicker(PhotoPicker newPicker) {
        if (newPicker == null) {
            // Dispose
            cancelPhotoUpload();
            photoPicker.setPhotoPickerCallback(null);
            photoPicker = null;
        } else {
            photoPicker = newPicker;
            photoPicker.setPhotoPickerCallback(this);
        }
    }

    public long getUserId() {
        return userId;
    }

    public boolean isMyProfile() {
        return userId == dataLayer.authPreferences().getUserId();
    }

    public void onSettingsButtonClick() {
        if (profileDetailView != null) {
            profileDetailView.openSettingsActivity();
        }
    }

    private void setLoadingProfile(boolean isLoading) {
        isLoadingProfile = isLoading;
        if (profileDetailView != null) {
            profileDetailView.showProfileLoadingIndicator(isLoadingProfile && user == null);
        }
    }

    private void loadProfileData() {
        if (isLoadingProfile) {
            return;
        }
        setLoadingProfile(true);
        profileLoadSubscription = (isMyProfile() ? dataLayer.getMyProfile(false) : dataLayer.getUserProfile(userId))
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User loadedUser) {
                        setLoadingProfile(false);
                        user = loadedUser;
                        if (profileDetailView != null) {
                            profileDetailView.onProfileLoaded(loadedUser);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingProfile(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void loadInviteAvailable() {
        inviteAvailableSubscription = dataLayer.isInviteAvailable()
                .observeOn(mainThread())
                .subscribe(new Subscriber<InviteAvailable>() {
                    @Override
                    public void onNext(InviteAvailable inviteAvailable) {
                        if (profileDetailView != null) {
                            profileDetailView.inviteAvailableLoaded(inviteAvailable);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingProfile(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    public void onBackgroundClick() {
        if (profileDetailView == null) {
            return;
        }
        if (isUploadingPhoto()) {
            cancelPhotoUpload();
            profileDetailView.showPhotoUploadingIndicator(false);
        } else if (photoPicker != null) {
            photoPicker.startCroppedPhotoPicker();
        }
    }

    public void onPhotoUploadRetryClick() {
        if (isUploadingPhoto()) {
            cancelPhotoUpload();
        }
        restartPhotoUpload(uploadPhotoUri);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Photo upload
    ///////////////////////////////////////////////////////////////////////////

    private boolean isUploadingPhoto() {
        return isActive(photoUploadSubscription);
    }

    private void restartPhotoUpload(Uri photoUri) {
        if (photoUri == null) {
            return;
        }
        if (!photoUri.equals(uploadPhotoUri)) {
            // Dispose of the previous upload.
            cancelPhotoUpload();
        }
        uploadPhotoUri = photoUri;
        if (profileDetailView != null) {
            profileDetailView.showPhotoUploadingIndicator(true);
            profileDetailView.setPhotoUploadProgress(0);
        }
        final ObservableTypedFile observableFile = ObservableTypedFile.withPhotoUri(uploadPhotoUri);
        photoUploadProgressSubscription = observableFile.getProgressObservable()
                .sample(100, TimeUnit.MILLISECONDS, Schedulers.computation())
                .concatWith(Observable.just(100))
                .observeOn(mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer progress) {
                        if (profileDetailView != null) {
                            profileDetailView.setPhotoUploadProgress(progress);
                        }
                    }
                });
        photoUploadSubscription = dataLayer.uploadPhoto(observableFile)
                .observeOn(mainThread())
                .subscribe(new Subscriber<FileUploadResult>() {
                    @Override
                    public void onNext(FileUploadResult result) {
//                        form.photoHash = result.hash;
                        if (photoPicker != null) {
                            photoPicker.disposeOfPhotoFile(uploadPhotoUri);
                            uploadPhotoUri = null;
                        }
                        if (profileDetailView != null) {
                            profileDetailView.showPhotoUploadingIndicator(false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (profileDetailView != null) {
                            profileDetailView.showPhotoUploadingIndicator(false);
                            profileDetailView.showPhotoUploadError();
                        }
                    }
                });
    }

    private void cancelPhotoUpload() {
        if (isActive(photoUploadProgressSubscription)) {
            photoUploadProgressSubscription.unsubscribe();
            photoUploadProgressSubscription = null;
        }
        if (isActive(photoUploadSubscription)) {
            photoUploadSubscription.unsubscribe();
            photoUploadSubscription = null;
        }
        if (photoPicker != null && uploadPhotoUri != null) {
            photoPicker.disposeOfPhotoFile(uploadPhotoUri);
        }
        uploadPhotoUri = null;
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoPickerCallback interface
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onCroppedPhotoPicked(@NonNull Uri croppedPhotoUri) {
        if (profileDetailView != null) {
            profileDetailView.showSelectedPhoto(croppedPhotoUri);
        }
        restartPhotoUpload(croppedPhotoUri);
    }

    @Override
    public void onPhotoPickCanceled() {
    }

    @Override
    public void onPhotoPickError(Throwable e) {
        if (profileDetailView == null) {
            return;
        }
        if (e instanceof IOException) {
            profileDetailView.showTempPhotoStorageError();
        } else if (e instanceof ActivityNotFoundException) {
            profileDetailView.showCropActivityNotAvailable();
        }
    }
}
