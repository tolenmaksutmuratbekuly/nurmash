package com.nurmash.nurmash.mvp.promotion;

import android.support.annotation.StringRes;
import android.view.View;

import com.nurmash.nurmash.model.AppData;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.MvpView;

public interface PromoDetailView extends MvpView {
    void showLoadingIndicator(boolean show);

    void onPromotionDataLoaded(Promotion contest);

    void onLinkedAppDataLoaded(AppData appData);

    void showNextButton(boolean show);

    void setNextButtonText(String text);

    void setNextButtonCallback(View.OnClickListener callback);

    void openGooglePlayLink(String appLink);

    void openStepsSummaryActivity(long promoId);

    void openCompetitorListActivity(Promotion promo);

    void openTotalPrizeInfoActivity(Promotion promo);

    void openShareStepActivity(long promoId);

    void openPhotoDetailActivity(long competitorId);

    void openPastPromoDetailActivity(Promotion promo);

    String getString(@StringRes int stringRes, Object... formatArgs);

    void setNextButtonEnabled(boolean can_participate);

    void openNoParticipantsActivity();

    void timerStateBeforeStart(String timerStr, boolean isPromotionStarted);
}
