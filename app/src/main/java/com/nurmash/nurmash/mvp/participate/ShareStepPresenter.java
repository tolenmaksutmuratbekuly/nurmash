package com.nurmash.nurmash.mvp.participate;

import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.social.ShareProvider;
import com.nurmash.nurmash.data.social.facebook.FacebookProvider;
import com.nurmash.nurmash.data.social.facebook.FacebookProviderException;
import com.nurmash.nurmash.data.social.google.GoogleProviderException;
import com.nurmash.nurmash.data.social.google.GoogleShareProvider;
import com.nurmash.nurmash.data.social.twitter.TwitterProvider;
import com.nurmash.nurmash.data.social.twitter.TwitterProviderException;
import com.nurmash.nurmash.data.social.vk.VKProvider;
import com.nurmash.nurmash.data.social.vk.VKProviderException;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.FilesProvider;
import com.nurmash.nurmash.util.FileUtils;
import com.nurmash.nurmash.util.StringUtils;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import com.vk.sdk.api.VKError;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import rx.functions.Func2;
import timber.log.Timber;

import static com.nurmash.nurmash.config.NurmashConfig.FACEBOOK;
import static com.nurmash.nurmash.config.NurmashConfig.GOOGLE;
import static com.nurmash.nurmash.config.NurmashConfig.TWITTER;
import static com.nurmash.nurmash.config.NurmashConfig.VK;
import static com.nurmash.nurmash.model.ParticipationData.ALL_STEPS_DONE;
import static com.nurmash.nurmash.model.ParticipationData.LAST_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SHARE_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SURVEY_STEP;
import static com.nurmash.nurmash.model.ParticipationData.VIDEO_STEP;
import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class ShareStepPresenter implements MvpPresenter<ShareStepView> {
    private static final String TEMP_FILE_NAME = "share_temp.jpg";

    private final DataLayer dataLayer;
    private final long promoId;
    private final FacebookProvider facebookProvider;
    private final TwitterProvider twitterProvider;
    private final GoogleShareProvider googleShareProvider;
    private final VKProvider vkProvider;
    private Uri photoFileUri;
    private ShareStepView shareStepView;
    private FilesProvider filesProvider;
    private ParticipationData participationData;
    private String photoHash;
    private File tempPhotoFile;
    private Subscription participationDataLoadSubscription;
    private Subscription uploadResultSubscription;
    private Subscription uploadProgressSubscription;
    private Subscription shareSubscription;
    private Subscription commitLastStepSubscription;
    private ErrorHandler errorHandler;

    public ShareStepPresenter(DataLayer dataLayer, long promoId, Uri photoFileUri,
                              FacebookProvider facebookProvider,
                              TwitterProvider twitterProvider,
                              GoogleShareProvider googleShareProvider,
                              VKProvider vkProvider) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
        this.photoFileUri = photoFileUri;
        this.facebookProvider = facebookProvider;
        this.twitterProvider = twitterProvider;
        this.googleShareProvider = googleShareProvider;
        this.vkProvider = vkProvider;
    }

    @Override
    public void attachView(ShareStepView view) {
        shareStepView = view;
        if (photoFileUri != null) {
            shareStepView.showPhotoPreview(photoFileUri.toString());
            if (photoHash == null) uploadPhoto(photoFileUri);
        }
    }

    @Override
    public void detachView() {
        if (participationDataLoadSubscription != null) {
            participationDataLoadSubscription.unsubscribe();
            participationDataLoadSubscription = null;
        }
        if (uploadResultSubscription != null) {
            uploadResultSubscription.unsubscribe();
            uploadResultSubscription = null;
        }
        if (uploadProgressSubscription != null) {
            uploadProgressSubscription.unsubscribe();
            uploadProgressSubscription = null;
        }
        if (shareSubscription != null) {
            shareSubscription.unsubscribe();
            shareSubscription = null;
        }
        if (commitLastStepSubscription != null) {
            commitLastStepSubscription.unsubscribe();
            commitLastStepSubscription = null;
        }
        FileUtils.deleteFileQuietly(tempPhotoFile);
        shareStepView = null;
    }

    public void saveInstanceState(Bundle outState) {
        outState.putString("PHOTO_HASH", photoHash);
    }

    public void restoreInstanceState(Bundle state) {
        if (state != null) photoHash = state.getString("PHOTO_HASH");
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setFilesProvider(FilesProvider filesProvider) {
        this.filesProvider = filesProvider;
    }

    public void onTwitterShareClick() {
        share(TWITTER, twitterProvider, getLocalPhotoFileUri());
    }

    public void onFacebookShareClick() {
        Observable<Uri> photoUrlObs = Observable.fromCallable(new Callable<Uri>() {
            @Override
            public Uri call() {
                return Uri.parse(PhotoUrl.orig(photoHash));
            }
        });
        share(FACEBOOK, facebookProvider, photoUrlObs);
    }

    public void onVKShareClick() {
        share(VK, vkProvider, getLocalPhotoFileUri());
    }

    public void onGoogleShareClick() {
        share(GOOGLE, googleShareProvider, getLocalPhotoFileUri());
    }

    public void onNextStepClick() {
        if (shareStepView == null) return;

        switch (participationData.getNextStep()) {
            case VIDEO_STEP:
                throw new IllegalStateException("Video step cannot be the next step after sharing step.");
            case SHARE_STEP:
                throw new IllegalStateException("Sharing step cannot be the next step after sharing step.");
            case SURVEY_STEP:
                shareStepView.openSurveyStepActivity(promoId);
                shareStepView.finish();
                break;
            case LAST_STEP:
            case ALL_STEPS_DONE:
                finalizeSteps();
                break;
        }
    }

    public void reloadParticipationData() {
        if (isShareInProgress()) return;
        if (shareStepView != null) shareStepView.showLoadingIndicator(true);
        if (participationDataLoadSubscription != null)
            participationDataLoadSubscription.unsubscribe();
        participationDataLoadSubscription = dataLayer.getParticipationData(promoId, true)
                .observeOn(mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        setParticipationData(data);
                    }

                    @Override
                    public void onCompleted() {
                        if (shareStepView != null) shareStepView.showLoadingIndicator(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (shareStepView != null) shareStepView.showLoadingIndicator(false);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }

    public void retryPhotoUpload() {
        uploadPhoto(photoFileUri);
    }

    private void uploadPhoto(Uri photoFileUri) {
        if (photoHash != null && !photoHash.isEmpty()) {
            // We already have photo hash. No need to upload anything.
            return;
        }
        if (isActive(uploadResultSubscription)) {
            // Upload is already in progress.
            return;
        }
        if (shareStepView != null) {
            shareStepView.showPhotoUploadIndicator(true);
            shareStepView.onPhotoUploadProgress(0);
        }
        final ObservableTypedFile observableFile = ObservableTypedFile.withPhotoUri(photoFileUri);
        uploadProgressSubscription = observableFile.getProgressObservable()
                .sample(100, TimeUnit.MILLISECONDS)
                .onErrorResumeNext(Observable.<Integer>empty())
                .observeOn(mainThread())
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onNext(Integer progress) {
                        if (shareStepView != null) shareStepView.onPhotoUploadProgress(progress);
                    }

                    @Override
                    public void onCompleted() {
                        if (shareStepView != null) shareStepView.onPhotoUploadProgress(100);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
        uploadResultSubscription = dataLayer.uploadPhoto(observableFile)
                .observeOn(mainThread())
                .subscribe(new Subscriber<FileUploadResult>() {
                    @Override
                    public void onNext(FileUploadResult result) {
                        photoHash = result.hash;
                    }

                    @Override
                    public void onCompleted() {
                        if (shareStepView != null) shareStepView.showPhotoUploadIndicator(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (shareStepView == null) return;
                        shareStepView.showPhotoUploadIndicator(false);
                        shareStepView.onPhotoUploadFailed();
                    }
                });
    }

    private void setParticipationData(ParticipationData data) {
        participationData = data;
        if (photoHash == null) photoHash = data.getCompetitorPhotoHash();
        if (shareStepView != null) {
            if (participationData.hasSharedOn(FACEBOOK)) shareStepView.setFacebookShareCompleted();
            if (participationData.hasSharedOn(TWITTER)) shareStepView.setTwitterShareCompleted();
            if (participationData.hasSharedOn(GOOGLE)) shareStepView.setGoogleShareCompleted();
            if (participationData.hasSharedOn(VK)) shareStepView.setVKShareCompleted();
            if (photoFileUri == null && photoHash != null && !photoHash.isEmpty()) {
                shareStepView.showPhotoPreview(PhotoUrl.xl(photoHash));
            }
        }
    }

    private Observable<Competitor> getCompetitorObs() {
        Competitor competitor = participationData.getCompetitor();
        if (competitor != null) {
            return Observable.just(competitor);
        }
        return dataLayer.commitInitialStep(participationData)
                .observeOn(mainThread())
                .map(new Func1<ParticipationData, Competitor>() {
                    @Override
                    public Competitor call(ParticipationData data) {
                        setParticipationData(data);
                        return data.getCompetitor();
                    }
                });
    }

    private Observable<Uri> getLocalPhotoFileUri() {
        if (photoFileUri != null) {
            return Observable.just(photoFileUri);
        }
        if (photoHash == null) {
            return Observable.error(new NullPointerException("photoHash == null"));
        }
        if (tempPhotoFile != null) {
            FileUtils.deleteFileQuietly(tempPhotoFile);
        }
        tempPhotoFile = new File(filesProvider.getExternalCacheDir(), TEMP_FILE_NAME);
        return dataLayer.downloadPhotoToFile(PhotoUrl.ORIGINAL, photoHash, tempPhotoFile)
                .observeOn(mainThread())
                .map(new Func1<File, Uri>() {
                    @Override
                    public Uri call(File file) {
                        photoFileUri = Uri.fromFile(file);
                        return photoFileUri;
                    }
                });
    }

    private boolean isShareInProgress() {
        return shareSubscription != null && !shareSubscription.isUnsubscribed();
    }

    private void share(final String socialNetwork, final ShareProvider sharer, Observable<Uri> photoUrlObs) {
        if (shareStepView == null) return;
        if (photoHash == null) {
            shareStepView.showPhotoNotUploadedYetMessage();
            return;
        }
        shareStepView.showCommitProgressIndicator(true);
        final String comment = StringUtils.trim(shareStepView.getComment());
        if (shareSubscription != null) shareSubscription.unsubscribe();
        Observable<Competitor> competitorObs = getCompetitorObs();
        shareSubscription = Observable.combineLatest(competitorObs, photoUrlObs,
                new Func2<Competitor, Uri, Pair<Uri, Uri>>() {
                    @Override
                    public Pair<Uri, Uri> call(Competitor competitor, Uri photoUrl) {
                        return Pair.create(Uri.parse(competitor.getVoteUrl(socialNetwork)), photoUrl);
                    }
                })
                .flatMap(new Func1<Pair<Uri, Uri>, Observable<Object>>() {
                    @Override
                    public Observable<Object> call(Pair<Uri, Uri> shareData) {
                        return sharer.share(shareData.first, shareData.second, comment);
                    }
                })
                .flatMap(new Func1<Object, Observable<ParticipationData>>() {
                    @Override
                    public Observable<ParticipationData> call(Object o) {
                        return dataLayer.commitShareStep(participationData, socialNetwork, photoHash, comment);
                    }
                })
                .observeOn(mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        if (shareStepView != null) {
                            shareStepView.showCommitProgressIndicator(false);
                        }
                        setParticipationData(data);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (shareStepView != null) {
                            shareStepView.showCommitProgressIndicator(false);
                        }
                        handleSharingError(e);
                    }
                });
    }

    private void handleSharingError(Throwable e) {
        Timber.e(new SubscriberErrorWrapper(e), null);

        if (e instanceof TwitterProviderException) {
            switch (((TwitterProviderException) e).errorCode) {
                case TWITTER_APP_NOT_AVAILABLE:
                    if (shareStepView != null) {
                        shareStepView.onTwitterAppNotAvailable();
                    }
                    return;

                case CANCELED:
                    // Don't show error if user canceled.
                    return;
            }
        }

        if (e instanceof FacebookProviderException) {
            switch (((FacebookProviderException) e).errorCode) {
                case FACEBOOK_SDK_ERROR:
                case RECEIVED_NULL_POST_ID:
                    if (shareStepView != null) {
                        shareStepView.onFacebookShareFailed();
                    }
                    return;

                case CANCELED:
                    // Don't show error if user canceled.
                    return;
            }
        }

        if (e instanceof GoogleProviderException) {
            switch (((GoogleProviderException) e).errorCode) {
                case GOOGLE_SDK_ERROR:
                    if (e.getCause() instanceof ActivityNotFoundException) {
                        if (shareStepView != null) {
                            shareStepView.onGooglePlusAppNotAvailable();
                        }
                        return;
                    }
                    break;

                case CANCELED:
                    // Don't show error if user canceled.
                    return;
            }
        }

        if (e instanceof VKProviderException) {
            if (((VKProviderException) e).getErrorCode() == VKError.VK_CANCELED) {
                // Don't show error if user canceled.
                return;
            }
        }

        if (errorHandler != null) {
            errorHandler.handleError(e);
        }
    }

    private void finalizeSteps() {
        if (commitLastStepSubscription != null && !commitLastStepSubscription.isUnsubscribed())
            return;
        if (shareStepView != null) shareStepView.showCommitProgressIndicator(true);
        // If the last step is already committed this observable will just emit onNext immediately,
        // without making any network calls.
        commitLastStepSubscription = dataLayer.commitLastStep(participationData)
                .observeOn(mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        if (shareStepView != null) {
                            shareStepView.openMyPhotoActivity(data.getCompetitorId());
                            shareStepView.finish();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (shareStepView != null) shareStepView.showCommitProgressIndicator(false);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }
}
