package com.nurmash.nurmash.mvp.prizes.verification;

import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.mvp.MvpView;

public interface DocsVerifyResultView extends MvpView {
    void showLoadingIndicator(boolean show);

    void profileVerificationStatus(ProfileVerifyStatus profileVerifyStatus);
}