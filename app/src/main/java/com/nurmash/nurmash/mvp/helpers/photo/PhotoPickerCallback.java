package com.nurmash.nurmash.mvp.helpers.photo;

import android.net.Uri;
import android.support.annotation.NonNull;

public interface PhotoPickerCallback {
    void onCroppedPhotoPicked(@NonNull Uri photoUri);

    void onPhotoPickCanceled();

    void onPhotoPickError(Throwable e);
}
