package com.nurmash.nurmash.mvp.participate;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;

public interface ChecklistItem {
    @StringRes
    int getTitleRes();

    @StyleRes
    int getTextAppearanceRes();

    @DrawableRes
    int getIconRes();

    @DrawableRes
    int getCheckMarkRes();
}
