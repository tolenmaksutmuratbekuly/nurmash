package com.nurmash.nurmash.mvp.prizes.verification;

import android.net.Uri;
import android.support.annotation.IntRange;

import com.nurmash.nurmash.mvp.MvpView;

public interface PhotoDocPassportView extends MvpView {
    boolean openCameraApp(Uri tempPhotoUri);

    void openGalleryApp();

    boolean openCropApp(Uri inFileUri, Uri outFileUri);

    void showTempStorageError();

    void showCameraAppError();

    void showCropAppError();

    void showPhotoNotSelectedError();

    void onPhotoSuccessUploaded();

    void onPhotoUploadFailed();

    void showPhotoUploadIndicator(boolean show);

    void onPhotoUploadProgress(@IntRange(from = 0, to = 100) int progress);

    void showPhotoPreview(String photoUrl);

}

