package com.nurmash.nurmash.mvp.prizes.discount;

import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface CurrentDiscountsView extends MvpView {
    void showPhotosLoadingIndicator(boolean show);

    void setPrizeItems(List<PrizeItem> prizeItems);

    void addPrizeItems(List<PrizeItem> prizeItems);

    void showPhotoListPlaceholder(@StringRes int textRes, @StringRes int btnRes);

    void openPhotoDetailActivity(long competitorId);

    void finishActivity();
}