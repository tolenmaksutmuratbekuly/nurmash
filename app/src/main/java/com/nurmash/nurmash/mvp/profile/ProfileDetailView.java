package com.nurmash.nurmash.mvp.profile;

import android.net.Uri;
import android.support.annotation.StringRes;

import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

public interface ProfileDetailView extends MvpView {
    void showProfileLoadingIndicator(boolean show);

    void onProfileLoaded(User user);

    void openSettingsActivity();

    void inviteAvailableLoaded(InviteAvailable inviteAvailable);

    void showPhotoError(@StringRes int errorRes, boolean scrollToView);

    void showSelectedPhoto(Uri photoFileUri);

    void showPhotoUploadingIndicator(boolean show);

    void setPhotoUploadProgress(int progress);

    void showPhotoUploadError();

    void showTempPhotoStorageError();

    void showCropActivityNotAvailable();

}
