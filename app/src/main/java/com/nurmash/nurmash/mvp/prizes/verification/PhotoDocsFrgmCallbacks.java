package com.nurmash.nurmash.mvp.prizes.verification;

import android.support.annotation.IntRange;

public interface PhotoDocsFrgmCallbacks {
    void onPhotoUploadFailed();

    void showPhotoUploadIndicator(boolean show);

    void onPhotoUploadProgress(@IntRange(from = 0, to = 100) int progress);
}
