package com.nurmash.nurmash.mvp.photo;

import com.nurmash.nurmash.model.json.LikesItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface CompetitorLikesView extends MvpView {
    void showLoadingIndicator(boolean show);

    void setLikeItems(List<LikesItem> items);

    void addLikeItems(List<LikesItem> items);

    void openUserProfileActivity(User user);

}

