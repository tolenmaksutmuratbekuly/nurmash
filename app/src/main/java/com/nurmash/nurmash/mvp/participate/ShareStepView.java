package com.nurmash.nurmash.mvp.participate;

import android.support.annotation.IntRange;

import com.nurmash.nurmash.mvp.MvpView;

public interface ShareStepView extends MvpView {
    void showPhotoPreview(String photoUrl);

    void showLoadingIndicator(boolean show);

    void showPhotoUploadIndicator(boolean show);

    void onPhotoUploadProgress(@IntRange(from = 0, to = 100) int progress);

    void showPhotoNotUploadedYetMessage();

    void onPhotoUploadFailed();

    String getComment();

    void showCommitProgressIndicator(boolean show);

    void setTwitterShareCompleted();

    void setFacebookShareCompleted();

    void setVKShareCompleted();

    void setGoogleShareCompleted();

    void onFacebookShareFailed();

    void onTwitterAppNotAvailable();

    void onGooglePlusAppNotAvailable();

    void openSurveyStepActivity(long promoId);

    void openMyPhotoActivity(long competitorId);

    void finish();
}
