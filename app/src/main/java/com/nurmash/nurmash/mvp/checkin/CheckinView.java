package com.nurmash.nurmash.mvp.checkin;

import com.nurmash.nurmash.mvp.MvpView;

public interface CheckinView extends MvpView {
    void showLoadingIndicator(boolean show);

    void openProfileEditActivity();
}
