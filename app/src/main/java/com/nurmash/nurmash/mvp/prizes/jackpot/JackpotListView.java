package com.nurmash.nurmash.mvp.prizes.jackpot;

import com.nurmash.nurmash.model.json.JackpotList;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface JackpotListView extends MvpView {
    void showLoadingIndicator(boolean show);

    void showJackpotTitle(String title);

    void showJackpotMoneyList(String jackpotAmountStr);

    void showJackpotTimeLeft(String timeLeftStr);

    void viewHoldersState(boolean display_winners);

    void setWinners(List<JackpotList.Winners> winners);
}
