package com.nurmash.nurmash.mvp.prizes.verification;

import android.net.Uri;
import android.os.Bundle;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.exception.SDCardNotMountedException;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.CameraUtils;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PhotoDocPassportPresenter implements MvpPresenter<PhotoDocPassportView> {
    private String TEMP_PHOTO_URI = "TEMP_PHOTO_URI";
    private String TEMP_CROP_URI = "TEMP_CROP_URI";
    private final DataLayer dataLayer;
    private Subscription uploadResultSubscription;
    private Subscription uploadProgressSubscription;
    private ErrorHandler errorHandler;
    private PhotoDocPassportView photoDocumentPassportView;
    private Uri tempPhotoUri, tempCropUri;

    public PhotoDocPassportPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void restoreInstanceState(Bundle state) {
        if (state != null) {
            if (state.containsKey(TEMP_PHOTO_URI))
                tempPhotoUri = Uri.parse(state.getString(TEMP_PHOTO_URI));
            if (state.containsKey(TEMP_CROP_URI))
                tempCropUri = Uri.parse(state.getString(TEMP_CROP_URI));
        }
    }

    public void saveInstanceState(Bundle outState) {
        if (tempPhotoUri != null) outState.putString(TEMP_PHOTO_URI, tempPhotoUri.toString());
        if (tempCropUri != null) outState.putString(TEMP_CROP_URI, tempCropUri.toString());
    }

    @Override
    public void attachView(PhotoDocPassportView view) {
        photoDocumentPassportView = view;
    }

    @Override
    public void detachView() {
        photoDocumentPassportView = null;
        if (uploadResultSubscription != null) {
            uploadResultSubscription.unsubscribe();
            uploadResultSubscription = null;
        }

        if (uploadProgressSubscription != null) {
            uploadProgressSubscription.unsubscribe();
            uploadProgressSubscription = null;
        }
    }

    public void onCameraOptionSelected() {
        if (photoDocumentPassportView == null) return;

        tempPhotoUri = createTempFile();
        if (tempPhotoUri == null) {
            photoDocumentPassportView.showTempStorageError();
        } else if (!photoDocumentPassportView.openCameraApp(tempPhotoUri)) {
            photoDocumentPassportView.showCameraAppError();
            deleteFile(tempPhotoUri);
            tempPhotoUri = null;
        }
    }

    public void onGalleryOptionSelected() {
        if (photoDocumentPassportView != null) photoDocumentPassportView.openGalleryApp();
    }

    public void onCameraResult(boolean success) {
        if (tempPhotoUri == null) return;
        if (success) {
            openCropApp(tempPhotoUri);
        } else {
            deleteFile(tempPhotoUri);
            tempPhotoUri = null;
        }
    }

    public void onGalleryResult(Uri photoUri) {
        if (photoUri != null) openCropApp(photoUri);
    }

    public void onCropResult(boolean success) {
        if (tempCropUri == null) return;
        if (success) {
            if (photoDocumentPassportView != null) {
                photoDocumentPassportView.showPhotoPreview(tempCropUri.toString());
            }
        } else {
            deleteFile(tempCropUri);
            tempCropUri = null;
        }
    }

    private boolean openCropApp(Uri photoUri) {
        if (photoUri == null || photoDocumentPassportView == null) return false;
        if (tempCropUri != null) deleteFile(tempCropUri);
        tempCropUri = createTempFile();
        if (!photoDocumentPassportView.openCropApp(photoUri, tempCropUri)) {
            photoDocumentPassportView.showCropAppError();
            deleteFile(tempCropUri);
            return false;
        }
        return true;
    }

    private static Uri createTempFile() {
        try {
            return CameraUtils.getTemporaryImageFileUri();
        } catch (IOException | SDCardNotMountedException e) {
            Timber.e(e, null);
            return null;
        }
    }

    private static void deleteFile(Uri fileUri) {
        //noinspection ResultOfMethodCallIgnored
        new File(fileUri.getPath()).delete();
    }

    public void sendDocumentClick() {
        uploadPhoto(tempCropUri);
    }

    public void retryPhotoUpload() {
        uploadPhoto(tempCropUri);
    }

    private void uploadPhoto(Uri photoFileUri) {
        if (photoFileUri != null) {
            if (isActive(uploadResultSubscription)) {
                // Upload is already in progress.
                return;
            }
            if (photoDocumentPassportView != null) {
                photoDocumentPassportView.showPhotoUploadIndicator(true);
                photoDocumentPassportView.onPhotoUploadProgress(0);
            }
            final ObservableTypedFile observableFile = ObservableTypedFile.withPhotoUri(photoFileUri);
            uploadProgressSubscription = observableFile.getProgressObservable()
                    .sample(100, TimeUnit.MILLISECONDS)
                    .onErrorResumeNext(Observable.<Integer>empty())
                    .observeOn(mainThread())
                    .subscribe(new Subscriber<Integer>() {
                        @Override
                        public void onNext(Integer progress) {
                            if (photoDocumentPassportView != null)
                                photoDocumentPassportView.onPhotoUploadProgress(progress);
                        }

                        @Override
                        public void onCompleted() {
                            if (photoDocumentPassportView != null)
                                photoDocumentPassportView.onPhotoUploadProgress(100);
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (errorHandler != null) {
                                errorHandler.handleError(e);
                            }
                        }
                    });
            uploadResultSubscription = dataLayer.profileVerify(observableFile, null, true)
                    .observeOn(mainThread())
                    .subscribe(new Subscriber<FileUploadResult>() {
                        @Override
                        public void onNext(FileUploadResult result) {

                        }

                        @Override
                        public void onCompleted() {
                            if (photoDocumentPassportView != null) {
                                photoDocumentPassportView.showPhotoUploadIndicator(false);
                                photoDocumentPassportView.onPhotoSuccessUploaded();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(new SubscriberErrorWrapper(e), null);
                            if (photoDocumentPassportView == null) return;
                            photoDocumentPassportView.showPhotoUploadIndicator(false);
                            photoDocumentPassportView.onPhotoUploadFailed();
                            if (errorHandler != null) {
                                errorHandler.handleError(e);
                            }
                        }
                    });
        } else {
            if (photoDocumentPassportView != null) {
                photoDocumentPassportView.showPhotoNotSelectedError();
            }
        }
    }
}

