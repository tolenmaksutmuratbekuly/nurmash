package com.nurmash.nurmash.mvp.participate;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.model.ParticipationData.ALL_STEPS_DONE;
import static com.nurmash.nurmash.model.ParticipationData.LAST_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SHARE_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SURVEY_STEP;
import static com.nurmash.nurmash.model.ParticipationData.VIDEO_STEP;

public class VideoStepPresenter implements MvpPresenter<VideoStepView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private VideoStepView videoStepView;
    private ParticipationData participationData;
    private Subscription participationDataLoadSubscription;
    private Subscription commitVideoStepSubscription;
    private Subscription commitLastStepSubscription;
    private ErrorHandler errorHandler;

    public VideoStepPresenter(DataLayer dataLayer, long promoId) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
    }

    @Override
    public void attachView(VideoStepView view) {
        videoStepView = view;
        loadParticipationData();
    }

    @Override
    public void detachView() {
        if (participationDataLoadSubscription != null) {
            participationDataLoadSubscription.unsubscribe();
            participationDataLoadSubscription = null;
        }
        if (commitVideoStepSubscription != null) {
            commitVideoStepSubscription.unsubscribe();
            commitVideoStepSubscription = null;
        }
        if (commitLastStepSubscription != null) {
            commitLastStepSubscription.unsubscribe();
            commitLastStepSubscription = null;
        }
        videoStepView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onVideoEnded() {
        if (participationData.getPromoState() == Promotion.STATE_RUNNING && !participationData.isStepComplete(VIDEO_STEP)) {
            commitVideoStep();
        }
    }

    public void onPlaybackError(Exception e) {
        Timber.e(e, "promoId = %d, videoId = %s", promoId, getVideoId());
        if (videoStepView != null) videoStepView.onPlaybackError();
    }

    private String getVideoId() {
        return participationData != null ? participationData.getPromoVideoId() : null;
    }

    private void loadParticipationData() {
        if (videoStepView != null) videoStepView.showLoadingIndicator(true);
        if (participationDataLoadSubscription != null)
            participationDataLoadSubscription.unsubscribe();
        participationDataLoadSubscription = dataLayer.getParticipationData(promoId, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        participationData = data;
                        if (videoStepView != null) {
                            videoStepView.showLoadingIndicator(false);
                            videoStepView.loadVideo(getVideoId());
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }

    private void commitVideoStep() {
        if (videoStepView != null) videoStepView.showLoadingIndicator(true);
        if (commitVideoStepSubscription != null) commitVideoStepSubscription.unsubscribe();
        commitVideoStepSubscription = dataLayer.commitVideoStep(participationData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        participationData = data;
                        openNextStepActivity();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }

    private void openNextStepActivity() {
        if (videoStepView == null) return;
        switch (participationData.getNextStep()) {
            case VIDEO_STEP:
                throw new IllegalStateException("Video step cannot be next step after video step.");
            case SHARE_STEP:
                videoStepView.openShareStepActivity(promoId);
                videoStepView.finish();
                break;
            case SURVEY_STEP:
                videoStepView.openSurveyStepActivity(promoId);
                videoStepView.finish();
                break;
            case LAST_STEP:
            case ALL_STEPS_DONE:
                finalizeSteps();
                break;
        }
    }

    private void finalizeSteps() {
        if (commitLastStepSubscription != null && !commitLastStepSubscription.isUnsubscribed())
            return;
        if (videoStepView != null) videoStepView.showLoadingIndicator(true);
        commitLastStepSubscription = dataLayer.commitLastStep(participationData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        participationData = data;
                        if (videoStepView != null) videoStepView.finish();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (videoStepView != null) videoStepView.showLoadingIndicator(false);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }
}
