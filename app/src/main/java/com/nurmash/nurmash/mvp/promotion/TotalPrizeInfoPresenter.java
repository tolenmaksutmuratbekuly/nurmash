package com.nurmash.nurmash.mvp.promotion;

import android.view.View;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.TimeLeft;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class TotalPrizeInfoPresenter implements MvpPresenter<TotalPrizeInfoView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private Promotion promo;
    private ParticipationData participationData;
    private TotalPrizeInfoView promoDetailView;
    private TimeLeft startTimeLeft;
    private Subscription participationDataLoadSubscription;
    private Subscription timeLeftUpdateSubscription;
    private StringsProvider strings;
    private ErrorHandler errorHandler;
    private boolean isLoadingData;

    public TotalPrizeInfoPresenter(DataLayer dataLayer, Promotion promo) {
        this.dataLayer = dataLayer;
        this.promo = promo;
        this.promoId = promo.id;
    }

    @Override
    public void attachView(TotalPrizeInfoView view) {
        promoDetailView = view;
        updateNextButtonView();
        loadParticipationData(false);
    }

    @Override
    public void detachView() {
        if (participationDataLoadSubscription != null) {
            participationDataLoadSubscription.unsubscribe();
            participationDataLoadSubscription = null;
        }
        if (timeLeftUpdateSubscription != null) {
            timeLeftUpdateSubscription.unsubscribe();
            timeLeftUpdateSubscription = null;
        }
        promoDetailView = null;
    }

    public void setStringsProvider(StringsProvider strings) {
        this.strings = strings;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onNextClick() {
        if (promoDetailView != null) promoDetailView.openStepsSummaryActivity(promoId);
    }

    private void setLoadingData(boolean isLoading) {
        isLoadingData = isLoading;
        if (promoDetailView != null) {
            promoDetailView.showLoadingIndicator(participationData == null && isLoading);
        }
    }

    private void loadParticipationData(boolean useCache) {
        if (isLoadingData) {
            return;
        }
        setLoadingData(true);
        participationDataLoadSubscription = dataLayer.getParticipationData(promoId, useCache)
                .observeOn(mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        setLoadingData(false);
                        setParticipationData(data);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingData(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void setParticipationData(ParticipationData data) {
        participationData = data;
        promo = data.getPromotion();
        updateNextButtonView();
    }

    private void updateNextButtonView() {
        if (promoDetailView == null) {
            return;
        }
        if (participationData == null) {
            promoDetailView.showNextButton(false);
            return;
        }
        if (timeLeftUpdateSubscription != null) {
            timeLeftUpdateSubscription.unsubscribe();
        }
        promoDetailView.showNextButton(true);
        switch (participationData.getPromoState()) {
            case Promotion.STATE_UNKNOWN:
                promoDetailView.setNextButtonText(strings.getString(R.string.action_accept_terms));
                promoDetailView.setNextButtonCallback(null);
                break;
            case Promotion.STATE_BEFORE_START:
                startTimeLeft = new TimeLeft(promo.start_date);
                promoDetailView.setNextButtonText(formatTimeLeft(startTimeLeft));
                promoDetailView.setNextButtonCallback(null);
                scheduleTimeLeftUpdate();
                break;
            case Promotion.STATE_RUNNING:
                Competitor competitor = participationData.getCompetitor();
                if (competitor == null) {
                    promoDetailView.setNextButtonText(strings.getString(R.string.action_accept_terms));
                    promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            participate();
                        }
                    });
                } else if (!competitor.hasCompletedAllSteps()) {
                    promoDetailView.setNextButtonText(strings.getString(R.string.action_continue_participation));
                    promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            participate();
                        }
                    });
                } else if (competitor.isDisqualified()) {
                    promoDetailView.setNextButtonText(strings.getString(R.string.info_text_you_are_disqualified));
                    promoDetailView.setNextButtonCallback(null);
                } else if (promo.isPhotoSharingPromo()) {
                    if (competitor.hasSharedOnAllNetworks()) {
                        promoDetailView.setNextButtonText(strings.getString(R.string.info_text_all_steps_completed));
                        promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                seeMyPhoto();
                            }
                        });
                    } else {
                        promoDetailView.setNextButtonText(strings.getString(R.string.action_share_more));
                        promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                shareMore();
                            }
                        });
                    }
                } else {
                    promoDetailView.setNextButtonText(strings.getString(R.string.info_text_all_steps_completed));
                    promoDetailView.setNextButtonCallback(null);
                }
                break;
            case Promotion.STATE_ENDED_RECENTLY:
            case Promotion.STATE_ENDED:
                promoDetailView.setNextButtonText(strings.getString(R.string.info_text_promo_ended));
                promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        seeWinners();
                    }
                });
                break;
        }
    }

    public String formatTimeLeft(TimeLeft timeLeft) {
        StringBuilder builder = new StringBuilder(64);
        builder.append(strings.getString(R.string.info_text_promo_starts_in));
        if (timeLeft.getDays() > 0) {
            builder.append(' ');
            builder.append(strings.getQuantityString(R.plurals.time_left_days_format,
                    timeLeft.getDays(), timeLeft.getDays()));
        }
        if (timeLeft.getHours() > 0 || timeLeft.getDays() > 0) {
            builder.append(' ');
            builder.append(strings.getString(R.string.time_left_hours_format, timeLeft.getHours()));
        }
        if (timeLeft.getDays() == 0 && (timeLeft.getHours() > 0 || timeLeft.getMinutes() > 0)) {
            builder.append(' ');
            builder.append(strings.getString(R.string.time_left_minutes_format, timeLeft.getMinutes()));
        }
        if (timeLeft.getDays() == 0 && (timeLeft.getHours() > 0 || timeLeft.getMinutes() > 0 || timeLeft.getSeconds() > 0)) {
            builder.append(' ');
            builder.append(strings.getString(R.string.time_left_seconds_format, timeLeft.getSeconds()));
        }
        return builder.toString();
    }

    private void scheduleTimeLeftUpdate() {
        if (timeLeftUpdateSubscription != null) {
            timeLeftUpdateSubscription.unsubscribe();
        }
        timeLeftUpdateSubscription = Observable.interval(1, 1, TimeUnit.SECONDS, Schedulers.newThread())
                .onBackpressureLatest()
                .observeOn(mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onNext(Long ignored) {
                        startTimeLeft.update();
                        if (promoDetailView != null) {
                            promoDetailView.setNextButtonText(formatTimeLeft(startTimeLeft));
                        }
                        if (startTimeLeft.isNoTimeLeft()) {
                            loadParticipationData(false);
                            unsubscribe();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }
                });
    }

    private void participate() {
        if (promoDetailView != null) {
            promoDetailView.openStepsSummaryActivity(promoId);
        }
    }

    private void shareMore() {
        if (promoDetailView != null) {
            promoDetailView.openShareStepActivity(promoId);
        }
    }

    private void seeMyPhoto() {
        if (promoDetailView != null) {
            promoDetailView.openPhotoDetailActivity(participationData.getCompetitorId());
        }
    }

    private void seeWinners() {
        if (promoDetailView != null) {
            promoDetailView.openPastPromoDetailActivity(promo);
        }
    }
}
