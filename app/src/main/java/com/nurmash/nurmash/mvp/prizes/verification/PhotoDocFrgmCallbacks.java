package com.nurmash.nurmash.mvp.prizes.verification;

public interface PhotoDocFrgmCallbacks {
    void onIDCardClick();

    void onPassportClick();

}
