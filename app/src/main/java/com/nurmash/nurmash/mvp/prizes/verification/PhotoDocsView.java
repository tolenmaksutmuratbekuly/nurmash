package com.nurmash.nurmash.mvp.prizes.verification;

import com.nurmash.nurmash.mvp.MvpView;

public interface PhotoDocsView extends MvpView {
    void onIDCardButtonClick();

    void onPassportButtonClick();

    void onBackEventClick();

}