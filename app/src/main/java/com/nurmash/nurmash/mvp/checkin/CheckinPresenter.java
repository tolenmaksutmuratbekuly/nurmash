package com.nurmash.nurmash.mvp.checkin;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.UserPrizesData;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class CheckinPresenter implements MvpPresenter<CheckinView> {
    private final DataLayer dataLayer;
    private CheckinView checkinView;
    private StringsProvider strings;
    private Subscription prizesLoadSubscription;
    private Subscription checkProfileSubscription;

    public CheckinPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(CheckinView view) {
        checkinView = view;
        checkinView.showLoadingIndicator(isActive(prizesLoadSubscription));
        reloadPrizeInfo();
    }

    @Override
    public void detachView() {
        checkinView = null;
    }

    public void onDestroy() {
        if (prizesLoadSubscription != null) {
            prizesLoadSubscription.unsubscribe();
            prizesLoadSubscription = null;
        }
    }

    public void onResume() {
        dataLayer.notificationsHelper().clearAllWinningsNotifications();
    }

    private void reloadPrizeInfo() {
        if (isActive(prizesLoadSubscription)) {
            return;
        }
        if (checkinView != null) {//prizesData == null &&
            checkinView.showLoadingIndicator(true);
        }

    }
}
