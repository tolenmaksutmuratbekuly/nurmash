package com.nurmash.nurmash.mvp.settings;

import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.social.AuthProvider;
import com.nurmash.nurmash.data.social.facebook.FacebookProvider;
import com.nurmash.nurmash.data.social.facebook.FacebookProviderException;
import com.nurmash.nurmash.data.social.google.GoogleAuthProvider;
import com.nurmash.nurmash.data.social.google.GoogleProviderException;
import com.nurmash.nurmash.data.social.vk.VKProvider;
import com.nurmash.nurmash.data.social.vk.VKProviderException;
import com.nurmash.nurmash.model.Gender;
import com.nurmash.nurmash.model.ProfileEditForm;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.json.UserLoad;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPicker;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPickerCallback;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.RxUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import com.vk.sdk.api.VKError;

import org.parceler.Parcels;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

// TODO: 3/3/16 Refactor this class because it is responsible for too many things: Presenting, Validation, Uploading, Linking accounts
public class ProfileEditPresenter implements MvpPresenter<ProfileEditView>, PhotoPickerCallback {
    private final String FORM_KEY = "PROFILE_EDIT_FORM";
    private final DateFormat BIRTHDATE_DISPLAY_FORMAT = new SimpleDateFormat("MMM d, yyyy", Locale.getDefault());

    private final DataLayer dataLayer;
    private final FacebookProvider facebookProvider;
    private final GoogleAuthProvider googleAuthProvider;
    private final VKProvider vkProvider;
    private final boolean registration;
    private ProfileEditForm form;
    private boolean isFacebookLinked;
    private boolean isGoogleLinked;
    private boolean isVKLinked;
    private PhotoPicker photoPicker;
    private ErrorHandler errorHandler;
    private ProfileEditView profileEditView;
    private Subscription profileLoadSubscription;
    private Subscription profileSaveSubscription;
    private Subscription photoUploadSubscription;
    private Subscription photoUploadProgressSubscription;
    private Subscription socialLinkSubscription;
    private Uri uploadPhotoUri;
    private UserLoad userTemp;

    public ProfileEditPresenter(DataLayer dataLayer,
                                FacebookProvider facebookProvider,
                                GoogleAuthProvider googleAuthProvider,
                                VKProvider vkProvider) {
        this.dataLayer = dataLayer;
        this.registration = !dataLayer.authPreferences().isUserProfileComplete();
        this.facebookProvider = facebookProvider;
        this.googleAuthProvider = googleAuthProvider;
        this.vkProvider = vkProvider;
    }

    public void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putParcelable(FORM_KEY, Parcels.wrap(form));
        }
    }

    public void onRestoreInstanceState(Bundle savedState) {
        if (savedState != null && savedState.containsKey(FORM_KEY)) {
            form = Parcels.unwrap(savedState.getParcelable(FORM_KEY));
        } else {
            form = new ProfileEditForm();
        }
    }

    @Override
    public void attachView(ProfileEditView view) {
        if (form == null) {
            throw new NullPointerException("form == null. Did you forget to call onRestoreInstanceState() ?");
        }
        profileEditView = view;
        profileEditView.showProfileLoadingIndicator(isActive(profileLoadSubscription));
        if (registration) {
            profileEditView.setTitle(R.string.label_activity_registration);
            profileEditView.setSaveButtonTitle(R.string.action_continue);
        } else {
            profileEditView.setTitle(R.string.label_activity_settings);
            profileEditView.setSaveButtonTitle(R.string.action_save);
        }
        if (isUploadingPhoto()) {
            profileEditView.showPhotoUploadOverlay(true);
            profileEditView.showPhotoUploadingIndicator(true);
        }
        updateFormViews();
        loadMyProfile();
    }

    @Override
    public void detachView() {
        if (profileLoadSubscription != null) {
            profileLoadSubscription.unsubscribe();
            profileLoadSubscription = null;
        }
        if (profileSaveSubscription != null) {
            profileSaveSubscription.unsubscribe();
            profileSaveSubscription = null;
        }
        if (socialLinkSubscription != null) {
            socialLinkSubscription.unsubscribe();
            socialLinkSubscription = null;
        }
        profileEditView = null;
    }

    public void setPhotoPicker(PhotoPicker newPicker) {
        if (newPicker == null) {
            // Dispose
            cancelPhotoUpload();
            photoPicker.setPhotoPickerCallback(null);
            photoPicker = null;
        } else {
            photoPicker = newPicker;
            photoPicker.setPhotoPickerCallback(this);
        }
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public boolean isLogoutButtonEnabled() {
        return !registration;
    }

    public void onBackButtonClick() {
        if (profileEditView != null) {
            if (registration) {
                profileEditView.logout();
            } else {
                profileEditView.finish();
            }
        }
    }

    public void onLogoutButtonClick() {
        if (profileEditView != null) {
            profileEditView.logout();
        }
    }

    public void onPhotoClick() {
        if (profileEditView == null) {
            return;
        }
        if (isUploadingPhoto()) {
            cancelPhotoUpload();
            // Restore the preview of the last successfully uploaded photo.
            profileEditView.showSelectedPhoto(form.photoHash);
            profileEditView.showPhotoUploadOverlay(false);
            profileEditView.showPhotoUploadingIndicator(false);
        } else if (photoPicker != null) {
            photoPicker.startCroppedPhotoPicker();
        }
    }

    public void onPhotoUploadRetryClick() {
        if (isUploadingPhoto()) {
            cancelPhotoUpload();
        }
        restartPhotoUpload(uploadPhotoUri);
    }

    public void onBirthdateFieldClick() {
        if (profileEditView != null) {
            if (userTemp.flags.is_verified == 1) {
                profileEditView.needToVerifyAgainMessage(form.birthdate);
            } else {
                profileEditView.showBirthdatePickerView(form.birthdate);
            }
        }
    }

    public void onCountryFieldClick() {
        if (profileEditView != null) {
            profileEditView.showCountryPickerView();
        }
    }

    public void onRegionFieldClick() {
        if (profileEditView != null && form.country != null) {
            profileEditView.showRegionPickerView(form.getCountryId());
        }
    }

    public void onCityFieldClick() {
        if (profileEditView != null && form.country != null) {
            if (form.region == null || form.getRegionId() == 0) {
                profileEditView.showCityPickerViewWithoutRegion(form.getCountryId());
            } else {
                profileEditView.showCityPickerView(form.getCountryId(), form.getRegionId());
            }
        }
    }

    public void setDisplayName(String displayName) {
        form.displayName = trim(displayName);
        if (profileEditView != null) {
            profileEditView.setDisplayName(form.displayName);
        }
        validateDisplayName(false);
    }

    public void setFirstName(String firstName) {
        form.firstName = trim(firstName);
        if (profileEditView != null) {
            profileEditView.setFirstName(form.firstName);
        }
        validateFirstName(false);
    }

    public void setLastName(String lastName) {
        form.lastName = trim(lastName);
        if (profileEditView != null) {
            profileEditView.setLastName(form.lastName);
        }
        validateLastName(false);
    }

    public void checkVerifyStatus() {
        if (profileEditView != null) {
            if (userTemp.flags.is_verified == 1) {
                profileEditView.needToVerifyAgainMessage(null);
            }
        }
    }

    public void setNativeFirstName(String nativeFirstName) {
        form.nativeFirstName = trim(nativeFirstName);
        if (profileEditView != null) {
            profileEditView.setNativeFirstName(form.nativeFirstName);
        }
        validateNativeFirstName(false);
    }

    public void setNativeLastName(String nativeLastName) {
        form.nativeLastName = trim(nativeLastName);
        if (profileEditView != null) {
            profileEditView.setNativeLastName(form.nativeLastName);
        }
        validateNativeLastName(false);
    }

    public void setEmail(String email) {
        form.email = trim(email);
        if (profileEditView != null) {
            profileEditView.setEmail(form.email);
        }
        validateEmail(false);
    }

    public void setBirthdate(Date birthdate) {
        form.birthdate = birthdate;
        if (profileEditView != null) {
            profileEditView.setBirthdate(form.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT));
        }
        validateBirthdate(false);
    }

    public void setGender(Gender gender) {
        form.gender = gender;
    }

    public void setCountry(Country country) {
        form.country = country;
        form.region = null;
        form.city = null;
        if (profileEditView != null) {
            profileEditView.setCountryTitle(form.getCountryTitle());
            profileEditView.setRegionTitle(form.getRegionTitle());
            profileEditView.setCityTitle(form.getCityTitle());
        }
    }

    public void setRegion(Region region) {
        form.region = region;
        form.city = null;
        if (profileEditView != null) {
            profileEditView.setRegionTitle(form.getRegionTitle());
            profileEditView.setCityTitle(form.getCityTitle());
        }
    }

    public void setCity(City city) {
        form.city = city;
        if (profileEditView != null) {
            profileEditView.setCityTitle(form.getCityTitle());
        }
    }

    public void onFacebookLinkClick() {
        if (isFacebookLinked) {
            return;
        }
        performLink(facebookProvider, new Func1<String, Observable<User>>() {
            @Override
            public Observable<User> call(String token) {
                return dataLayer.linkFacebookAccount(token);
            }
        });
    }

    public void onGoogleLinkClick() {
        if (isGoogleLinked) {
            return;
        }
        performLink(googleAuthProvider, new Func1<String, Observable<User>>() {
            @Override
            public Observable<User> call(String token) {
                return dataLayer.linkGoogleAccount(token);
            }
        });
    }

    public void onVKLinkClick() {
        if (isVKLinked) {
            return;
        }
        performLink(vkProvider, new Func1<String, Observable<User>>() {
            @Override
            public Observable<User> call(String token) {
                return dataLayer.linkVKAccount(token);
            }
        });
    }

    public void saveProfile() {
        boolean ok = true;
        //noinspection ConstantConditions
        ok &= validateDisplayName(ok);
        ok &= validateFirstName(ok);
        ok &= validateLastName(ok);
        ok &= validateNativeFirstName(ok);
        ok &= validateNativeLastName(ok);
        ok &= validateEmail(ok);
        ok &= validateBirthdate(ok);
        ok &= validateGender(ok);
        ok &= validateCountry(ok);
        ok &= validateRegion(ok);
        ok &= validateCity(ok);
        ok &= validatePhoto(ok);
        if (ok) {
            saveInternal();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoPickerCallback interface
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onCroppedPhotoPicked(@NonNull Uri croppedPhotoUri) {
        if (profileEditView != null) {
            profileEditView.showSelectedPhoto(croppedPhotoUri);
        }
        restartPhotoUpload(croppedPhotoUri);
    }

    @Override
    public void onPhotoPickCanceled() {
    }

    @Override
    public void onPhotoPickError(Throwable e) {
        if (profileEditView == null) {
            return;
        }
        if (e instanceof IOException) {
            profileEditView.showTempPhotoStorageError();
        } else if (e instanceof ActivityNotFoundException) {
            profileEditView.showCropActivityNotAvailable();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Private implementation
    ///////////////////////////////////////////////////////////////////////////

    private void loadMyProfile() {
        if (isActive(profileLoadSubscription)) {
            return;
        }
        if (profileEditView != null) {
            profileEditView.showProfileLoadingIndicator(true);
        }
        profileLoadSubscription = dataLayer.getMyProfile(false)
                .observeOn(mainThread())
                .subscribe(new Subscriber<UserLoad>() {
                    @Override
                    public void onNext(UserLoad user) {
                        userTemp = user;
                        setUser(user);
                        if (profileEditView != null) {
                            profileEditView.showProfileLoadingIndicator(false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (profileEditView != null) {
                            profileEditView.showProfileLoadingIndicator(false);
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void setUser(User user) {
        form.fillWithUser(user);
        updateFormViews();
        isFacebookLinked = user.hasLinkedFacebookAccount();
        isGoogleLinked = user.hasLinkedGoogleAccount();
        isVKLinked = user.hasLinkedVKAccount();
        updateSocialLinkViews();
    }

    private void updateFormViews() {
        if (profileEditView != null) {
            profileEditView.setFirstName(form.firstName);
            profileEditView.setLastName(form.lastName);
            profileEditView.setNativeFirstName(form.nativeFirstName);
            profileEditView.setNativeLastName(form.nativeLastName);
            profileEditView.setDisplayName(form.displayName);
            profileEditView.setEmail(form.email);
            profileEditView.setBirthdate(form.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT));
            profileEditView.setGender(form.gender);
            profileEditView.setCountryTitle(form.getCountryTitle());
            profileEditView.setRegionTitle(form.getRegionTitle());
            profileEditView.setCityTitle(form.getCityTitle());
            if (isUploadingPhoto()) {
                profileEditView.showSelectedPhoto(uploadPhotoUri);
            } else {
                profileEditView.showSelectedPhoto(form.photoHash);
            }
        }
    }

    private void updateSocialLinkViews() {
        if (profileEditView != null) {
            profileEditView.setFacebookLinkActive(isFacebookLinked);
            profileEditView.setGoogleLinkActive(isGoogleLinked);
            profileEditView.setVKLinkActive(isVKLinked);
        }
    }

    public void saveInternal() {
        if (isActive(profileSaveSubscription)) {
            return;
        }
        if (profileEditView != null) {
            profileEditView.showProfileSavingIndicator(true);
        }
        profileSaveSubscription = dataLayer.saveProfile(form, false)
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        setUser(user);
                        if (profileEditView != null) {
                            if (registration) {
                                profileEditView.openMainScreen();
                                profileEditView.finish();
                            } else {
                                profileEditView.updateProfileData();
                                profileEditView.finish();
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (profileEditView != null) {
                            profileEditView.showProfileSavingIndicator(false);
                        }
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private boolean validateDisplayName(boolean scrollToView) {
        int errorRes = form.validateDisplayName();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showDisplayNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateFirstName(boolean scrollToView) {
        int errorRes = form.validateFirstName();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showFirstNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateLastName(boolean scrollToView) {
        int errorRes = form.validateLastName();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showLastNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateNativeFirstName(boolean scrollToView) {
        int errorRes = form.validateNativeFirstName();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showNativeFirstNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateNativeLastName(boolean scrollToView) {
        int errorRes = form.validateNativeLastName();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showNativeLastNameError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateEmail(boolean scrollToView) {
        int errorRes = form.validateEmail();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showEmailError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateBirthdate(boolean scrollToView) {
        int errorRes = form.validateBirthdate();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showBirthdateError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateGender(boolean scrollToView) {
        int errorRes = form.validateGender();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showGenderError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateCountry(boolean scrollToView) {
        int errorRes = form.validateCountry();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showCountryError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateRegion(boolean scrollToView) {
        int errorRes = form.validateRegion();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showRegionError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validateCity(boolean scrollToView) {
        int errorRes = form.validateCity();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showCityError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    private boolean validatePhoto(boolean scrollToView) {
        if (isUploadingPhoto()) {
            profileEditView.showPhotoError(R.string.error_message_upload_photo_not_finished_yet, scrollToView);
            return false;
        }
        int errorRes = form.validatePhoto();
        if (errorRes != 0 && profileEditView != null) {
            profileEditView.showPhotoError(errorRes, scrollToView);
        }
        return errorRes == 0;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Photo upload
    ///////////////////////////////////////////////////////////////////////////

    private boolean isUploadingPhoto() {
        return isActive(photoUploadSubscription);
    }

    private void restartPhotoUpload(Uri photoUri) {
        if (photoUri == null) {
            return;
        }
        if (!photoUri.equals(uploadPhotoUri)) {
            // Dispose of the previous upload.
            cancelPhotoUpload();
        }
        uploadPhotoUri = photoUri;
        if (profileEditView != null) {
            profileEditView.showPhotoUploadOverlay(true);
            profileEditView.showPhotoUploadingIndicator(true);
            profileEditView.setPhotoUploadProgress(0);
        }
        final ObservableTypedFile observableFile = ObservableTypedFile.withPhotoUri(uploadPhotoUri);
        photoUploadProgressSubscription = observableFile.getProgressObservable()
                .sample(100, TimeUnit.MILLISECONDS, Schedulers.computation())
                .concatWith(Observable.just(100))
                .observeOn(mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer progress) {
                        if (profileEditView != null) {
                            profileEditView.setPhotoUploadProgress(progress);
                        }
                    }
                });
        photoUploadSubscription = dataLayer.uploadPhoto(observableFile)
                .observeOn(mainThread())
                .subscribe(new Subscriber<FileUploadResult>() {
                    @Override
                    public void onNext(FileUploadResult result) {
                        form.photoHash = result.hash;
                        if (photoPicker != null) {
                            photoPicker.disposeOfPhotoFile(uploadPhotoUri);
                            uploadPhotoUri = null;
                        }
                        if (profileEditView != null) {
                            profileEditView.showPhotoUploadOverlay(false);
                            profileEditView.showPhotoUploadingIndicator(false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (profileEditView != null) {
                            profileEditView.showPhotoUploadingIndicator(false);
                            profileEditView.showPhotoUploadError();
                        }
                    }
                });
    }

    private void cancelPhotoUpload() {
        if (isActive(photoUploadProgressSubscription)) {
            photoUploadProgressSubscription.unsubscribe();
            photoUploadProgressSubscription = null;
        }
        if (isActive(photoUploadSubscription)) {
            photoUploadSubscription.unsubscribe();
            photoUploadSubscription = null;
        }
        if (photoPicker != null && uploadPhotoUri != null) {
            photoPicker.disposeOfPhotoFile(uploadPhotoUri);
        }
        uploadPhotoUri = null;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Link social account
    ///////////////////////////////////////////////////////////////////////////

    private void performLink(final AuthProvider provider, Func1<String, Observable<User>> linkRequest) {
        if (RxUtils.isActive(socialLinkSubscription)) {
            return;
        }
        if (profileEditView != null) profileEditView.showLinkProgressIndicator(true);
        socialLinkSubscription = provider.getTokenForAuth()
                .flatMap(linkRequest)
                .observeOn(mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        setUser(user);
                        if (profileEditView != null) {
                            profileEditView.showLinkProgressIndicator(false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), "provider = %s", provider);

                        if (profileEditView != null)
                            profileEditView.showLinkProgressIndicator(false);

                        if (e instanceof FacebookProviderException) {
                            switch (((FacebookProviderException) e).errorCode) {
                                case FACEBOOK_SDK_ERROR:
                                    if (profileEditView != null)
                                        profileEditView.onFacebookSdkError();
                                    break;
                                case PERMISSION_NOT_GRANTED:
                                    if (profileEditView != null)
                                        profileEditView.onEmailPermissionNotGranted();
                                    break;
                            }
                        } else if (e instanceof GoogleProviderException) {
                            switch (((GoogleProviderException) e).errorCode) {
                                case GOOGLE_API_CONNECTING:
                                    if (profileEditView != null)
                                        profileEditView.onGoogleApiConnectingError();
                                    break;
                                case GOOGLE_API_NOT_CONNECTED:
                                    if (profileEditView != null)
                                        profileEditView.onGoogleApiNotConnectedError();
                                    break;
                                case GOOGLE_SDK_ERROR:
                                    if (profileEditView != null) profileEditView.onGoogleSdkError();
                                    break;
                            }
                        } else if (e instanceof VKProviderException) {
                            if (((VKProviderException) e).getErrorCode() != VKError.VK_CANCELED && profileEditView != null) {
                                profileEditView.onVKSdkError();
                            }
                        } else {
                            if (e instanceof ApiError && ((ApiError) e).hasCannotLinkExistingUserError()) {
                                profileEditView.onAccountAlreadyLinkedError();
                            } else {
                                // Delegate displaying error message to the default error handler.
                                if (errorHandler != null) errorHandler.handleError(e);
                            }
                        }
                    }
                });
    }
}
