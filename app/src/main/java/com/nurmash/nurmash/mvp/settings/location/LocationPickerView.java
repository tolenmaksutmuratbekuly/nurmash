package com.nurmash.nurmash.mvp.settings.location;

import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface LocationPickerView extends MvpView {
    void showLoadingIndicator(boolean show);

    void setItemTitles(List<String> titles);

    void finishWithResult(Object result);
}
