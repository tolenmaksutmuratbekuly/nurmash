package com.nurmash.nurmash.mvp.promotion;

import com.nurmash.nurmash.mvp.MvpView;

public interface PastPromoDetailView extends MvpView {
    void showLoadingIndicator(boolean show);

    void showTitle(String title);

    void showPhoto(String photoUrl);
}
