package com.nurmash.nurmash.mvp.profile;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.FeedItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class FeedListPresenter implements MvpPresenter<FeedListView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private final List<FeedItem> loadedItems = new ArrayList<>();
    private FeedListView feedListView;
    private ErrorHandler errorHandler;
    private Subscription feedLoadSubscription;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private boolean isLoadingItems;

    public FeedListPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    private void resetLoadedItems() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void attachView(FeedListView view) {
        feedListView = view;
        if (isFinalListEmpty()) {
            showEmptyView();
        } else {
            feedListView.setFeedItems(loadedItems);
            feedListView.showFeedLoadingIndicator(isLoadingItems);
            if (loadedItems.isEmpty()) {
                loadNextPage();
            }
        }
    }

    @Override
    public void detachView() {
        if (feedLoadSubscription != null) {
            feedLoadSubscription.unsubscribe();
            feedLoadSubscription = null;
        }
        feedListView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onResume() {
        dataLayer.notificationsHelper().clearAllPhotoLikeNotifications();
        dataLayer.notificationsHelper().clearAllPhotoCommentNotifications();
        dataLayer.notificationsHelper().clearAllCommentAnswerNotifications();
    }

    public void onRefreshAction() {
        reload();
    }

    public void onRequestMoreItems() {
        loadNextPage();
    }

    public void onItemClick(FeedItem item) {
        if (item != null && feedListView != null) {
            feedListView.openPhotoDetailActivity(item.competitor_id);
        }
    }

    public void onUserClick(User user) {
        if (feedListView != null && user != null && user.id != dataLayer.authPreferences().getUserId()) {
            feedListView.openUserProfileActivity(user);
        }
    }

    public void onEmptyPlaceholderButtonClick() {
        if (feedListView != null) {
            feedListView.finishActivity();
        }
    }

    private void setLoadingItems(boolean isLoading) {
        isLoadingItems = isLoading;
        if (feedListView != null) {
            feedListView.showFeedLoadingIndicator(isLoadingItems);
        }
    }

    private void loadNextPage() {
        if (noMoreItems || isLoadingItems) {
            return;
        }
        setLoadingItems(true);
        feedLoadSubscription = dataLayer.getMyFeed(numPagesLoaded + 1, ITEMS_PER_PAGE)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<FeedItem>>() {
                    @Override
                    public void onNext(List<FeedItem> feedItems) {
                        setLoadingItems(false);
                        onNextPageLoaded(feedItems);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void reload() {
        if (feedLoadSubscription != null) {
            feedLoadSubscription.unsubscribe();
        }
        setLoadingItems(true);
        feedLoadSubscription = dataLayer.getMyFeed(1, ITEMS_PER_PAGE)
                .observeOn(mainThread())
                .subscribe(new Subscriber<List<FeedItem>>() {
                    @Override
                    public void onNext(List<FeedItem> feedItems) {
                        setLoadingItems(false);
                        resetLoadedItems();
                        onNextPageLoaded(feedItems);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void onNextPageLoaded(List<FeedItem> feedItems) {
        if (feedItems == null || feedItems.isEmpty()) {
            noMoreItems = true;
            if (isFinalListEmpty()) {
                showEmptyView();
            }
        } else {
            numPagesLoaded = numPagesLoaded + 1;
            loadedItems.addAll(feedItems);
            if (feedListView != null) {
                if (numPagesLoaded == 1) {
                    feedListView.setFeedItems(feedItems);
                } else {
                    feedListView.addFeedItems(feedItems);
                }
            }
        }
    }

    private boolean isFinalListEmpty() {
        return noMoreItems && loadedItems.isEmpty();
    }

    private void showEmptyView() {
        if (feedListView != null) {
            feedListView.showFeedListPlaceholder(R.string.info_text_my_profile_feed_empty, R.string.action_participate);
        }
    }
}
