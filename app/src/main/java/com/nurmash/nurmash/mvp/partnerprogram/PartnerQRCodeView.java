package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.mvp.MvpView;

public interface PartnerQRCodeView extends MvpView {
    void showLoadingIndicator(boolean show);

    void inviteAvailableLoaded(InviteAvailable inviteAvailable);
}

