package com.nurmash.nurmash.mvp.photo;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Func1;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PhotoDetailPresenter implements MvpPresenter<PhotoDetailView> {
    private final DataLayer dataLayer;
    private final long competitorId;
    private Competitor competitor;
    private PhotoDetailView photoDetailView;
    private ErrorHandler errorHandler;
    private Subscription dataLoadSubscription;
    private Subscription likePhotoSubscription;
    private Subscription reportPhotoSubscription;

    public PhotoDetailPresenter(DataLayer dataLayer, long competitorId) {
        this.dataLayer = dataLayer;
        this.competitorId = competitorId;
    }

    @Override
    public void attachView(PhotoDetailView view) {
        photoDetailView = view;
        updatePhotoDetailView(competitor);
        loadCompetitorData();
    }

    @Override
    public void detachView() {
        if (dataLoadSubscription != null) {
            dataLoadSubscription.unsubscribe();
            dataLoadSubscription = null;
        }
        if (likePhotoSubscription != null) {
            likePhotoSubscription.unsubscribe();
            likePhotoSubscription = null;
        }
        if (reportPhotoSubscription != null) {
            reportPhotoSubscription.unsubscribe();
            reportPhotoSubscription = null;
        }
        photoDetailView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onResume() {
        loadCompetitorData();
        dataLayer.notificationsHelper().clearPhotoLikeNotifications(competitorId);
        dataLayer.notificationsHelper().clearPhotoCommentNotifications(competitorId);
        dataLayer.notificationsHelper().clearCommentAnswerNotifications(competitorId);

    }

    public void onLikeClick() {
        likePhoto();
    }

    public void onCommentButtonClick() {
        if (photoDetailView != null) {
            photoDetailView.openCommentListActivity(competitor, true);
        }
    }

    public void onMoreCommentsButtonClick() {
        if (photoDetailView != null) {
            photoDetailView.openCommentListActivity(competitor, false);
        }
    }

    public void onUserClick(User user) {
        if (photoDetailView != null) {
            photoDetailView.openUserProfileActivity(user);
        }
    }

    public void onOpenPromoButtonClick() {
        if (photoDetailView != null && competitor != null) {
            photoDetailView.openPromoDetailActivity(competitor.getPromoId());
        }
    }

    public void onComplaintSelected(Complaint complaint) {
        reportPhoto(complaint);
    }

    private void setCompetitor(Competitor newCompetitor) {
        competitor = newCompetitor;
        updatePhotoDetailView(competitor);
    }

    private void updatePhotoDetailView(Competitor competitor) {
        if (competitor == null || photoDetailView == null) {
            return;
        }
        photoDetailView.onCompetitorLoaded(competitor, dataLayer.authPreferences().getUserId());
    }

    private void updateCompetitorLoadingIndicator(boolean loading) {
        if (photoDetailView == null) {
            return;
        }
        photoDetailView.showLoadingIndicator(competitor == null && loading);
    }

    private void loadCompetitorData() {
        if (isActive(dataLoadSubscription)) {
            return;
        }
        updateCompetitorLoadingIndicator(true);
        dataLoadSubscription = dataLayer.getCompetitor(competitorId)
                .flatMap(new Func1<Competitor, Observable<Competitor>>() {
                    @Override
                    public Observable<Competitor> call(final Competitor c) {
                        // Fetch more detailed promotion data.
                        return dataLayer.getPromotion(c.getPromoId())
                                .map(new Func1<Promotion, Competitor>() {
                                    @Override
                                    public Competitor call(Promotion detailedPromotion) {
                                        c.promo = detailedPromotion;
                                        return c;
                                    }
                                });
                    }
                })
                .observeOn(mainThread())
                .subscribe(new Subscriber<Competitor>() {
                    @Override
                    public void onNext(Competitor c) {
                        updateCompetitorLoadingIndicator(false);
                        setCompetitor(c);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        updateCompetitorLoadingIndicator(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void likePhoto() {
        if (competitor == null || !competitor.canLike() || isActive(likePhotoSubscription)) {
            return;
        }
        likePhotoSubscription = dataLayer.likePhoto(competitor)
                .observeOn(mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        competitor.like_count += 1;
                        competitor.is_liked = true;
                        setCompetitor(competitor);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private boolean isMyPhoto() {
        return competitor != null && competitor.getUserId() == dataLayer.authPreferences().getUserId();
    }

    private void reportPhoto(Complaint complaint) {
        if (complaint == null || isMyPhoto() || competitor == null || !competitor.canReport()
                || isActive(reportPhotoSubscription)) {
            return;
        }
        if (photoDetailView != null) {
            photoDetailView.showReportLoadingIndicator(true);
        }
        reportPhotoSubscription = dataLayer.reportPhoto(competitorId, complaint)
                .observeOn(mainThread())
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        if (photoDetailView != null) {
                            photoDetailView.showReportLoadingIndicator(false);
                        }
                    }
                })
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void aVoid) {
                        competitor.has_user_complaint = true;
                        setCompetitor(competitor);
                        if (photoDetailView != null) {
                            photoDetailView.showPhotoReportedMessage();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);

                        if (e instanceof ApiError && ((ApiError) e).hasComplaintExistsError()) {
                            if (photoDetailView != null) {
                                photoDetailView.showPhotoComplaintExistsError();
                            }
                            return;
                        }

                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}
