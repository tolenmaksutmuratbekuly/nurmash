package com.nurmash.nurmash.mvp.main;

import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.MvpPresenter;

public class HotPromoSlidePresenter implements MvpPresenter<HotPromoSlideView> {
    private HotPromoSlideView hotPromoSliderView;
    private boolean isLoading;

    public HotPromoSlidePresenter() {
    }

    @Override
    public void attachView(HotPromoSlideView view) {
        hotPromoSliderView = view;
    }

    @Override
    public void detachView() {
        hotPromoSliderView = null;
    }

    public void onPromoClick(Promotion promo) {
        if (hotPromoSliderView != null && promo != null) {
            int state = promo.getState(System.currentTimeMillis());
            switch (state) {
                case Promotion.STATE_UNKNOWN:
                case Promotion.STATE_RUNNING:
                case Promotion.STATE_BEFORE_START:
                    hotPromoSliderView.openPromoDetailActivity(promo);
                    break;
                case Promotion.STATE_ENDED_RECENTLY:
                case Promotion.STATE_ENDED:
                default:
                    throw new IllegalStateException("Unsupported promotion state: " + state);
            }
        }
    }
}
