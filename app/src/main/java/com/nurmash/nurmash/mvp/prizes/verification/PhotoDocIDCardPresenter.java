package com.nurmash.nurmash.mvp.prizes.verification;

import android.net.Uri;
import android.os.Bundle;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.exception.SDCardNotMountedException;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.CameraUtils;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PhotoDocIDCardPresenter implements MvpPresenter<PhotoDocIDCardView> {
    private String TEMP_PHOTO_URI_1 = "TEMP_PHOTO_URI1";
    private String TEMP_CROP_URI_1 = "TEMP_CROP_URI1";
    private String TEMP_PHOTO_URI_2 = "TEMP_PHOTO_URI2";
    private String TEMP_CROP_URI_2 = "TEMP_CROP_URI2";
    private String TYPE_FRONT = "front";
    private String TYPE_BACK = "back";
    private final DataLayer dataLayer;
    private Subscription uploadResultSubscription;
    private Subscription uploadProgressSubscription;
    private ErrorHandler errorHandler;
    private PhotoDocIDCardView photoDocIDCardView;
    private Uri tempPhotoUri1, tempCropUri1, tempPhotoUri2, tempCropUri2;
    private ObservableTypedFile observableFile2;

    public PhotoDocIDCardPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void restoreInstanceState(Bundle state) {
        if (state != null) {
            if (state.containsKey(TEMP_PHOTO_URI_1))
                tempPhotoUri1 = Uri.parse(state.getString(TEMP_PHOTO_URI_1));
            if (state.containsKey(TEMP_CROP_URI_1))
                tempCropUri1 = Uri.parse(state.getString(TEMP_CROP_URI_1));

            if (state.containsKey(TEMP_PHOTO_URI_2))
                tempPhotoUri2 = Uri.parse(state.getString(TEMP_PHOTO_URI_2));
            if (state.containsKey(TEMP_CROP_URI_2))
                tempCropUri2 = Uri.parse(state.getString(TEMP_CROP_URI_2));
        }
    }

    public void saveInstanceState(Bundle outState) {
        if (tempPhotoUri1 != null) outState.putString(TEMP_PHOTO_URI_1, tempPhotoUri1.toString());
        if (tempCropUri1 != null) outState.putString(TEMP_CROP_URI_1, tempCropUri1.toString());

        if (tempPhotoUri2 != null) outState.putString(TEMP_PHOTO_URI_2, tempPhotoUri2.toString());
        if (tempCropUri2 != null) outState.putString(TEMP_CROP_URI_2, tempCropUri2.toString());
    }

    @Override
    public void attachView(PhotoDocIDCardView view) {
        photoDocIDCardView = view;
    }

    @Override
    public void detachView() {
        photoDocIDCardView = null;
        if (uploadResultSubscription != null) {
            uploadResultSubscription.unsubscribe();
            uploadResultSubscription = null;
        }

        if (uploadProgressSubscription != null) {
            uploadProgressSubscription.unsubscribe();
            uploadProgressSubscription = null;
        }
    }

    public void onCameraOptionSelected(String typeCamera) {
        if (photoDocIDCardView == null) return;

        if (typeCamera.equals(TYPE_FRONT)) {
            tempPhotoUri1 = createTempFile();
            if (tempPhotoUri1 == null) {
                photoDocIDCardView.showTempStorageError();
            } else if (!photoDocIDCardView.openCameraApp(tempPhotoUri1)) {
                photoDocIDCardView.showCameraAppError();
                deleteFile(tempPhotoUri1);
                tempPhotoUri1 = null;
            }
        } else if (typeCamera.equals(TYPE_BACK)) {
            tempPhotoUri2 = createTempFile();
            if (tempPhotoUri2 == null) {
                photoDocIDCardView.showTempStorageError();
            } else if (!photoDocIDCardView.openCameraApp(tempPhotoUri2)) {
                photoDocIDCardView.showCameraAppError();
                deleteFile(tempPhotoUri2);
                tempPhotoUri2 = null;
            }
        }
    }

    public void onCameraResult(boolean success, String typeCamera) {
        if (typeCamera.equals(TYPE_FRONT)) {
            if (tempPhotoUri1 == null) return;
            if (success) {
                openCropApp(tempPhotoUri1, typeCamera);
            } else {
                deleteFile(tempPhotoUri1);
                tempPhotoUri1 = null;
            }
        } else if (typeCamera.equals(TYPE_BACK)) {
            if (tempPhotoUri2 == null) return;
            if (success) {
                openCropApp(tempPhotoUri2, typeCamera);
            } else {
                deleteFile(tempPhotoUri2);
                tempPhotoUri2 = null;
            }
        }
    }

    public void onGalleryOptionSelected() {
        if (photoDocIDCardView != null) photoDocIDCardView.openGalleryApp();
    }

    public void onGalleryResult(Uri photoUri, String typeGallery) {
        if (photoUri != null) openCropApp(photoUri, typeGallery);
    }

    private boolean openCropApp(Uri photoUri, String frontOrBack) {
        if (photoUri == null || photoDocIDCardView == null) return false;
        if (frontOrBack.equals(TYPE_FRONT)) {
            if (tempCropUri1 != null) deleteFile(tempCropUri1);
            tempCropUri1 = createTempFile();
            if (!photoDocIDCardView.openCropApp(photoUri, tempCropUri1, frontOrBack)) {
                photoDocIDCardView.showCropAppError();
                deleteFile(tempCropUri1);
                return false;
            }
        } else if (frontOrBack.equals(TYPE_BACK)) {
            if (tempCropUri2 != null) deleteFile(tempCropUri2);
            tempCropUri2 = createTempFile();
            if (!photoDocIDCardView.openCropApp(photoUri, tempCropUri2, frontOrBack)) {
                photoDocIDCardView.showCropAppError();
                deleteFile(tempCropUri2);
                return false;
            }
        }
        return true;
    }

    public void onCropResult(boolean success, String frontOrBack) {
        if (frontOrBack.equals(TYPE_FRONT)) {
            if (tempCropUri1 == null) return;
            if (success) {
                if (photoDocIDCardView != null) {
                    photoDocIDCardView.showPhotoPreview(tempCropUri1.toString(), frontOrBack.equals(TYPE_FRONT));
                }
            } else {
                deleteFile(tempCropUri1);
                tempCropUri1 = null;
            }
        } else if (frontOrBack.equals(TYPE_BACK)) {
            if (tempCropUri2 == null) return;
            if (success) {
                if (photoDocIDCardView != null) {
                    photoDocIDCardView.showPhotoPreview(tempCropUri2.toString(), frontOrBack.equals(TYPE_FRONT));
                }
            } else {
                deleteFile(tempCropUri2);
                tempCropUri2 = null;
            }
        }
    }

    private static Uri createTempFile() {
        try {
            return CameraUtils.getTemporaryImageFileUri();
        } catch (IOException | SDCardNotMountedException e) {
            Timber.e(e, null);
            return null;
        }
    }

    private static void deleteFile(Uri fileUri) {
        //noinspection ResultOfMethodCallIgnored
        new File(fileUri.getPath()).delete();
    }

    public void sendDocumentClick() {
        uploadPhoto(tempCropUri1, tempCropUri2);
    }

    public void retryPhotoUpload() {
        uploadPhoto(tempCropUri1, tempCropUri2);
    }

    private void uploadPhoto(Uri photoFileUri1, Uri photoFileUri2) {
        if (photoFileUri1 != null && photoFileUri2 != null) {
            if (isActive(uploadResultSubscription)) {
                // Upload is already in progress.
                return;
            }
            if (photoDocIDCardView != null) {
                photoDocIDCardView.showPhotoUploadIndicator(true);
                photoDocIDCardView.onPhotoUploadProgress(0);
            }
            ObservableTypedFile observableFile1 = ObservableTypedFile.withPhotoUri(photoFileUri1);
            observableFile2 = ObservableTypedFile.withPhotoUri(photoFileUri2);
            progressUpload(observableFile1, true);
            uploadResultSubscription = dataLayer.profileVerify(observableFile1, observableFile2, false)
                    .observeOn(mainThread())
                    .subscribe(new Subscriber<FileUploadResult>() {
                        @Override
                        public void onNext(FileUploadResult result) {

                        }

                        @Override
                        public void onCompleted() {
                            if (photoDocIDCardView != null) {
                                photoDocIDCardView.showPhotoUploadIndicator(false);
                                photoDocIDCardView.onPhotoSuccessUploaded();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(new SubscriberErrorWrapper(e), null);
                            if (photoDocIDCardView == null) return;
                            photoDocIDCardView.showPhotoUploadIndicator(false);
                            photoDocIDCardView.onPhotoUploadFailed();
                            if (errorHandler != null) {
                                errorHandler.handleError(e);
                            }
                        }
                    });
        } else {
            if (photoDocIDCardView != null) {
                photoDocIDCardView.showPhotosNotSelectedError();
            }
        }
    }

    private void progressUpload(ObservableTypedFile observableFile, final boolean isFirstFile) {
        uploadProgressSubscription = observableFile.getProgressObservable()
                .sample(100, TimeUnit.MILLISECONDS)
                .onErrorResumeNext(Observable.<Integer>empty())
                .observeOn(mainThread())
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onNext(Integer progress) {
                        if (photoDocIDCardView != null)
                            photoDocIDCardView.onPhotoUploadProgress(progress);
                    }

                    @Override
                    public void onCompleted() {
                        if (photoDocIDCardView != null) {
                            photoDocIDCardView.onPhotoUploadProgress(100);
                            if (isFirstFile && observableFile2 != null) {
                                progressUpload(observableFile2, false);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }
}

