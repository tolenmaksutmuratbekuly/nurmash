package com.nurmash.nurmash.mvp.settings.location;

import android.util.Pair;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rx.Subscription;

public abstract class LocationPickerPresenter<T> implements MvpPresenter<LocationPickerView> {
    protected static final int LOCATION_ID_NONE = 0;

    protected final DataLayer dataLayer;
    protected final long countryId;
    protected final long regionId;
    protected Subscription itemsLoadSubscription;
    protected LocationPickerView locationPickerView;
    private ErrorHandler errorHandler;
    private List<T> items;
    private List<String> titles;

    protected LocationPickerPresenter(DataLayer dataLayer, long countryId, long regionId) {
        this.dataLayer = dataLayer;
        this.countryId = countryId;
        this.regionId = regionId;
    }

    @Override
    public void attachView(LocationPickerView view) {
        locationPickerView = view;
        if (items == null) {
            loadItems();
        } else {
            locationPickerView.setItemTitles(titles);
        }
    }

    @Override
    public void detachView() {
        if (itemsLoadSubscription != null) {
            itemsLoadSubscription.unsubscribe();
            itemsLoadSubscription = null;
        }
        locationPickerView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onItemSelected(int index) {
        if (locationPickerView != null) {
            locationPickerView.finishWithResult(items.get(index));
        }
    }

    protected void showLoadingIndicator(boolean show) {
        if (locationPickerView != null) {
            locationPickerView.showLoadingIndicator(show);
        }
    }

    protected void onItemsLoaded(List<Pair<T, String>> itemsWithTitles) {
        items = new ArrayList<>(itemsWithTitles.size());
        titles = new ArrayList<>(itemsWithTitles.size());
        for (Pair<T, String> p : itemsWithTitles) {
            items.add(p.first);
            titles.add(p.second);
        }
        if (locationPickerView != null) {
            locationPickerView.showLoadingIndicator(false);
            locationPickerView.setItemTitles(titles);
        }
    }

    protected void onItemsLoadFailed(Throwable e) {
        if (locationPickerView != null) {
            locationPickerView.showLoadingIndicator(false);
        }
        if (errorHandler != null) {
            errorHandler.handleError(e);
        }
    }

    protected abstract void loadItems();
}
