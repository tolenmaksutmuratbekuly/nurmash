package com.nurmash.nurmash.mvp.login;

import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

public interface LoginView extends MvpView {
    void showLoadingIndicator(boolean show);

    void openRegistrationScreen(User user);

    void openIntroScreen();

    void openMainScreen();

    void finish();

    void onFacebookSdkError();

    void onEmailPermissionNotGranted();

    void onGoogleApiConnectingError();

    void onGoogleApiNotConnectedError();

    void onGoogleSdkError();

    void onVKSdkError();

    void showTokenExpiredError();

    void showUserBannedError();
}
