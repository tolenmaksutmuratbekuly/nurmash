package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.mvp.MvpView;

public interface QRScanerView extends MvpView {
    void showLoadingIndicator(boolean show);

    void onSuccessUserInvited();

    void canNotInviteYourself();

    void invalidQRCodeError();

    void invitedAlreadyError();
}

