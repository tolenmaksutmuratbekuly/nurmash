package com.nurmash.nurmash.mvp.photo;

import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface PhotoCommentsView extends MvpView {
    void showLoadingIndicator(boolean show, boolean translucent);

    void showCommentsLoadingIndicator(boolean show);

    void setSendButtonEnabled(boolean enabled);

    void showOriginalComment(Comment origComment);

    void setComments(List<Comment> comments);

    void addOldComments(List<Comment> comments);

    void addNewComment(Comment comment);

    void clearCommentSelection();

    void clearCommentInputText();

    void setCommentInputText(String text);

    void focusOnCommentInput();

    void showCommentBodyTooLongErrorMessage(int length, int limit);

    void showCommentContextMenu(boolean show);

    void showCommentCopiedToastMessage();

    void openCommentReportDialog();

    void showCommentReportedToastMessage();

    void showCommentComplaintExistsError();

    void openUserProfileActivity(User user);
}
