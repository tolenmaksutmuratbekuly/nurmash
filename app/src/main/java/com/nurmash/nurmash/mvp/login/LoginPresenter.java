package com.nurmash.nurmash.mvp.login;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.social.AuthProvider;
import com.nurmash.nurmash.data.social.facebook.FacebookProvider;
import com.nurmash.nurmash.data.social.facebook.FacebookProviderException;
import com.nurmash.nurmash.data.social.google.GoogleAuthProvider;
import com.nurmash.nurmash.data.social.google.GoogleProviderException;
import com.nurmash.nurmash.data.social.vk.VKProvider;
import com.nurmash.nurmash.data.social.vk.VKProviderException;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import com.vk.sdk.api.VKError;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import timber.log.Timber;

import static com.nurmash.nurmash.util.retrofit.RetrofitUtils.isHttpUnauthorizedError;

public class LoginPresenter implements MvpPresenter<LoginView> {
    private final DataLayer dataLayer;
    private final FacebookProvider facebookAuthProvider;
    private final GoogleAuthProvider googleAuthProvider;
    private final VKProvider vkProvider;
    private LoginView loginView;
    private Subscription loginSubscription;
    private Subscription googleLogoutSubscription;
    private ErrorHandler errorHandler;

    public LoginPresenter(DataLayer dataLayer,
                          FacebookProvider facebookAuthProvider,
                          GoogleAuthProvider googleAuthProvider,
                          VKProvider vkProvider) {
        this.dataLayer = dataLayer;
        this.facebookAuthProvider = facebookAuthProvider;
        this.googleAuthProvider = googleAuthProvider;
        this.vkProvider = vkProvider;
    }

    @Override
    public void attachView(LoginView view) {
        loginView = view;
    }

    @Override
    public void detachView() {
        if (loginSubscription != null) {
            loginSubscription.unsubscribe();
            loginSubscription = null;
        }
        if (googleLogoutSubscription != null) {
            googleLogoutSubscription.unsubscribe();
            googleLogoutSubscription = null;
        }
        loginView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onAppLaunchIntentAction() {
        if (loginView == null) {
            return;
        }

        if (dataLayer.authPreferences().isAuthenticated()) {
            if (dataLayer.authPreferences().isUserProfileComplete()) {
                loginView.openMainScreen();
                loginView.finish();
            } else {
                loginView.openRegistrationScreen(null);
                loginView.showLoadingIndicator(false);
            }
        } else {
            if (!dataLayer.authPreferences().hasShownIntro()) {
                loginView.openIntroScreen();
                loginView.finish();
            }
        }
    }

    public void onLogoutIntentAction() {
        performLogout();
    }

    public void onAuthTokenExpiredIntentAction() {
        dataLayer.wipeOut();
        if (loginView != null) {
            loginView.showTokenExpiredError();
        }
    }

    public void onUserBannedErrorIntentAction() {
        performLogout();
        if (loginView != null) {
            loginView.showUserBannedError();
        }
    }

    public void onSkipIntroIntentAction() {
        dataLayer.authPreferences().setShownIntro(true);
    }

    public void onFacebookLoginIntentAction() {
        dataLayer.authPreferences().setShownIntro(true);
        performFacebookLogin();
    }

    public void onGoogleLoginIntentAction() {
        dataLayer.authPreferences().setShownIntro(true);
        performGoogleLogin();
    }

    public void onVKLoginIntentAction() {
        dataLayer.authPreferences().setShownIntro(true);
        performVKLogin();
    }

    public void onFacebookButtonClick() {
        performFacebookLogin();
    }

    public void onGoogleButtonClick() {
        performGoogleLogin();
    }

    public void onVKButtonClick() {
        performVKLogin();
    }

    private void performFacebookLogin() {
        performLogin(facebookAuthProvider, new Func1<String, Observable<User>>() {
            @Override
            public Observable<User> call(String token) {
                return dataLayer.authFacebook(token);
            }
        });
    }

    private void performGoogleLogin() {
        performLogin(googleAuthProvider, new Func1<String, Observable<User>>() {
            @Override
            public Observable<User> call(String token) {
                return dataLayer.authGoogle(token);
            }
        });
    }

    private void performVKLogin() {
        performLogin(vkProvider, new Func1<String, Observable<User>>() {
            @Override
            public Observable<User> call(String token) {
                return dataLayer.authVK(token);
            }
        });
    }

    private void performLogin(final AuthProvider provider, Func1<String, Observable<User>> nurmashAuthRequest) {
        if (loginView != null) loginView.showLoadingIndicator(true);
        if (loginSubscription != null) loginSubscription.unsubscribe();
        loginSubscription = provider.getTokenForAuth()
                .flatMap(nurmashAuthRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        if (loginView == null) return;

                        if (user.is_profile_complete) {
                            // This is a returning user
                            loginView.openMainScreen();
                            loginView.finish();
                        } else {
                            // This is a new user
                            loginView.openRegistrationScreen(user);
                            loginView.showLoadingIndicator(false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), "provider = %s", provider);

                        if (loginView != null) loginView.showLoadingIndicator(false);

                        if (e instanceof FacebookProviderException) {
                            switch (((FacebookProviderException) e).errorCode) {
                                case FACEBOOK_SDK_ERROR:
                                    providerLogout(provider);
                                    if (loginView != null) {
                                        loginView.onFacebookSdkError();
                                    }
                                    break;
                                case PERMISSION_NOT_GRANTED:
                                    if (loginView != null) loginView.onEmailPermissionNotGranted();
                                    break;
                            }
                        } else if (e instanceof GoogleProviderException) {
                            switch (((GoogleProviderException) e).errorCode) {
                                case GOOGLE_API_CONNECTING:
                                    if (loginView != null) loginView.onGoogleApiConnectingError();
                                    break;
                                case GOOGLE_API_NOT_CONNECTED:
                                    if (loginView != null) loginView.onGoogleApiNotConnectedError();
                                    break;
                                case GOOGLE_SDK_ERROR:
                                    providerLogout(provider);
                                    if (loginView != null) {
                                        loginView.onGoogleSdkError();
                                    }
                                    break;
                            }
                        } else if (e instanceof VKProviderException) {
                            if (((VKProviderException) e).getErrorCode() != VKError.VK_CANCELED) {
                                providerLogout(provider);
                                if (loginView != null) {
                                    loginView.onVKSdkError();
                                }
                            }
                        } else {
                            if (isInvalidTokenError(e) && provider == googleAuthProvider) {
                                // This might mean that our server could not authorize with the provided token. If we revoke
                                // access, user should be prompted to authorize our app from scratch next time, so we should
                                // get a valid token next time.
                                googleAuthProvider.revokeAccess();
                            } else {
                                providerLogout(provider);
                            }

                            // Delegate displaying error message to the default error handler.
                            if (errorHandler != null) errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void providerLogout(AuthProvider provider) {
        if (provider == googleAuthProvider) {
            googleAuthProvider.logout();
        } else if (provider == facebookAuthProvider) {
            facebookAuthProvider.logout();
        } else if (provider == vkProvider) {
            vkProvider.logout();
        } else {
            throw new IllegalArgumentException("Unknown provider: " + provider);
        }
    }

    private static boolean isInvalidTokenError(Throwable e) {
        return isHttpUnauthorizedError(e)
                || (e instanceof ApiError) && ((ApiError) e).hasInvalidGoogleTokenError();
    }

    private void performLogout() {
        if (loginView != null) {
            loginView.showLoadingIndicator(true);
        }
        dataLayer.wipeOut();
        facebookAuthProvider.logout();
        vkProvider.logout();
        googleLogoutSubscription = googleAuthProvider.logoutAsync()
                .onErrorResumeNext(Observable.<Void>just(null))
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void v) {
                        if (loginView != null) {
                            loginView.showLoadingIndicator(false);
                        }
                    }
                });
    }
}
