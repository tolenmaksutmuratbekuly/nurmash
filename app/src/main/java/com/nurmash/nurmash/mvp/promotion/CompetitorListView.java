package com.nurmash.nurmash.mvp.promotion;

import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.MvpView;

import java.util.List;

public interface CompetitorListView extends MvpView {
    void showLoadingIndicator(boolean show);

    void setCompetitors(List<Competitor> competitors, long authUserId);

    void addCompetitors(List<Competitor> competitors);

    void openListDetailFragment(long competitorId, int competitorIndex);

    void openProfileDetailActivity(long userId);

    void showReportLoadingIndicator(boolean show);

    void showPhotoReportedMessage();

    void showPhotoComplaintExistsError();

    void openCommentListActivity(Competitor competitor, boolean focusInput);

    void openUserProfileActivity(User user);

    void openPromoDetailActivity(long promoId);

    void updatePhotoDetail(int competitorPosition);

    void openCompetitorLikesActivity(Competitor competitor);
}
