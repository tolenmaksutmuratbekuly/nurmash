package com.nurmash.nurmash.mvp.helpers;

import android.support.annotation.PluralsRes;
import android.support.annotation.StringRes;

public interface StringsProvider {
    String getString(@StringRes int resId, Object... formatArgs);

    String getQuantityString(@PluralsRes int resId, int quantity, Object... args);
}
