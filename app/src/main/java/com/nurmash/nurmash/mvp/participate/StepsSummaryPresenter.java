package com.nurmash.nurmash.mvp.participate;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.util.Pair;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.model.ParticipationData.ALL_STEPS_DONE;
import static com.nurmash.nurmash.model.ParticipationData.LAST_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SHARE_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SURVEY_STEP;
import static com.nurmash.nurmash.model.ParticipationData.VIDEO_STEP;

public class StepsSummaryPresenter implements MvpPresenter<StepsSummaryView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private ParticipationData participationData;
    private StepsSummaryView stepsSummaryView;
    private Subscription participationDataLoadSubscription;
    private Subscription commitLastStepSubscription;
    private ErrorHandler errorHandler;

    public StepsSummaryPresenter(DataLayer dataLayer, long promoId) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
    }

    @Override
    public void attachView(StepsSummaryView view) {
        stepsSummaryView = view;
    }

    @Override
    public void detachView() {
        if (participationDataLoadSubscription != null) {
            participationDataLoadSubscription.unsubscribe();
            participationDataLoadSubscription = null;
        }
        if (commitLastStepSubscription != null) {
            commitLastStepSubscription.unsubscribe();
            commitLastStepSubscription = null;
        }
        stepsSummaryView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void loadParticipationData() {
        if (stepsSummaryView != null) stepsSummaryView.showLoadingIndicator(true);
        if (participationDataLoadSubscription != null) participationDataLoadSubscription.unsubscribe();
        participationDataLoadSubscription = dataLayer.getParticipationData(promoId, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        setParticipationData(data);
                    }

                    @Override
                    public void onCompleted() {
                        if (stepsSummaryView != null) stepsSummaryView.showLoadingIndicator(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (stepsSummaryView != null) stepsSummaryView.showLoadingIndicator(false);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }

    public void onNextClick() {
        if (stepsSummaryView == null) return;

        switch (participationData.getNextStep()) {
            case VIDEO_STEP:
                stepsSummaryView.openVideoStepActivity(promoId);
                break;
            case SHARE_STEP:
                stepsSummaryView.openShareStepActivity(promoId);
                break;
            case SURVEY_STEP:
                stepsSummaryView.openSurveyStepActivity(promoId);
                break;
            case LAST_STEP:
            case ALL_STEPS_DONE:
                finalizeSteps();
                break;
        }
    }

    private void finalizeSteps() {
        if (participationData.getCompetitor() != null && participationData.getCompetitor().hasCompletedAllSteps()) {
            finish();
            return;
        }

        if (commitLastStepSubscription != null && !commitLastStepSubscription.isUnsubscribed()) return;
        if (stepsSummaryView != null) stepsSummaryView.showLoadingIndicator(true);
        commitLastStepSubscription = dataLayer.commitLastStep(participationData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        setParticipationData(data);
                        if (stepsSummaryView == null) stepsSummaryView.showLoadingIndicator(false);
                        finish();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        if (stepsSummaryView != null) stepsSummaryView.showLoadingIndicator(false);
                        if (errorHandler != null) errorHandler.handleError(e);
                    }
                });
    }

    private void finish() {
        if (stepsSummaryView == null) {
            return;
        }
        if (participationData.getPromotion().isPhotoSharingPromo()) {
            stepsSummaryView.openMyPhotoActivity(participationData.getCompetitorId());
        } else {
            stepsSummaryView.finish();
        }
    }

    private void setParticipationData(ParticipationData data) {
        participationData = data;
        if (stepsSummaryView == null) return;

        stepsSummaryView.setChecklistItems(getCheckListItems(data.getSteps()));

        switch (data.getNextStep()) {
            case VIDEO_STEP:
                stepsSummaryView.setNextActionTitle(R.string.action_watch_video);
                break;
            case SHARE_STEP:
                stepsSummaryView.setNextActionTitle(R.string.action_take_photo);
                break;
            case SURVEY_STEP:
                stepsSummaryView.setNextActionTitle(R.string.action_take_survey);
                break;
            case LAST_STEP:
            case ALL_STEPS_DONE:
                if (participationData.getPromotion().isPhotoSharingPromo()) {
                    stepsSummaryView.setNextActionTitle(R.string.action_see_my_photo);
                } else {
                    stepsSummaryView.setNextActionTitle(R.string.action_go_back);
                }
                break;
        }
    }

    private List<ChecklistItem> getCheckListItems(List<Pair<String, Boolean>> steps) {
        List<ChecklistItem> items = new ArrayList<>(steps.size() + 1);
        for (int i = 0; i < steps.size(); ++i) {
            String step = steps.get(i).first;
            boolean complete = steps.get(i).second;
            switch (step) {
                case VIDEO_STEP:
                    items.add(new StepsChecklistItem(complete, R.string.label_video_step,
                            R.drawable.ic_step_video_active, R.drawable.ic_step_video_inactive));
                    break;
                case SHARE_STEP:
                    items.add(new StepsChecklistItem(complete, R.string.label_photo_step,
                            R.drawable.ic_step_photo_active, R.drawable.ic_step_photo_inactive));
                    items.add(new StepsChecklistItem(complete, R.string.label_share_step,
                            R.drawable.ic_step_share_active, R.drawable.ic_step_share_inactive));
                    break;
                case SURVEY_STEP:
                    items.add(new StepsChecklistItem(complete, R.string.label_survey_step,
                            R.drawable.ic_step_survey_active, R.drawable.ic_step_survey_inactive));
                    break;
            }
        }
        return items;
    }

    private class StepsChecklistItem implements ChecklistItem {
        private final int titleRes;
        private final int checkedIconRes;
        private final int uncheckedIconRes;
        private boolean checked;

        StepsChecklistItem(boolean checked,
                           @StringRes int titleRes,
                           @DrawableRes int checkedIconRes,
                           @DrawableRes int uncheckedIconRes) {
            this.checked = checked;
            this.titleRes = titleRes;
            this.checkedIconRes = checkedIconRes;
            this.uncheckedIconRes = uncheckedIconRes;
        }

        @Override
        public int getTitleRes() {
            return titleRes;
        }

        @Override
        public int getTextAppearanceRes() {
            return checked ? R.style.ParticipationSteps_TextAppearance_Label_Active :
                    R.style.ParticipationSteps_TextAppearance_Label_Inactive;
        }

        @Override
        public int getIconRes() {
            return checked ? checkedIconRes : uncheckedIconRes;
        }

        @Override
        public int getCheckMarkRes() {
            return checked ? R.drawable.ic_step_check_active : R.drawable.ic_step_check_inactive;
        }
    }
}
