package com.nurmash.nurmash.mvp.participate;

import android.net.Uri;
import android.os.Bundle;

import com.nurmash.nurmash.model.exception.SDCardNotMountedException;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.CameraUtils;

import java.io.File;
import java.io.IOException;

import timber.log.Timber;

public class PhotoStepPresenter implements MvpPresenter<PhotoStepView> {
    private final long promoId;
    private PhotoStepView photoStepView;
    private Uri tempPhotoUri;
    private Uri tempCropUri;

    public PhotoStepPresenter(long promoId) {
        this.promoId = promoId;
    }

    @Override
    public void attachView(PhotoStepView view) {
        photoStepView = view;
    }

    @Override
    public void detachView() {
        photoStepView = null;
    }

    public void restoreInstanceState(Bundle state) {
        if (state != null) {
            if (state.containsKey("TEMP_PHOTO_URI")) tempPhotoUri = Uri.parse(state.getString("TEMP_PHOTO_URI"));
            if (state.containsKey("TEMP_CROP_URI")) tempCropUri = Uri.parse(state.getString("TEMP_CROP_URI"));
        }
    }

    public void saveInstanceState(Bundle outState) {
        if (tempPhotoUri != null) outState.putString("TEMP_PHOTO_URI", tempPhotoUri.toString());
        if (tempCropUri != null) outState.putString("TEMP_CROP_URI", tempCropUri.toString());
    }

    public void onCameraOptionSelected() {
        if (photoStepView == null) return;
        tempPhotoUri = createTempFile();
        if (tempPhotoUri == null) {
            photoStepView.showTempStorageError();
        } else if (!photoStepView.openCameraApp(tempPhotoUri)) {
            photoStepView.showCameraAppError();
            deleteFile(tempPhotoUri);
            tempPhotoUri = null;
        }
    }

    public void onGalleryOptionSelected() {
        if (photoStepView != null) photoStepView.openGalleryApp();
    }

    public void onCameraResult(boolean success) {
        if (tempPhotoUri == null) return;
        if (success) {
            openCropApp(tempPhotoUri);
        } else {
            deleteFile(tempPhotoUri);
            tempPhotoUri = null;
        }
    }

    public void onGalleryResult(Uri photoUri) {
        if (photoUri != null) openCropApp(photoUri);
    }

    public void onCropResult(boolean success) {
        if (tempCropUri == null) return;
        if (success) {
            if (photoStepView != null) {
                photoStepView.openShareStepActivity(promoId, tempCropUri);
                photoStepView.finish();
            }
        } else {
            deleteFile(tempCropUri);
            tempCropUri = null;
        }
    }

    private boolean openCropApp(Uri photoUri) {
        if (photoUri == null || photoStepView == null) return false;
        if (tempCropUri != null) deleteFile(tempCropUri);
        tempCropUri = createTempFile();
        if (!photoStepView.openCropApp(photoUri, tempCropUri)) {
            photoStepView.showCropAppError();
            deleteFile(tempCropUri);
            return false;
        }
        return true;
    }

    private static Uri createTempFile() {
        try {
            return CameraUtils.getTemporaryImageFileUri();
        } catch (IOException | SDCardNotMountedException e) {
            Timber.e(e, null);
            return null;
        }
    }

    private static void deleteFile(Uri fileUri) {
        //noinspection ResultOfMethodCallIgnored
        new File(fileUri.getPath()).delete();
    }
}
