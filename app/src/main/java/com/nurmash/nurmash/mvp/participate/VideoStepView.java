package com.nurmash.nurmash.mvp.participate;

import com.nurmash.nurmash.mvp.MvpView;

public interface VideoStepView extends MvpView {
    void showLoadingIndicator(boolean show);

    void loadVideo(String videoId);

    void openShareStepActivity(long promoId);

    void openSurveyStepActivity(long promoId);

    void finish();

    void onPlaybackError();
}
