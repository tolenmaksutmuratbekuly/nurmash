package com.nurmash.nurmash.mvp.prizes.withdraw;

import com.nurmash.nurmash.mvp.MvpView;

public interface CardTransferView extends MvpView {
    void showAmountPrize(String amountOfPrize);

    void showTaxAmount(String taxAmount);

    void showComission(String comissionAmount);

    void showAmountCrediting(String creditingAmount);

    void showLoadingIndicator(boolean show);

    void showInvalidCardNumberErrorToast();

    void showInvalidCardConfirmationErrorToast();

    void finishWithSuccess();
}
