package com.nurmash.nurmash.mvp.promotion;

import android.view.View;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.AppData;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.TimeLeft;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.text.TextUtils.isEmpty;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PromoDetailPresenter implements MvpPresenter<PromoDetailView> {
    private final DataLayer dataLayer;
    private final long promoId;
    private Promotion promo;
    private ParticipationData participationData;
    private AppData linkedAppData;
    private PromoDetailView promoDetailView;
    private Subscription participationDataLoadSubscription;
    private Subscription appDataLoadSubscription;
    private ErrorHandler errorHandler;
    private StringsProvider strings;
    private boolean isLoadingData;
    private TimeLeft startTimeLeft;
    private Subscription timeLeftUpdateSubscription;

    public PromoDetailPresenter(DataLayer dataLayer, Promotion promo) {
        this(dataLayer, promo.id);
        this.promo = promo;
    }

    public PromoDetailPresenter(DataLayer dataLayer, long promoId) {
        this.dataLayer = dataLayer;
        this.promoId = promoId;
    }

    @Override
    public void attachView(PromoDetailView view) {
        promoDetailView = view;
        updatePromoView();
        if (promo != null) {
            updateNextButtonView(promo.can_participate);
        }
        promoDetailView.showLoadingIndicator(participationData == null && isLoadingData);
        loadParticipationData(false);
    }

    @Override
    public void detachView() {
        if (participationDataLoadSubscription != null) {
            participationDataLoadSubscription.unsubscribe();
            participationDataLoadSubscription = null;
        }
        if (appDataLoadSubscription != null) {
            appDataLoadSubscription.unsubscribe();
            appDataLoadSubscription = null;
        }
        if (timeLeftUpdateSubscription != null) {
            timeLeftUpdateSubscription.unsubscribe();
            timeLeftUpdateSubscription = null;
        }
        promoDetailView = null;
    }

    public ParticipationData getParticipationData() {
        return participationData;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setStringsProvider(StringsProvider strings) {
        this.strings = strings;
    }

    public void onResume() {
        loadParticipationData(true);
        dataLayer.notificationsHelper().clearPromoNotification(promoId);
        dataLayer.notificationsHelper().clearDisqualNotification(promoId);
    }

    public void onCompetitorsPreviewClick() {
        if (promoDetailView == null) {
            return;
        }

        if (promo.participants_amount == 0) {
            promoDetailView.openNoParticipantsActivity();
        } else if (participationData != null) {
            promoDetailView.openCompetitorListActivity(promo);
        }
    }

    public void onTotalPrizeClick() {
        if (promoDetailView != null && promo != null && participationData != null) {
            promoDetailView.openTotalPrizeInfoActivity(promo);
        }
    }

    public void onInstallAppClick() {
        String appLink = promo.mobile_apps.android;
        if (!isEmpty(appLink) && promoDetailView != null) {
            promoDetailView.openGooglePlayLink(appLink);
        }
    }

    public void onNextClick() {
        if (promoDetailView != null) promoDetailView.openStepsSummaryActivity(promoId);
    }

    private void setLoadingData(boolean isLoading) {
        isLoadingData = isLoading;
        if (promoDetailView != null) {
            promoDetailView.showLoadingIndicator(participationData == null && isLoading);
        }
    }

    private void loadParticipationData(boolean useCache) {
        if (isLoadingData) {
            return;
        }
        setLoadingData(true);
        participationDataLoadSubscription = dataLayer.getParticipationData(promoId, useCache)
                .observeOn(mainThread())
                .subscribe(new Subscriber<ParticipationData>() {
                    @Override
                    public void onNext(ParticipationData data) {
                        setLoadingData(false);
                        setParticipationData(data);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingData(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void setParticipationData(ParticipationData data) {
        participationData = data;
        promo = data.getPromotion();
        updatePromoView();
        updateNextButtonView(promo.can_participate);
        loadLinkedAppData();
    }

    private void updatePromoView() {
        if (promoDetailView == null) {
            return;
        }
        promoDetailView.onPromotionDataLoaded(promo);
    }

    private void updateNextButtonView(boolean can_participate) {
        if (promoDetailView == null) {
            return;
        }
        if (participationData == null) {
            promoDetailView.showNextButton(false);
            return;
        }
        if (timeLeftUpdateSubscription != null) {
            timeLeftUpdateSubscription.unsubscribe();
        }
        promoDetailView.showNextButton(true);
        switch (participationData.getPromoState()) {
            case Promotion.STATE_UNKNOWN:
                promoDetailView.setNextButtonText(strings.getString(R.string.action_accept_terms));
                promoDetailView.setNextButtonCallback(null);
                break;
            case Promotion.STATE_BEFORE_START:
                startTimeLeft = new TimeLeft(promo.start_date);
                promoDetailView.timerStateBeforeStart(formatTimeLeft(startTimeLeft), false);
                promoDetailView.setNextButtonCallback(null);
                promoDetailView.setNextButtonText(strings.getString(R.string.action_accept_terms));
                scheduleTimeLeftUpdate();
                break;
            case Promotion.STATE_RUNNING:
                promoDetailView.setNextButtonEnabled(can_participate);
                Competitor competitor = participationData.getCompetitor();
                if (competitor == null) {
                    promoDetailView.setNextButtonText(strings.getString(R.string.action_accept_terms));
                    promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            participate();
                        }
                    });
                } else if (!competitor.hasCompletedAllSteps()) {
                    promoDetailView.setNextButtonText(strings.getString(R.string.action_continue_participation));
                    promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            participate();
                        }
                    });
                } else if (competitor.isDisqualified()) {
                    promoDetailView.setNextButtonText(strings.getString(R.string.info_text_you_are_disqualified));
                    promoDetailView.setNextButtonCallback(null);
                } else if (promo.isPhotoSharingPromo()) {
                    if (competitor.hasSharedOnAllNetworks()) {
                        promoDetailView.setNextButtonText(strings.getString(R.string.info_text_all_steps_completed));
                        promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                seeMyPhoto();
                            }
                        });
                    } else {
                        promoDetailView.setNextButtonText(strings.getString(R.string.action_share_more));
                        promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                shareMore();
                            }
                        });
                    }
                } else {
                    promoDetailView.setNextButtonText(strings.getString(R.string.info_text_all_steps_completed));
                    promoDetailView.setNextButtonCallback(null);
                }
                break;
            case Promotion.STATE_ENDED_RECENTLY:
            case Promotion.STATE_ENDED:
                promoDetailView.setNextButtonText(strings.getString(R.string.info_text_promo_ended));
                promoDetailView.setNextButtonCallback(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        seeWinners();
                    }
                });
                break;
        }
    }

    public String formatTimeLeft(TimeLeft timeLeft) {
        StringBuilder builder = new StringBuilder(64);
        if (timeLeft.getDays() > 0) {
            builder.append(strings.getString(R.string.time_left_day_format, timeLeft.getDays()));
            builder.append(' ');
        }
        if (timeLeft.getHours() > 0 || builder.length() > 0) {
            builder.append(strings.getString(R.string.time_left_hours_format, timeLeft.getHours()));
            builder.append(' ');
        }
        if (timeLeft.getMinutes() > 0 || builder.length() > 0) {
            builder.append(strings.getString(R.string.time_left_minutes_format, timeLeft.getMinutes()));
            builder.append(' ');
        }
        builder.append(strings.getString(R.string.time_left_seconds_format, timeLeft.getSeconds()));
        return builder.toString();
    }

    private void scheduleTimeLeftUpdate() {
        if (timeLeftUpdateSubscription != null) {
            timeLeftUpdateSubscription.unsubscribe();
        }
        timeLeftUpdateSubscription = Observable.interval(1, 1, TimeUnit.SECONDS, Schedulers.newThread())
                .onBackpressureLatest()
                .observeOn(mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onNext(Long ignored) {
                        startTimeLeft.update();
                        promoDetailView.timerStateBeforeStart(formatTimeLeft(startTimeLeft), false);
                        if (startTimeLeft.isNoTimeLeft()) {
                            promoDetailView.timerStateBeforeStart(null, true);
                            loadParticipationData(false);
                            unsubscribe();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }
                });
    }

    private void participate() {
        if (promoDetailView != null) {
            promoDetailView.openStepsSummaryActivity(promoId);
        }
    }

    private void shareMore() {
        if (promoDetailView != null) {
            promoDetailView.openShareStepActivity(promoId);
        }
    }

    private void seeMyPhoto() {
        if (promoDetailView != null) {
            promoDetailView.openPhotoDetailActivity(participationData.getCompetitorId());
        }
    }

    private void seeWinners() {
        if (promoDetailView != null) {
            promoDetailView.openPastPromoDetailActivity(promo);
        }
    }

    private void loadLinkedAppData() {
        if (linkedAppData != null) {
            return;
        }
        String linkedAppId = promo == null ? null : promo.getLinkedAppId();
        if (linkedAppId == null) {
            return;
        }
        appDataLoadSubscription = dataLayer.getPlayMarketAppData(linkedAppId)
                .observeOn(mainThread())
                .subscribe(new Subscriber<AppData>() {
                    @Override
                    public void onNext(AppData appData) {
                        linkedAppData = appData;
                        if (appData != null) {
                            if (promoDetailView != null) {
                                promoDetailView.onLinkedAppDataLoaded(appData);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }

                    @Override
                    public void onCompleted() {
                    }
                });
    }
}
