package com.nurmash.nurmash.mvp;

public interface MvpPresenter<T extends MvpView> {
    void attachView(T view);

    void detachView();
}
