package com.nurmash.nurmash.mvp.prizes.withdraw;

import com.nurmash.nurmash.mvp.MvpView;

public interface MobileBalanceTransferView extends MvpView {
    void showAmountPrize(String amountOfPrize);

    void showTaxAmount(String taxAmount);

    void showComission(String comissionAmount);

    void showAmountCrediting(String creditingAmount);

    void showLoadingIndicator(boolean show);

    void showOperatorNotSelectedErrorToast();

    void showInvalidPhoneNumberErrorToast();

    void showInvalidPhoneNumbersAraNotSameErrorToast();

    void finishWithSuccess();

    void carrierCheckState(boolean hasError);

    void phoneNumberCheckState(boolean hasError);

    void phoneNumberConfirmationCheckState(boolean hasError);

    void setLocalPhoneNumber(String localPhoneNumber);
}
