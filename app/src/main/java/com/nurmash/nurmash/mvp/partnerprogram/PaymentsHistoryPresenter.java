package com.nurmash.nurmash.mvp.partnerprogram;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.InviteList;
import com.nurmash.nurmash.model.json.PartnerInvitations;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.MvpPresenter;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PaymentsHistoryPresenter implements MvpPresenter<PaymentsHistoryView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private final List<InviteList> loadedItems = new ArrayList<>();
    private PaymentsHistoryView invitationsView;
    private ErrorHandler errorHandler;
    private Subscription feedLoadSubscription;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private boolean isLoadingItems;

    public PaymentsHistoryPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    private void resetLoadedItems() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void attachView(PaymentsHistoryView view) {
        invitationsView = view;
        if (isFinalListEmpty()) {
            showEmptyView();
        } else {
            invitationsView.setInvitationItems(loadedItems);
            invitationsView.showLoadingIndicator(isLoadingItems);
            if (loadedItems.isEmpty()) {
                loadNextPage();
            }
        }
    }

    @Override
    public void detachView() {
        if (feedLoadSubscription != null) {
            feedLoadSubscription.unsubscribe();
            feedLoadSubscription = null;
        }
        invitationsView = null;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void onRefreshAction() {
        reload();
    }

    public void onRequestMoreItems() {
        loadNextPage();
    }

    private void setLoadingItems(boolean isLoading) {
        isLoadingItems = isLoading;
        if (invitationsView != null) {
            invitationsView.showLoadingIndicator(isLoadingItems);
        }
    }

    private void loadNextPage() {
        if (noMoreItems || isLoadingItems) {
            return;
        }
        setLoadingItems(true);
        feedLoadSubscription = dataLayer.getPartnerInvitations(numPagesLoaded + 1, ITEMS_PER_PAGE, 1)
                .observeOn(mainThread())
                .subscribe(new Subscriber<PartnerInvitations>() {
                    @Override
                    public void onNext(PartnerInvitations invitation) {
                        setLoadingItems(false);
                        if (invitationsView != null && invitation != null && invitation.sum != null) {
                            invitationsView.setTotalWithDrawnSum(invitation.sum.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + invitation.getCurrencyCode());
                        }
                        onNextPageLoaded(invitation.invite_list);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void reload() {
        if (feedLoadSubscription != null) {
            feedLoadSubscription.unsubscribe();
        }
        setLoadingItems(true);
        feedLoadSubscription = dataLayer.getPartnerInvitations(1, ITEMS_PER_PAGE, 1)
                .observeOn(mainThread())
                .subscribe(new Subscriber<PartnerInvitations>() {
                    @Override
                    public void onNext(PartnerInvitations invitation) {
                        setLoadingItems(false);
                        resetLoadedItems();
                        onNextPageLoaded(invitation.invite_list);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                        setLoadingItems(false);
                        if (errorHandler != null) {
                            errorHandler.handleError(e);
                        }
                    }
                });
    }

    private void onNextPageLoaded(List<InviteList> inviteList) {
        if (inviteList == null || inviteList.isEmpty()) {
            noMoreItems = true;
            if (isFinalListEmpty()) {
                showEmptyView();
            }
        } else {
            numPagesLoaded = numPagesLoaded + 1;
            loadedItems.addAll(inviteList);
            if (invitationsView != null) {
                if (numPagesLoaded == 1) {
                    invitationsView.setInvitationItems(inviteList);
                } else {
                    invitationsView.addInvitationItems(inviteList);
                }
            }
        }
    }

    private boolean isFinalListEmpty() {
        return noMoreItems && loadedItems.isEmpty();
    }

    private void showEmptyView() {
        if (invitationsView != null) {
            invitationsView.showEmptyView();
        }
    }
}
