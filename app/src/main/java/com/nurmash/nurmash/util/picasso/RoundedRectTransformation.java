package com.nurmash.nurmash.util.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import com.squareup.picasso.Transformation;

public class RoundedRectTransformation implements Transformation {
    private final float radius;
    private final String key;

    public RoundedRectTransformation(int radius) {
        this.radius = radius;
        this.key = String.format("%s_%d", RoundedRectTransformation.class.getSimpleName(), radius);
    }

    @Override
    public Bitmap transform(Bitmap source) {
        if (source == null || source.isRecycled()) {
            return null;
        }

        Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), source.getConfig());
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);
        canvas.drawRoundRect(new RectF(0, 0, output.getWidth(), output.getHeight()), radius, radius, paint);

        if (output != source) {
            source.recycle();
        }

        return output;
    }

    @Override
    public String key() {
        return key;
    }
}
