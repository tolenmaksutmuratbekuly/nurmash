package com.nurmash.nurmash.util.support;

import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class ArrayPagerAdapter<T> extends PagerAdapter {
    private final List<T> items;

    public ArrayPagerAdapter() {
        items = new ArrayList<>();
    }

    public ArrayPagerAdapter(Collection<T> initialItems) {
        items = new ArrayList<>(initialItems);
    }

    public ArrayPagerAdapter(int capacity) {
        items = new ArrayList<>(capacity);
    }

    public void addItem(T item) {
        items.add(item);
    }

    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
