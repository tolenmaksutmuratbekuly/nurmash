package com.nurmash.nurmash.util.picasso;

import android.app.Application;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.util.Validate;

import static com.nurmash.nurmash.util.ContextUtils.dp2pixels;

/**
 * Holds static instances of common Picasso transformations.
 */
public class Transformations {
    public static RoundedRectTransformation ROUND_RECT_2DP;
    public static CircleTransformation CIRCLE_NO_BORDER;
    public static CircleTransformation CIRCLE_WHITE_BORDER_1DP;
    public static CircleTransformation CIRCLE_GRAY_BORDER_2DP;

    /**
     * This must be called during application start up (e.g. inside {@link Application#onCreate()}).
     *
     * @param context Application context. Needed for DP<->PIXELS conversions.
     */
    public static void initialize(Application context) {
        Validate.runningOnUiThread();
        ROUND_RECT_2DP = new RoundedRectTransformation(dp2pixels(context, 2));
        CIRCLE_NO_BORDER = new CircleTransformation();
        CIRCLE_WHITE_BORDER_1DP = new CircleTransformation(dp2pixels(context, 1), Color.WHITE);
        CIRCLE_GRAY_BORDER_2DP = new CircleTransformation(dp2pixels(context, 2), ContextCompat.getColor(context, R.color.jackpot_winner_photo));
    }
}
