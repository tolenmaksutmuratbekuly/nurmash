package com.nurmash.nurmash.util.rx;

import com.nurmash.nurmash.model.json.ApiResponse;

import rx.Observable;
import rx.functions.Func1;

public class Transformers {
    private static final Observable.Transformer responseDataExtractor = new Observable.Transformer<ApiResponse, Object>() {
        @Override
        public Observable<Object> call(Observable<ApiResponse> o) {
            return o.map(new Func1<ApiResponse, Object>() {
                @Override
                public Object call(ApiResponse response) {
                    return response.extractData();
                }
            });
        }
    };

    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<ApiResponse<T>, T> responseDataExtractor() {
        return (Observable.Transformer<ApiResponse<T>, T>) responseDataExtractor;
    }
}
