package com.nurmash.nurmash.util.android;

import android.content.Context;
import android.net.Uri;

import com.nurmash.nurmash.BuildConfig;
import com.nurmash.nurmash.util.DebugUtils;

import java.io.File;
import java.io.IOException;

public class TempFilesManager {
    private final Context context;
    private final String tempFilePrefix;
    private final String tempFileSuffix;

    public TempFilesManager(Context context, String filePrefix, String fileSuffix) {
        // File.createTempFile() requires prefix to be at least 3 characters long.
        if (filePrefix.length() < 3) {
            throw new IllegalArgumentException("filePrefix must be at least 3 characters");
        }
        this.context = context;
        this.tempFilePrefix = filePrefix;
        this.tempFileSuffix = fileSuffix;
    }

    /**
     * @return Uri to a new temporary file.
     * @throws IOException if creation of temporary file failed.
     */
    public Uri createTempFile() throws IOException {
        File tempFilesDir = getTempFilesDir();
        if (tempFilesDir == null) {
            return null;
        }
        Uri result = Uri.fromFile(File.createTempFile(tempFilePrefix, tempFileSuffix, tempFilesDir));
        if (BuildConfig.DEBUG) {
            DebugUtils.androidAssert(isTempFileUri(result));
        }
        return result;
    }

    public boolean isTempFileUri(Uri uri) {
        if (uri == null) {
            return false;
        }
        File tempFilesDir = getTempFilesDir();
        File file = new File(uri.getPath());
        String filename = file.getName();
        String parentDir = file.getParent();
        return filename.startsWith(tempFilePrefix)
                && tempFileSuffix != null && filename.endsWith(tempFileSuffix)
                && parentDir != null && parentDir.equals(tempFilesDir.getPath());
    }

    public void deleteTempFile(Uri fileUri) {
        if (isTempFileUri(fileUri)) {
            //noinspection ResultOfMethodCallIgnored
            new File(fileUri.getPath()).delete();
        }
    }

    File getTempFilesDir() {
        return context.getExternalCacheDir();
    }
}
