package com.nurmash.nurmash.util;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;

import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.mvp.helpers.ClipboardHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import timber.log.Timber;

@SuppressWarnings("unused")
public class DebugUtils {
    public static Map<String, Object> bundleToMap(Bundle bundle) {
        if (bundle == null) {
            return null;
        }

        Map<String, Object> result = new ArrayMap<>();
        for (String key : bundle.keySet()) {
            result.put(key, result.get(key));
        }
        return result;
    }

    public static String activityResultCodeToString(int resultCode) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                return "RESULT_OK(" + resultCode + ")";
            case Activity.RESULT_CANCELED:
                return "RESULT_CANCELED(" + resultCode + ")";
            case Activity.RESULT_FIRST_USER:
                return "RESULT_FIRST_USER(" + resultCode + ")";
            default:
                return Integer.toString(resultCode);
        }
    }

    public static void androidAssert(boolean condition) {
        if (!condition) {
            throw new AssertionError();
        }
    }

    public static void androidAssert(String message, boolean condition) {
        if (!condition) {
            throw new AssertionError(message);
        }
    }

    public static boolean copyParticipationDataToClipboard(ClipboardHelper clipboard,
                                                           long userId,
                                                           String token,
                                                           ParticipationData data) {
        JSONObject json = new JSONObject();
        try {
            json.put("token", token);
            json.put("user_id", userId);
            json.put("contest_id", data.getPromoId());
            if (data.getCompetitor() != null) {
                json.put("competitor_id", data.getCompetitorId());
            }
            clipboard.copyText(json.toString(4));
            return true;
        } catch (JSONException e) {
            Timber.e(e, null);
            return false;
        }
    }
}
