package com.nurmash.nurmash.util.rx;

import retrofit.client.Response;
import rx.Observable.Operator;
import rx.Subscriber;

/**
 * Validates that emitted {@link Response} object is non-null and that it's {@link Response#getBody()} is also
 * non-null. If validation fails {@link NullPointerException} is thrown which should be delivered to the original
 * subscriber's {@link Subscriber#onError(Throwable)}.
 */
public class ResponseBodyValidator implements Operator<Response, Response> {
    @Override
    public Subscriber<? super Response> call(final Subscriber<? super Response> s) {
        return new Subscriber<Response>(s) {
            @Override
            public void onNext(Response response) {
                if (response == null) {
                    throw new NullPointerException("response == null");
                }
                if (response.getBody() == null) {
                    throw new NullPointerException("response.getBody() == null");
                }
                if (!s.isUnsubscribed()) {
                    s.onNext(response);
                }
            }

            @Override
            public void onCompleted() {
                if (!s.isUnsubscribed()) {
                    s.onCompleted();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (!s.isUnsubscribed()) {
                    s.onError(e);
                }
            }
        };
    }
}
