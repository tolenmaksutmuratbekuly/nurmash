package com.nurmash.nurmash.util.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import org.solovyev.android.views.llm.LinearLayoutManager;

/**
 * This class is just a convenient rename of
 * {@link org.solovyev.android.views.llm.LinearLayoutManager org.solovyev.android.views.llm.LinearLayoutManager},
 * to avoid confusing it with {@link android.support.v7.widget.LinearLayoutManager}.
 */
public class WrapContentLinearLayoutManager extends LinearLayoutManager {
    public WrapContentLinearLayoutManager(Context context) {
        super(context);
    }

    public WrapContentLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public WrapContentLinearLayoutManager(RecyclerView view) {
        super(view);
    }

    public WrapContentLinearLayoutManager(RecyclerView view, int orientation, boolean reverseLayout) {
        super(view, orientation, reverseLayout);
    }
}
