package com.nurmash.nurmash.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class ToastUtils {
    public static Toast centeredToast(Context context, @StringRes int resId, int duration) {
        Toast toast = Toast.makeText(context, resId, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        return toast;
    }
}
