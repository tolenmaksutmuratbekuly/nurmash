package com.nurmash.nurmash.util.leakcanary;

import com.nurmash.nurmash.util.Validate;
import com.squareup.leakcanary.RefWatcher;

public class GlobalRefWatcher {
    private static RefWatcher instance = null;

    public static void setInstance(RefWatcher newInstance) {
        Validate.runningOnUiThread();
        if (instance != null) {
            throw new IllegalStateException("App global RefWatcher instance is already set.");
        }
        instance = newInstance == null ? RefWatcher.DISABLED : newInstance;
    }

    public static RefWatcher getInstance() {
        return instance;
    }
}
