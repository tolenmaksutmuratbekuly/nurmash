package com.nurmash.nurmash.util.retrofit;

import android.net.Uri;

import com.nurmash.nurmash.model.exception.UserCancelledRequestException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import retrofit.mime.TypedFile;
import rx.Observable;
import rx.subjects.BehaviorSubject;

// TODO: 20/01/2016 Come up with a better way to track file upload progress, than tracking it directly from target file.
public class ObservableTypedFile extends TypedFile {
    private static final int BUFFER_SIZE = 4096;

    private volatile boolean uploadCancelled;
    private final BehaviorSubject<Integer> progressSubject = BehaviorSubject.create(0);
    private final long totalLength;
    private int lastProgress;

    public static ObservableTypedFile withPhotoUri(Uri fileUri) {
        return new ObservableTypedFile("application/octet-stream", new File(fileUri.getPath()));
    }

    public ObservableTypedFile(String mimeType, File file) {
        super(mimeType, file);
        this.totalLength = file.length();
    }

    public void setCancelled() {
        uploadCancelled = true;
    }

    public Observable<Integer> getProgressObservable() {
        return progressSubject.asObservable().distinctUntilChanged();
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        if (totalLength == 0) {
            throw new IOException("Zero length file.");
        }

        if (lastProgress != 0) {
            lastProgress = 0;
            progressSubject.onNext(0);
        }
        byte[] buffer = new byte[BUFFER_SIZE];
        FileInputStream in = new FileInputStream(file());
        long totalRead = 0;

        try {
            int read;
            while ((read = in.read(buffer)) != -1) {
                if (uploadCancelled) throw new UserCancelledRequestException();
                out.write(buffer, 0, read);
                totalRead += read;
                onProgress(totalRead);
            }
            progressSubject.onCompleted();
        } finally {
            in.close();
        }
    }

    private void onProgress(long read) {
        int newProgress = (int) (100 * read / totalLength);
        if (newProgress > lastProgress) {
            lastProgress = newProgress;
            progressSubject.onNext(lastProgress);
        }
    }
}
