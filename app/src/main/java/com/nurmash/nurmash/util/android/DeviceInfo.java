package com.nurmash.nurmash.util.android;

import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;

import com.nurmash.nurmash.BuildConfig;
import com.nurmash.nurmash.R;

public class DeviceInfo {
    public final String OS = "android";
    public final String OS_VERSION = Build.VERSION.RELEASE;
    public final String APP_VERSION = BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";
    public final String DEVICE_ID;
    public final String DEVICE_TYPE;

    public DeviceInfo(Context context) {
        boolean isTablet = context.getResources().getBoolean(R.bool.is_tablet);
        DEVICE_TYPE = isTablet ? "tablet" : "mobile";
        DEVICE_ID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public String getVersionName() {
        return BuildConfig.VERSION_NAME;
    }
}
