package com.nurmash.nurmash.util;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.nurmash.nurmash.model.exception.SDCardNotMountedException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CameraUtils {
    public static Intent newGalleryIntent() {
        Intent result = new Intent(Intent.ACTION_PICK);
        result.setType("image/*");
        return result;
    }

    public static Intent newCameraIntent(Uri outputFileUri) {
        Intent result = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        result.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        return result;
    }

    public static Intent newCropIntent(Uri inputFileUri, Uri outputFileUri, boolean needAspect) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(inputFileUri, "image/*");
        cropIntent.putExtra("crop", "true");
        if (needAspect) {
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);
        }
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        return cropIntent;
    }

    public static boolean isCameraAvailable(PackageManager packageManager) {
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /**
     * Create a File for saving an image
     */
    public static Uri getTemporaryImageFileUri() throws IOException, SDCardNotMountedException {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        if (!isExternalStorageWritable()) {
            throw new SDCardNotMountedException();
        }

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Nurmash");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            throw new IOException();
        }

        // Create a media file name
        //noinspection SpellCheckingInspection
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String fileName = "IMG_" + timeStamp;
        File imageFile = File.createTempFile(fileName, ".jpg", mediaStorageDir);
        return Uri.fromFile(imageFile);
    }
}
