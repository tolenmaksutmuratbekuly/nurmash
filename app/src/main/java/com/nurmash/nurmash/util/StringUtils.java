package com.nurmash.nurmash.util;

public class StringUtils {
    public static String trim(CharSequence s) {
        return s != null ? s.toString().trim() : null;
    }

    public static int length(String s) {
        return s == null ? 0 : s.length();
    }

    public static boolean isDigits(CharSequence s) {
        if (s == null) {
            return false;
        }
        for (int i = 0; i < s.length(); ++i) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
