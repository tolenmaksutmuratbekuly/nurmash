package com.nurmash.nurmash.util.annotation;

import android.support.annotation.StringDef;

import com.nurmash.nurmash.util.ApiUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({ApiUtils.FILTER_NEW_PROMOTIONS, ApiUtils.FILTER_MY_PROMOTIONS, ApiUtils.FILTER_PAST_PROMOTIONS})
@Retention(RetentionPolicy.SOURCE)
public @interface PromoFilter {
}
