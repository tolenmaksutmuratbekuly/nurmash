package com.nurmash.nurmash.util.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.ColorInt;

import com.squareup.picasso.Transformation;

import java.util.Locale;

public class CircleTransformation implements Transformation {
    private final String key;
    private final int borderWidth;
    private final int borderColor;

    /**
     * Circle transformation without border.
     */
    public CircleTransformation() {
        this(0, 0);
    }

    /**
     * Circle transformation with border.
     */
    public CircleTransformation(int borderWidth, @ColorInt int borderColor) {
        this.borderWidth = Math.max(0, borderWidth);
        this.borderColor = borderColor;
        this.key = String.format(Locale.US, "circle_%d_#%08x", borderWidth, borderColor);
    }

    @Override
    public Bitmap transform(Bitmap source) {
        // Copied from http://stackoverflow.com/a/26112408

        if (source == null || source.isRecycled()) {
            return null;
        }

        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r - borderWidth, paint);

        squaredBitmap.recycle();

        // Draw border if necessary
        if (borderWidth > 0) {
            Paint borderPaint = new Paint();
            borderPaint.setColor(borderColor);
            borderPaint.setStyle(Paint.Style.STROKE);
            borderPaint.setAntiAlias(true);
            borderPaint.setStrokeWidth(borderWidth);
            canvas.drawCircle(r, r, r - borderWidth, borderPaint);
        }

        return bitmap;
    }

    @Override
    public String key() {
        return key;
    }
}
