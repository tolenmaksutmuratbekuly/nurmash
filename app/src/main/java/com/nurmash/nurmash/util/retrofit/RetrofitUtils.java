package com.nurmash.nurmash.util.retrofit;

import android.util.Base64;

import retrofit.RetrofitError;

public class RetrofitUtils {
    public static String createBasicAuthHeader(String username, String password) {
        if (username == null || password == null) {
            return null;
        }
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }

    public static boolean isHttpError(Throwable e, int status) {
        return (e instanceof RetrofitError) && checkStatus((RetrofitError) e, status);
    }

    public static boolean isHttpUnauthorizedError(Throwable e) {
        return isHttpError(e, 401);
    }

    public static boolean isHttpForbiddenError(Throwable e) {
        return isHttpError(e, 403);
    }

    private static boolean checkStatus(RetrofitError e, int status) {
        return e != null && e.getResponse() != null && e.getResponse().getStatus() == status;
    }
}
