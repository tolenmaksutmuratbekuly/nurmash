package com.nurmash.nurmash.util.android;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import com.nurmash.nurmash.mvp.helpers.ClipboardHelper;

import static android.content.Context.CLIPBOARD_SERVICE;

public class AndroidClipboardHelper implements ClipboardHelper {
    private Context context;

    public AndroidClipboardHelper(Context context) {
        this.context = context;
    }

    private ClipboardManager getClipboardManager() {
        return (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
    }

    @Override
    public void copyText(CharSequence text) {
        getClipboardManager().setPrimaryClip(ClipData.newPlainText(null, text));
    }
}
