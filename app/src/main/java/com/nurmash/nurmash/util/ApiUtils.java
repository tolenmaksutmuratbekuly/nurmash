package com.nurmash.nurmash.util;

@Deprecated
public class ApiUtils {
    // TODO: 2/29/16 Backend does not use filters anymore, so we need to remove them from our app
    @Deprecated public static final String FILTER_NEW_PROMOTIONS = "nin";
    @Deprecated public static final String FILTER_MY_PROMOTIONS = "in";
    @Deprecated public static final String FILTER_PAST_PROMOTIONS = "completed";
}
