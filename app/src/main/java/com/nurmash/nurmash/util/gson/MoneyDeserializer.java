package com.nurmash.nurmash.util.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.nurmash.nurmash.model.Money;

import java.lang.reflect.Type;

public class MoneyDeserializer implements JsonDeserializer<Money> {
    @Override
    public Money deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json == null || json.isJsonNull()) {
            return null;
        }
        return Money.getTypeMoney(json.getAsBigDecimal());
    }
}
