package com.nurmash.nurmash.util;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatUtils {
    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("d MMM yyyy", Locale.getDefault());

    public static String formatDisplayDate(Date date) {
        return date != null ? FormatUtils.DISPLAY_DATE_FORMAT.format(date) : "...";
    }

    private static volatile NumberFormat moneyAmountFormat;

    public static NumberFormat getMoneyAmountDisplayFormat() {
        if (moneyAmountFormat == null) {
            synchronized (FormatUtils.class) {
                moneyAmountFormat = NumberFormat.getNumberInstance(Locale.getDefault());
                moneyAmountFormat.setMaximumFractionDigits(2);
                moneyAmountFormat.setMinimumFractionDigits(0);
            }
        }
        return moneyAmountFormat;
    }
}
