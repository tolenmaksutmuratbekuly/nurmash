package com.nurmash.nurmash.util.rx;

/**
 * This wrapper is used for including {@link rx.Subscriber#onError(Throwable)} in the stack trace.
 * Use it inside your implementations of {@link rx.Subscriber#onError(Throwable)} or {@link rx.Observer#onError(Throwable)}
 * before logging the exception. That way you will be able to see in the logs which particular subscriber received the
 * onError call.
 */
public class SubscriberErrorWrapper extends Throwable {
    public SubscriberErrorWrapper(Throwable t) {
        super(t);
    }

    @Override
    public String toString() {
        String message = getLocalizedMessage();
        String name = getClass().getSimpleName();
        if (message == null) {
            return name;
        } else {
            return name + ": " + message;
        }
    }
}
