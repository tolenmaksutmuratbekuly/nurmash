package com.nurmash.nurmash.util;

import android.os.Build;
import android.support.annotation.StyleRes;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import static android.text.TextUtils.isEmpty;

public class ViewUtils {
    public static int getRelativeTopInAncestor(ViewGroup ancestor, View view) {
        if (view == null || view == ancestor) {
            return 0;
        } else {
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                return view.getTop() + getRelativeTopInAncestor(ancestor, (View) parent);
            } else {
                return view.getTop();
            }
        }
    }

    public static ScrollView findScrollViewAncestor(View view) {
        if (view == null) {
            return null;
        } else {
            ViewParent parent = view.getParent();
            if (parent instanceof ScrollView) {
                return (ScrollView) parent;
            } else if (parent instanceof View) {
                return findScrollViewAncestor((View) parent);
            } else {
                return null;
            }
        }
    }

    public static void scrollToView(View view) {
        ScrollView scrollView = findScrollViewAncestor(view);
        if (scrollView != null) {
            scrollView.smoothScrollTo(0, getRelativeTopInAncestor(scrollView, view));
        }
    }

    @SuppressWarnings("deprecation")
    public static void setTextAppearance(TextView textView, @StyleRes int resId) {
        if (Build.VERSION.SDK_INT < 23) {
            textView.setTextAppearance(textView.getContext(), resId);
        } else {
            textView.setTextAppearance(resId);
        }
    }

    public static void moveEditorCursorToEnd(EditText editText) {
        editText.setSelection(editText.getText().length());
    }

    public static void setTextIfDiffers(TextView textView, CharSequence newText) {
        CharSequence oldText = textView.getText();
        if (isEmpty(newText)) {
            if (!isEmpty(oldText)) {
                textView.setText(newText);
            }
        } else if (!newText.equals(oldText)) {
            textView.setText(newText);
        }
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
