package com.nurmash.nurmash.util.listgridviews;

import android.widget.AbsListView;

import timber.log.Timber;

public abstract class EndlessScrollListenerGridListViews implements AbsListView.OnScrollListener {
    private boolean onLoadMoreFromTop;
    private int visibleThreshold = 5;
    private int currentPage = 0;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int startingPageIndex = 0;

    public EndlessScrollListenerGridListViews(boolean onLoadMoreFromTop) {
        this.onLoadMoreFromTop = onLoadMoreFromTop;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }

        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
            currentPage++;
        }

        if (!loading) {
            if (!onLoadMoreFromTop && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                loading = onLoadMore(currentPage + 1, totalItemCount); // BOTTOM Loader
            } else if (onLoadMoreFromTop && firstVisibleItem == 0) {
                loading = onLoadMore(currentPage + 1, totalItemCount); // TOP Loader
            }
        }
    }

    public abstract boolean onLoadMore(int page, int totalItemsCount);

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }
}
