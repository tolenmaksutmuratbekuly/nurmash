package com.nurmash.nurmash.util.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateDeserializer implements JsonDeserializer<Date> {
    private final String[] formats;
    private final SimpleDateFormat[] dateFormats;

    public DateDeserializer(String... formats) {
        if (formats.length == 0) {
            throw new IllegalArgumentException("Need to provide at least one date format.");
        }

        this.formats = formats;
        this.dateFormats = new SimpleDateFormat[formats.length];
        for (int i = 0; i < formats.length; ++i) {
            this.dateFormats[i] = new SimpleDateFormat(formats[i], Locale.US);
            this.dateFormats[i].setTimeZone(TimeZone.getTimeZone("UTC"));
        }
    }

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String dateStr = json.getAsString();
        synchronized (dateFormats) {
            for (DateFormat format : dateFormats) {
                try {
                    return format.parse(dateStr);
                } catch (ParseException ignored) {
                }
            }
        }
        throw new JsonParseException("Couldn't parse date string: \"" + dateStr + "\" " +
                "using one of the supported date formats: " + Arrays.toString(formats));
    }
}
