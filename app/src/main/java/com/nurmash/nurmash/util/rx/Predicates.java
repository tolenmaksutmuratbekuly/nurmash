package com.nurmash.nurmash.util.rx;

import java.util.Collection;

import rx.functions.Func1;

public class Predicates {
    private static final Func1 nonNull = new Func1<Object, Boolean>() {
        @Override
        public Boolean call(Object x) {
            return x != null;
        }
    };

    private static final Func1 nonEmpty = new Func1<Collection, Boolean>() {
        @Override
        public Boolean call(Collection collection) {
            return collection != null && !collection.isEmpty();
        }
    };

    @SuppressWarnings("unchecked")
    public static <T> Func1<T, Boolean> nonNull() {
        return (Func1<T, Boolean>) nonNull;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Collection> Func1<T, Boolean> nonEmpty() {
        return (Func1<T, Boolean>) nonEmpty;
    }
}
