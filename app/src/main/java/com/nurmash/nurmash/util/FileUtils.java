package com.nurmash.nurmash.util;

import android.support.annotation.Nullable;

import java.io.File;

public class FileUtils {
    public static void deleteFileQuietly(@Nullable File file) {
        if (file != null) {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
    }
}
