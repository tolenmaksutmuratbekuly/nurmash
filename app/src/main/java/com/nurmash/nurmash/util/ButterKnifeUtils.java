package com.nurmash.nurmash.util;

import android.view.View;

import butterknife.ButterKnife;

public class ButterKnifeUtils {
    public static final ButterKnife.Setter<View, Integer> VISIBILITY = new ButterKnife.Setter<View, Integer>() {
        @Override
        public void set(View view, Integer visibility, int index) {
            view.setVisibility(visibility);
        }
    };
}
