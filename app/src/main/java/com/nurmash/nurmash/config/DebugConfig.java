package com.nurmash.nurmash.config;

import com.nurmash.nurmash.BuildConfig;

public class DebugConfig {
    public static final boolean DEV_BUILD = BuildConfig.FLAVOR.equals("dev");

    public static final boolean PICASSO_DEBUG_ENABLED = false;

    public static final boolean STETHO_ENABLED = BuildConfig.DEBUG;

    public static final boolean LEAK_CANARY_ENABLED = false;
}
