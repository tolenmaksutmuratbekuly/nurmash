package com.nurmash.nurmash.config;

import android.support.annotation.StringDef;

import com.nurmash.nurmash.BuildConfig;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.nurmash.nurmash.BuildConfig.NURMASH_BASIC_AUTH_PASSWORD;
import static com.nurmash.nurmash.BuildConfig.NURMASH_BASIC_AUTH_USER;
import static com.nurmash.nurmash.BuildConfig.NURMASH_IMAGE_UPLOAD_SUBDOMAIN;
import static com.nurmash.nurmash.util.retrofit.RetrofitUtils.createBasicAuthHeader;

public class NurmashConfig {
    public static final String SCHEME = "https://";
    public static final String DOMAIN = BuildConfig.NURMASH_HOST;
    public static final String API_BASE_URL = SCHEME + "api." + DOMAIN + "/v1";
    public static final String IMAGE_UPLOAD_BASE_URL = SCHEME + NURMASH_IMAGE_UPLOAD_SUBDOMAIN + DOMAIN;
    public static final String IMAGES_BASE_URL = SCHEME + "imgs." + DOMAIN;
    public static final String GO_BASE_URL = SCHEME + "go." + DOMAIN;
    public static final String BASIC_AUTH_HEADER = createBasicAuthHeader(NURMASH_BASIC_AUTH_USER, NURMASH_BASIC_AUTH_PASSWORD);

    // Social networks
    public static final String TWITTER = "twitter";
    public static final String FACEBOOK = "facebook";
    public static final String VK = "vk";
    public static final String GOOGLE = "google";

    @StringDef({FACEBOOK, TWITTER, GOOGLE, VK})
    @Retention(RetentionPolicy.SOURCE)
    public @interface SocialNetwork {
    }

    public static final int COMMENT_BODY_LENGTH_LIMIT = 4096;

    // Withdraw methods
    public static final String PAYPAL = "paypal";
    public static final String WIRE_TRANSFER = "wire";
}
