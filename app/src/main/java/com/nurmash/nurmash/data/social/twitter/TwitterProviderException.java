package com.nurmash.nurmash.data.social.twitter;

import android.os.Bundle;

import com.nurmash.nurmash.util.DebugUtils;

public class TwitterProviderException extends Exception {
    public final ErrorCode errorCode;
    public final int activityResultCode;
    public final Bundle activityDataExtras;

    public TwitterProviderException(ErrorCode errorCode, int activityResultCode, Bundle activityDataExtras) {
        this.errorCode = errorCode;
        this.activityResultCode = activityResultCode;
        this.activityDataExtras = activityDataExtras;
    }

    public TwitterProviderException(ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.activityResultCode = 0;
        this.activityDataExtras = null;
    }

    @Override
    public String getMessage() {
        if (errorCode == ErrorCode.FAILED) {
            return "errorCode = " + errorCode
                    + ", activityResultCode = " + DebugUtils.activityResultCodeToString(activityResultCode)
                    + ", activityDataExtras = " + DebugUtils.bundleToMap(activityDataExtras);
        } else {
            return "errorCode = " + errorCode;
        }
    }

    public enum ErrorCode {
        CANCELED,
        FAILED,
        TWITTER_APP_NOT_AVAILABLE
    }
}
