package com.nurmash.nurmash.data.api.google;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class PlayMarketApiManager {
    public static PlayMarketService createGooglePlayService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://play.google.com")
                .setClient(new OkClient())
                .build();
        return restAdapter.create(PlayMarketService.class);
    }
}
