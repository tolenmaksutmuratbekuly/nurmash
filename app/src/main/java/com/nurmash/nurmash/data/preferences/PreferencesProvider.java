package com.nurmash.nurmash.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static android.text.TextUtils.isEmpty;

public class PreferencesProvider implements AuthPreferences, PushPreferences {
    private static final String USER_ID_KEY = "USER_ID";
    private static final String AUTH_TOKEN_KEY = "AUTH_TOKEN";
    private static final String USER_PROFILE_COMPLETE_KEY = "USER_PROFILE_COMPLETE";
    private static final String HAS_SHOWN_INTRO_KEY = "HAS_SHOWN_INTRO";
    private static final String PUSH_NOTIFICATIONS_ENABLED_KEY = "PUSH_NOTIFICATIONS_ENABLED";
    private static final String PUSH_TOKEN_SENT_TO_SERVER_KEY = "PUSH_TOKEN_SENT_TO_SERVER";
    private static final String HAS_SHOWN_PARTNER_INTRO_KEY = "HAS_SHOWN_PARTNER_INTRO";
    private static final String HAS_SHOWN_PARTNER_QR_INTRO_KEY = "HAS_SHOWN_PARTNER_QR_INTRO";
    private static final String HAS_SHOWN_PARTNER_QR_DESCRIPTION_KEY = "HAS_SHOWN_PARTNER_QR_DESCRIPTION";
    private static final String HAS_SHOWN_PARTNER_INVITE_DESCRIPTION_KEY = "HAS_SHOWN_PARTNER_INVITE_DESCRIPTION";
    private static final String HAS_SHOWN_PARTNER_BALANCE_DESCRIPTION_KEY = "HAS_SHOWN_PARTNER_BALANCE_DESCRIPTION";
    private static final String APP_VERSION_AVAILABILITY_KEY = "VERSION_AVAILABILITY";
    private static final String LOCAL_PHONE_NUMBER = "LOCAL_PHONE_NUMBER";

    private final SharedPreferences sharedPreferences;

    public PreferencesProvider(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearAllPreferences() {
        synchronized (sharedPreferences) {
            // Don't clear the HAS_SHOWN_INTRO key, because we don't want to show intro again when user logs out.
            boolean shownIntro = sharedPreferences.getBoolean(HAS_SHOWN_INTRO_KEY, false);
            sharedPreferences.edit().clear().putBoolean(HAS_SHOWN_INTRO_KEY, shownIntro).apply();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // AuthPreferences implementation
    ///////////////////////////////////////////////////////////////////////////

    public boolean isAuthenticated() {
        return !isEmpty(sharedPreferences.getString(AUTH_TOKEN_KEY, null));
    }

    public void setAuthToken(String authToken) {
        synchronized (sharedPreferences) {
            if (authToken == null) {
                sharedPreferences.edit().remove(AUTH_TOKEN_KEY).apply();
            } else {
                sharedPreferences.edit().putString(AUTH_TOKEN_KEY, authToken).apply();
            }
        }
    }

    public String getAuthToken() {
        return sharedPreferences.getString(AUTH_TOKEN_KEY, null);
    }

    public void setUserProfileComplete(boolean complete) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(USER_PROFILE_COMPLETE_KEY, complete).apply();
        }
    }

    public boolean isUserProfileComplete() {
        return sharedPreferences.getBoolean(USER_PROFILE_COMPLETE_KEY, false);
    }

    public long getUserId() {
        return sharedPreferences.getLong(USER_ID_KEY, 0);
    }

    public void setUserId(long userId) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putLong(USER_ID_KEY, userId).apply();
        }
    }

    @Override
    public boolean hasShownIntro() {
        return sharedPreferences.getBoolean(HAS_SHOWN_INTRO_KEY, false);
    }

    @Override
    public void setShownIntro(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(HAS_SHOWN_INTRO_KEY, shown).apply();
        }
    }

    @Override
    public boolean hasShownPartnerIntro() {
        return sharedPreferences.getBoolean(HAS_SHOWN_PARTNER_INTRO_KEY, false);
    }

    @Override
    public void setShownPartnerIntro(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(HAS_SHOWN_PARTNER_INTRO_KEY, shown).apply();
        }
    }

    @Override
    public boolean hasShownPartnerQRIntro() {
        return sharedPreferences.getBoolean(HAS_SHOWN_PARTNER_QR_INTRO_KEY, false);
    }

    @Override
    public void setShownPartnerQRIntro(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(HAS_SHOWN_PARTNER_QR_INTRO_KEY, shown).apply();
        }
    }

    @Override
    public boolean hasShownQRDescription() {
        return sharedPreferences.getBoolean(HAS_SHOWN_PARTNER_QR_DESCRIPTION_KEY, false);
    }

    @Override
    public void setShownQRDescription(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(HAS_SHOWN_PARTNER_QR_DESCRIPTION_KEY, shown).apply();
        }
    }

    @Override
    public boolean hasShownInviteDescription() {
        return sharedPreferences.getBoolean(HAS_SHOWN_PARTNER_INVITE_DESCRIPTION_KEY, false);
    }

    @Override
    public void setShownInviteDescription(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(HAS_SHOWN_PARTNER_INVITE_DESCRIPTION_KEY, shown).apply();
        }
    }

    @Override
    public boolean hasShownBalanceDescription() {
        return sharedPreferences.getBoolean(HAS_SHOWN_PARTNER_BALANCE_DESCRIPTION_KEY, false);
    }

    @Override
    public void setShownBalanceDescription(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(HAS_SHOWN_PARTNER_BALANCE_DESCRIPTION_KEY, shown).apply();
        }
    }

    @Override
    public String getAppAvailabilityVersion() {
        return sharedPreferences.getString(APP_VERSION_AVAILABILITY_KEY, "");
    }

    @Override
    public void setAppAvailability(String version) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(APP_VERSION_AVAILABILITY_KEY, version).apply();
        }
    }

    @Override
    public String getPhoneNumber() {
        return sharedPreferences.getString(LOCAL_PHONE_NUMBER, "");
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(LOCAL_PHONE_NUMBER, phoneNumber).apply();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PushPreferences implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isPushNotificationsEnabled() {
        return sharedPreferences.getBoolean(PUSH_NOTIFICATIONS_ENABLED_KEY, true);
    }

    @Override
    public void setPushNotificationsEnabled(boolean enabled) {
        synchronized (sharedPreferences) {
            if (!sharedPreferences.contains(PUSH_NOTIFICATIONS_ENABLED_KEY) ||
                    enabled != sharedPreferences.getBoolean(PUSH_NOTIFICATIONS_ENABLED_KEY, false)) {
                sharedPreferences.edit()
                        .putBoolean(PUSH_NOTIFICATIONS_ENABLED_KEY, enabled)
                        .remove(PUSH_TOKEN_SENT_TO_SERVER_KEY)
                        .apply();
            }
        }
    }

    @Override
    public boolean isPushTokenSentToServer() {
        return sharedPreferences.getBoolean(PUSH_TOKEN_SENT_TO_SERVER_KEY, false);
    }

    @Override
    public void setPushTokenSentToServer(boolean sent) {
        synchronized (sharedPreferences) {
            if (isAuthenticated() && isPushNotificationsEnabled()) {
                sharedPreferences.edit().putBoolean(PUSH_TOKEN_SENT_TO_SERVER_KEY, sent).apply();
            }
        }
    }
}
