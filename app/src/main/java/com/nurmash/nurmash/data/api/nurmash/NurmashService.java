package com.nurmash.nurmash.data.api.nurmash;

import com.nurmash.nurmash.model.json.ApiResponse;
import com.nurmash.nurmash.model.json.AuthResult;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.FeedItem;
import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.model.json.JackpotList;
import com.nurmash.nurmash.model.json.LikesItem;
import com.nurmash.nurmash.model.json.MobappCompatible;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PartnerInvitations;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.json.UserLoad;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.RegionsResult;
import com.nurmash.nurmash.model.json.prizes.PrizeInfo;

import java.util.List;
import java.util.Map;

import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

public interface NurmashService {
    @FormUrlEncoded
    @POST("/auth.facebook")
    Observable<ApiResponse<AuthResult>> authFacebook(@Field("token") String token,
                                                     @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST("/auth.google")
    Observable<ApiResponse<AuthResult>> authGoogle(@Field("token") String token,
                                                   @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST("/auth.vk")
    Observable<ApiResponse<AuthResult>> authVK(@Field("token") String token,
                                               @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST("/auth.facebook")
    Observable<ApiResponse<AuthResult>> linkFacebookAccount(@Field("auth_token") String authToken,
                                                            @Field("token") String token);

    @FormUrlEncoded
    @POST("/auth.google")
    Observable<ApiResponse<AuthResult>> linkGoogleAccount(@Field("auth_token") String authToken,
                                                          @Field("token") String token);

    @FormUrlEncoded
    @POST("/auth.vk")
    Observable<ApiResponse<AuthResult>> linkVKAccount(@Field("auth_token") String authToken,
                                                      @Field("token") String token);

    @FormUrlEncoded
    @POST("/push.token")
    Observable<ApiResponse<Void>> sendPushToken(@Field("auth_token") String authToken,
                                                @Field("os") String os,
                                                @Field("device_id") String deviceId,
                                                @Field("token") String pushToken);

    @GET("/promo.list")
    Observable<ApiResponse<List<Promotion>>> getPromoList(@Query("auth_token") String authToken,
                                                          @Query("page") int page,
                                                          @Query("items_per_page") int itemsPerPage);

    @GET("/promo.list.completed")
    Observable<ApiResponse<List<Promotion>>> getCompletedPromoList(@Query("auth_token") String authToken,
                                                                   @Query("page") int page,
                                                                   @Query("items_per_page") int itemsPerPage);

    @GET("/promo.list.my")
    Observable<ApiResponse<List<Promotion>>> getMyPromoList(@Query("auth_token") String authToken,
                                                            @Query("page") int page,
                                                            @Query("items_per_page") int itemsPerPage);

    @GET("/promo.list.soon")
    Observable<ApiResponse<List<Promotion>>> getSoonPromoList(@Query("auth_token") String authToken,
                                                              @Query("page") int page,
                                                              @Query("items_per_page") int itemsPerPage);

    @GET("/promo.item")
    Observable<ApiResponse<Promotion>> getPromotion(@Query("auth_token") String authToken,
                                                    @Query("promo_id") long promoId);

    @GET("/promo.competitor.list")
    Observable<ApiResponse<List<Competitor>>> getCompetitorList(@Query("auth_token") String authToken,
                                                                @Query("promo_id") long promoId,
                                                                @Query("display") String display,
                                                                @Query("page") int page,
                                                                @Query("items_per_page") int itemsPerPage);

    @GET("/promo.competitor.me")
    Observable<ApiResponse<Competitor>> getCompetitorMe(@Query("auth_token") String authToken,
                                                        @Query("promo_id") long promoId);

    @GET("/promo.competitor.item")
    Observable<ApiResponse<Competitor>> getCompetitorItem(@Query("auth_token") String authToken,
                                                          @Query("competitor_id") long competitorId);

    @FormUrlEncoded
    @POST("/step.init")
    Observable<ApiResponse<Competitor>> stepInit(@Field("auth_token") String authToken,
                                                 @Field("promo_id") long promoId,
                                                 @FieldMap Map<String, String> extraParams);

    @FormUrlEncoded
    @POST("/step.video")
    Observable<ApiResponse<Competitor>> stepVideo(@Field("auth_token") String authToken,
                                                  @Field("promo_id") long promoId,
                                                  @FieldMap Map<String, String> extraParams);

    @FormUrlEncoded
    @POST("/step.sharing")
    Observable<ApiResponse<Competitor>> stepShare(@Field("auth_token") String authToken,
                                                  @Field("promo_id") long promoId,
                                                  @Field("share") String shareName,
                                                  @Field("photo") String photoHash,
                                                  @Field("description") String description,
                                                  @FieldMap Map<String, String> extraParams);

    @FormUrlEncoded
    @POST("/step.survey")
    Observable<ApiResponse<Competitor>> stepSurvey(@Field("auth_token") String authToken,
                                                   @Field("promo_id") long promoId,
                                                   @Field("answers") String answerIds,
                                                   @FieldMap Map<String, String> extraParams);

    @FormUrlEncoded
    @POST("/step.survey")
    Observable<ApiResponse<Competitor>> stepSurveyOld(@Field("auth_token") String authToken,
                                                      @Field("promo_id") long promoId,
                                                      @Field("answer") String answerIds,
                                                      @FieldMap Map<String, String> extraParams);

    @FormUrlEncoded
    @POST("/step.last")
    Observable<ApiResponse<Competitor>> stepLast(@Field("auth_token") String authToken,
                                                 @Field("promo_id") long promoId);

    @GET("/photo.comments")
    Observable<ApiResponse<List<Comment>>> getCommentList(@Query("auth_token") String authToken,
                                                          @Query("competitor_id") long competitorId,
                                                          @Query("page") int page,
                                                          @Query("items_per_page") int itemsPerPage);

    @FormUrlEncoded
    @POST("/photo.comments")
    Observable<ApiResponse<Comment>> postComment(@Field("auth_token") String authToken,
                                                 @Field("competitor_id") long competitorId,
                                                 @Field("body") String body,
                                                 @Field("reply_to") Long replyCommentId);

    @FormUrlEncoded
    @POST("/photo.like")
    Observable<ApiResponse<Void>> likePhoto(@Field("auth_token") String authToken,
                                            @Field("competitor_id") long competitorId);

    @FormUrlEncoded
    @POST("/photo.complaint")
    Observable<ApiResponse<Void>> reportPhoto(@Field("auth_token") String authToken,
                                              @Field("competitor_id") long competitorId,
                                              @Field("description") String description);

    @FormUrlEncoded
    @POST("/comment.complaint")
    Observable<ApiResponse<Void>> reportComment(@Field("auth_token") String authToken,
                                                @Field("competitor_id") long competitorId,
                                                @Field("comment_id") long commentId,
                                                @Field("description") String description);

    @GET("/profile.me")
    Observable<ApiResponse<UserLoad>> getMyProfile(@Query("auth_token") String authToken);

    @FormUrlEncoded
    @POST("/profile.edit")
    Observable<ApiResponse<User>> editMyProfile(@FieldMap Map<String, String> fields);

    @GET("/profile.item")
    Observable<ApiResponse<User>> getUserProfile(@Query("auth_token") String authToken,
                                                 @Query("user_id") long userId);

    @GET("/profile.photos")
    Observable<ApiResponse<List<Competitor>>> getMyPhotos(@Query("auth_token") String authToken,
                                                          @Query("page") int page,
                                                          @Query("items_per_page") int itemsPerPage);

    @GET("/profile.photos")
    Observable<ApiResponse<List<Competitor>>> getUserPhotos(@Query("auth_token") String authToken,
                                                            @Query("user_id") long userId,
                                                            @Query("page") int page,
                                                            @Query("items_per_page") int itemsPerPage);

    @GET("/profile.feed")
    Observable<ApiResponse<List<FeedItem>>> getMyFeed(@Query("auth_token") String authToken,
                                                      @Query("page") int page,
                                                      @Query("items_per_page") int itemsPerPage);

    @GET("/geo.countries")
    Observable<ApiResponse<List<Country>>> getCountries(@Query("auth_token") String authToken);

    @GET("/geo.regions")
    Observable<ApiResponse<RegionsResult>> getRegions(@Query("auth_token") String authToken,
                                                      @Query("country_id") long countryId);

    @GET("/geo.cities")
    Observable<ApiResponse<List<City>>> getCities(@Query("auth_token") String authToken,
                                                  @Query("region_id") long regionId);

    @GET("/prize.info")
    Observable<ApiResponse<PrizeInfo>> getPrizeInfo(@Query("auth_token") String authToken);


    @FormUrlEncoded
    @POST("/withdraw")
    Observable<ApiResponse<Void>> withdrawMoney(@Field("auth_token") String authToken,
                                                @FieldMap Map<String, String> params);

    @GET("/promo.competitor.likes")
    Observable<ApiResponse<List<LikesItem>>> getCompetitorLikes(@Query("auth_token") String authToken,
                                                                @Query("page") int page,
                                                                @Query("items_per_page") int itemsPerPage,
                                                                @Query("competitor_id") long competitor_id);

    @GET("/profile.verify.status")
    Observable<ApiResponse<ProfileVerifyStatus>> getProfileVerifyStatus(@Query("auth_token") String authToken);

    @GET("/invite.bonus")
    Observable<ApiResponse<PartnerBalance>> getPartnerProgramBalance(@Query("auth_token") String authToken);

    @GET("/invite.list")
    Observable<ApiResponse<PartnerInvitations>> getPartnerInvitations(@Query("auth_token") String authToken,
                                                                      @Query("page") int page,
                                                                      @Query("items_per_page") int itemsPerPage,
                                                                      @Query("withdrawn") int withdrawn);

    @GET("/invite.available")
    Observable<ApiResponse<InviteAvailable>> isInviteAvailable(@Query("auth_token") String authToken);

    @FormUrlEncoded
    @POST("/invite")
    Observable<ApiResponse<Void>> inviteUserQRCode(@Field("auth_token") String authToken,
                                                   @Field("user_id") String scannedUserId);

    @GET("/mobapp.compatible")
    Observable<ApiResponse<MobappCompatible>> getMobappCompatibility(@Query("auth_token") String authToken, @Query("version") String version, @Query("os") String os);

    @GET("/prize.list")
    Observable<ApiResponse<List<PrizeItem>>> getPrizeList(@Query("auth_token") String authToken,
                                                          @Query("page") int page,
                                                          @Query("items_per_page") int itemsPerPage);

    @GET("/prize.jackpot.list")
    Observable<ApiResponse<JackpotList>> getJackpotList(@Query("auth_token") String authToken);

    @FormUrlEncoded
    @POST("/invite.withdraw")
    Observable<ApiResponse<Void>> withdrawInviteBonus(@Field("auth_token") String authToken,
                                                      @FieldMap Map<String, String> params);

}
