package com.nurmash.nurmash.data.social;

import rx.Observable;

public interface AuthProvider {
    Observable<String> getTokenForAuth();
}
