package com.nurmash.nurmash.data.api.nurmash;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nurmash.nurmash.BuildConfig;
import com.nurmash.nurmash.config.DebugConfig;
import com.nurmash.nurmash.config.NurmashConfig;
import com.nurmash.nurmash.data.api.nurmash.photo.PhotoDownloadService;
import com.nurmash.nurmash.data.api.nurmash.photo.PhotoUploadService;
import com.nurmash.nurmash.model.BackendDateFormats;
import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.util.gson.DateDeserializer;
import com.nurmash.nurmash.util.gson.MoneyDeserializer;
import com.squareup.okhttp.OkHttpClient;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class NurmashApiManager {
    public static NurmashService createNurmashService() {
        // Setup Json parser
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer(BackendDateFormats.ALL))
                .registerTypeAdapter(Money.class, new MoneyDeserializer())
                .create();
        // Http client
        OkHttpClient httpClient = createDefaultClient();
        if (DebugConfig.STETHO_ENABLED) {
            // If Stetho is enabled, StethoInterceptor allows monitoring network packets in Chrome Dev Tools on your
            // PC through chrome://inspect
            httpClient.networkInterceptors().add(new StethoInterceptor());
        }
        // Setup Retrofit service
        RestAdapter.Builder adapterBuilder = new RestAdapter.Builder();
        adapterBuilder.setEndpoint(NurmashConfig.API_BASE_URL);
        adapterBuilder.setClient(new OkClient(httpClient));
        adapterBuilder.setConverter(new GsonConverter(gson));
        adapterBuilder.setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC : RestAdapter.LogLevel.NONE);
        if (NurmashConfig.BASIC_AUTH_HEADER != null) {
            adapterBuilder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Authorization", NurmashConfig.BASIC_AUTH_HEADER);
                }
            });
        }
        return adapterBuilder.build().create(NurmashService.class);
    }

    public static PhotoUploadService createPhotoUploadService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(NurmashConfig.IMAGE_UPLOAD_BASE_URL)
                .setClient(new OkClient(createDefaultClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC : RestAdapter.LogLevel.NONE)
                .build();
        return restAdapter.create(PhotoUploadService.class);
    }

    public static PhotoUploadService createPhotoDocumentUploadService() {
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setEndpoint(NurmashConfig.API_BASE_URL)
//                .setClient(new OkClient(createDefaultClient()))
//                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC : RestAdapter.LogLevel.NONE)
//                .build();
//        return restAdapter.create(PhotoUploadService.class);

        // Setup Json parser
        Gson gson = new GsonBuilder()
                .create();

        // Setup Retrofit service
        RestAdapter.Builder adapterBuilder = new RestAdapter.Builder();
        adapterBuilder.setEndpoint(NurmashConfig.API_BASE_URL);
        adapterBuilder.setClient(new OkClient(createDefaultClient()));
        adapterBuilder.setConverter(new GsonConverter(gson));
        adapterBuilder.setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC : RestAdapter.LogLevel.NONE);
        if (NurmashConfig.BASIC_AUTH_HEADER != null) {
            adapterBuilder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Authorization", NurmashConfig.BASIC_AUTH_HEADER);
                }
            });
        }
        return adapterBuilder.build().create(PhotoUploadService.class);
    }

    public static PhotoDownloadService createPhotoDownloadService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(NurmashConfig.IMAGES_BASE_URL)
                .setClient(new OkClient(createDefaultClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC : RestAdapter.LogLevel.NONE)
                .build();
        return restAdapter.create(PhotoDownloadService.class);
    }

    private static OkHttpClient createDefaultClient() {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.setConnectTimeout(1, TimeUnit.MINUTES);
        httpClient.setReadTimeout(1, TimeUnit.MINUTES);
        httpClient.setWriteTimeout(1, TimeUnit.MINUTES);
        return httpClient;
    }
}
