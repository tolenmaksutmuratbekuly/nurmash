package com.nurmash.nurmash.data.api.nurmash.photo;

import com.nurmash.nurmash.model.json.FileUploadResult;

import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import rx.Observable;

public interface PhotoUploadService {
    @Multipart
    @POST("/upload_sq")
    Observable<FileUploadResult> uploadPhoto(@Part("file") TypedFile file);

    @Multipart
    @POST("/profile.verify")
    Observable<FileUploadResult> profileVerifyPassport(
            @Query("auth_token") String authToken,
            @Part("file1") TypedFile file);

    @Multipart
    @POST("/profile.verify")
    Observable<FileUploadResult> profileVerifyIDCard(@Query("auth_token") String authToken,
                                                     @Part("file1") TypedFile file1,
                                                     @Part("file2") TypedFile file2);
}
