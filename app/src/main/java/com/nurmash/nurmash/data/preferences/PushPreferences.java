package com.nurmash.nurmash.data.preferences;

public interface PushPreferences {
    boolean isPushNotificationsEnabled();

    void setPushNotificationsEnabled(boolean enabled);

    boolean isPushTokenSentToServer();

    void setPushTokenSentToServer(boolean sent);
}
