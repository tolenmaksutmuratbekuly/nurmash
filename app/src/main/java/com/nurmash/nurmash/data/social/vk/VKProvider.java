package com.nurmash.nurmash.data.social.vk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.nurmash.nurmash.data.social.AuthProvider;
import com.nurmash.nurmash.data.social.ShareProvider;
import com.nurmash.nurmash.util.Validate;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKShareDialog;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import timber.log.Timber;

public class VKProvider implements AuthProvider, ShareProvider {
    private final AppCompatActivity activity;

    private VKCallback<VKAccessToken> loginCallback;

    public VKProvider(AppCompatActivity activity) {
        this.activity = activity;
    }

    /**
     * <b>Don't forget to call this method from {@link Activity#onActivityResult(int, int, Intent)}</b>
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (loginCallback != null && VKSdk.onActivityResult(requestCode, resultCode, data, loginCallback)) {
            loginCallback = null;
            return true;
        }
        return false;
    }

    @Override
    public Observable<String> getTokenForAuth() {
        return obtainPermissions(VKScope.EMAIL);
    }

    public void logout() {
        VKSdk.logout();
    }

    @Override
    public Observable<Object> share(@NonNull final Uri url, @Nullable final Uri imageUrl, @Nullable final String text) {
        return obtainPermissions(VKScope.WALL, VKScope.PHOTOS)
                // Delay for 100ms before showing share dialog fragment to allow activity to fully resume after sign-in
                // process. Otherwise, FragmentManager might throw an exception because we are trying to commit a transaction
                // before Activity#onResume() was called.
                .delay(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Func1<String, Observable<Object>>() {
                    @Override
                    public Observable<Object> call(String s) {
                        return shareInternal(url, imageUrl, text);
                    }
                });
    }

    private Observable<String> obtainPermissions(final String... scopes) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(final Subscriber<? super String> subscriber) {
                Validate.runningOnUiThread();

                VKAccessToken currentToken = VKAccessToken.currentToken();
                if (currentToken != null && currentToken.hasScope(scopes)) {
                    subscriber.onNext(currentToken.accessToken);
                    subscriber.onCompleted();
                    return;
                }

                loginCallback = new VKCallback<VKAccessToken>() {
                    @Override
                    public void onResult(VKAccessToken token) {
                        subscriber.onNext(token.accessToken);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onError(VKError error) {
                        subscriber.onError(new VKProviderException(error));
                    }
                };
                VKSdk.login(activity, scopes);
            }
        });
    }

    private Observable<Object> shareInternal(@NonNull final Uri url, @Nullable final Uri imageUrl, @Nullable final String text) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(final Subscriber<? super Object> subscriber) {
                Validate.runningOnUiThread();

                Bitmap bitmap = null;
                if (imageUrl != null) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), imageUrl);
                    } catch (IOException e) {
                        Timber.e(e, null);
                    }
                }

                VKShareDialogBuilder dialog = new VKShareDialogBuilder()
                        .setText(text)
                        .setAttachmentLink("", url.toString())
                        .setShareDialogListener(new VKShareDialog.VKShareDialogListener() {
                            @Override
                            public void onVkShareComplete(int postId) {
                                subscriber.onNext(postId);
                                subscriber.onCompleted();
                            }

                            @Override
                            public void onVkShareCancel() {
                                subscriber.onError(new VKProviderException(new VKError(VKError.VK_CANCELED)));
                            }

                            @Override
                            public void onVkShareError(VKError error) {
                                subscriber.onError(new VKProviderException(error));
                            }
                        });

                if (bitmap != null) {
                    dialog.setAttachmentImages(new VKUploadImage[]{
                            new VKUploadImage(bitmap, VKImageParameters.pngImage())
                    });
                }

                dialog.show(activity.getSupportFragmentManager(), "VKShareDialog");
            }
        });
    }
}
