package com.nurmash.nurmash.data.preferences;

public interface AuthPreferences {
    boolean isAuthenticated();

    String getAuthToken();

    void setAuthToken(String authToken);

    long getUserId();

    void setUserId(long userId);

    boolean isUserProfileComplete();

    void setUserProfileComplete(boolean complete);

    boolean hasShownIntro();

    void setShownIntro(boolean shown);

    boolean hasShownPartnerIntro();

    void setShownPartnerIntro(boolean shown);

    boolean hasShownPartnerQRIntro();

    void setShownPartnerQRIntro(boolean shown);

    boolean hasShownQRDescription();

    void setShownQRDescription(boolean shown);

    boolean hasShownInviteDescription();

    void setShownInviteDescription(boolean shown);

    boolean hasShownBalanceDescription();

    void setShownBalanceDescription(boolean shown);

    String getAppAvailabilityVersion();

    void setAppAvailability(String version);

    String getPhoneNumber();

    void setPhoneNumber(String phoneNumber);
}
