package com.nurmash.nurmash.data.api.nurmash.photo;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Streaming;
import rx.Observable;

public interface PhotoDownloadService {
    @GET("/{size}/{hash}")
    @Streaming
    Observable<Response> getPhoto(@Path("size") String size, @Path("hash") String hash);
}
