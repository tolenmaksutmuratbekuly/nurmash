package com.nurmash.nurmash.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.nurmash.nurmash.config.NurmashConfig.SocialNetwork;
import com.nurmash.nurmash.data.api.google.PlayMarketApiManager;
import com.nurmash.nurmash.data.api.google.PlayMarketService;
import com.nurmash.nurmash.data.api.nurmash.NurmashApiManager;
import com.nurmash.nurmash.data.api.nurmash.NurmashService;
import com.nurmash.nurmash.data.api.nurmash.photo.PhotoDownloadService;
import com.nurmash.nurmash.data.api.nurmash.photo.PhotoUploadService;
import com.nurmash.nurmash.data.preferences.AuthPreferences;
import com.nurmash.nurmash.data.preferences.PreferencesProvider;
import com.nurmash.nurmash.data.preferences.PushPreferences;
import com.nurmash.nurmash.gcm.NotificationsHelper;
import com.nurmash.nurmash.gcm.PushRegistrationHelper;
import com.nurmash.nurmash.model.AppData;
import com.nurmash.nurmash.model.BackendDateFormats;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.PhotoUrl.PhotoSize;
import com.nurmash.nurmash.model.ProfileEditForm;
import com.nurmash.nurmash.model.exception.CommentBodyTooLongException;
import com.nurmash.nurmash.model.exception.EmptyCommentBodyException;
import com.nurmash.nurmash.model.json.ApiResponse;
import com.nurmash.nurmash.model.json.AuthResult;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.FeedItem;
import com.nurmash.nurmash.model.json.FileUploadResult;
import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.model.json.JackpotList;
import com.nurmash.nurmash.model.json.LikesItem;
import com.nurmash.nurmash.model.json.MobappCompatible;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PartnerInvitations;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.json.UserLoad;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.model.json.geo.RegionsResult;
import com.nurmash.nurmash.model.json.prizes.PrizeInfo;
import com.nurmash.nurmash.model.withdraw.WithdrawForm;
import com.nurmash.nurmash.model.withdraw.WithdrawForm2;
import com.nurmash.nurmash.util.ApiUtils;
import com.nurmash.nurmash.util.Validate;
import com.nurmash.nurmash.util.android.DeviceInfo;
import com.nurmash.nurmash.util.retrofit.ObservableTypedFile;
import com.nurmash.nurmash.util.rx.ResponseBodyValidator;
import com.nurmash.nurmash.util.rx.Transformers;
import com.squareup.okhttp.internal.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

import okio.BufferedSink;
import okio.Okio;
import okio.Source;
import retrofit.client.Response;
import rx.Observable;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.config.NurmashConfig.COMMENT_BODY_LENGTH_LIMIT;
import static com.nurmash.nurmash.model.ParticipationData.ALL_STEPS_DONE;
import static com.nurmash.nurmash.model.ParticipationData.SHARE_STEP;
import static com.nurmash.nurmash.model.ParticipationData.SURVEY_STEP;
import static com.nurmash.nurmash.model.ParticipationData.VIDEO_STEP;
import static com.nurmash.nurmash.util.rx.Predicates.nonNull;

public class DataLayer {
    private static volatile DataLayer instance;

    public static DataLayer getInstance(Context context) {
        if (instance == null) {
            synchronized (DataLayer.class) {
                if (instance == null) instance = new DataLayer(context.getApplicationContext());
            }
        }
        return instance;
    }

    private final NurmashService nurmashService;
    private final PhotoUploadService photoUploadService;
    private final PhotoUploadService photoDocumentUploadService;
    private final PhotoDownloadService photoDownloadService;
    private final PlayMarketService playMarketService;
    private final PreferencesProvider preferencesProvider;
    private final Picasso picasso;
    private final DeviceInfo deviceInfo;
    private final PushRegistrationHelper pushRegistrationHelper;
    private final NotificationsHelper notificationsHelper;
    private User cachedUserProfile;
    private ParticipationData cachedParticipationData;

    private DataLayer(Context context) {
        preferencesProvider = new PreferencesProvider(context);
        nurmashService = NurmashApiManager.createNurmashService();
        photoUploadService = NurmashApiManager.createPhotoUploadService();
        photoDocumentUploadService = NurmashApiManager.createPhotoDocumentUploadService();
        photoDownloadService = NurmashApiManager.createPhotoDownloadService();
        playMarketService = PlayMarketApiManager.createGooglePlayService();
        picasso = Picasso.with(context);
        deviceInfo = new DeviceInfo(context);
        pushRegistrationHelper = new PushRegistrationHelper(context, this);
        notificationsHelper = new NotificationsHelper(context, this);
    }

    public AuthPreferences authPreferences() {
        return preferencesProvider;
    }

    public PushPreferences pushPreferences() {
        return preferencesProvider;
    }

    public PushRegistrationHelper pushRegistrationHelper() {
        return pushRegistrationHelper;
    }

    public NotificationsHelper notificationsHelper() {
        return notificationsHelper;
    }

    private String getAuthToken() {
        return preferencesProvider.getAuthToken();
    }

    private void setCachedUserProfile(User user) {
        cachedUserProfile = user;
        preferencesProvider.setUserProfileComplete(user != null && user.is_profile_complete);
    }

    public User getCachedUserProfile() {
        return cachedUserProfile;
    }

    private synchronized void setCachedParticipationData(ParticipationData data) {
        cachedParticipationData = data;
    }

    public void wipeOut() {
        setCachedUserProfile(null);
        setCachedParticipationData(null);
        pushRegistrationHelper.cancelOperationsInProgress();
        notificationsHelper.clearAllNotifications();
        preferencesProvider.clearAllPreferences();
    }

    public Observable<User> authFacebook(String token) {
        return authenticate(nurmashService.authFacebook(token, deviceInfo.DEVICE_ID));
    }

    public Observable<User> authGoogle(String token) {
        return authenticate(nurmashService.authGoogle(token, deviceInfo.DEVICE_ID));
    }

    public Observable<User> authVK(String token) {
        return authenticate(nurmashService.authVK(token, deviceInfo.DEVICE_ID));
    }

    public Observable<User> authenticate(Observable<ApiResponse<AuthResult>> requestObs) {
        return requestObs
                .compose(Transformers.<AuthResult>responseDataExtractor())
                .map(new Func1<AuthResult, User>() {
                    @Override
                    public User call(AuthResult auth) {
                        preferencesProvider.setAuthToken(auth.token);
                        preferencesProvider.setUserId(auth.user.id);
                        setCachedUserProfile(auth.user);
                        return auth.user;
                    }
                })
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        wipeOut();
                    }
                });
    }

    public Observable<User> linkFacebookAccount(final String token) {
        return linkAccount(new Func1<String, Observable<ApiResponse<AuthResult>>>() {
            @Override
            public Observable<ApiResponse<AuthResult>> call(String authToken) {
                return nurmashService.linkFacebookAccount(authToken, token);
            }
        });
    }

    public Observable<User> linkGoogleAccount(final String token) {
        return linkAccount(new Func1<String, Observable<ApiResponse<AuthResult>>>() {
            @Override
            public Observable<ApiResponse<AuthResult>> call(String authToken) {
                return nurmashService.linkGoogleAccount(authToken, token);
            }
        });
    }

    public Observable<User> linkVKAccount(final String token) {
        return linkAccount(new Func1<String, Observable<ApiResponse<AuthResult>>>() {
            @Override
            public Observable<ApiResponse<AuthResult>> call(String authToken) {
                return nurmashService.linkVKAccount(authToken, token);
            }
        });
    }

    public Observable<User> linkAccount(Func1<String, Observable<ApiResponse<AuthResult>>> requestCreator) {
        String authToken = getAuthToken();
        if (authToken == null) {
            throw new IllegalStateException("Cannot link account while unauthenticated.");
        }
        Observable<ApiResponse<AuthResult>> requestObs = requestCreator.call(authToken);
        return requestObs
                .compose(Transformers.<AuthResult>responseDataExtractor())
                .map(new Func1<AuthResult, User>() {
                    @Override
                    public User call(AuthResult auth) {
                        return auth.user;
                    }
                })
                .doOnNext(new Action1<User>() {
                    @Override
                    public void call(User user) {
                        setCachedUserProfile(user);
                    }
                });
    }

    public Observable<Void> sendPushToken(String gcmToken) {
        return nurmashService.sendPushToken(getAuthToken(), deviceInfo.OS, deviceInfo.DEVICE_ID, gcmToken)
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<UserLoad> getMyProfile(boolean useCached) {
        Observable<UserLoad> profileFetcher = nurmashService.getMyProfile(getAuthToken())
                .compose(Transformers.<UserLoad>responseDataExtractor())
                .doOnNext(new Action1<UserLoad>() {
                    @Override
                    public void call(UserLoad user) {
                        setCachedUserProfile(user);
                    }
                });
        return Observable.just(useCached ? (UserLoad) cachedUserProfile : null)
                .filter(nonNull())
                .concatWith(profileFetcher)
                .take(1);
    }

    public Observable<User> saveProfile(ProfileEditForm form, boolean isRegistration) {
        if (!form.allFieldsValid(isRegistration)) {
            return Observable.error(new Exception("Some fields are not valid."));
        }

        Map<String, String> fields = new ArrayMap<>(13);
        fields.put("auth_token", getAuthToken());
        fields.put("first_name", form.firstName);
        fields.put("last_name", form.lastName);
        fields.put("email", form.email);
        fields.put("birthdate", form.getBirthdateStr(BackendDateFormats.DATE));
        fields.put("gender", Integer.toString(form.gender.intValue));
        fields.put("country_id", Long.toString(form.getCountryId()));
        fields.put("region_id", Long.toString(form.getRegionId()));
        fields.put("city_id", Long.toString(form.getCityId()));
        fields.put("photo", form.photoHash);
        fields.put("display_name", form.displayName.length() > 0
                ? form.displayName
                : form.firstName + " " + (form.lastName.length() > 0 ? form.lastName.substring(0, 1) : form.lastName));

        if (!isRegistration) {
            fields.put("first_name_native", form.nativeFirstName);
            fields.put("last_name_native", form.nativeLastName);
        }

        return nurmashService.editMyProfile(fields)
                .compose(Transformers.<User>responseDataExtractor())
                .doOnNext(new Action1<User>() {
                    @Override
                    public void call(User user) {
                        setCachedUserProfile(user);
                    }
                });
    }

    public Observable<User> getUserProfile(long userId) {
        return nurmashService.getUserProfile(getAuthToken(), userId)
                .compose(Transformers.<User>responseDataExtractor());
    }

    public Observable<List<Competitor>> getUserPhotos(long userId, int page, int itemsPerPage) {
        return nurmashService.getUserPhotos(getAuthToken(), userId, page, itemsPerPage)
                .compose(Transformers.<List<Competitor>>responseDataExtractor());
    }

    public Observable<List<Promotion>> getPromoList(String filter, int page, int itemsPerPage) {
        if (filter == null)
            return Observable.error(new IllegalArgumentException("Invalid promotions filter."));

        switch (filter) {
            case ApiUtils.FILTER_NEW_PROMOTIONS:
                return nurmashService.getPromoList(getAuthToken(), page, itemsPerPage)
                        .compose(Transformers.<List<Promotion>>responseDataExtractor());

            case ApiUtils.FILTER_MY_PROMOTIONS:
                return nurmashService.getMyPromoList(getAuthToken(), page, itemsPerPage)
                        .compose(Transformers.<List<Promotion>>responseDataExtractor());

            case ApiUtils.FILTER_PAST_PROMOTIONS:
                return nurmashService.getCompletedPromoList(getAuthToken(), page, itemsPerPage)
                        .compose(Transformers.<List<Promotion>>responseDataExtractor());

            default:
                return Observable.error(new IllegalArgumentException("Invalid contest filter."));
        }
    }

    public Observable<List<Promotion>> getSoonPromoList(int page, int itemsPerPage) {
        return nurmashService.getSoonPromoList(getAuthToken(), page, itemsPerPage)
                .compose(Transformers.<List<Promotion>>responseDataExtractor());
    }

    public Observable<Promotion> getPromotion(long promoId) {
        return nurmashService.getPromotion(getAuthToken(), promoId)
                .compose(Transformers.<Promotion>responseDataExtractor());
    }

    public Observable<ParticipationData> getParticipationData(final long promoId, boolean useCache) {
        // Setup cached observable
        Observable<ParticipationData> cacheObs;
        if (useCache) {
            cacheObs = Observable.just(cachedParticipationData)
                    .filter(new Func1<ParticipationData, Boolean>() {
                        @Override
                        public Boolean call(ParticipationData data) {
                            return data != null && data.getPromotion().id == promoId;
                        }
                    });
        } else {
            cacheObs = Observable.empty();
        }
        // Setup fetcher from remote service
        Observable<Competitor> competitorFetcher = nurmashService.getCompetitorMe(getAuthToken(), promoId)
                .compose(Transformers.<Competitor>responseDataExtractor());
        Observable<Promotion> promoFetcher = nurmashService.getPromotion(getAuthToken(), promoId)
                .compose(Transformers.<Promotion>responseDataExtractor());
        Observable<ParticipationData> fetchObs = Observable.zip(promoFetcher, competitorFetcher,
                new Func2<Promotion, Competitor, ParticipationData>() {
                    @Override
                    public ParticipationData call(Promotion promotion, Competitor competitor) {
                        return new ParticipationData(promotion, competitor);
                    }
                })
                .doOnNext(new Action1<ParticipationData>() {
                    @Override
                    public void call(ParticipationData data) {
                        setCachedParticipationData(data);
                    }
                });
        // Only if cacheObs is empty, data is going to be fetched from remote service.
        return cacheObs.concatWith(fetchObs).take(1);
    }

    public Observable<Competitor> getCompetitor(long competitorId) {
        return nurmashService.getCompetitorItem(getAuthToken(), competitorId)
                .compose(Transformers.<Competitor>responseDataExtractor());
    }

    public Observable<List<Competitor>> getCompetitorList(long promoId, String display, int page, int itemsPerPage) {
        return nurmashService.getCompetitorList(getAuthToken(), promoId, display, page, itemsPerPage)
                .compose(Transformers.<List<Competitor>>responseDataExtractor());
    }

    public Observable<Void> likePhoto(Competitor competitor) {
        if (competitor.is_liked) {
            return Observable.just(null);
        }
        return nurmashService.likePhoto(getAuthToken(), competitor.id)
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<Void> reportPhoto(long competitorId, Complaint complaint) {
        return nurmashService.reportPhoto(getAuthToken(), competitorId, complaint.getCode())
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<Void> reportComment(long competitorId, long commentId, Complaint complaint) {
        return nurmashService.reportComment(getAuthToken(), competitorId, commentId, complaint.getCode())
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<List<Comment>> getCommentList(long competitorId, int page, int itemsPerPage) {
        return nurmashService.getCommentList(getAuthToken(), competitorId, page, itemsPerPage)
                .compose(Transformers.<List<Comment>>responseDataExtractor());
    }

    public Observable<Comment> postComment(long competitorId, String commentBody, Comment replyComment) {
        if (isEmpty(commentBody)) {
            return Observable.error(new EmptyCommentBodyException());
        }
        if (commentBody.length() > COMMENT_BODY_LENGTH_LIMIT) {
            return Observable.error(new CommentBodyTooLongException(commentBody.length(),
                    COMMENT_BODY_LENGTH_LIMIT));
        }
        Long replyCommentId = replyComment == null ? null : replyComment.id;
        return nurmashService.postComment(getAuthToken(), competitorId, commentBody, replyCommentId)
                .compose(Transformers.<Comment>responseDataExtractor());
    }

    public Observable<List<Competitor>> getMyPhotos(int page, int itemsPerPage) {
        return nurmashService.getMyPhotos(getAuthToken(), page, itemsPerPage)
                .compose(Transformers.<List<Competitor>>responseDataExtractor());
    }

    public Observable<List<FeedItem>> getMyFeed(int page, int itemsPerPage) {
        return nurmashService.getMyFeed(getAuthToken(), page, itemsPerPage)
                .compose(Transformers.<List<FeedItem>>responseDataExtractor());
    }

    public Observable<ParticipationData> commitInitialStep(final ParticipationData data) {
        if (data.getCompetitor() != null) {
            return Observable.just(data);
        }
        return commitStep(data, new Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>>() {
            @Override
            public Observable<ApiResponse<Competitor>> call(String authToken, Map<String, String> extraParams) {
                return nurmashService.stepInit(authToken, data.getPromoId(), extraParams);
            }
        });
    }

    public Observable<ParticipationData> commitVideoStep(final ParticipationData data) {
        if (!data.promoHasStep(VIDEO_STEP) || data.isStepComplete(VIDEO_STEP)) {
            return Observable.just(data);
        }
        return commitStep(data, new Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>>() {
            @Override
            public Observable<ApiResponse<Competitor>> call(String authToken, Map<String, String> extraParams) {
                return nurmashService.stepVideo(authToken, data.getPromoId(), extraParams);
            }
        });
    }

    public Observable<ParticipationData> commitShareStep(final ParticipationData data,
                                                         @SocialNetwork final String socialNetwork,
                                                         final String photoHash,
                                                         final String description) {
        if (!data.promoHasStep(SHARE_STEP) || (data.isStepComplete(SHARE_STEP) && data.hasSharedOn(socialNetwork))) {
            return Observable.just(data);
        }
        return commitStep(data, new Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>>() {
            @Override
            public Observable<ApiResponse<Competitor>> call(String authToken, Map<String, String> extraParams) {
                return nurmashService.stepShare(authToken, data.getPromoId(), socialNetwork, photoHash, description, extraParams);
            }
        });
    }

    public Observable<ParticipationData> commitSurveyStep(final ParticipationData data, final String answerIds) {
        if (!data.promoHasStep(SURVEY_STEP) || data.isStepComplete(SURVEY_STEP)) {
            return Observable.just(data);
        }
        return commitStep(data, new Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>>() {
            @Override
            public Observable<ApiResponse<Competitor>> call(String authToken, Map<String, String> extraParams) {
                return nurmashService.stepSurvey(authToken, data.getPromoId(), answerIds, extraParams);
            }
        });
    }

    public Observable<ParticipationData> commitSurveyStepOld(final ParticipationData data, final String answerId) {
        if (!data.promoHasStep(SURVEY_STEP) || data.isStepComplete(SURVEY_STEP)) {
            return Observable.just(data);
        }
        return commitStep(data, new Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>>() {
            @Override
            public Observable<ApiResponse<Competitor>> call(String authToken, Map<String, String> extraParams) {
                return nurmashService.stepSurveyOld(authToken, data.getPromoId(), answerId, extraParams);
            }
        });
    }

    public Observable<ParticipationData> commitLastStep(final ParticipationData data) {
        if (ALL_STEPS_DONE.equals(data.getNextStep())) {
            return Observable.just(data);
        }
        return commitStep(data, new Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>>() {
            @Override
            public Observable<ApiResponse<Competitor>> call(String authToken, Map<String, String> extraParams) {
                return nurmashService.stepLast(authToken, data.getPromoId());
            }
        });
    }

    public Observable<ParticipationData> commitStep(final ParticipationData data,
                                                    Func2<String, Map<String, String>, Observable<ApiResponse<Competitor>>> requestCreator) {
        String authToken = getAuthToken();
        if (authToken == null) {
            throw new IllegalStateException("Cannot commit step while unauthenticated.");
        }
        Map<String, String> extraParams = new LinkedHashMap<>();
        extraParams.put("device", deviceInfo.DEVICE_TYPE);
        extraParams.put("os", deviceInfo.OS);
        extraParams.put("os_version", deviceInfo.OS_VERSION);
        extraParams.put("app_version", deviceInfo.APP_VERSION);
        return requestCreator.call(authToken, extraParams)
                .compose(Transformers.<Competitor>responseDataExtractor())
                .map(new Func1<Competitor, ParticipationData>() {
                    @Override
                    public ParticipationData call(Competitor competitor) {
                        ParticipationData newData = new ParticipationData(data.getPromotion(), competitor);
                        setCachedParticipationData(newData);
                        return newData;
                    }
                });
    }

    public Observable<List<Country>> getCountryList() {
        return nurmashService.getCountries(getAuthToken())
                .compose(Transformers.<List<Country>>responseDataExtractor());
    }

    public Observable<List<Region>> getRegionList(long countryId) {
        return nurmashService.getRegions(getAuthToken(), countryId)
                .compose(Transformers.<RegionsResult>responseDataExtractor())
                .map(new Func1<RegionsResult, List<Region>>() {
                    @Override
                    public List<Region> call(RegionsResult result) {
                        return Arrays.asList(result.regions);
                    }
                });
    }

    public Observable<List<City>> getNoRegionCityList(long countryId) {
        return nurmashService.getRegions(getAuthToken(), countryId)
                .compose(Transformers.<RegionsResult>responseDataExtractor())
                .map(new Func1<RegionsResult, List<City>>() {
                    @Override
                    public List<City> call(RegionsResult regionsResult) {
                        if (regionsResult == null || regionsResult.no_region_cities == null) {
                            return Collections.emptyList();
                        } else {
                            return Arrays.asList(regionsResult.no_region_cities);
                        }
                    }
                });
    }

    public Observable<List<City>> getCityList(long regionId) {
        return nurmashService.getCities(getAuthToken(), regionId)
                .compose(Transformers.<List<City>>responseDataExtractor());
    }

    public Observable<PrizeInfo> getPrizesData() {
        String authToken = getAuthToken();
        return nurmashService.getPrizeInfo(authToken)
                .compose(Transformers.<PrizeInfo>responseDataExtractor());
    }

    @Deprecated
    public Observable<Void> withdrawMoney(BigDecimal amount, WithdrawForm form) {
        return nurmashService.withdrawMoney(getAuthToken(), form.buildParamsMap(amount))
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<Void> withdrawMoney2(WithdrawForm2 form) {
        return nurmashService.withdrawMoney(getAuthToken(), form.buildParamsMap())
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<FileUploadResult> uploadPhoto(final ObservableTypedFile observableFile) {
        return photoUploadService.uploadPhoto(observableFile)
                .filter(nonNull())
                .doOnUnsubscribe(new Action0() {
                    @Override
                    public void call() {
                        observableFile.setCancelled();
                    }
                });
    }

    public Observable<Bitmap> getPhotoBitmap(String url, Transformation transformation) {
        final RequestCreator requestCreator = picasso.load(url);
        if (transformation != null) {
            requestCreator.transform(transformation);
        }
        // Trigger prefetch
        requestCreator.fetch();
        return Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                return requestCreator.get();
            }
        });
    }

    public Observable<File> downloadPhotoToFile(@PhotoSize String photoSize, String photoHash, final File file) {
        if (photoSize == null) {
            return Observable.error(new IllegalArgumentException("photoSize == null"));
        }
        if (photoHash == null) {
            return Observable.error(new IllegalArgumentException("photoHash == null"));
        }
        if (file == null) {
            return Observable.error(new IllegalArgumentException("file == null"));
        }
        return photoDownloadService.getPhoto(photoSize, photoHash)
                .lift(new ResponseBodyValidator())
                .map(new Func1<Response, File>() {
                    @Override
                    public File call(Response response) {
                        Validate.runningNotOnUiThread();
                        Source source = null;
                        BufferedSink sink = null;
                        try {
                            source = Okio.source(response.getBody().in());
                            sink = Okio.buffer(Okio.sink(file));
                            sink.writeAll(source);
                        } catch (IOException e) {
                            throw Exceptions.propagate(e);
                        } finally {
                            Util.closeQuietly(source);
                            Util.closeQuietly(sink);
                        }
                        return file;
                    }
                });
    }

    public Observable<List<LikesItem>> getCompetitorLikes(int page, int itemsPerPage, long competitor_id) {
        return nurmashService.getCompetitorLikes(getAuthToken(), page, itemsPerPage, competitor_id)
                .compose(Transformers.<List<LikesItem>>responseDataExtractor());
    }

    public Observable<AppData> getPlayMarketAppData(@NonNull final String appId) {
        return playMarketService.getAppDetailsPage(appId, Locale.getDefault().toString())
                .lift(new ResponseBodyValidator())
                .map(new Func1<Response, AppData>() {
                    @Override
                    public AppData call(Response response) {
                        Validate.runningNotOnUiThread();
                        InputStream bodyStream = null;
                        try {
                            bodyStream = response.getBody().in();
                            Document document = Jsoup.parse(bodyStream, null, response.getUrl());
                            return AppData.parse(appId, document);
                        } catch (IOException e) {
                            throw Exceptions.propagate(e);
                        } finally {
                            Util.closeQuietly(bodyStream);
                        }
                    }
                });
    }

    public Observable<ProfileVerifyStatus> getProfileVerifyStatus() {
        return nurmashService.getProfileVerifyStatus(getAuthToken())
                .compose(Transformers.<ProfileVerifyStatus>responseDataExtractor());
    }

    public Observable<FileUploadResult> profileVerify(final ObservableTypedFile observableFile1, final ObservableTypedFile observableFile2, boolean isPassport) {
        return isPassport ? photoDocumentUploadService.profileVerifyPassport(getAuthToken(), observableFile1)
                .filter(nonNull())
                .doOnUnsubscribe(new Action0() {
                    @Override
                    public void call() {
                        observableFile1.setCancelled();
                    }
                })
                : photoDocumentUploadService.profileVerifyIDCard(getAuthToken(), observableFile1, observableFile2)
                .filter(nonNull())
                .doOnUnsubscribe(new Action0() {
                    @Override
                    public void call() {
                        observableFile1.setCancelled();
                        observableFile2.setCancelled();
                    }
                });
    }

    public Observable<PartnerBalance> getPartnerProgramBalance() {
        return nurmashService.getPartnerProgramBalance(getAuthToken())
                .compose(Transformers.<PartnerBalance>responseDataExtractor());
    }

    public Observable<PartnerInvitations> getPartnerInvitations(int page, int itemsPerPage, int withdrawn) {
        return nurmashService.getPartnerInvitations(getAuthToken(), page, itemsPerPage, withdrawn)
                .compose(Transformers.<PartnerInvitations>responseDataExtractor());
    }

    public Observable<InviteAvailable> isInviteAvailable() {
        return nurmashService.isInviteAvailable(getAuthToken())
                .compose(Transformers.<InviteAvailable>responseDataExtractor());
    }

    public Observable<Void> inviteUserQRCode(String scannedUserId) {
        return nurmashService.inviteUserQRCode(getAuthToken(), scannedUserId)
                .compose(Transformers.<Void>responseDataExtractor());
    }

    public Observable<MobappCompatible> getMobappCompatibility() {
        return nurmashService.getMobappCompatibility(getAuthToken(),
                deviceInfo.getVersionName(),
                deviceInfo.OS)
                .compose(Transformers.<MobappCompatible>responseDataExtractor());
    }

    public Observable<List<PrizeItem>> getPrizeList(int page, int itemsPerPage) {
        return nurmashService.getPrizeList(getAuthToken(), page, itemsPerPage)
                .compose(Transformers.<List<PrizeItem>>responseDataExtractor());
    }

    public Observable<JackpotList> getJackpotList() {
        return nurmashService.getJackpotList(getAuthToken())
                .compose(Transformers.<JackpotList>responseDataExtractor());
    }

    public Observable<Void> withdrawInviteBonus(WithdrawForm2 form) {
        return nurmashService.withdrawInviteBonus(getAuthToken(), form.buildParamsMap())
                .compose(Transformers.<Void>responseDataExtractor());
    }
}
