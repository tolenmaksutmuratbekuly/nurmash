package com.nurmash.nurmash.data.social.twitter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nurmash.nurmash.data.social.ShareProvider;
import com.nurmash.nurmash.util.Validate;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;

import rx.Observable;
import rx.Subscriber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.nurmash.nurmash.data.social.twitter.TwitterProviderException.ErrorCode.CANCELED;
import static com.nurmash.nurmash.data.social.twitter.TwitterProviderException.ErrorCode.FAILED;
import static com.nurmash.nurmash.data.social.twitter.TwitterProviderException.ErrorCode.TWITTER_APP_NOT_AVAILABLE;

public class TwitterProvider implements ShareProvider {
    public static final int TWEET_COMPOSER_REQUEST_CODE = 14331;

    private final Activity activity;
    private TweetComposerCallback callback;

    public TwitterProvider(Activity activity) {
        this.activity = activity;
    }

    @Override
    public Observable<Object> share(@NonNull Uri url, @Nullable Uri imageUrl, @Nullable String text) {
        URL convertedUrl;
        try {
            convertedUrl = new URL(url.toString());
        } catch (MalformedURLException e) {
            return Observable.error(e);
        }

        TweetComposer.Builder builder = new TweetComposer.Builder(activity).url(convertedUrl);
        if (text != null) builder.text(text);
        if (imageUrl != null) builder.image(imageUrl);
        Intent intent = builder.createIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // This intent is for web view, because probably native Twitter app was not found on the device.
            // Unfortunately, Twitter's web view does not send result codes back, so we have no way to determine
            // if user actually posted a tweet or cancelled the action.
            return Observable.error(new TwitterProviderException(TWITTER_APP_NOT_AVAILABLE));
        } else {
            return shareInternal(intent);
        }
    }

    /**
     * <b>Don't forget to call this method from {@link Activity#onActivityResult(int, int, Intent)}</b>
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TWEET_COMPOSER_REQUEST_CODE) {
            switch (resultCode) {
                case RESULT_OK:
                    callback.onSuccess();
                    break;
                case RESULT_CANCELED:
                    callback.onCanceled();
                    break;
                default:
                    callback.onFailed(resultCode, data == null ? null : data.getExtras());
            }
            // Don't forget to null the callback.
            callback = null;
            return true;
        } else {
            return false;
        }
    }

    private Observable<Object> shareInternal(final Intent shareIntent) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(final Subscriber<? super Object> subscriber) {
                Validate.runningOnUiThread();

                callback = new TweetComposerCallback() {
                    @Override
                    public void onSuccess() {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onCanceled() {
                        subscriber.onError(new TwitterProviderException(CANCELED));
                    }

                    @Override
                    public void onFailed(int resultCode, Bundle dataExtras) {
                        subscriber.onError(new TwitterProviderException(FAILED, resultCode, dataExtras));
                    }
                };
                activity.startActivityForResult(shareIntent, TWEET_COMPOSER_REQUEST_CODE);
            }
        });
    }

    private interface TweetComposerCallback {
        void onSuccess();

        void onCanceled();

        void onFailed(int resultCode, Bundle data);
    }
}
