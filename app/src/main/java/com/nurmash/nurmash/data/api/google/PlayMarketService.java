package com.nurmash.nurmash.data.api.google;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface PlayMarketService {
    @GET("/store/apps/details")
    Observable<Response> getAppDetailsPage(@Query("id") String appId, @Query("hl") String locale);
}
