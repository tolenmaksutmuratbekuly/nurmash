package com.nurmash.nurmash.data.social.vk;

import com.vk.sdk.api.VKError;

public class VKProviderException extends RuntimeException {
    private final VKError error;

    public VKProviderException(VKError error) {
        this.error = error;
    }

    public int getErrorCode() {
        return error == null ? 0 : error.errorCode;
    }

    @Override
    public String getMessage() {
        return error == null ? null : error.toString();
    }
}
