package com.nurmash.nurmash.data.social.google;

import android.accounts.Account;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.nurmash.nurmash.BuildConfig;
import com.nurmash.nurmash.data.social.AuthProvider;
import com.nurmash.nurmash.util.DebugUtils;
import com.nurmash.nurmash.util.Validate;

import java.io.IOException;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.nurmash.nurmash.data.social.google.GoogleProviderException.ErrorCode.GOOGLE_API_CONNECTING;
import static com.nurmash.nurmash.data.social.google.GoogleProviderException.ErrorCode.GOOGLE_API_NOT_CONNECTED;

public class GoogleAuthProvider implements AuthProvider, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int SIGN_IN_INTENT_REQUEST_CODE = 901;
    private static final String OAUTH2_SCOPE = "oauth2:" + Scopes.PLUS_LOGIN + " " + Scopes.EMAIL;

    private final Activity activity;
    private final GoogleSignInOptions signInOptions;
    private GoogleApiClient googleApiClient;
    private SignInIntentCallback signInCallback;
    private CustomClientConnectionListener clientConnectionListener;

    public GoogleAuthProvider(Activity activity) {
        this.activity = activity;

        signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Plus.SCOPE_PLUS_LOGIN)
                .requestEmail()
                .build();
    }

    public void disconnect() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    /**
     * <b>Don't forget to call this method from {@link Activity#onActivityResult(int, int, Intent)}</b>
     */
    public boolean onActivityResult(int requestCode, @SuppressWarnings("UnusedParameters") int resultCode, Intent data) {
        if (requestCode == SIGN_IN_INTENT_REQUEST_CODE) {
            if (signInCallback != null) {
                signInCallback.call(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
                signInCallback = null;
            }
            return true;
        }
        return false;
    }

    @Override
    public Observable<String> getTokenForAuth() {
        return connect()
                .flatMap(new Func1<Void, Observable<GoogleSignInAccount>>() {
                    @Override
                    public Observable<GoogleSignInAccount> call(Void aVoid) {
                        return signIn();
                    }
                })
                .observeOn(Schedulers.io())
                .flatMap(new Func1<GoogleSignInAccount, Observable<String>>() {
                    @Override
                    public Observable<String> call(GoogleSignInAccount account) {
                        return getToken(account.getEmail());
                    }
                });
    }

    public void revokeAccess() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.revokeAccess(googleApiClient);
        }
    }

    public void logout() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(googleApiClient);
        }
    }

    public Observable<Void> logoutAsync() {
        return connect().map(new Func1<Void, Void>() {
            @Override
            public Void call(Void aVoid) {
                Auth.GoogleSignInApi.signOut(googleApiClient);
                return null;
            }
        });
    }

    private synchronized GoogleApiClient getGoogleApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(activity)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        return googleApiClient;
    }

    private Observable<Void> connect() {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(final Subscriber<? super Void> subscriber) {
                Validate.runningOnUiThread();
                GoogleApiClient client = getGoogleApiClient();
                if (client.isConnecting()) {
                    subscriber.onError(new GoogleProviderException(GOOGLE_API_CONNECTING));
                } else if (client.isConnected()) {
                    subscriber.onNext(null);
                    subscriber.onCompleted();
                } else {
                    if (BuildConfig.DEBUG) {
                        DebugUtils.androidAssert(clientConnectionListener == null);
                    }
                    clientConnectionListener = new CustomClientConnectionListener() {
                        @Override
                        public void onConnected() {
                            subscriber.onNext(null);
                            subscriber.onCompleted();
                        }

                        @Override
                        public void onConnectionFailed() {
                            subscriber.onError(new GoogleProviderException(GOOGLE_API_NOT_CONNECTED));
                        }
                    };
                    client.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
                }
            }
        });

    }

    private Observable<GoogleSignInAccount> signIn() {
        return Observable.create(new Observable.OnSubscribe<GoogleSignInAccount>() {
            @Override
            public void call(final Subscriber<? super GoogleSignInAccount> subscriber) {
                Validate.runningOnUiThread();
                signInCallback = new SignInIntentCallback() {
                    @Override
                    public void call(GoogleSignInResult result) {
                        if (result.isSuccess()) {
                            subscriber.onNext(result.getSignInAccount());
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new GoogleProviderException(result.getStatus()));
                        }
                    }
                };
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                activity.startActivityForResult(intent, SIGN_IN_INTENT_REQUEST_CODE);
            }
        });
    }

    private Observable<String> getToken(final String accountName) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    Account account = new Account(accountName, GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
                    String token = GoogleAuthUtil.getToken(activity, account, OAUTH2_SCOPE);
                    subscriber.onNext(token);
                    subscriber.onCompleted();
                } catch (IOException | GoogleAuthException e) {
                    subscriber.onError(new GoogleProviderException(e));
                }
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // GoogleApiClient connection callbacks
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onConnected(Bundle bundle) {
        if (clientConnectionListener != null) {
            clientConnectionListener.onConnected();
            clientConnectionListener = null;
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        if (clientConnectionListener != null) {
            clientConnectionListener.onConnectionFailed();
            clientConnectionListener = null;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (clientConnectionListener != null) {
            clientConnectionListener.onConnectionFailed();
            clientConnectionListener = null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Internal callbacks
    ///////////////////////////////////////////////////////////////////////////

    private interface SignInIntentCallback {
        void call(GoogleSignInResult result);
    }

    private interface CustomClientConnectionListener {
        void onConnected();

        void onConnectionFailed();
    }
}
