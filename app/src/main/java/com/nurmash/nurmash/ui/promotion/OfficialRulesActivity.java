package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.nurmash.nurmash.util.ContextUtils.getRawAsString;

public class OfficialRulesActivity extends BaseActivity {
    @Bind(R.id.rules_web_view) WebView rulesWebView;

    public static Intent newIntent(Context context) {
        return new Intent(context, OfficialRulesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_official_rules);
        ButterKnife.bind(this);
        configureRulesWebView();
    }

    private void configureRulesWebView() {
        // TODO: 3/28/16 Load rules html resource asynchronously.
        // ContextUtils.getRawAsString() might be blocking the UI thread, which might be the reason of slow activity startup.
        String rulesHtml = getRawAsString(this, R.raw.official_rules);
        rulesWebView.loadDataWithBaseURL(null, rulesHtml, "text/html", "utf-8", null);
    }
}

