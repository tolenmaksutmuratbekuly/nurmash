package com.nurmash.nurmash.ui.prizes.verification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nurmash.nurmash.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DocsVerifyResultRejectedFrgm extends Fragment {
    private int status;
    @Bind(R.id.tv_rejected_reason) TextView tv_rejected_reason;

    public static DocsVerifyResultRejectedFrgm newInstance(int status) {
        DocsVerifyResultRejectedFrgm f = new DocsVerifyResultRejectedFrgm();
        Bundle args = new Bundle();
        args.putInt("status", status);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            status = args.getInt("status");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_verifyresult_rejected, viewGroup, false);
        ButterKnife.bind(this, rootView);

        if (status == -2) {
            tv_rejected_reason.setText(getString(R.string.label_verify_reason_badquality));
        } else if (status < 0) {
            tv_rejected_reason.setText(getString(R.string.label_verify_reason_rejected));
        }

        return rootView;
    }

    @OnClick(R.id.btn_sendagain)
    void onSendAgainClick() {
        getActivity().finish();
        startActivity(PhotoDocsActivity.newIntent(getActivity()));
    }

}
