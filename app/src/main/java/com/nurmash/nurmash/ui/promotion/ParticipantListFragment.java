package com.nurmash.nurmash.ui.promotion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.promotion.CompetitorListPresenter;
import com.nurmash.nurmash.mvp.promotion.CompetitorListView;
import com.nurmash.nurmash.ui.PhotoListAdapter;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.ui.profile.ProfileDetailActivity;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ParticipantListFragment extends Fragment implements CompetitorListView {
    CompetitorListPresenter competitorListPresenter;
    PhotoListAdapter photoListAdapter;
    Promotion promo;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;

    public static ParticipantListFragment newInstance(long promoId, boolean isPhotoSharingPromo, Promotion promo) {
        ParticipantListFragment fragment = new ParticipantListFragment();
        Bundle args = new Bundle();
        args.putLong("promoId", promoId);
        args.putBoolean("isPhotoSharingPromo", isPhotoSharingPromo);
        args.putParcelable("promo", Parcels.wrap(promo));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long promoId = getArguments().getLong("promoId");
        boolean isPhotoSharingPromo = getArguments().getBoolean("isPhotoSharingPromo");
        promo = Parcels.unwrap(getArguments().getParcelable("promo"));

        competitorListPresenter = new CompetitorListPresenter(DataLayer.getInstance(getActivity()), promoId, isPhotoSharingPromo, promo);
        photoListAdapter = new PhotoListAdapter(isPhotoSharingPromo, false);
        photoListAdapter.setCallback(new PhotoListAdapter.Callback() {
            @Override
            public void onCompetitorClick(Competitor competitor, int position) {
                competitorListPresenter.onCompetitorClick(competitor, position);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_participant_list, container, false);
        ButterKnife.bind(this, rootView);
        recyclerView.setAdapter(photoListAdapter);
        recyclerView.addOnScrollListener(new EndlessScrollListener(recyclerView, 20) {
            @Override
            public void onRequestMoreItems() {
                competitorListPresenter.loadMoreItems();
            }
        });
        competitorListPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Picasso.with(getContext()).cancelTag(photoListAdapter.picassoTag);
        recyclerView.clearOnScrollListeners();
        recyclerView.setAdapter(null);
        competitorListPresenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    ///////////////////////////////////////////////////////////////////////////
    // CompetitorListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        photoListAdapter.setLoading(show);
    }

    @Override
    public void setCompetitors(List<Competitor> competitors, long authUserId) {
        photoListAdapter.setCompetitors(competitors);
    }

    @Override
    public void addCompetitors(List<Competitor> competitors) {
        photoListAdapter.addCompetitors(competitors);
    }

    @Override
    public void openListDetailFragment(long competitorId, int competitorIndex) {
        startActivity(PhotoDetailActivity.newIntent(getActivity(), competitorId));
    }

    @Override
    public void openProfileDetailActivity(long userId) {
        startActivity(ProfileDetailActivity.newIntent(getActivity(), userId));
    }

    @Override
    public void showReportLoadingIndicator(boolean show) {
    }

    @Override
    public void showPhotoReportedMessage() {
    }

    @Override
    public void showPhotoComplaintExistsError() {
    }

    @Override
    public void openCommentListActivity(Competitor competitor, boolean focusInput) {

    }

    @Override
    public void openUserProfileActivity(User user) {

    }

    @Override
    public void openPromoDetailActivity(long promoId) {

    }

    @Override
    public void updatePhotoDetail(int competitorPosition) {

    }

    @Override
    public void openCompetitorLikesActivity(Competitor competitor) {

    }
}
