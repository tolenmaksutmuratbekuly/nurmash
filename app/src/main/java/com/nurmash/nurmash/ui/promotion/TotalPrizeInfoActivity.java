package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.promotion.TotalPrizeInfoPresenter;
import com.nurmash.nurmash.mvp.promotion.TotalPrizeInfoView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.participate.ShareStepActivity;
import com.nurmash.nurmash.ui.participate.StepsSummaryActivity;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.ViewUtils;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TotalPrizeInfoActivity extends BaseActivity implements TotalPrizeInfoView {
    private TotalPrizeInfoPresenter presenter;
    private Promotion promo;
    private Promotion.Prizes prizes;
    @Bind(R.id.fl_parent) FrameLayout fl_parent;
    @Bind(R.id.button_next) Button nextButton;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressOverlay;
    private RowViewHolder inc_prizes_main;
    private RowViewHolder inc_prizes_community;
    private RowViewHolder inc_prizes_company;
    private RowViewHolder inc_prizes_firstpool;
    private RowViewHolder inc_prizes_randompool;
    private RowViewHolder inc_prizes_jackpot;

    public static Intent newIntent(Context packageContext, Promotion promo) {
        Intent intent = new Intent(packageContext, TotalPrizeInfoActivity.class);
        intent.putExtra("promo", Parcels.wrap(promo));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_prizeinfo);
        ButterKnife.bind(this);

        // Presenter
        if (getIntent().hasExtra("promo")) {
            promo = Parcels.unwrap(getIntent().getParcelableExtra("promo"));
            presenter = new TotalPrizeInfoPresenter(DataLayer.getInstance(this), promo);
        }
        presenter.setStringsProvider(this);
        presenter.attachView(this);
        presenter.setErrorHandler(createErrorHandler());

        setPrizes();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    private ErrorHandler createErrorHandler() {
        RequestErrorHandler requestErrorHandler = new RequestErrorHandler();
        requestErrorHandler.setCallbacks(new DefaultRequestErrorCallbacks(this, fl_parent) {
            @Override
            public void onNetworkError() {
                super.onNetworkError();
            }

            @Override
            public void onRequestFailed() {
                super.onRequestFailed();
                nextButton.setVisibility(View.GONE);
            }

            @Override
            public void onInternalServerError() {
                super.onInternalServerError();
                nextButton.setVisibility(View.GONE);
            }

            @Override
            public void onPromoHasEndedError() {
                super.onPromoHasEndedError();
                nextButton.setVisibility(View.GONE);
            }

            @Override
            public void onPromoNotFoundError() {
                super.onPromoNotFoundError();
                nextButton.setVisibility(View.GONE);
            }
        });
        return requestErrorHandler;
    }

    public void setPrizes() {
        inc_prizes_main = new RowViewHolder(findViewById(R.id.inc_prizes_main));
        inc_prizes_community = new RowViewHolder(findViewById(R.id.inc_prizes_community));
        inc_prizes_company = new RowViewHolder(findViewById(R.id.inc_prizes_company));
        inc_prizes_firstpool = new RowViewHolder(findViewById(R.id.inc_prizes_firstpool));
        inc_prizes_randompool = new RowViewHolder(findViewById(R.id.inc_prizes_randompool));
        inc_prizes_jackpot = new RowViewHolder(findViewById(R.id.inc_prizes_jackpot));

        prizes = promo == null ? null : promo.prizes;
        if (prizes != null) {
            String currencyCode = "";
            if (promo.getCurrencyCode() != null) {
                currencyCode = promo.getCurrencyCode();
            }

            if (promo.isPhotoSharingPromo()) {
                inc_prizes_main.bindPrize(R.drawable.ic_cprize_main, R.string.label_main_prize, R.string.label_main_prize_description, prizes.place_1.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
                inc_prizes_community.bindPrize(R.drawable.ic_cprize_community, R.string.label_community_prize, R.string.label_community_prize_description, prizes.place_2.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
                inc_prizes_company.bindPrize(R.drawable.ic_cprize_company, R.string.label_company_prize, R.string.label_company_prize_description, prizes.place_3.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
            } else {
                inc_prizes_main.bindPrize(R.drawable.ic_cprize_main, R.string.label_first_place_prize, R.string.label_first_place_prize_description, prizes.place_1.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
                inc_prizes_community.bindPrize(R.drawable.ic_cprize_community, R.string.label_second_place_prize, R.string.label_second_place_prize_description, prizes.place_2.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
                inc_prizes_company.bindPrize(R.drawable.ic_cprize_company, R.string.label_third_place_prize, R.string.label_third_place_prize_description, prizes.place_3.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
            }

            inc_prizes_firstpool.bindPool(R.drawable.ic_cprize_firstpool, R.string.label_first_pool_prize, R.string.label_first_pool_prize_description, prizes.getFirstPoolSize(), prizes.first_each.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
            inc_prizes_randompool.bindPool(R.drawable.ic_cprize_randompool, R.string.label_random_pool_prize, R.string.label_random_pool_prize_description, prizes.getRandomPoolSize(), prizes.random_each.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);
            inc_prizes_jackpot.bindPrize(R.drawable.ic_cprize_jackpot, R.string.label_jackpot_fund, R.string.label_jackpot_fund_description, prizes.jp.toString(FormatUtils.getMoneyAmountDisplayFormat()), currencyCode);

        } else {
            inc_prizes_main.bindPrize(0, 0, 0, null, null);
            inc_prizes_community.bindPrize(0, 0, 0, null, null);
            inc_prizes_company.bindPrize(0, 0, 0, null, null);
            inc_prizes_firstpool.bindPrize(0, 0, 0, null, null);
            inc_prizes_randompool.bindPrize(0, 0, 0, null, null);
        }
    }

    @OnClick(R.id.button_next)
    public void onNextClick() {
        presenter.onNextClick();
    }

    class RowViewHolder {
        ViewGroup rowView;
        @Bind(R.id.img_prizes_info) ImageView img_prizes_info;
        @Bind(R.id.tv_prizes_title) TextView tv_prizes_title;
        @Bind(R.id.tv_prize_money) TextView tv_prize_money;
        @Bind(R.id.img_arrow_down) ImageView img_arrow_down;
        @Bind(R.id.view_expanded_separator) View view_expanded_separator;
        @Bind(R.id.ll_expandable) LinearLayout ll_expandable;
        @Bind(R.id.tv_prizes_title_expandable) TextView tv_prizes_title_expandable;
        @Bind(R.id.tv_prize_description) TextView tv_prize_description;

        RowViewHolder(View view) {
            ButterKnife.bind(this, view);
            rowView = (ViewGroup) view;
        }

        void bindPrize(@DrawableRes int imgResource, @StringRes int titleRes, @StringRes int description, String prize, String currencyCode) {
            if (titleRes == 0 || prize == null) {
                rowView.setVisibility(View.GONE);
                img_prizes_info.setImageResource(0);
                tv_prizes_title.setText(null);
                tv_prize_money.setText(null);
                tv_prizes_title_expandable.setText(null);
                tv_prize_description.setText(null);
            } else {
                rowView.setVisibility(View.VISIBLE);
                img_prizes_info.setImageResource(imgResource);
                tv_prizes_title.setText(titleRes);
                tv_prize_money.setText(prize + " " + currencyCode);
                tv_prizes_title_expandable.setText(titleRes);
                tv_prize_description.setText(description);
            }
        }

        void bindPool(@DrawableRes int imgResource, @StringRes int title, @StringRes int description, int poolSize, String prize, String currencyCode) {
            if (title == 0 || poolSize == 0 || prize == null) {
                rowView.setVisibility(View.GONE);
                img_prizes_info.setImageResource(0);
                tv_prizes_title.setText(null);
                tv_prize_money.setText(null);
                tv_prizes_title_expandable.setText(null);
                tv_prize_description.setText(null);
            } else {
                rowView.setVisibility(View.VISIBLE);
                img_prizes_info.setImageResource(imgResource);
                tv_prizes_title.setText(title);
                tv_prizes_title.setText(tv_prizes_title.getText().toString().replace("_", poolSize + ""));
                tv_prize_money.setText(prize + " " + currencyCode);
                tv_prizes_title_expandable.setText(tv_prizes_title.getText().toString().replace("_", poolSize + ""));
                tv_prize_description.setText(description);
                tv_prize_description.setText(tv_prize_description.getText().toString().replace("_", poolSize + ""));
            }
        }

        @OnClick(R.id.ll_main_prize)
        void onClickExpandCollapseLayout() {
            if (ll_expandable.getTag().toString().equals("collapse")) {
                ViewUtils.expand(ll_expandable);
                ll_expandable.setTag("expand");
                img_arrow_down.setImageResource(R.drawable.ic_arrow_up);
                view_expanded_separator.setVisibility(View.VISIBLE);
            } else if (ll_expandable.getTag().toString().equals("expand")) {
                ViewUtils.collapse(ll_expandable);
                ll_expandable.setTag("collapse");
                img_arrow_down.setImageResource(R.drawable.ic_arrow_down);
                view_expanded_separator.setVisibility(View.INVISIBLE);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // TotalPrizeInfoView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNextButton(boolean show) {
        nextButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNextButtonText(String text) {
        nextButton.setText(text);
    }

    @Override
    public void setNextButtonCallback(View.OnClickListener callback) {
        nextButton.setEnabled(callback != null);
        nextButton.setOnClickListener(callback);
    }

    @Override
    public void openStepsSummaryActivity(long promoId) {
        startActivity(StepsSummaryActivity.newIntent(this, promoId));
    }

    @Override
    public void openShareStepActivity(long promoId) {
        startActivity(ShareStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openPhotoDetailActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(this, competitorId));
    }

    @Override
    public void openPastPromoDetailActivity(Promotion promo) {
        startActivity(PastPromoDetailActivity.newIntent(this, promo));
    }
}
