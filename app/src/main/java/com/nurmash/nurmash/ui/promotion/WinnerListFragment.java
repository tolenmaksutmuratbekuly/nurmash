package com.nurmash.nurmash.ui.promotion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.promotion.WinnerListPresenter;
import com.nurmash.nurmash.mvp.promotion.WinnerListView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.ListPlaceholderViewHolder;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.ui.profile.ProfileDetailActivity;
import com.nurmash.nurmash.util.FormatUtils;
import com.squareup.picasso.Picasso;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class WinnerListFragment extends Fragment implements WinnerListView {
    WinnerListPresenter winnerListPresenter;
    WinnerListAdapter winnerListAdapter;
    @Bind(R.id.grid_view) StickyGridHeadersGridView gridView;
    @Bind(R.id.grid_placeholder_stub) ViewStub gridPlaceholderStub;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;
    ListPlaceholderViewHolder listPlaceholderViewHolder;

    public static WinnerListFragment newInstance(long promoId) {
        WinnerListFragment fragment = new WinnerListFragment();
        Bundle args = new Bundle();
        args.putLong("promoId", promoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long promoId = getArguments().getLong("promoId");
        winnerListPresenter = new WinnerListPresenter(DataLayer.getInstance(getActivity()), promoId);
        winnerListAdapter = new WinnerListAdapter(this, FormatUtils.getMoneyAmountDisplayFormat());
        winnerListAdapter.setCallback(new WinnerListAdapter.Callback() {
            @Override
            public void onWinnerClick(Promotion.Winner winner) {
                winnerListPresenter.onWinnerClick(winner);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_winner_list, container, false);
        ButterKnife.bind(this, rootView);
        configureGridView();
        winnerListPresenter.setErrorHandler(new RequestErrorHandler(
                new DefaultRequestErrorCallbacks(getActivity(), gridView)));
        winnerListPresenter.attachView(this);
        return rootView;
    }

    private void configureGridView() {
        gridView.setAdapter(winnerListAdapter);
    }

    @Override
    public void onDestroyView() {
        Picasso.with(getContext()).cancelTag(this);
        winnerListPresenter.detachView();
        winnerListPresenter.setErrorHandler(null);
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    ///////////////////////////////////////////////////////////////////////////
    // WinnerListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showWinners(Promotion promo) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        winnerListAdapter.setPromo(promo);
        gridView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNoWinnersPlaceholder(@StringRes int textRes, @StringRes int buttonTextRes) {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListPlaceholderViewHolder(gridPlaceholderStub.inflate());
        }
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setButtonText(buttonTextRes);
        listPlaceholderViewHolder.setButtonCallback(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                winnerListPresenter.onNoWinnersPlaceholderButtonClick();
            }
        });
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        gridView.setVisibility(View.GONE);
    }

    @Override
    public void openPhotoDetailActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(getActivity(), competitorId));
    }

    @Override
    public void openProfileDetailActivity(long userId) {
        startActivity(ProfileDetailActivity.newIntent(getActivity(), userId));
    }
}
