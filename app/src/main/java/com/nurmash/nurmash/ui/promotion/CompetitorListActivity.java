package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.promotion.CompetitorListFragmentCallbacks;
import com.nurmash.nurmash.mvp.promotion.CompetitorListPresenter;
import com.nurmash.nurmash.mvp.promotion.CompetitorListView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.photo.CompetitorLikesActivity;
import com.nurmash.nurmash.ui.photo.PhotoCommentsActivity;
import com.nurmash.nurmash.ui.photo.ReportDialogFragment;
import com.nurmash.nurmash.ui.profile.ProfileDetailActivity;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CompetitorListActivity extends BaseActivity implements CompetitorListView, CompetitorListFragmentCallbacks, ReportDialogFragment.Callback {
    CompetitorListPresenter competitorListPresenter;
    Promotion promo;
    CompetitorListFragment competitorListFrgm;
    CompetitorListDetailFragment competitorDetailListFrgm;
    FragmentTransaction frTrans;
    FragmentManager fragmentManager;
    Competitor bindedCompetitor = null;
    @Bind(R.id.frgm_list_container) FrameLayout frgm_list_container;
    @Bind(R.id.loading_view) View loading_view;

    public static Intent newIntent(Context packageContext, Promotion promo) {
        Intent intent = new Intent(packageContext, CompetitorListActivity.class);
        intent.putExtra("promo", Parcels.wrap(promo));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_competitor_list);
        ButterKnife.bind(this);
        promo = Parcels.unwrap(getIntent().getParcelableExtra("promo"));

        fragmentManager = getSupportFragmentManager();
        configureFragments();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (competitorListPresenter == null) {
            // Presenter
            competitorListPresenter = new CompetitorListPresenter(DataLayer.getInstance(this), promo);
            competitorListPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, frgm_list_container)));
            competitorListPresenter.attachView(this);
        }
    }

    private void configureFragments() {
        frTrans = getSupportFragmentManager().beginTransaction();
        competitorListFrgm = CompetitorListFragment.newInstance(promo);
        competitorListFrgm.setCallbacks(this);

        competitorDetailListFrgm = CompetitorListDetailFragment.newInstance();
        competitorDetailListFrgm.setCallbacks(this);

        frTrans.add(R.id.frgm_list_container, competitorListFrgm);
        frTrans.add(R.id.frgm_list_container, competitorDetailListFrgm);
        frTrans.hide(competitorDetailListFrgm);
        frTrans.commit();

    }

    @Override
    public void onBackPressed() {
        if (competitorDetailListFrgm.isVisible())
            hideDetailFragment(competitorDetailListFrgm.currentPosition());
        else
            finish();

    }

    @Override
    protected void onDestroy() {
        competitorListPresenter.setErrorHandler(null);
        competitorListPresenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // CompetitorListFragmentCallbacks implementation
    ///////////////////////////////////////////////////////////////////////////
    @Override
    public void loadMoreItems() {
        competitorListPresenter.loadMoreItems();
    }

    @Override
    public void onCompetitorClick(Competitor competitor, int position) {
        competitorListPresenter.onCompetitorClick(competitor, position);
    }

    @Override
    public void onUserClick(User user) {
        competitorListPresenter.onUserClick(user);
    }

    @Override
    public void onLikeClick(Competitor competitor, int competitorPosition) {
        competitorListPresenter.onLikeClick(competitor, competitorPosition);
    }

    @Override
    public void onCommentButtonClick(Competitor competitor) {
        competitorListPresenter.onCommentButtonClick(competitor);
    }

    @Override
    public void onCompetitorLikesButtonClick(Competitor competitor) {
        competitorListPresenter.onCompetitorLikesButtonClick(competitor);
    }

    @Override
    public void onOpenPromoButtonClick(Competitor competitor) {
        competitorListPresenter.onOpenPromoButtonClick(competitor);
    }

    @Override
    public void onMoreCommentsButtonClick(Competitor competitor) {
        competitorListPresenter.onMoreCommentsButtonClick(competitor);
    }

    @Override
    public void hideDetailFragment(int competitorIndex) {
        competitorListFrgm.scrollToPosition(competitorIndex);
        FragmentTransaction frgmTransaction = fragmentManager.beginTransaction();
        frgmTransaction.hide(competitorDetailListFrgm);
        frgmTransaction.show(competitorListFrgm);
        frgmTransaction.commit();
    }

    @Override
    public void openPhotoReportDialog(int competitorPosition) {
        ReportDialogFragment.newPhotoComplaintInstance(competitorPosition).show(fragmentManager, "ReportDialogFragment");
    }

    @Override
    public void setReportCompetitor(Competitor competitor) {
        this.bindedCompetitor = competitor;
    }

    ///////////////////////////////////////////////////////////////////////////
    // ReportDialogFragment.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onComplaintSelected(Complaint complaint, int competitorPosition) {
        if (bindedCompetitor != null)
            competitorListPresenter.onComplaintSelected(bindedCompetitor, complaint, competitorPosition);
    }

    @Override
    public void onComplaintSelected(Complaint complaint) {

    }

    ///////////////////////////////////////////////////////////////////////////
    // CompetitorListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void openListDetailFragment(long competitorId, int competitorIndex) {
        competitorDetailListFrgm.scrollToPosition(competitorIndex);
        FragmentTransaction frgmTransaction = getSupportFragmentManager().beginTransaction();
        frgmTransaction.hide(competitorListFrgm);
        frgmTransaction.show(competitorDetailListFrgm);
        frgmTransaction.commit();
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        loading_view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setCompetitors(List<Competitor> competitors, long authUserId) {
        competitorListFrgm.setCompetitors(competitors);
        competitorDetailListFrgm.setAuthUserId(authUserId);
        competitorDetailListFrgm.setCompetitors(competitors);
    }

    @Override
    public void addCompetitors(List<Competitor> competitors) {
        competitorListFrgm.addCompetitors(competitors);
        competitorDetailListFrgm.addCompetitors(competitors);
    }

    @Override
    public void openProfileDetailActivity(long userId) {
        startActivity(ProfileDetailActivity.newIntent(this, userId));
    }

    @Override
    public void showReportLoadingIndicator(boolean show) {
        competitorDetailListFrgm.reportLoadingIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPhotoReportedMessage() {
        Toast.makeText(this, R.string.info_message_photo_report_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPhotoComplaintExistsError() {
        Toast.makeText(this, R.string.error_message_photo_complaint_exists, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openCommentListActivity(Competitor competitor, boolean focusInput) {
        startActivity(PhotoCommentsActivity.newIntent(this, competitor.id, focusInput));
    }

    @Override
    public void openUserProfileActivity(User user) {
        startActivity(ProfileDetailActivity.newIntent(this, user));
    }

    @Override
    public void openPromoDetailActivity(long promoId) {
        startActivity(PromoDetailActivity.newIntent(this, promoId));
    }

    @Override
    public void updatePhotoDetail(int competitorPosition) {
        competitorListFrgm.refreshItem();
        competitorDetailListFrgm.refreshItem();
    }

    @Override
    public void openCompetitorLikesActivity(Competitor competitor) {
        startActivity(CompetitorLikesActivity.newIntent(this, competitor.id));
    }
}