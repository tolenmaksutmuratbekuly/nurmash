package com.nurmash.nurmash.ui.prizes.withdraw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.prizes.withdraw.CardTransferPresenter;
import com.nurmash.nurmash.mvp.prizes.withdraw.CardTransferView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.StringUtils;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;
import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.nurmash.nurmash.util.ViewUtils.moveEditorCursorToEnd;

public class CardTransferActivity extends BaseActivity implements CardTransferView {
    CardTransferPresenter cardTransferPresenter;
    PrizeItem prizeItem;
    PartnerBalance partnerBalance;
    @Bind(R.id.tv_amount_of_prize) TextView tv_amount_of_prize;
    @Bind(R.id.tv_tax) TextView tv_tax;
    @Bind(R.id.tv_comission) TextView tv_comission;
    @Bind(R.id.tv_amount_crediting) TextView tv_amount_crediting;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;
    @Bind({R.id.card_input_1, R.id.card_input_2, R.id.card_input_3, R.id.card_input_4}) EditText[] cardInputs;
    @Bind({R.id.card_input_confirm_1, R.id.card_input_confirm_2, R.id.card_input_confirm_3, R.id.card_input_confirm_4}) EditText[] cardConfirmInputs;

    public static Intent newIntent(Context packageContext, PrizeItem prizeItem) {
        Intent intent = new Intent(packageContext, CardTransferActivity.class);
        intent.putExtra("prizeItem", Parcels.wrap(prizeItem));
        return intent;
    }

    public static Intent newIntent(Context packageContext, PartnerBalance partnerBalance) {
        Intent intent = new Intent(packageContext, CardTransferActivity.class);
        intent.putExtra("partnerBalance", Parcels.wrap(partnerBalance));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_transfer);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra("prizeItem")) {
            prizeItem = Parcels.unwrap(getIntent().getParcelableExtra("prizeItem"));
            cardTransferPresenter = new CardTransferPresenter(DataLayer.getInstance(this), prizeItem);
        } else if (intent.hasExtra("partnerBalance")) {
            partnerBalance = Parcels.unwrap(getIntent().getParcelableExtra("partnerBalance"));
            cardTransferPresenter = new CardTransferPresenter(DataLayer.getInstance(this), partnerBalance);
        }

        if (cardTransferPresenter != null) {
            cardTransferPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this)));
            cardTransferPresenter.attachView(this);
        }
    }

    @Override
    protected void onDestroy() {
        cardTransferPresenter.setErrorHandler(null);
        cardTransferPresenter.detachView();
        super.onDestroy();
    }

    @OnFocusChange({R.id.card_input_1, R.id.card_input_2, R.id.card_input_3, R.id.card_input_4,
            R.id.card_input_confirm_1, R.id.card_input_confirm_2, R.id.card_input_confirm_3, R.id.card_input_confirm_4})
    void onInputFieldFocusChange(EditText editText, boolean focused) {
        if (focused) {
            moveEditorCursorToEnd(editText);
        }
    }

    @OnTextChanged(value = {R.id.card_input_1, R.id.card_input_2, R.id.card_input_3, R.id.card_input_4},
            callback = AFTER_TEXT_CHANGED)
    void onCardInputTextChanged(Editable s) {
        String trimmed = trim(s);
        if (trimmed.length() == 16 && StringUtils.isDigits(trimmed)) {
            // Handle the case when the card number has been pasted.
            for (int i = 0; i < cardInputs.length; ++i) {
                cardInputs[i].setText(trimmed.substring(i * 4, (i + 1) * 4));
            }
            return;
        }

        if (s.length() > 4) {
            s.delete(4, s.length());
        } else if (s.length() == 4) {
            // Focus next input
            for (int i = 0; i < cardInputs.length; ++i) {
                if (cardInputs[i].isFocused()) {
                    View next = cardInputs[i].focusSearch(View.FOCUS_DOWN);
                    if (next != null) {
                        next.requestFocus();
                    }
                    break;
                }
            }
        }
    }

    @OnTextChanged(value = {R.id.card_input_confirm_1, R.id.card_input_confirm_2, R.id.card_input_confirm_3, R.id.card_input_confirm_4},
            callback = AFTER_TEXT_CHANGED)
    void onCardConfirmInputTextChanged(Editable s) {
        String trimmed = trim(s);
        if (trimmed.length() == 16 && StringUtils.isDigits(trimmed)) {
            // Handle the case when the card number has been pasted.
            for (int i = 0; i < cardConfirmInputs.length; ++i) {
                cardConfirmInputs[i].setText(trimmed.substring(i * 4, (i + 1) * 4));
            }
            return;
        }

        if (s.length() > 4) {
            s.delete(4, s.length());
        } else if (s.length() == 4) {
            // Focus next input
            for (int i = 0; i < cardConfirmInputs.length; ++i) {
                if (cardConfirmInputs[i].isFocused()) {
                    View next = cardConfirmInputs[i].focusSearch(View.FOCUS_DOWN);
                    if (next != null) {
                        next.requestFocus();
                    }
                    break;
                }
            }
        }
    }

    @OnClick(R.id.claim_button)
    void onClaimPrizeClick() {
        for (int i = 0; i < cardInputs.length; ++i) {
            cardTransferPresenter.setCardNumberPart(i, cardInputs[i].getText().toString(), cardConfirmInputs[i].getText().toString());
        }
        cardTransferPresenter.onClaimPrizeClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // CardTransferView
    ///////////////////////////////////////////////////////////////////////////
    @Override
    public void showAmountPrize(String amountOfPrize) {
        tv_amount_of_prize.setText(amountOfPrize);
    }

    @Override
    public void showTaxAmount(String taxAmount) {
        tv_tax.setText(taxAmount);
    }

    @Override
    public void showComission(String comissionAmount) {
        tv_comission.setText(comissionAmount);
    }

    @Override
    public void showAmountCrediting(String creditingAmount) {
        tv_amount_crediting.setText(creditingAmount);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showInvalidCardNumberErrorToast() {
        Toast.makeText(this, R.string.error_message_invalid_card_number, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInvalidCardConfirmationErrorToast() {
        Toast.makeText(this, R.string.error_message_invalid_card_confirm_number, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishWithSuccess() {
        Toast.makeText(this, R.string.info_message_claim_prize_success, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }
}
