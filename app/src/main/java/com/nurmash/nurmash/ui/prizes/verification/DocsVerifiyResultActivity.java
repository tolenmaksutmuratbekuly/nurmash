package com.nurmash.nurmash.ui.prizes.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.mvp.prizes.verification.DocsVerifyResultPresenter;
import com.nurmash.nurmash.mvp.prizes.verification.DocsVerifyResultView;
import com.nurmash.nurmash.ui.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DocsVerifiyResultActivity extends BaseActivity implements DocsVerifyResultView {
    private DocsVerifyResultPresenter presenter;
    private DocsVerifyResultRejectedFrgm frgmRejected;
    private DocsVerifyResultPendingFrgm frgmPending;
    @Bind(R.id.progress_bar_overlay) ViewGroup progress_bar_overlay;
    private boolean isPending;
    private int status;

    public static Intent newIntent(Context packageContext, boolean isPending) {
        Intent i = new Intent(packageContext, DocsVerifiyResultActivity.class);
        i.putExtra("isPending", isPending);
        return i;
    }

    public static Intent newIntent(Context packageContext, boolean isPending, int status) {
        Intent i = new Intent(packageContext, DocsVerifiyResultActivity.class);
        i.putExtra("isPending", isPending);
        i.putExtra("status", status);
        return i;
    }

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, DocsVerifiyResultActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docsverify_result);
        ButterKnife.bind(this);

        // Presenter
        DataLayer dataLayer = DataLayer.getInstance(this);
        presenter = new DocsVerifyResultPresenter(dataLayer);
        presenter.attachView(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            isPending = extras.getBoolean("isPending");
            if (extras.containsKey("status")) {
                status = extras.getInt("status");
            }
            configureFragments();
        } else {
            presenter.profileVerifyStatus();
        }
    }

    private void configureFragments() {
        FragmentTransaction frTrans = getSupportFragmentManager().beginTransaction();
        frgmRejected = DocsVerifyResultRejectedFrgm.newInstance(status);
        frgmPending = DocsVerifyResultPendingFrgm.newInstance();

        if (isPending) {
            frTrans.add(R.id.frgm_verify_result, frgmPending, "pending");
        } else {
            frTrans.add(R.id.frgm_verify_result, frgmRejected, "rejected");
        }

        frTrans.commit();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // DocsVerifyResultView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progress_bar_overlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void profileVerificationStatus(ProfileVerifyStatus profileVerifyStatus) {
        this.isPending = profileVerifyStatus.isPendingRequest();
        this.status = profileVerifyStatus.status;
        configureFragments();
    }
}
