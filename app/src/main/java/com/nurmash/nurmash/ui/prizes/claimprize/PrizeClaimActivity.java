package com.nurmash.nurmash.ui.prizes.claimprize;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.prizes.info.PrizesInfoActivity;
import com.nurmash.nurmash.ui.prizes.withdraw.CardTransferActivity;
import com.nurmash.nurmash.ui.prizes.withdraw.MobileBalanceTransferActivity;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PrizeClaimActivity extends BaseActivity {
    private final int CARD_TRANSFER_REQUEST_CODE = 1;
    private final int MOBILE_TRANSFER_REQUEST_CODE = 2;

    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    PrizeItem prizeItem;
    PartnerBalance partnerBalance;

    public static Intent newIntent(Context packageContext, @NonNull PrizeItem prizeItem) {
        Intent intent = new Intent(packageContext, PrizeClaimActivity.class);
        intent.putExtra("prizeItem", Parcels.wrap(prizeItem));
        return intent;
    }

    public static Intent newIntent(Context packageContext, @NonNull PartnerBalance partnerBalance) {
        Intent intent = new Intent(packageContext, PrizeClaimActivity.class);
        intent.putExtra("partnerBalance", Parcels.wrap(partnerBalance));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prize_claim);
        ButterKnife.bind(this);
        configureRecyclerView();

        Intent intent = getIntent();
        if (intent.hasExtra("prizeItem")) {
            prizeItem = Parcels.unwrap(getIntent().getParcelableExtra("prizeItem"));
        } else if (intent.hasExtra("partnerBalance")) {
            partnerBalance = Parcels.unwrap(getIntent().getParcelableExtra("partnerBalance"));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == CARD_TRANSFER_REQUEST_CODE || requestCode == MOBILE_TRANSFER_REQUEST_CODE) && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private void configureRecyclerView() {
        PrizeClaimListAdapter adapter = new PrizeClaimListAdapter();
        adapter.addItem(R.drawable.ic_claim_bank_transfer, R.string.claim_prize_card);
        adapter.addItem(R.drawable.ic_claim_mobile, R.string.claim_prize_mobile);
        adapter.setCallback(new PrizeClaimListAdapter.Callback() {
            @Override
            public void onClaimPrizeItemClick(final PrizeClaimListAdapter.PrizeClaimItem item) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrizeClaimActivity.this);
                builder.setTitle(getString(R.string.label_attention));
                builder.setMessage(getString(R.string.label_attention_description));
                builder.setPositiveButton(getString(R.string.label_okay),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                switch (item.titleRes) {
                                    case R.string.claim_prize_card: {
                                        if (prizeItem != null) {
                                            if (prizeItem.amount.canBeWithdrawn()) {
                                                Intent intent = CardTransferActivity.newIntent(PrizeClaimActivity.this, prizeItem);
                                                startActivityForResult(intent, CARD_TRANSFER_REQUEST_CODE);
                                            } else {
                                                canNotBeWithdrawnDialog(prizeItem.getCurrencyCode());
                                            }
                                        } else if (partnerBalance != null) {
                                            if (partnerBalance.bonuses.available.canBeWithdrawn()) {
                                                Intent intent = CardTransferActivity.newIntent(PrizeClaimActivity.this, partnerBalance);
                                                startActivityForResult(intent, CARD_TRANSFER_REQUEST_CODE);
                                            } else {
                                                canNotBeWithdrawnDialog(partnerBalance.getCurrencyCode());
                                            }
                                        }
                                        break;
                                    }
                                    case R.string.claim_prize_mobile: {
                                        if (prizeItem != null) {
                                            Intent intent = MobileBalanceTransferActivity.newIntent(PrizeClaimActivity.this, prizeItem);
                                            startActivityForResult(intent, MOBILE_TRANSFER_REQUEST_CODE);
                                        } else if (partnerBalance != null) {
                                            Intent intent = MobileBalanceTransferActivity.newIntent(PrizeClaimActivity.this, partnerBalance);
                                            startActivityForResult(intent, MOBILE_TRANSFER_REQUEST_CODE);
                                        }
                                        break;
                                    }
                                }
                                dialog.cancel();
                            }
                        });

                builder.setNeutralButton(getString(R.string.label_more),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(PrizesInfoActivity.newWithdrawMethodsInfoIntent(PrizeClaimActivity.this));
                                dialog.cancel();
                            }
                        });

                builder.setNegativeButton(getString(R.string.label_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                builder.show();

            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflator = getMenuInflater();
        inflator.inflate(R.menu.menu_prizes, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                startActivity(PrizesInfoActivity.newWithdrawMethodsInfoIntent(this));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void canNotBeWithdrawnDialog(String currencyCode) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(getString(R.string.label_attention))
                .setCancelable(false)
                .setMessage(getString(R.string.lable_cannot_be_withdrawn_min_sum, currencyCode))
                .setPositiveButton(getString(R.string.label_okay),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();

                            }
                        }).show();
    }
}
