package com.nurmash.nurmash.ui.prizes.claimprize;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.util.FormatUtils;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrizeStatusActivity extends BaseActivity {
    static final int PRIZE_CLAIM_REQUEST_CODE = 1;
    PrizeItem prizeItem;
    PartnerBalance partnerBalance;
    int status;
    @Bind(R.id.img_status) ImageView img_status;
    @Bind(R.id.tv_status_title) TextView tv_status_title;
    @Bind(R.id.btn_back) Button btn_back;

    public static Intent newIntent(Context packageContext, @NonNull PrizeItem prizeItem) {
        Intent intent = new Intent(packageContext, PrizeStatusActivity.class);
        intent.putExtra("prizeItem", Parcels.wrap(prizeItem));
        return intent;
    }

    public static Intent newIntent(Context packageContext, @NonNull PartnerBalance partnerBalance) {
        Intent intent = new Intent(packageContext, PrizeStatusActivity.class);
        intent.putExtra("partnerBalance", Parcels.wrap(partnerBalance));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prize_status);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra("prizeItem")) {
            prizeItem = Parcels.unwrap(getIntent().getParcelableExtra("prizeItem"));
            status = prizeItem.status;
        } else if (intent.hasExtra("partnerBalance")) {
            partnerBalance = Parcels.unwrap(getIntent().getParcelableExtra("partnerBalance"));
            status = partnerBalance.last_claim.status;
        }
        
        if (status == 1 || status == 2) {
            img_status.setImageResource(R.drawable.prize_status_sent);
            tv_status_title.setText(getString(R.string.prize_status_sent));
            btn_back.setText(getString(R.string.action_go_back));
        } else if (status == 3) {
            img_status.setImageResource(R.drawable.prize_status_money);
            if (partnerBalance != null && !partnerBalance.bonuses.available.isZeroAmount()) {
                tv_status_title.setText(getString(R.string.prize_status_money_invite_bonus, partnerBalance.bonuses.available.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + partnerBalance.getCurrencyCode()));
                btn_back.setText(getString(R.string.action_claim_new_bonus));
            } else {
                tv_status_title.setText(getString(R.string.prize_status_money));
                btn_back.setText(getString(R.string.action_go_back));
            }
        } else if (status == -2) {
            img_status.setImageResource(R.drawable.prize_status_failed);
            tv_status_title.setText(getString(R.string.prize_status_failed1));
            btn_back.setText(getString(R.string.action_retry));
        } else if (status == -3) {
            img_status.setImageResource(R.drawable.prize_status_failed);
            tv_status_title.setText(getString(R.string.prize_status_failed2));
            btn_back.setText(getString(R.string.action_retry));
        } else {
            img_status.setImageResource(R.drawable.prize_status_failed);
            tv_status_title.setText(getString(R.string.error_message_server_internal));
        }
    }

    @OnClick(R.id.btn_back)
    void onClickBack() {
        if (status == 1 || status == 2) {
            finish();
        } else if (status == 3) {
            if (partnerBalance != null && !partnerBalance.bonuses.available.isZeroAmount()) {
                Intent intent = PrizeClaimActivity.newIntent(this, partnerBalance);
                startActivityForResult(intent, PRIZE_CLAIM_REQUEST_CODE);
            } else {
                finish();
            }
        } else if (status == -2 || status == -3) {
            if (prizeItem != null) {
                Intent intent = PrizeClaimActivity.newIntent(this, prizeItem);
                startActivityForResult(intent, PRIZE_CLAIM_REQUEST_CODE);
            } else if (partnerBalance != null) {
                Intent intent = PrizeClaimActivity.newIntent(this, partnerBalance);
                startActivityForResult(intent, PRIZE_CLAIM_REQUEST_CODE);
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PRIZE_CLAIM_REQUEST_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
