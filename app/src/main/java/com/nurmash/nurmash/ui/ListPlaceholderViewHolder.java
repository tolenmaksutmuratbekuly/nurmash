package com.nurmash.nurmash.ui;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListPlaceholderViewHolder {
    View view;
    @Bind(R.id.text) TextView textView;
    @Bind(R.id.button) Button button;

    @Nullable
    @Bind(R.id.list_placeholder_swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;

    public ListPlaceholderViewHolder(View view) {
        this.view = view;
        ButterKnife.bind(this, view);
        textView.setVisibility(View.GONE);
        button.setVisibility(View.GONE);
    }

    public void setText(@StringRes int textRes) {
        if (textRes == 0) {
            textView.setText(null);
            textView.setVisibility(View.GONE);
        } else {
            textView.setText(textRes);
            textView.setVisibility(View.VISIBLE);
        }
    }

    public void setButtonText(@StringRes int textRes) {
        if (textRes == 0) {
            button.setText(null);
            button.setVisibility(View.GONE);
        } else {
            button.setText(textRes);
            button.setVisibility(View.VISIBLE);
        }
    }

    public void setButtonCallback(View.OnClickListener clickListener) {
        button.setOnClickListener(clickListener);
    }

    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener refreshListener) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(refreshListener);
        }
    }

    public void setRefreshing(boolean refreshing) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(refreshing);
        }
    }

    public void setSwipeRefreshColorSchemeResources(@ColorRes int... colorResIds) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setColorSchemeResources(colorResIds);
        }
    }

    public void setVisibility(int visibility) {
        view.setVisibility(visibility);
    }
}
