package com.nurmash.nurmash.ui.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.recyclerview.SimpleViewHolder;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.widget.HListView;

public class PromoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int DAILY_PROMO = R.layout.lv_header_daily_promo;
    private static final int PROMO_CARD_LAYOUT_ID = R.layout.card_promotion;
    private static final int PROGRESS_BAR_LAYOUT_ID = R.layout.promo_list_loading_item;
    private static final int COMING_SOON_LAYOUT_ID = R.layout.promo_list_coming_soon_item;

    private final NumberFormat moneyAmountFormat;
    private boolean loading;
    private List<Promotion> promotions;
    private List<Promotion> soonPromotions;
    private Callback callback;

    public PromoListAdapter() {
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
        this.promotions = Collections.emptyList();
        this.soonPromotions = Collections.emptyList();
    }

    public void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public void setPromotions(List<Promotion> newItems) {
        promotions = new ArrayList<>(newItems);
        notifyDataSetChanged();
    }

    public void addPromotions(List<Promotion> newItems) {
        int positionStart = promotions.size();
        int itemCount = newItems.size();
        promotions.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setSoonPromotions(List<Promotion> newItems) {
        soonPromotions = new ArrayList<>(newItems);
        notifyDataSetChanged();
    }

    public void addSoonPromotions(List<Promotion> newItems) {
        int positionStart = promotions.size();
        if (!soonPromotions.isEmpty()) {
            positionStart += 1 + soonPromotions.size();
        }
        int itemCount = newItems.size();
        soonPromotions.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        int count = promotions.size();
        if (!soonPromotions.isEmpty()) {
            count += 1 + soonPromotions.size();
        }
        if (loading) {
            count += 1;
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return DAILY_PROMO;
        } else if (loading && position == getItemCount() - 1) {
            return PROGRESS_BAR_LAYOUT_ID;
        } else if (position == promotions.size()) {
            return COMING_SOON_LAYOUT_ID;
        } else {
            return PROMO_CARD_LAYOUT_ID;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case DAILY_PROMO:
                return new HeaderViewHolder(view);
            case PROMO_CARD_LAYOUT_ID:
                return new PromoCardViewHolder(view);
            case COMING_SOON_LAYOUT_ID:
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PromoCardViewHolder) {
            Promotion promotion = position < promotions.size() ? promotions.get(position) :
                    soonPromotions.get(position - promotions.size() - 1);
            ((PromoCardViewHolder) holder).bindPromotion(promotion);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.hlv_daily_promo) HListView hlv_daily_promo;

        public HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            if (callback != null) {
                callback.initDailyPromoHorizontalList(hlv_daily_promo);
            }
        }
    }

    class PromoCardViewHolder extends RecyclerView.ViewHolder {
        Context context;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.title) TextView titleView;
        @Bind(R.id.dates) TextView datesView;
        @Bind(R.id.participants_number) TextView participantsNumberView;
        //        @Bind(R.id.participation_incomplete_indicator) View participationIncompleteIndicator;
        @Bind(R.id.tv_total_prize) TextView tv_total_prize;

        boolean collapsed;
        Promotion bindedPromotion;

        public PromoCardViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        public void bindPromotion(Promotion promo) {
            if (promo == null || promo == bindedPromotion) return;
            bindedPromotion = promo;

            Picasso.with(context)
                    .load(PhotoUrl.grxl(promo.photo))
                    .fit()
                    .centerCrop()
                    .into(photoView);
            titleView.setText(promo.title);

            if (promo.deadline_type != null) {
                if (promo.deadline_type.equals(Promotion.DEADLINE_TYPE_PARTICIPANT)) {
                    participantsNumberView.setText(context.getString(R.string.participants_number_format, promo.participants_amount + " / " + promo.participants_boundary));
                    if (promo.start_date != null) {
                        datesView.setText(FormatUtils.formatDisplayDate(promo.start_date));
                    } else {
                        datesView.setText(null);
                    }
                } else if (promo.deadline_type.equals(Promotion.DEADLINE_TYPE_DATE)) {
                    participantsNumberView.setText(context.getString(R.string.participants_number_format, promo.participants_amount + ""));
                    if (promo.start_date != null && promo.stop_date != null) {
                        datesView.setText(String.format(Locale.US, "%s - %s", FormatUtils.formatDisplayDate(promo.start_date), FormatUtils.formatDisplayDate(promo.stop_date)));
                    } else {
                        datesView.setText(null);
                    }
                } else {
                    participantsNumberView.setText(null);
                }
            }
//            participationIncompleteIndicator.setVisibility(promo.incompleted ? View.VISIBLE : View.GONE);
            if (promo.prizes != null) {
                String totalPrizeSum = promo.prizes.getTotalPrize().toString(moneyAmountFormat);
                if (promo.getCurrencyCode() != null) {
                    totalPrizeSum += " " + promo.getCurrencyCode();
                }
                tv_total_prize.setText(totalPrizeSum);
            }
        }

        @OnClick(R.id.content_view)
        void onCardClick() {
            if (callback != null) {
                callback.onPromotionClick(bindedPromotion);
            }
        }

        @Override
        public String toString() {
            return "PromoCardViewHolder{"
                    + "id=" + bindedPromotion.id
                    + ", title=" + bindedPromotion.title
                    + ", " + super.toString()
                    + "}";
        }
    }

    public interface Callback {
        void onPromotionClick(Promotion promotion);

        void initDailyPromoHorizontalList(HListView hlv_daily_promo);
    }
}
