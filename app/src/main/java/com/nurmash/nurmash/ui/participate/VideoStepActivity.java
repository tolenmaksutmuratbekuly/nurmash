package com.nurmash.nurmash.ui.participate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.participate.VideoStepPresenter;
import com.nurmash.nurmash.mvp.participate.VideoStepView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class VideoStepActivity extends BaseActivity implements VideoStepView {
    private static final int YT_PLAYER_STATE_ENDED = 0;

    VideoStepPresenter videoStepPresenter;
    @Bind(R.id.wv_youtube_player) WebView youTubePlayerView;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;
    @Bind(R.id.tv_timer) TextView tv_timer;
    // Most of the code is copied from https://github.com/DrKLO/Telegram/blob/1c74ed4587f9a7e086067165eaea0532a6c33cae/TMessagesProj/src/main/java/org/telegram/ui/Components/WebFrameLayout.java
    // For YouTube Frame API reference see: https://developers.google.com/youtube/iframe_api_reference
    private static final String PLAYER_HTML_TEMPLATE = "<!DOCTYPE html>" +
            "<html>" +
            "<head>" +
            "<style>" +
            "body {" +
            "  margin: 0;" +
            "  width: 100%%;" +
            "  height: 100%%;" +
            "  background-color: #000;" +
            "}" +
            "</style>" +
            "</head>" +
            "<body>" +
            "<div id='player'></div>" +
            "<script>" +
            "  var tag = document.createElement('script');" +
            "  tag.src = 'https://www.youtube.com/iframe_api';" +
            "  var firstScriptTag = document.getElementsByTagName('script')[0];" +
            "  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);" +
            "" +
            "  function onYouTubePlayerAPIReady () {" +
            "    player = new YT.Player('player', {" +
            "      videoId: '%1$s'," +
            "      playerVars: {" +
            "        autoplay: 1," +
            "        controls: 0," +
            "        fs: 0," +
            "        iv_load_policy: 3," +
            "        modestbranding: 1," +
            "        rel: 0," +
            "        showinfo: 0," +
            "        disablekb: 1," +
            "      }," +
            "      events: {" +
            "        'onReady': onPlayerReady," +
            "        'onStateChange': onPlayerStateChange," +
            "        'onError': onPlayerError," +
            "      }" +
            "    });" +
            "    player.setSize(window.innerWidth, window.innerHeight);" +
            "  }" +
            "" +
            "  function onPlayerReady(event) {" +
            "    var player = event.target;" +
            "    player.setVolume(100);" +
            "    player.unMute();" +
            "    player.playVideo();" +
            "  }" +
            "  var timerVideoPlaying;" +
            "  function onPlayerStateChange(event) {" +
            "    var player = event.target;" +
            "    Activity.onPlayerStateChange(event.data);" +
            "    if (event.data == 1) {" +
            "       timerVideoPlaying = setInterval(function() { " +
            "           Activity.onPlayingTime(player.getCurrentTime(), player.getDuration());" +
            "       }, 100);" +
            "    } else {" +
            "       clearInterval(timerVideoPlaying);" +
            "    }" +
            "  }" +
            "  function onPlayerError(event) {" +
            "    Activity.onPlayerError(event.data);" +
            "  }" +
            "</script>" +
            "</body>" +
            "</html>";

    public static Intent newIntent(Context packageContext, long promoId) {
        Intent intent = new Intent(packageContext, VideoStepActivity.class);
        intent.putExtra("promoId", promoId);
        return intent;
    }

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_step);
        ButterKnife.bind(this);

        if (youTubePlayerView != null) {
            youTubePlayerView.getSettings().setJavaScriptEnabled(true);
            youTubePlayerView.getSettings().setDomStorageEnabled(true);
            youTubePlayerView.setWebChromeClient(new WebChromeClient() {
            });
            if (Build.VERSION.SDK_INT >= 17) {
                youTubePlayerView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            }
            String userAgent = youTubePlayerView.getSettings().getUserAgentString();
            if (userAgent != null) {
                userAgent = userAgent.replace("Android", "");
                youTubePlayerView.getSettings().setUserAgentString(userAgent);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                youTubePlayerView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.setAcceptThirdPartyCookies(youTubePlayerView, true);
            }
        }

        progressBarOverlay.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        // Presenter
        long promoId = getIntent().getLongExtra("promoId", 0);
        videoStepPresenter = new VideoStepPresenter(DataLayer.getInstance(this), promoId);
        videoStepPresenter.attachView(this);
        videoStepPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        videoStepPresenter.setErrorHandler(null);
        videoStepPresenter.detachView();
        if (Build.VERSION.SDK_INT < 18) {
            youTubePlayerView.clearView();
        } else {
            youTubePlayerView.loadUrl("about:blank");
        }
        youTubePlayerView.destroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }

    // VideoStepView implementation
    @Override
    public void showLoadingIndicator(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void loadVideo(String videoId) {
        String playerHtml = String.format(PLAYER_HTML_TEMPLATE, videoId);
        youTubePlayerView.addJavascriptInterface(this, "Activity");
        youTubePlayerView.loadDataWithBaseURL("https://youtube.com", playerHtml, "text/html", "utf-8", null);

    }

    @Override
    public void openShareStepActivity(long promoId) {
        startActivity(PhotoStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openSurveyStepActivity(long promoId) {
        startActivity(SurveyStepActivity.newIntent(this, promoId));
    }

    @Override
    public void onPlaybackError() {
        Toast.makeText(this, R.string.error_message_youtube_playback, Toast.LENGTH_LONG).show();
    }

    // YouTubePlayer StateChangeListener implementation
    @SuppressWarnings("unused")
    @JavascriptInterface
    public void onPlayerStateChange(int state) {
        // For more info see: https://developers.google.com/youtube/iframe_api_reference#onStateChange
        if (state == YT_PLAYER_STATE_ENDED) {
            videoStepPresenter.onVideoEnded();
        }
    }

    @SuppressWarnings("unused")
    @JavascriptInterface
    public void onPlayingTime(int currentTime, int duration) {
        int totalSecs = duration - currentTime;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        tv_timer.setText(minutes + ":" + seconds);
    }

    @SuppressWarnings("unused")
    @JavascriptInterface
    public void onPlayerError(int errorReason) {
        Toast.makeText(this, getString(R.string.error_message_youtube_initialization), Toast.LENGTH_LONG).show();
        // For more info see: https://developers.google.com/youtube/iframe_api_reference#onError
        videoStepPresenter.onPlaybackError(new Exception("YouTube player error: " + errorReason));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
