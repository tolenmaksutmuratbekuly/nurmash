package com.nurmash.nurmash.ui.profile;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.profile.CallbackBackPressed;
import com.nurmash.nurmash.mvp.profile.ProfileDetailPresenter;
import com.nurmash.nurmash.mvp.profile.ProfileDetailView;
import com.nurmash.nurmash.ui.AndroidPhotoPicker;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.partnerprogram.PartnerProgramActivity;
import com.nurmash.nurmash.ui.partnerprogram.PartnerProgramIntroActivity;
import com.nurmash.nurmash.ui.partnerprogram.QRScanerActivity;
import com.nurmash.nurmash.ui.settings.ProfileEditActivity;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.text.TextUtils.isEmpty;
import static butterknife.ButterKnife.findById;
import static com.nurmash.nurmash.util.SnackbarUtils.multilineSnackbar;
import static com.nurmash.nurmash.util.StringUtils.trim;
import static com.squareup.picasso.MemoryPolicy.NO_CACHE;

public class ProfileDetailFragment extends Fragment implements ProfileDetailView, CallbackBackPressed {
    private static final int PHOTO_PICKER_BASE_REQUEST_CODE = 1;
    private DataLayer dataLayer;
    ProfileDetailPresenter profileDetailPresenter;
    AndroidPhotoPicker photoPicker;
    PagerAdapter pagerAdapter;
    Context mActivity;
    private User user;
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;
    private Direction direction = Direction.NONE;
    private int previousFingerPositionY;
    private int previousFingerPositionX;
    private int baseLayoutPosition;
    private Rect startBounds;
    private float startScaleFinal;

    enum Direction {
        UP_DOWN,
        NONE
    }

    private State mCurrentState = State.IDLE;

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    @Bind(R.id.fl_profile_parent) FrameLayout fl_profile_parent;
    @Bind(R.id.user_name) TextView userNameView;
    @Bind(R.id.user_location) TextView userLocationView;
    @Bind(R.id.user_photo) ImageView userPhotoView;
    @Bind(R.id.img_expanded) ImageView img_expanded;
    @Bind(R.id.user_photo_container) LinearLayout user_photo_container;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.pager) ViewPager viewPager;
    @Bind(R.id.tabs) TabLayout tabLayout;
    @Bind(R.id.img_profile_bg) ImageView img_profile_bg;
    @Bind(R.id.profile_photo_upload_progress) CircularProgressView photoUploadProgressView;
    private MenuItem menu_qrcode, menu_partner_program;
    Callback callback;

    public static ProfileDetailFragment newInstance(Bundle args) {
        ProfileDetailFragment fragment = new ProfileDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileDetailFragment newMyProfileInstance() {
        return newInstance(new Bundle());
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        dataLayer = DataLayer.getInstance(getContext());
        if (args.containsKey("user")) {
            User user = Parcels.unwrap(args.getParcelable("user"));
            profileDetailPresenter = new ProfileDetailPresenter(dataLayer, user);
        } else if (args.containsKey("userId")) {
            profileDetailPresenter = new ProfileDetailPresenter(dataLayer, args.getLong("userId"));
        } else {
            profileDetailPresenter = new ProfileDetailPresenter(dataLayer);
        }
        pagerAdapter = createPagerAdapter(profileDetailPresenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_detail, container, false);
        ButterKnife.bind(this, rootView);

        // PhotoPicker
        photoPicker = new AndroidPhotoPicker(getActivity(), PHOTO_PICKER_BASE_REQUEST_CODE);
        photoPicker.onRestoreInstanceState(savedInstanceState);
        profileDetailPresenter.setErrorHandler(
                new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), rootView)));
        profileDetailPresenter.setPhotoPicker(photoPicker);
        profileDetailPresenter.attachView(this);
        viewPager.setAdapter(pagerAdapter);
        configureToolbar();
        configureAppBarLayout(rootView);
        configureTabLayout();
        configureHighlightView();
        setRandomProfileBackground();

        if (!profileDetailPresenter.isMyProfile()) {
            menu_qrcode.setVisible(false);
            menu_partner_program.setVisible(false);
            img_profile_bg.setClickable(false);
        }
        return rootView;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (photoPicker.onActivityResult(requestCode, resultCode, data, true)) return;
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setRandomProfileBackground() {
        TypedArray images = getResources().obtainTypedArray(R.array.profile_bg_images);
        int choice = (int) (Math.random() * images.length());
        img_profile_bg.setImageResource(images.getResourceId(choice, R.drawable.profile_bg1));
        images.recycle();
    }

    private void configureAppBarLayout(View rootView) {
        AppBarLayout appbar_layout = findById(rootView, R.id.appbar_layout);
        appbar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    if (mCurrentState != State.EXPANDED) {
                        if (toolbar != null) {
                            toolbar.setNavigationIcon(profileDetailPresenter.isMyProfile() ? R.drawable.ic_prof_settings_white : R.drawable.ic_arrow_back_white);
                            menu_qrcode.setIcon(R.drawable.ic_prof_qr_white);
                            menu_partner_program.setIcon(R.drawable.ic_prof_partner_white);
                        }
                    }
                    mCurrentState = State.EXPANDED;
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (mCurrentState != State.COLLAPSED) {
                        if (toolbar != null) {
                            toolbar.setNavigationIcon(profileDetailPresenter.isMyProfile() ? R.drawable.ic_prof_settings_black : R.drawable.ic_arrow_back_black);
                            menu_qrcode.setIcon(R.drawable.ic_prof_qr_black);
                            menu_partner_program.setIcon(R.drawable.ic_prof_partner_black);
                        }
                    }
                    mCurrentState = State.COLLAPSED;
                } else {
                    mCurrentState = State.IDLE;
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        tabLayout.setupWithViewPager(null);
        viewPager.setAdapter(null);
        profileDetailPresenter.setErrorHandler(null);
        profileDetailPresenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        profileDetailPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.img_profile_bg)
    void onBackgroundClick() {
//        TODO: 11.01.2016 when API will be ready
//        profileDetailPresenter.onBackgroundClick();
    }

    @OnClick(R.id.user_photo)
    void onOpenUserPhoto() {
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        userPhotoView.getGlobalVisibleRect(startBounds);
        user_photo_container.getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, globalOffset.y * 3);
        finalBounds.offset(-globalOffset.x, globalOffset.y * 3);

        final float startScale;
        if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;

        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height());
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;

        }

        userPhotoView.setAlpha(0f);
        user_photo_container.setVisibility(View.VISIBLE);
        img_expanded.setVisibility(View.VISIBLE);
        img_expanded.setPivotX(0f);
        img_expanded.setPivotY(0f);

        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator.ofFloat(img_expanded, View.X, startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(img_expanded, View.Y, startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_X, startScale, 1f))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        startScaleFinal = startScale;
        img_expanded.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent ev) {
                final int y = (int) ev.getRawY();
                final int x = (int) ev.getRawX();

                if (ev.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    previousFingerPositionX = x;
                    previousFingerPositionY = y;
                    baseLayoutPosition = (int) v.getY();

                } else if (ev.getActionMasked() == MotionEvent.ACTION_MOVE) {
                    int diffY = y - previousFingerPositionY;
                    int diffX = x - previousFingerPositionX;

                    if (direction == Direction.NONE) {
                        if (Math.abs(diffX) < Math.abs(diffY)) {
                            direction = Direction.UP_DOWN;
                        } else {
                            direction = Direction.NONE;
                        }
                    }

                    if (direction == Direction.UP_DOWN) {
                        v.setY(baseLayoutPosition + diffY);
                        v.requestLayout();
                        return true;
                    }

                } else if (ev.getActionMasked() == MotionEvent.ACTION_UP) {
                    if (direction == Direction.UP_DOWN) {
                        int height = v.getHeight();
                        if (Math.abs(v.getY()) > (height / 6))
                            closeHighlightImage(startBounds, startScaleFinal);
                        else
                            v.setY(baseLayoutPosition);

                        direction = Direction.NONE;
                        return true;
                    } else {
                        closeHighlightImage(startBounds, startScaleFinal);
                    }
                    direction = Direction.NONE;
                }

                return true;
            }
        });
    }

    private void closeHighlightImage(Rect startBounds, float startScaleFinal) {
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator.ofFloat(img_expanded, View.X, startBounds.left))
                .with(ObjectAnimator.ofFloat(img_expanded, View.Y, startBounds.top))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_X, startScaleFinal))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_Y, startScaleFinal));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animationDone();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                animationDone();
            }

            private void animationDone() {
                userPhotoView.setAlpha(1f);
                user_photo_container.setVisibility(View.GONE);
                img_expanded.setVisibility(View.GONE);
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;
    }

    private PagerAdapter createPagerAdapter(ProfileDetailPresenter profileDetailPresenter) {
        FragmentArrayAdapter pagerAdapter = new FragmentArrayAdapter(getChildFragmentManager());
        if (profileDetailPresenter.isMyProfile()) {
            PhotoListFragment photoListFragment = PhotoListFragment.newInstance();
            photoListFragment.setCallback(new PhotoListFragment.Callback() {
                @Override
                public void participateClick() {
                    if (callback != null) {
                        callback.participateClick();
                    }
                }
            });

            FeedListFragment feedListFragment = FeedListFragment.newInstance();
            feedListFragment.setCallback(new FeedListFragment.Callback() {
                @Override
                public void participateClick() {
                    if (callback != null) {
                        callback.participateClick();
                    }
                }
            });
            pagerAdapter.addFragment(photoListFragment, getString(R.string.label_photos_tab));
            pagerAdapter.addFragment(feedListFragment, getString(R.string.label_updates_tab));
        } else {
            pagerAdapter.addFragment(PhotoListFragment.newInstance(profileDetailPresenter.getUserId()), null);
        }
        return pagerAdapter;
    }

    private void configureToolbar() {
        if (profileDetailPresenter.isMyProfile()) {
            toolbar.setNavigationIcon(R.drawable.ic_prof_settings_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profileDetailPresenter.onSettingsButtonClick();
                }
            });
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();

                }
            });
        }

        toolbar.inflateMenu(R.menu.menu_profile_detail);
        menu_qrcode = toolbar.getMenu().findItem(R.id.menu_qrcode);
        menu_partner_program = toolbar.getMenu().findItem(R.id.menu_partner_program);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.menu_qrcode) {
                    if (!dataLayer.authPreferences().hasShownPartnerQRIntro()) {
                        dataLayer.authPreferences().setShownPartnerQRIntro(true);
                        startActivity(PartnerProgramIntroActivity.newIntent(getActivity(), true, user));
                    } else {
                        startActivity(QRScanerActivity.newIntent(getActivity(), user));
                    }
                    return true;
                } else if (id == R.id.menu_partner_program) {
                    if (!dataLayer.authPreferences().hasShownPartnerIntro()) {
                        dataLayer.authPreferences().setShownPartnerIntro(true);
                        startActivity(PartnerProgramIntroActivity.newIntent(getActivity(), false, user));
                    } else {
                        startActivity(PartnerProgramActivity.newIntent(getActivity(), user));
                    }
                    return true;
                }
                return false;
            }
        });

    }

    private void configureTabLayout() {
        if (profileDetailPresenter.isMyProfile()) {
            tabLayout.setVisibility(View.VISIBLE);
            tabLayout.setupWithViewPager(viewPager);
        } else {
            tabLayout.setVisibility(View.GONE);
        }
    }

    private void configureHighlightView() {
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    private boolean activityFinish() {
        if (this.mActivity != null) {
            ((Activity) this.mActivity).finish();
            return true;
        } else {
            return false;
        }
    }

    private void showSelectedPhotoInternal(String url) {
        Picasso.with(getActivity()).load(isEmpty(url) ? null : url)
                .memoryPolicy(NO_CACHE)
                .fit()
                .into(img_profile_bg);
    }

    ///////////////////////////////////////////////////////////////////////////
    // CallbackBackPressed implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isPhotoOpenOnBackPressed() {
        if (img_expanded != null) {
            if (img_expanded.getVisibility() == View.VISIBLE) {
                closeHighlightImage(startBounds, startScaleFinal);
            } else {
                activityFinish();
            }
            return true;
        } else {
            return activityFinish();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // ProfileDetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showProfileLoadingIndicator(boolean show) {
        photoUploadProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onProfileLoaded(User user) {
        if (user == null) {
            return;
        }

        this.user = user;

        // Name
        userNameView.setText(user.getDisplayName());
        // Location
        String countryName = user.country == null ? null : trim(user.country.title);
        String cityName = user.city == null ? null : trim(user.city.title);
        String location;
        if (isEmpty(countryName)) {
            if (isEmpty(cityName)) {
                location = getString(R.string.dot_dot_dot);
            } else {
                location = cityName;
            }
        } else {
            if (isEmpty(cityName)) {
                location = countryName;
            } else {
                location = String.format(Locale.US, "%s, %s", cityName, countryName);
            }
        }
        userLocationView.setText(location);

        // Photo
        if (isEmpty(user.photo)) {
            userPhotoView.setVisibility(View.GONE);
        } else {
            Picasso.with(getContext())
                    .load(PhotoUrl.l(user.photo))
                    .fit()
                    .transform(Transformations.CIRCLE_WHITE_BORDER_1DP)
                    .into(userPhotoView);

            Picasso.with(getContext())
                    .load(PhotoUrl.xl(user.photo))
                    .placeholder(R.drawable.bg_placeholder_gradient)
                    .into(img_expanded);
        }
    }

    @Override
    public void openSettingsActivity() {
        startActivity(ProfileEditActivity.newIntent(getContext()));
    }

    @Override
    public void inviteAvailableLoaded(InviteAvailable inviteAvailable) {
        if (profileDetailPresenter.isMyProfile()) {
            menu_qrcode.setVisible(inviteAvailable.can_scan);
            menu_partner_program.setVisible(inviteAvailable.can_view);
        }
    }

    @Override
    public void showPhotoError(@StringRes int errorRes, boolean scrollToView) {
        if (scrollToView) {
            Toast.makeText(getActivity(), errorRes, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showSelectedPhoto(Uri photoFileUri) {
        showSelectedPhotoInternal(photoFileUri == null ? null : photoFileUri.toString());
    }

    @Override
    public void showPhotoUploadingIndicator(boolean show) {
        photoUploadProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setPhotoUploadProgress(int progress) {
        photoUploadProgressView.setProgress(progress);
    }

    @Override
    public void showPhotoUploadError() {
        Snackbar snackbar = multilineSnackbar(fl_profile_parent, R.string.photo_upload_failed, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDetailPresenter.onPhotoUploadRetryClick();
            }
        });
        snackbar.show();
    }

    @Override
    public void showTempPhotoStorageError() {
        Snackbar.make(fl_profile_parent, R.string.error_message_storage_create_temp_file, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCropActivityNotAvailable() {
        Snackbar.make(fl_profile_parent, R.string.error_message_crop_not_available, Snackbar.LENGTH_LONG).show();
    }

    public interface Callback {
        void participateClick();
    }
}
