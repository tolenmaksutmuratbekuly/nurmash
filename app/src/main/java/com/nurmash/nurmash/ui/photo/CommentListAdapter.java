package com.nurmash.nurmash.ui.photo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentListAdapter extends BaseAdapter {
    private static final int COMMENT_ITEM_LAYOUT = R.layout.photo_comment_list_item;
    private static final long ORIGINAL_COMMENT_ID = 0;
    private Context context;
    private Comment origComment;

    // Comment list ordered from newest to oldest.
    private List<Comment> comments = new ArrayList<>();
    private Callback callback;
    private long selectedCommentId = -1;

    public CommentListAdapter(Context context) {
        this.context = context;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setOriginalComment(Comment origComment) {
        this.origComment = origComment;
        notifyDataSetChanged();
    }

    public void setComments(List<Comment> newComments) {
        comments = new ArrayList<>(newComments);
        notifyDataSetChanged();
    }

    public int addNewComment(Comment comment) {
        int addedComentPosition = comments.size();
        comments.add(addedComentPosition, comment);
        notifyDataSetChanged();
        return addedComentPosition;
    }

    public void addOldComments(List<Comment> newComments) {
        Collections.reverse(newComments);
        comments.addAll(0, newComments);
        notifyDataSetChanged();
    }

    public void clearSelectedComment() {
        setSelectedComment(null);
    }

    private void setSelectedComment(Comment comment) {
        if (comment == null) {
            setSelectedCommentId(-1);
        } else if (comment == origComment) {
            setSelectedCommentId(ORIGINAL_COMMENT_ID);
        } else {
            setSelectedCommentId(comment.id);
        }
    }

    private void setSelectedCommentId(long commentId) {
        long oldSelectionId = selectedCommentId;
        selectedCommentId = commentId;
        if (oldSelectionId != selectedCommentId) {
            int oldSelectionPosition = getCommentPositionById(oldSelectionId);
            if (oldSelectionPosition != -1) {
                notifyDataSetChanged();
            }
            int newSelectionPosition = getCommentPositionById(selectedCommentId);
            if (newSelectionPosition != -1) {
                notifyDataSetChanged();
            }
        }
    }

    // If there is an original comment, it should be the first item in the list.
    private int getOrigCommentPosition() {
        return origComment != null ? 0 : -1;
    }

    private int getCommentPositionById(long id) {
        if (id == ORIGINAL_COMMENT_ID) {
            return getOrigCommentPosition();
        }
        if (id == -1 || comments == null) {
            return -1;
        }
        for (int i = 0; i < comments.size(); ++i) {
            Comment comment = comments.get(i);
            if (comment != null && comment.id == id) {
                return getCommentPosition(i);
            }
        }
        return -1;
    }

    private int getCommentPosition(int index) {
        return index;
    }

    private int getCommentIndex(int position) {
        return (origComment != null ? position - 1 : position);
    }

    @Override
    public int getCount() {
        return (origComment != null ? 1 : 0)
                + comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(COMMENT_ITEM_LAYOUT, parent, false);
            convertView.setTag(new CommentViewHolder(convertView));
        }

        CommentViewHolder viewHolder = (CommentViewHolder) convertView.getTag();
        if (position == getOrigCommentPosition()) {
            viewHolder.bindComment(origComment);
        } else {
            viewHolder.bindComment(comments.get(getCommentIndex(position)));
        }
        return convertView;
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        Context context;
        Comment bindedComment;
        @Bind(R.id.user_photo) ImageView userPhotoView;
        @Bind(R.id.user_name) TextView userNameView;
        @Bind(R.id.comment_body) TextView commentBodyView;
        @Bind(R.id.timestamp) TextView timestampView;

        public CommentViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindComment(Comment comment) {
            bindedComment = comment;
            if (comment == null) {
                userPhotoView.setImageDrawable(null);
                userNameView.setText(null);
                commentBodyView.setText(null);
                timestampView.setText(null);
                return;
            }
            itemView.setActivated(comment.id == selectedCommentId);
            User user = comment.user;
            if (user != null) {
                if (!TextUtils.isEmpty(user.photo)) {
                    Picasso.with(context).load(PhotoUrl.l(user.photo))
                            .fit()
                            .transform(Transformations.CIRCLE_NO_BORDER)
                            .into(userPhotoView);
                } else {
                    userPhotoView.setImageDrawable(null);
                }
                userNameView.setText(user.getDisplayName());
            } else {
                userPhotoView.setImageDrawable(null);
                userNameView.setText(null);
            }
            commentBodyView.setText(comment.body);
            if (comment.created_at != null) {
                timestampView.setText(new PrettyTime().format(comment.created_at));
            } else {
                timestampView.setText(null);
            }
        }

        @OnClick({R.id.user_photo})
        void onUserClick() {
            if (callback != null && bindedComment != null) {
                if (bindedComment.user != null) {
                    callback.onUserClick(bindedComment.user);
                }
            }
        }

        @OnClick({R.id.item_view, R.id.user_name})
        void onCommentSelect() {
            if (callback != null && bindedComment != null) {
                setSelectedComment(bindedComment);
                callback.onCommentSelected(bindedComment);
            }
        }
    }

    public interface Callback {
        void onUserClick(User user);

        void onCommentSelected(Comment comment);
    }
}
