package com.nurmash.nurmash.ui.prizes.verification;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocFrgmCallbacks;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoDocsFrgm extends Fragment {
    private PhotoDocFrgmCallbacks callbacks;

    public static PhotoDocsFrgm newInstance() {
        return new PhotoDocsFrgm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_document, viewGroup, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    public void setCallbacks(PhotoDocFrgmCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @OnClick(R.id.btn_idcard)
    void onIDCardClick() {
        callbacks.onIDCardClick();
    }

    @OnClick(R.id.btn_passport)
    void onPassportClick() {
        callbacks.onPassportClick();
    }
}