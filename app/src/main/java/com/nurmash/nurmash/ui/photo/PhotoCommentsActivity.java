package com.nurmash.nurmash.ui.photo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.photo.PhotoCommentsPresenter;
import com.nurmash.nurmash.mvp.photo.PhotoCommentsView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.profile.ProfileDetailActivity;
import com.nurmash.nurmash.util.ToastUtils;
import com.nurmash.nurmash.util.android.AndroidClipboardHelper;
import com.nurmash.nurmash.util.listgridviews.EndlessScrollListenerGridListViews;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.nurmash.nurmash.util.ContextUtils.hideSoftKeyboard;
import static com.nurmash.nurmash.util.ContextUtils.showSoftKeyboard;
import static com.nurmash.nurmash.util.SnackbarUtils.multilineSnackbar;
import static com.nurmash.nurmash.util.ViewUtils.moveEditorCursorToEnd;

public class PhotoCommentsActivity extends BaseActivity implements PhotoCommentsView, ReportDialogFragment.Callback {
    PhotoCommentsPresenter photoCommentsPresenter;
    CommentListAdapter commentListAdapter;
    ActionMode currentActionMode;
    @Bind(R.id.comment_list) ListView commentListView;
    @Bind(R.id.comment_input) EditText commentInputView;
    @Bind(R.id.button_send) Button sendButton;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;
    @Bind(R.id.loading_view) View loading_view;
    private boolean isFirstTime;

    public static Intent newIntent(Context packageContext, Competitor competitor, boolean focusInput) {
        Intent intent = new Intent(packageContext, PhotoCommentsActivity.class);
        intent.putExtra("competitor", Parcels.wrap(competitor));
        intent.putExtra("focusInput", focusInput);
        return intent;
    }

    public static Intent newIntent(Context packageContext, long competitorId, boolean focusInput) {
        Intent intent = new Intent(packageContext, PhotoCommentsActivity.class);
        intent.putExtra("competitorId", competitorId);
        intent.putExtra("focusInput", focusInput);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_comments);
        isFirstTime = true;
        ButterKnife.bind(this);
        configureListView();
        // Request focus if needed
        if (getIntent().getBooleanExtra("focusInput", false)) {
            commentInputView.requestFocus();
        }

        // Presenter
        Intent intent = getIntent();
        if (intent.hasExtra("competitor")) {
            Competitor competitor = Parcels.unwrap(getIntent().getParcelableExtra("competitor"));
            photoCommentsPresenter = new PhotoCommentsPresenter(DataLayer.getInstance(this), competitor);
        } else {
            long competitorId = getIntent().getLongExtra("competitorId", 0);
            photoCommentsPresenter = new PhotoCommentsPresenter(DataLayer.getInstance(this), competitorId);
        }
        photoCommentsPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, commentListView)));
        photoCommentsPresenter.setClipboardHelper(new AndroidClipboardHelper(this));
        photoCommentsPresenter.attachView(this);
    }

    @Override
    protected void onPause() {
        View view = getCurrentFocus();
        if (view != null) {
            hideSoftKeyboard(this);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        photoCommentsPresenter.setClipboardHelper(null);
        photoCommentsPresenter.setErrorHandler(null);
        photoCommentsPresenter.detachView();
        super.onDestroy();
    }

    private void configureListView() {
        commentListAdapter = new CommentListAdapter(this);
        commentListAdapter.setCallback(new CommentListAdapter.Callback() {
            @Override
            public void onUserClick(User user) {
                photoCommentsPresenter.onUserClick(user);
            }

            @Override
            public void onCommentSelected(Comment comment) {
                photoCommentsPresenter.onCommentSelected(comment);
            }
        });

        commentListView.setAdapter(commentListAdapter);
        commentListView.setOnScrollListener(new EndlessScrollListenerGridListViews(true) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                photoCommentsPresenter.onRequestMoreComments();
                return true;
            }
        });
    }

    @OnTextChanged(R.id.comment_input)
    void onCommentInputTextChanged(CharSequence text) {
        photoCommentsPresenter.onCommentInputTextChanged(text != null ? text.toString() : null);
    }

    @OnClick(R.id.button_send)
    void onSendClick() {
        photoCommentsPresenter.onSendClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // ReportDialogFragment.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onComplaintSelected(Complaint complaint, int competitorPosition) {

    }

    @Override
    public void onComplaintSelected(Complaint complaint) {
        photoCommentsPresenter.onReportReasonSelected(complaint);
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoCommentsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show, boolean translucent) {
        if (show) {
            if (translucent) {
                progressBarOverlay.setBackgroundColor(Color.parseColor("#aaffffff"));
            } else {
                progressBarOverlay.setBackgroundColor(Color.WHITE);
            }
            progressBarOverlay.setVisibility(View.VISIBLE);
        } else {
            progressBarOverlay.setVisibility(View.GONE);
        }
    }

    @Override
    public void showCommentsLoadingIndicator(boolean show) {
        loading_view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setSendButtonEnabled(boolean enabled) {
        sendButton.setEnabled(enabled);
    }

    @Override
    public void showOriginalComment(Comment origComment) {
        commentListAdapter.setOriginalComment(origComment);
    }

    @Override
    public void setComments(List<Comment> comments) {
        commentListAdapter.setComments(comments);
    }

    @Override
    public void addOldComments(List<Comment> comments) {
        commentListAdapter.addOldComments(comments);
        if (isFirstTime) {
            if (comments.size() > 0) {
                commentListView.setSelection(comments.size());
                isFirstTime = false;
            }
        }
    }

    @Override
    public void addNewComment(Comment comment) {
        commentListView.setSelection(commentListAdapter.addNewComment(comment));
    }

    @Override
    public void clearCommentSelection() {
        commentListAdapter.clearSelectedComment();
    }

    @Override
    public void clearCommentInputText() {
        commentInputView.setText("");
    }

    @Override
    public void setCommentInputText(String text) {
        commentInputView.setText(text);
        moveEditorCursorToEnd(commentInputView);
    }

    @Override
    public void focusOnCommentInput() {
        commentInputView.requestFocus();
        showSoftKeyboard(commentInputView);
    }

    @Override
    public void showCommentBodyTooLongErrorMessage(int length, int limit) {
        String message = getString(R.string.error_message_comment_too_long, length, limit);
        multilineSnackbar(commentListView, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showCommentContextMenu(boolean show) {
        if (show) {
            if (currentActionMode == null) {
                currentActionMode = startActionMode(new CommentActionModeCallback());
            } else {
                currentActionMode.invalidate();
            }
        } else {
            if (currentActionMode != null) {
                currentActionMode.finish();
            }
        }
    }

    @Override
    public void showCommentCopiedToastMessage() {
        ToastUtils.centeredToast(this, R.string.info_message_comment_copied, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openCommentReportDialog() {
        ReportDialogFragment.newCommentComplaintInstance().show(getSupportFragmentManager(), "ReportDialogFragment");
    }

    @Override
    public void showCommentReportedToastMessage() {
        ToastUtils.centeredToast(this, R.string.info_message_photo_report_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCommentComplaintExistsError() {
        ToastUtils.centeredToast(this, R.string.error_message_comment_complaint_exists, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openUserProfileActivity(User user) {
        startActivity(ProfileDetailActivity.newIntent(this, user));
    }

    ///////////////////////////////////////////////////////////////////////////
    // ActionMode.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    class CommentActionModeCallback implements ActionMode.Callback {
        void updateVisibleItems(Menu menu) {
            MenuItem copyItem = menu.findItem(R.id.copy);
            if (copyItem != null) {
                copyItem.setVisible(photoCommentsPresenter.isCopyActionEnabled());
            }
            MenuItem replyItem = menu.findItem(R.id.reply);
            if (replyItem != null) {
                replyItem.setVisible(photoCommentsPresenter.isReplyActionEnabled());
            }
            MenuItem reportItem = menu.findItem(R.id.report);
            if (reportItem != null) {
                reportItem.setVisible(photoCommentsPresenter.isReportActionEnabled());
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_comment_action, menu);
            updateVisibleItems(menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            updateVisibleItems(menu);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.copy:
                    photoCommentsPresenter.onCommentCopyAction();
                    return true;
                case R.id.reply:
                    photoCommentsPresenter.onCommentReplyAction();
                    return true;
                case R.id.report:
                    photoCommentsPresenter.onCommentReportAction();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            currentActionMode = null;
            photoCommentsPresenter.onCommentContextMenuDestroy();
        }
    }
}
