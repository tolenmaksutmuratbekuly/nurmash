package com.nurmash.nurmash.ui.partnerprogram;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.StringUtils.trim;

public class PartnerProgramActivity extends BaseActivity {
    @Bind(R.id.user_name) TextView userNameView;
    @Bind(R.id.user_photo) ImageView userPhotoView;
    @Bind(R.id.user_location) TextView userLocationView;
    @Bind(R.id.img_expanded) ImageView img_expanded;
    @Bind(R.id.user_photo_container) LinearLayout user_photo_container;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.pager) ViewPager viewPager;
    @Bind(R.id.tabs) TabLayout tabLayout;
    private MenuItem info;

    private Context context;
    private User user;
    private PagerAdapter pagerAdapter;
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;
    private Direction direction = Direction.NONE;
    private int previousFingerPositionY;
    private int previousFingerPositionX;
    private int baseLayoutPosition;
    private Rect startBounds;
    private float startScaleFinal;

    enum Direction {
        UP_DOWN,
        NONE
    }

    private State mCurrentState = State.IDLE;

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    public static Intent newIntent(Context packageContext, User user) {
        Intent intent = new Intent(packageContext, PartnerProgramActivity.class);
        intent.putExtra("user", Parcels.wrap(user));
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner_program);
        ButterKnife.bind(this);
        context = this;

        Intent intent = getIntent();
        if (intent.hasExtra("user")) {
            user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
        }

        if (user != null) {
            // Name
            userNameView.setText(user.getDisplayName());

            // Location
            String countryName = user.country == null ? null : trim(user.country.title);
            String cityName = user.city == null ? null : trim(user.city.title);
            String location;
            if (isEmpty(countryName)) {
                if (isEmpty(cityName)) {
                    location = getString(R.string.dot_dot_dot);
                } else {
                    location = cityName;
                }
            } else {
                if (isEmpty(cityName)) {
                    location = countryName;
                } else {
                    location = String.format(Locale.US, "%s, %s", cityName, countryName);
                }
            }
            userLocationView.setText(location);

            // Photo
            if (isEmpty(user.photo)) {
                userPhotoView.setVisibility(View.GONE);
            } else {
                Picasso.with(this)
                        .load(PhotoUrl.l(user.photo))
                        .fit()
                        .transform(Transformations.CIRCLE_WHITE_BORDER_1DP)
                        .into(userPhotoView);

                Picasso.with(this)
                        .load(PhotoUrl.xl(user.photo))
                        .placeholder(R.drawable.bg_placeholder_gradient)
                        .into(img_expanded);
            }
        }

        pagerAdapter = createPagerAdapter();
        viewPager.setAdapter(pagerAdapter);

        configureToolbar();
        configureAppBarLayout();
        configureTabLayout();
        configureHighlightView();

    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @OnClick(R.id.user_photo)
    void onOpenUserPhoto() {
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        userPhotoView.getGlobalVisibleRect(startBounds);
        user_photo_container.getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, globalOffset.y * 3);
        finalBounds.offset(-globalOffset.x, globalOffset.y * 3);

        final float startScale;
        if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;

        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height());
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;

        }

        userPhotoView.setAlpha(0f);
        user_photo_container.setVisibility(View.VISIBLE);
        img_expanded.setVisibility(View.VISIBLE);
        img_expanded.setPivotX(0f);
        img_expanded.setPivotY(0f);

        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator.ofFloat(img_expanded, View.X, startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(img_expanded, View.Y, startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_X, startScale, 1f))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        startScaleFinal = startScale;
        img_expanded.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent ev) {
                final int y = (int) ev.getRawY();
                final int x = (int) ev.getRawX();

                if (ev.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    previousFingerPositionX = x;
                    previousFingerPositionY = y;
                    baseLayoutPosition = (int) v.getY();

                } else if (ev.getActionMasked() == MotionEvent.ACTION_MOVE) {
                    int diffY = y - previousFingerPositionY;
                    int diffX = x - previousFingerPositionX;

                    if (direction == Direction.NONE) {
                        if (Math.abs(diffX) < Math.abs(diffY)) {
                            direction = Direction.UP_DOWN;
                        } else {
                            direction = Direction.NONE;
                        }
                    }

                    if (direction == Direction.UP_DOWN) {
                        v.setY(baseLayoutPosition + diffY);
                        v.requestLayout();
                        return true;
                    }

                } else if (ev.getActionMasked() == MotionEvent.ACTION_UP) {
                    if (direction == Direction.UP_DOWN) {
                        int height = v.getHeight();
                        if (Math.abs(v.getY()) > (height / 6))
                            closeHighlightImage(startBounds, startScaleFinal);
                        else
                            v.setY(baseLayoutPosition);

                        direction = Direction.NONE;
                        return true;
                    } else {
                        closeHighlightImage(startBounds, startScaleFinal);
                    }
                    direction = Direction.NONE;
                }

                return true;
            }
        });
    }

    private void closeHighlightImage(Rect startBounds, float startScaleFinal) {
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator.ofFloat(img_expanded, View.X, startBounds.left))
                .with(ObjectAnimator.ofFloat(img_expanded, View.Y, startBounds.top))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_X, startScaleFinal))
                .with(ObjectAnimator.ofFloat(img_expanded, View.SCALE_Y, startScaleFinal));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animationDone();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                animationDone();
            }

            private void animationDone() {
                userPhotoView.setAlpha(1f);
                user_photo_container.setVisibility(View.GONE);
                img_expanded.setVisibility(View.GONE);
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;
    }

    private PagerAdapter createPagerAdapter() {
        FragmentArrayAdapter pagerAdapter = new FragmentArrayAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(PartnerQRCodeFragment.newInstance(user), getString(R.string.label_partner_yourcode));
        pagerAdapter.addFragment(PartnerInvitationsFragment.newInstance(), getString(R.string.label_partner_invitations));
        pagerAdapter.addFragment(PartnerBalanceFragment.newInstance(), getString(R.string.label_partner_balance));

        return pagerAdapter;
    }

    private void configureToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        toolbar.inflateMenu(R.menu.menu_prizes);
        info = toolbar.getMenu().findItem(R.id.info);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.info:
                        startActivity(PartnerProgramInfoActivity.newIntent(context));
                        return true;
                }
                return false;
            }
        });
    }

    private void configureAppBarLayout() {
        AppBarLayout appbar_layout = (AppBarLayout) findViewById(R.id.appbar_layout);
        appbar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    if (mCurrentState != State.EXPANDED) {
                        if (toolbar != null) {
                            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
                            info.setIcon(R.drawable.ic_info_white);
                        }
                    }
                    mCurrentState = State.EXPANDED;
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (mCurrentState != State.COLLAPSED) {
                        if (toolbar != null) {
                            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
                            info.setIcon(R.drawable.ic_info_black);
                        }
                    }
                    mCurrentState = State.COLLAPSED;
                } else {
                    if (mCurrentState != State.IDLE) {
                    }
                    mCurrentState = State.IDLE;
                }
            }
        });
    }

    private void configureTabLayout() {
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void configureHighlightView() {
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

    }
}
