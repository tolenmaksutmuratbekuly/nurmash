package com.nurmash.nurmash.ui.prizes.info;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PrizesInfoActivity extends BaseActivity {
    private static final String SHOW_PRIZES_INFO_ACTION = "SHOW_PRIZES_INFO_ACTION";
    private static final String SHOW_WITHDRAW_METHODS_INFO_ACTION = "SHOW_WITHDRAW_METHODS_INFO_ACTION";

    @Bind(R.id.recycler_view) RecyclerView recyclerView;

    private static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, PrizesInfoActivity.class);
    }

    public static Intent newPrizesInfoIntent(Context packageContext) {
        return newIntent(packageContext).setAction(SHOW_PRIZES_INFO_ACTION);
    }

    public static Intent newWithdrawMethodsInfoIntent(Context packageContext) {
        return newIntent(packageContext).setAction(SHOW_WITHDRAW_METHODS_INFO_ACTION);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prizes_info);
        ButterKnife.bind(this);
        configureRecyclerView(getIntent().getAction());
    }

    private void configureRecyclerView(String action) {
        if (action == null) {
            return;
        }
        PrizesInfoListAdapter adapter = new PrizesInfoListAdapter();
        switch (action) {
            case SHOW_PRIZES_INFO_ACTION:
                adapter.addItem(R.drawable.prizes_info_user_balance_logo, R.string.prizes_info_total_prizes_label,
                        R.string.prizes_info_total_prizes_text);
                adapter.addItem(R.drawable.prizes_info_jackpot_logo, R.string.prizes_info_jackpot_label,
                        R.string.prizes_info_jackpot_text);
                break;
            case SHOW_WITHDRAW_METHODS_INFO_ACTION:
                adapter.addItem(R.drawable.ic_claim_bank_transfer, R.string.prizes_info_cardtransfer_label,
                        R.string.prizes_info_cardtransfer_text_long);
                adapter.addItem(R.drawable.ic_claim_mobile, R.string.prizes_info_mobile_credit_label,
                        R.string.prizes_info_mobile_credit_text_long);
                break;
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
