package com.nurmash.nurmash.ui.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.profile.PhotoListPresenter;
import com.nurmash.nurmash.mvp.profile.PhotoListView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.ListPlaceholderViewHolder;
import com.nurmash.nurmash.ui.PhotoListAdapter;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PhotoListFragment extends Fragment implements PhotoListView {
    PhotoListPresenter photoListPresenter;
    PhotoListAdapter photoListAdapter;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list_placeholder_stub) ViewStub listPlaceholderStub;
    EndlessScrollListener endlessScrollListener;
    ListPlaceholderViewHolder listPlaceholderViewHolder;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    private Callback callback;

    public static PhotoListFragment newInstance() {
        return new PhotoListFragment();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public static PhotoListFragment newInstance(long userId) {
        PhotoListFragment fragment = new PhotoListFragment();
        Bundle args = new Bundle();
        args.putLong("userId", userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataLayer dataLayer = DataLayer.getInstance(getContext());
        if (getArguments() != null && getArguments().containsKey("userId")) {
            photoListPresenter = new PhotoListPresenter(dataLayer, getArguments().getLong("userId"));
        } else {
            photoListPresenter = new PhotoListPresenter(dataLayer);
        }
        photoListAdapter = new PhotoListAdapter(true, false);
        photoListAdapter.setCallback(new PhotoListAdapter.Callback() {
            @Override
            public void onCompetitorClick(Competitor competitor, int position) {
                photoListPresenter.onPhotoClick(competitor);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_photo_list, container, false);
        ButterKnife.bind(this, rootView);
        configureRecyclerView();
        // Presenter
        photoListPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), recyclerView)));
        photoListPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Picasso.with(getContext()).cancelTag(photoListAdapter.picassoTag);
        photoListPresenter.setErrorHandler(null);
        photoListPresenter.detachView();
        recyclerView.clearOnScrollListeners();
        recyclerView.setAdapter(null);
        endlessScrollListener = null;
        listPlaceholderViewHolder = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void configureRecyclerView() {
        recyclerView.setAdapter(photoListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                photoListPresenter.onRequestMorePhotos();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                photoListPresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    private ListPlaceholderViewHolder getListPlaceholderViewHolder() {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListPlaceholderViewHolder(listPlaceholderStub.inflate());
            listPlaceholderViewHolder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    photoListPresenter.onRefreshAction();
                }
            });
            listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
        }
        return listPlaceholderViewHolder;
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showPhotosLoadingIndicator(boolean show) {
        photoListAdapter.setLoading(show);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
            if (listPlaceholderViewHolder != null) {
                listPlaceholderViewHolder.setRefreshing(false);
            }
        }
    }

    @Override
    public void setPhotoItems(List<Competitor> competitors) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        photoListAdapter.setCompetitors(competitors);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addPhotoItems(List<Competitor> competitors) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        photoListAdapter.addCompetitors(competitors);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void showPhotoListPlaceholder(@StringRes int textRes, @StringRes int btnRes) {
        ListPlaceholderViewHolder listPlaceholderViewHolder = getListPlaceholderViewHolder();
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setButtonText(btnRes);
        listPlaceholderViewHolder.setButtonCallback(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.participateClick();
                }
            }
        });
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void openPhotoDetailActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(getContext(), competitorId));
    }

    @Override
    public void finishActivity() {
        getActivity().finish();
    }

    public interface Callback {
        void participateClick();
    }
}
