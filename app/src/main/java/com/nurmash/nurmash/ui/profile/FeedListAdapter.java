package com.nurmash.nurmash.ui.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.FeedItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.nurmash.nurmash.util.recyclerview.SimpleViewHolder;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

class FeedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int FEED_ITEM_LAYOUT_ID = R.layout.profile_feed_list_item;
    private static final int LOADING_ITEM_LAYOUT_ID = R.layout.profile_feed_list_loading_item;

    private Callback callback;
    private boolean loading;
    private List<FeedItem> items = new ArrayList<>();

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setLoading(boolean loading) {
        if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(items.size());
        } else if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(items.size());
        }
    }

    public void setItems(List<FeedItem> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            items = new ArrayList<>();
        } else {
            items = new ArrayList<>(newItems);
        }
        notifyDataSetChanged();
    }

    void addItems(List<FeedItem> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            return;
        }
        int positionStart = items.size();
        int itemCount = newItems.size();
        items.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    private int getLoadingIndicatorPosition() {
        return items.size();
    }

    private int getItemIndex(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size() + (loading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getLoadingIndicatorPosition()) {
            return LOADING_ITEM_LAYOUT_ID;
        } else {
            return FEED_ITEM_LAYOUT_ID;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case FEED_ITEM_LAYOUT_ID:
                return new FeedItemViewHolder(view);
            case LOADING_ITEM_LAYOUT_ID:
                return new SimpleViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FeedItemViewHolder) {
            ((FeedItemViewHolder) holder).bindFeedItem(items.get(getItemIndex(position)));
        }
    }

    class FeedItemViewHolder extends RecyclerView.ViewHolder {
        Context context;
        FeedItem bindedFeedItem;
        @Bind(R.id.author_photo) ImageView authorPhotoView;
        @Bind(R.id.author_name) TextView authorNameView;
        @Bind(R.id.timestamp) TextView timestampView;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.text) TextView textView;

        FeedItemViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindFeedItem(FeedItem item) {
            bindedFeedItem = item;
            // Author name
            User author = item == null ? null : item.author;
            if (author != null) {
                authorNameView.setText(author.getDisplayName());
            } else {
                authorNameView.setText(null);
            }
            // Author photo
            String authorPhotoHash = author == null ? null : author.photo;
            if (authorPhotoHash != null && !authorPhotoHash.isEmpty()) {
                Picasso.with(context).load(PhotoUrl.l(authorPhotoHash))
                        .fit()
                        .transform(Transformations.CIRCLE_NO_BORDER)
                        .into(authorPhotoView);
            } else {
                authorPhotoView.setImageDrawable(null);
            }
            // Timestamp
            Date timestamp = item == null ? null : item.datetime;
            if (timestamp != null) {
                PrettyTime prettyTime = new PrettyTime();
                timestampView.setText(prettyTime.format(timestamp));
            } else {
                timestampView.setText(null);
            }
            // Competitor photo
            String photoHash = item == null ? null : item.photo;
            if (photoHash != null && !photoHash.isEmpty()) {
                Picasso.with(context).load(PhotoUrl.l(photoHash))
                        .fit()
                        .transform(Transformations.ROUND_RECT_2DP)
                        .into(photoView);
            } else {
                photoView.setImageDrawable(null);
            }
            // Text
            if (item != null && item.isComment()) {
                timestampView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_feed_comment, 0, 0, 0);
                textView.setText(R.string.info_text_user_commented_photo);
            } else if (item != null && item.isCommentReply()) {
                timestampView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_feed_comment_reply, 0, 0, 0);
                textView.setText(R.string.info_text_user_comment_reply);
            } else if (item != null && item.isLike()) {
                timestampView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_feed_like, 0, 0, 0);
                textView.setText(R.string.info_text_user_liked_your_photo);
            } else {
                textView.setText(null);
            }
        }

        @OnClick(R.id.item_view)
        void onClick() {
            if (callback != null) callback.onFeedItemClick(bindedFeedItem);
        }

        @OnClick(R.id.author_photo)
        void onAuthorClick() {
            if (callback != null && bindedFeedItem != null && bindedFeedItem.author != null) {
                callback.onUserClick(bindedFeedItem.author);
            }
        }
    }

    public interface Callback {
        void onFeedItemClick(FeedItem feedItem);

        void onUserClick(User user);
    }
}
