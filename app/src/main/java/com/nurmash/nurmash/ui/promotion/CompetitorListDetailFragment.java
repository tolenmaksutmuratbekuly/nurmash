package com.nurmash.nurmash.ui.promotion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.promotion.CompetitorListFragmentCallbacks;
import com.nurmash.nurmash.ui.photo.PhotoDetailContentAdapter;
import com.nurmash.nurmash.util.listgridviews.EndlessScrollListenerGridListViews;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class CompetitorListDetailFragment extends Fragment implements StickyListHeadersListView.OnStickyHeaderChangedListener {
    PhotoDetailContentAdapter photoDetailAdapter;
    private CompetitorListFragmentCallbacks callbacks;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.lv_competitors_detail) StickyListHeadersListView lv_competitors_detail;
    @Bind(R.id.report_loading_indicator) ViewGroup reportLoadingIndicator;

    public static CompetitorListDetailFragment newInstance() {
        return new CompetitorListDetailFragment();
    }

    public void setCallbacks(CompetitorListFragmentCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_competitor_list_detail, container, false);
        ButterKnife.bind(this, rootView);

        configureRecyclerView();
        configureToolbar();
        return rootView;
    }

    private void configureRecyclerView() {
        photoDetailAdapter = new PhotoDetailContentAdapter(getActivity());
        photoDetailAdapter.setCallbacks(new PhotoDetailContentAdapter.Callbacks() {
            @Override
            public void onUserClick(User user) {
                callbacks.onUserClick(user);
            }

            @Override
            public void onLikeButtonClick(Competitor competitor, int competitorPosition) {
                callbacks.onLikeClick(competitor, competitorPosition);
            }

            @Override
            public void onCommentButtonClick(Competitor competitor) {
                callbacks.onCommentButtonClick(competitor);
            }

            @Override
            public void onCompetitorLikesButtonClick(Competitor competitor) {
                callbacks.onCompetitorLikesButtonClick(competitor);
            }

            @Override
            public void onReportButtonClick(Competitor competitor, int competitorPosition) {
                callbacks.openPhotoReportDialog(competitorPosition);
                callbacks.setReportCompetitor(competitor);
            }

            @Override
            public void onOpenPromoButtonClick(Competitor competitor) {
                callbacks.onOpenPromoButtonClick(competitor);
            }

            @Override
            public void onMoreCommentsButtonClick(Competitor competitor) {
                callbacks.onMoreCommentsButtonClick(competitor);
            }
        });

        lv_competitors_detail.setOnStickyHeaderChangedListener(this);
        lv_competitors_detail.setDrawingListUnderStickyHeader(true);
        lv_competitors_detail.setAreHeadersSticky(true);
        lv_competitors_detail.setAdapter(photoDetailAdapter);
        lv_competitors_detail.setOnScrollListener(new EndlessScrollListenerGridListViews(false) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                callbacks.loadMoreItems();
                return true;
            }
        });
    }

    private void configureToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbacks.hideDetailFragment(currentPosition());
            }
        });
    }

    public int currentPosition() {
        return lv_competitors_detail.getLastVisiblePosition();
    }

    public void addCompetitors(List<Competitor> competitors) {
        photoDetailAdapter.addCompetitors(competitors);
    }

    public void setAuthUserId(long authUserId) {
        photoDetailAdapter.setAuthUserId(authUserId);
    }

    public void setCompetitors(List<Competitor> competitors) {
        photoDetailAdapter.setCompetitors(competitors);
    }

    public void refreshItem() {
        photoDetailAdapter.refreshItem();
    }

    public void scrollToPosition(int competitorIndex) {
        lv_competitors_detail.setSelection(competitorIndex);
    }

    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
    }
}