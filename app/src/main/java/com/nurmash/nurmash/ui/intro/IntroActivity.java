package com.nurmash.nurmash.ui.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.login.LoginActivity;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnPageChange;
import me.relex.circleindicator.CircleIndicator;

public class IntroActivity extends BaseActivity {
    FragmentArrayAdapter pagerAdapter;
    @Bind(R.id.pager) ViewPager pager;
    @Bind(R.id.pager_indicator) CircleIndicator pagerIndicator;
    @Bind(R.id.button_next) TextView buttonNext;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, IntroActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        configureViewPager();
    }

    void configureViewPager() {
        pagerAdapter = new FragmentArrayAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(IntroPageFragment.newInstance(
                R.string.intro_title_page_1,
                R.string.intro_text_page_1,
                R.drawable.img_intro_page_1));
        pagerAdapter.addFragment(IntroPageFragment.newInstance(
                R.string.intro_title_page_2,
                R.string.intro_text_page_2,
                R.drawable.img_intro_page_2));
        pagerAdapter.addFragment(IntroPageFragment.newInstance(
                R.string.intro_title_page_3,
                R.string.intro_text_page_3,
                R.drawable.img_intro_page_3));
        pagerAdapter.addFragment(IntroPageFragment.newInstance(
                R.string.intro_title_page_4,
                R.string.intro_text_page_4,
                R.drawable.img_intro_page_4));
        pagerAdapter.addFragment(IntroLoginFragment.newInstance());
        pager.setAdapter(pagerAdapter);
        pagerIndicator.setViewPager(pager);
    }

    @OnPageChange(value = R.id.pager, callback = OnPageChange.Callback.PAGE_SELECTED)
    void onPageSelected(int position) {
        if (position == pagerAdapter.getCount() - 1) {
            buttonNext.setText(R.string.action_login);
        } else {
            buttonNext.setText(R.string.action_next);
        }
    }

    @OnClick(R.id.button_next)
    void onNextClick() {
        if (pager.getCurrentItem() == pagerAdapter.getCount() - 1) {
            openLoginActivity();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() + 1);
        }
    }

    @OnClick(R.id.button_skip)
    void onSkipClick() {
        openLoginActivity();
    }

    void openLoginActivity() {
        startActivity(LoginActivity.newIntent(this).setAction(LoginActivity.ACTION_SKIP_INTRO));
    }
}
