package com.nurmash.nurmash.ui.photo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Complaint;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.photo.PhotoDetailPresenter;
import com.nurmash.nurmash.mvp.photo.PhotoDetailView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.profile.ProfileDetailActivity;
import com.nurmash.nurmash.ui.promotion.PromoDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class PhotoDetailActivity extends BaseActivity implements PhotoDetailView, ReportDialogFragment.Callback, StickyListHeadersListView.OnStickyHeaderChangedListener {
    PhotoDetailPresenter photoDetailPresenter;
    PhotoDetailContentAdapter photoDetailAdapter;
    Context context;
    @Bind(R.id.lv_competitors_detail) StickyListHeadersListView lv_competitors_detail;
    @Bind(R.id.loading_indicator) ViewGroup loadingIndicator;
    @Bind(R.id.report_loading_indicator) ViewGroup reportLoadingIndicator;

    public static Intent newIntent(Context packageContext, long competitorId) {
        Intent intent = new Intent(packageContext, PhotoDetailActivity.class);
        intent.putExtra("competitorId", competitorId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        context = this;
        ButterKnife.bind(this);
        configureRecyclerView();
        // Presenter
        long competitorId = getIntent().getLongExtra("competitorId", 0);
        photoDetailPresenter = new PhotoDetailPresenter(DataLayer.getInstance(this), competitorId);
        photoDetailPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, lv_competitors_detail)));
        photoDetailPresenter.attachView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        photoDetailPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            Picasso.with(this).cancelTag(this);
        }
        photoDetailPresenter.setErrorHandler(null);
        photoDetailPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
    }

    private void configureRecyclerView() {
        photoDetailAdapter = new PhotoDetailContentAdapter(this);
        photoDetailAdapter.setCallbacks(new PhotoDetailContentAdapter.Callbacks() {
            @Override
            public void onUserClick(User user) {
                photoDetailPresenter.onUserClick(user);
            }

            @Override
            public void onLikeButtonClick(Competitor competitor, int competitorPosition) {
                photoDetailPresenter.onLikeClick();
            }

            @Override
            public void onCommentButtonClick(Competitor competitor) {
                photoDetailPresenter.onCommentButtonClick();
            }

            @Override
            public void onCompetitorLikesButtonClick(Competitor competitor) {
                startActivity(CompetitorLikesActivity.newIntent(context, competitor.id));
            }

            @Override
            public void onReportButtonClick(Competitor competitor, int competitorPosition) {
                openPhotoReportDialog();
            }

            @Override
            public void onOpenPromoButtonClick(Competitor competitor) {
                photoDetailPresenter.onOpenPromoButtonClick();
            }

            @Override
            public void onMoreCommentsButtonClick(Competitor competitor) {
                photoDetailPresenter.onMoreCommentsButtonClick();
            }
        });
        lv_competitors_detail.setOnStickyHeaderChangedListener(this);
        lv_competitors_detail.setDrawingListUnderStickyHeader(true);
        lv_competitors_detail.setAreHeadersSticky(true);
        lv_competitors_detail.setAdapter(photoDetailAdapter);
    }

    private void openPhotoReportDialog() {
        ReportDialogFragment.newPhotoComplaintInstance(-1).show(getSupportFragmentManager(), "ReportDialogFragment");
    }

    ///////////////////////////////////////////////////////////////////////////
    // ReportDialogFragment.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onComplaintSelected(Complaint complaint, int competitorPosition) {

    }

    @Override
    public void onComplaintSelected(Complaint complaint) {
        photoDetailPresenter.onComplaintSelected(complaint);
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoDetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        loadingIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onCompetitorLoaded(Competitor competitor, long authUserId) {
        photoDetailAdapter.setAuthUserId(authUserId);
        photoDetailAdapter.setCompetitors(Collections.singletonList(competitor));
    }

    @Override
    public void showReportLoadingIndicator(boolean show) {
        reportLoadingIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPhotoReportedMessage() {
        Toast.makeText(this, R.string.info_message_photo_report_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPhotoComplaintExistsError() {
        Toast.makeText(this, R.string.error_message_photo_complaint_exists, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openCommentListActivity(Competitor competitor, boolean focusInput) {
        startActivity(PhotoCommentsActivity.newIntent(this, competitor, focusInput));
    }

    @Override
    public void openUserProfileActivity(User user) {
        startActivity(ProfileDetailActivity.newIntent(this, user));
    }

    @Override
    public void openPromoDetailActivity(long promoId) {
        startActivity(PromoDetailActivity.newIntent(this, promoId));
    }
}
