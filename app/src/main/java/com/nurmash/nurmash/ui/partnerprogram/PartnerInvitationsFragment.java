package com.nurmash.nurmash.ui.partnerprogram;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.InviteList;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.partnerprogram.PartnerInvitationsPresenter;
import com.nurmash.nurmash.mvp.partnerprogram.PartnerInvitationsView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PartnerInvitationsFragment extends Fragment implements PartnerInvitationsView {
    PartnerInvitationsPresenter invitationsPresenter;
    InvitationsListAdapter invitationsListAdapter;
    EndlessScrollListener endlessScrollListener;
    private List<InviteList> items = new ArrayList<>();
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.loading_indicator) ViewGroup loading_indicator;
    @Bind(R.id.tv_invite_description) TextView tv_invite_description;
    @Bind(R.id.tv_invited_users_sum) TextView tv_invited_users_sum;
    @Bind(R.id.tv_sorting_users) TextView tv_sorting_users;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    public static PartnerInvitationsFragment newInstance() {
        return new PartnerInvitationsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invitationsPresenter = new PartnerInvitationsPresenter(DataLayer.getInstance(getContext()));
        invitationsListAdapter = new InvitationsListAdapter(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_partner_invitations, container, false);
        ButterKnife.bind(this, rootView);

        DataLayer dataLayer = DataLayer.getInstance(getContext());
        if (dataLayer.authPreferences().hasShownInviteDescription()) {
            tv_invite_description.setVisibility(View.GONE);
        } else {
            tv_invite_description.setVisibility(View.VISIBLE);
            dataLayer.authPreferences().setShownInviteDescription(true);
        }

        long myUserId = dataLayer.authPreferences().getUserId();
        configureRecyclerView(myUserId);

        // Presenter
        invitationsPresenter.setMyUserId(myUserId);
        invitationsPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), recyclerView)));
        invitationsPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        invitationsPresenter.setErrorHandler(null);
        invitationsPresenter.detachView();
        recyclerView.setAdapter(null);
        recyclerView.clearOnScrollListeners();
        endlessScrollListener = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void configureRecyclerView(long myUserId) {
        invitationsListAdapter.setMyUserId(myUserId);
        recyclerView.setAdapter(invitationsListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                invitationsPresenter.onRequestMoreItems();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                invitationsPresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    @OnClick(R.id.tv_sorting_users)
    void sortingUsersClick() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_sort_users);

        final LinearLayout ll_sort_all = (LinearLayout) dialog.findViewById(R.id.ll_sort_all);
        final LinearLayout ll_sort_active = (LinearLayout) dialog.findViewById(R.id.ll_sort_active);
        final LinearLayout ll_sort_nonactive = (LinearLayout) dialog.findViewById(R.id.ll_sort_nonactive);
        final LinearLayout ll_cancel = (LinearLayout) dialog.findViewById(R.id.ll_cancel);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_sort_all.isPressed()) {
                    tv_sorting_users.setText(getString(R.string.label_users_all));
                    invitationsListAdapter.clearAllItems();
                    invitationsListAdapter.setItems(items);
                } else if (ll_sort_active.isPressed()) {
                    tv_sorting_users.setText(getString(R.string.label_users_active));
                    activeNonActive(1);
                } else if (ll_sort_nonactive.isPressed()) {
                    tv_sorting_users.setText(getString(R.string.label_users_nonactive));
                    activeNonActive(0);
                }
                dialog.dismiss();
            }
        };

        ll_sort_all.setOnClickListener(clickListener);
        ll_sort_active.setOnClickListener(clickListener);
        ll_sort_nonactive.setOnClickListener(clickListener);
        ll_cancel.setOnClickListener(clickListener);

        dialog.show();
    }

    private void activeNonActive(int status) {
        ArrayList<InviteList> tempItems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).is_fulfilled == status) {
                tempItems.add(items.get(i));
            }
        }
        invitationsListAdapter.clearAllItems();
        invitationsListAdapter.setItems(tempItems);
    }

    ///////////////////////////////////////////////////////////////////////////
    // PartnerInvitationsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        invitationsListAdapter.setLoading(show);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void setInvitationItems(List<InviteList> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            items = new ArrayList<>();
        } else {
            items = new ArrayList<>(newItems);
        }

        invitationsListAdapter.setItems(newItems);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addInvitationItems(List<InviteList> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            return;
        }
        items.addAll(newItems);
        invitationsListAdapter.addItems(newItems);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void invitedUsersSum(int invite_count) {
        tv_invited_users_sum.setText(getString(R.string.label_invited_users_amount, invite_count));
    }
}
