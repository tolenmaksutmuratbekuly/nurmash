package com.nurmash.nurmash.ui.promotion;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SamplePhotosAdapter extends RecyclerView.Adapter<SamplePhotosAdapter.SamplePhotoViewHolder> {
    private List<String> photoHashes = Collections.emptyList();
    private List<String> photoUrls = Collections.emptyList();

    public void setPhotoHashes(List<String> newHashes) {
        if (photoHashes.equals(newHashes)) {
            return;
        }
        photoHashes = newHashes;
        if (photoHashes == null || photoHashes.isEmpty()) {
            photoUrls = Collections.emptyList();
        } else {
            photoUrls = new ArrayList<>(photoHashes.size());
            for (int i = 0; i < photoHashes.size(); ++i) {
                String hash = photoHashes.get(i);
                if (hash != null) {
                    photoUrls.add(PhotoUrl.xl(hash));
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public SamplePhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.promo_detail_sample_photo_item, parent, false);
        return new SamplePhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SamplePhotoViewHolder holder, int position) {
        holder.bindPhoto(position);
    }

    @Override
    public int getItemCount() {
        return photoUrls.size();
    }

    class SamplePhotoViewHolder extends RecyclerView.ViewHolder {
        Context context;
        int bindedPosition;
        @Bind(R.id.photo) ImageView imageView;

        public SamplePhotoViewHolder(View view) {
            super(view);
            this.context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindPhoto(int position) {
            this.bindedPosition = position;
            Picasso.with(context).load(photoUrls.get(position))
                    .placeholder(R.drawable.bg_placeholder_gradient)
                    .into(imageView);
        }

        @OnClick(R.id.photo)
        void onClick() {
            context.startActivity(PhotoPagerActivity.newIntent(context, photoUrls, bindedPosition));
        }
    }
}
