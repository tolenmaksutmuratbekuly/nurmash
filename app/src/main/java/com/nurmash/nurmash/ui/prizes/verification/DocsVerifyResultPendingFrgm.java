package com.nurmash.nurmash.ui.prizes.verification;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.main.MainActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class DocsVerifyResultPendingFrgm extends Fragment {
    public static DocsVerifyResultPendingFrgm newInstance() {
        return new DocsVerifyResultPendingFrgm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_document_sent, viewGroup, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @OnClick(R.id.btn_backto_promotions)
    void onNewPromotionsButtonClick() {
        getActivity().finish();
        startActivity(MainActivity.newIntent(getActivity()));
    }
}