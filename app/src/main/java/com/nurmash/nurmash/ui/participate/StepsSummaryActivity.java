package com.nurmash.nurmash.ui.participate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.ViewGroup;
import android.widget.Button;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.participate.ChecklistItem;
import com.nurmash.nurmash.mvp.participate.StepsSummaryPresenter;
import com.nurmash.nurmash.mvp.participate.StepsSummaryView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class StepsSummaryActivity extends BaseActivity implements StepsSummaryView {
    StepsSummaryPresenter stepsSummaryPresenter;
    CheckListAdapter checklistAdapter;
    @Bind(R.id.content_view) ViewGroup contentView;
    @Bind(R.id.checklist_view) RecyclerView checklistView;
    @Bind(R.id.button_next) Button nextButton;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;

    public static Intent newIntent(Context packageContext, long promoId) {
        Intent intent = new Intent(packageContext, StepsSummaryActivity.class);
        intent.putExtra("promoId", promoId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps_summary);
        ButterKnife.bind(this);
        // RecyclerView
        configureChecklistView();
        // Presenter
        long promoId = getIntent().getLongExtra("promoId", 0);
        stepsSummaryPresenter = new StepsSummaryPresenter(DataLayer.getInstance(this), promoId);
        stepsSummaryPresenter.attachView(this);
        stepsSummaryPresenter.setErrorHandler(createErrorHandler());
    }

    @Override
    protected void onResume() {
        super.onResume();
        stepsSummaryPresenter.loadParticipationData();
    }

    @Override
    protected void onDestroy() {
        stepsSummaryPresenter.setErrorHandler(null);
        stepsSummaryPresenter.detachView();
        super.onDestroy();
    }

    private void configureChecklistView() {
        checklistAdapter = new CheckListAdapter();
        checklistView.setAdapter(checklistAdapter);
        checklistView.setLayoutManager(new LinearLayoutManager(this));
        ((SimpleItemAnimator) checklistView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private ErrorHandler createErrorHandler() {
        RequestErrorHandler requestErrorHandler = new RequestErrorHandler();
        requestErrorHandler.setCallbacks(new DefaultRequestErrorCallbacks(this, contentView) {
            @Override
            public void onNetworkError() {
                super.onNetworkError();
                nextButton.setEnabled(false);
            }

            @Override
            public void onRequestFailed() {
                super.onRequestFailed();
                nextButton.setEnabled(false);
            }

            @Override
            public void onInternalServerError() {
                super.onInternalServerError();
                nextButton.setEnabled(false);
            }

            @Override
            public void onPromoHasEndedError() {
                super.onPromoHasEndedError();
                nextButton.setEnabled(false);
            }

            @Override
            public void onPromoNotFoundError() {
                super.onPromoNotFoundError();
                nextButton.setEnabled(false);
            }
        });
        return requestErrorHandler;
    }

    @OnClick(R.id.button_next)
    public void onNextClick() {
        stepsSummaryPresenter.onNextClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // StepsSummaryView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? VISIBLE : GONE);
    }

    @Override
    public void setChecklistItems(List<ChecklistItem> items) {
        checklistAdapter.setItems(items);
    }

    @Override
    public void setNextActionTitle(@StringRes int resId) {
        nextButton.setText(resId);
    }

    @Override
    public void openVideoStepActivity(long promoId) {
        startActivity(VideoStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openShareStepActivity(long promoId) {
        // Sharing starts with first taking a photo.
        startActivity(PhotoStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openSurveyStepActivity(long promoId) {
        startActivity(SurveyStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openMyPhotoActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(this, competitorId));
    }
}
