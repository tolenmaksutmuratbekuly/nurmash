package com.nurmash.nurmash.ui.prizes.withdraw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.withdraw.PaypalForm;
import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.mvp.prizes.withdraw.PaypalFormPresenter;
import com.nurmash.nurmash.mvp.prizes.withdraw.PaypalFormView;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;

import static com.nurmash.nurmash.util.ContextUtils.hideSoftKeyboard;

public class PaypalFormFragment extends WithdrawFormFragment<PaypalForm> implements PaypalFormView {
    final PaypalFormPresenter paypalFormPresenter;
    @Bind(R.id.form_view) ViewGroup formView;
    @Bind(R.id.account_input) MaterialEditText accountInput;
    @Bind(R.id.first_name_input) MaterialEditText firstNameInput;
    @Bind(R.id.last_name_input) MaterialEditText lastNameInput;

    public static PaypalFormFragment newInstance() {
        PaypalFormFragment fragment = new PaypalFormFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public PaypalFormFragment() {
        paypalFormPresenter = new PaypalFormPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_paypal_withdraw_form, container, false);
        ButterKnife.bind(this, rootView);
        paypalFormPresenter.attachView(this, DataLayer.getInstance(getActivity()));
        return rootView;
    }

    @Override
    public void onDestroyView() {
        paypalFormPresenter.detachView();
        hideSoftKeyboard(formView);
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @OnEditorAction(R.id.account_input)
    boolean onAccountInputAction(int action) {
        if (action == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyboard(formView);
            formView.requestFocus();
            return true;
        }
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Abstract methods implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public PaypalForm getForm() {
        return paypalFormPresenter.getForm(firstNameInput.getText().toString(), lastNameInput.getText().toString(),
                accountInput.getText().toString());
    }

    @Override
    public void onFormValidationError(ValidationError error) {
        paypalFormPresenter.onValidationError(error);
    }

    ///////////////////////////////////////////////////////////////////////////
    // PaypalFormView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showAccount(String account) {
        accountInput.setText(account);
    }

    @Override
    public void showFirstName(String firstName) {
        firstNameInput.setText(firstName);
    }

    @Override
    public void showLastName(String lastName) {
        lastNameInput.setText(lastName);
    }

    @Override
    public void showAccountError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(accountInput, errorRes, scrollTo);
    }

    @Override
    public void showFirstNameError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(firstNameInput, errorRes, scrollTo);
    }

    @Override
    public void showLastNameError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(lastNameInput, errorRes, scrollTo);
    }
}
