package com.nurmash.nurmash.ui.participate;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.mvp.participate.ChecklistItem;
import com.nurmash.nurmash.util.ViewUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CheckListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int DESCRIPTION_LAYOUT = R.layout.steps_description_item;
    private static final int CHECKLIST_ITEM_LAYOUT = R.layout.steps_checklist_item;

    private List<ChecklistItem> items = Collections.emptyList();

    public void setItems(List<ChecklistItem> items) {
        this.items = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    private int getDescriptionPosition() {
        return 0;
    }

    private int getCheckListItemIndex(int position) {
        return position - 1;
    }

    @Override
    public int getItemCount() {
        return 1 + items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getDescriptionPosition()) {
            return DESCRIPTION_LAYOUT;
        } else {
            return CHECKLIST_ITEM_LAYOUT;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case DESCRIPTION_LAYOUT:
                return new DescriptionViewHolder(view);
            case CHECKLIST_ITEM_LAYOUT:
                return new CheckListItemViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CheckListItemViewHolder) {
            ((CheckListItemViewHolder) holder).bindItem(getCheckListItemIndex(position));
        }
    }

    class DescriptionViewHolder extends RecyclerView.ViewHolder {
        DescriptionViewHolder(View view) {
            super(view);
            ((TextView) view.findViewById(R.id.text)).setText(R.string.info_text_participation_steps);
        }
    }

    class CheckListItemViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text) TextView textView;

        CheckListItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bindItem(int itemIndex) {
            ChecklistItem item = items.get(itemIndex);
            String title = textView.getContext().getString(item.getTitleRes());
            textView.setText(String.format(Locale.US, "%d. %s", itemIndex + 1, title));
            textView.setCompoundDrawablesWithIntrinsicBounds(item.getIconRes(), 0, item.getCheckMarkRes(), 0);
            ViewUtils.setTextAppearance(textView, item.getTextAppearanceRes());
        }
    }
}
