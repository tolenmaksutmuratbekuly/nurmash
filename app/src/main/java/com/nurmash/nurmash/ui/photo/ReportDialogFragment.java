package com.nurmash.nurmash.ui.photo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.Complaint;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class ReportDialogFragment extends DialogFragment {
    private Callback callback;
    private List<Complaint> complaints;
    private Complaint selectedComplaint;

    public static ReportDialogFragment newPhotoComplaintInstance(int competitorPosition) {
        ReportDialogFragment fragment = new ReportDialogFragment();
        Bundle args = new Bundle();
        args.putInt("titleResId", R.string.title_dialog_report_photo);
        args.putParcelable("complaints", Parcels.wrap(new ArrayList<>(Complaint.getPhotoComplaints())));
        args.putInt("competitorPosition", competitorPosition);
        fragment.setArguments(args);
        return fragment;
    }

    public static ReportDialogFragment newCommentComplaintInstance() {
        ReportDialogFragment fragment = new ReportDialogFragment();
        Bundle args = new Bundle();
        args.putInt("titleResId", R.string.title_dialog_report_comment);
        args.putParcelable("complaints", Parcels.wrap(new ArrayList<>(Complaint.getCommentComplaints())));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Callback) {
            callback = (Callback) activity;

        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        @StringRes int titleResId = getArguments().getInt("titleResId");
        complaints = Parcels.unwrap(getArguments().getParcelable("complaints"));
        String[] complaintTitles = getComplaintTitles();

        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(titleResId)
                .setSingleChoiceItems(complaintTitles, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedComplaint = complaints.get(which);
                    }
                })
                .setPositiveButton(R.string.action_report, null)
                .setNegativeButton(R.string.action_cancel, null)
                .create();

        // Override positive button action to prevent closing the dialog if complaint reason was not selected.
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                Button reportButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                reportButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (selectedComplaint != null) {
                            if (callback != null) {
                                if (getArguments().containsKey("competitorPosition")) {
                                    int competitorPosition = getArguments().getInt("competitorPosition");
                                    if (competitorPosition != -1)
                                        callback.onComplaintSelected(selectedComplaint, getArguments().getInt("competitorPosition"));
                                    else
                                        callback.onComplaintSelected(selectedComplaint);

                                } else {
                                    callback.onComplaintSelected(selectedComplaint);
                                }
                            }
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        return dialog;
    }

    private String[] getComplaintTitles() {
        String[] titles = new String[complaints.size()];
        for (int i = 0; i < complaints.size(); ++i) {
            titles[i] = getString(complaints.get(i).getDescriptionStringId());
        }
        return titles;
    }

    public interface Callback {
        void onComplaintSelected(Complaint complaint, int competitorPosition);

        void onComplaintSelected(Complaint complaint);
    }
}
