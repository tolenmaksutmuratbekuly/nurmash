package com.nurmash.nurmash.ui.participate;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.Survey;
import com.nurmash.nurmash.util.ViewUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SurveyStepContentAdapter extends RecyclerView.Adapter {
    private String surveyType;
    private String question;
    private String childrenId;
    private List<Survey.Answer> answers = Collections.emptyList();
    private String selectedAnswerId;
    private Callback callback;

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setSurveyType(String type) {
        this.surveyType = type;
    }

    public void setQuestion(String question, String childrenId, ArrayList<Survey.Answer> answers) {
        this.question = question;
        this.childrenId = childrenId;
        this.answers = answers;
        notifyDataSetChanged();
    }

    public void setSelectedAnswerId(String answerId) {
        selectedAnswerId = answerId;
        notifyDataSetChanged();
    }

    private int getQuestionPosition() {
        return 0;
    }

    private int getAnswerPosition(int answerIndex) {
        return 1 + answerIndex;
    }

    @Override
    public int getItemCount() {
        return 1 + answers.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getQuestionPosition()) {
            return R.layout.survey_step_item_question;
        } else {
            return R.layout.survey_step_item_answer;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case R.layout.survey_step_item_question:
                return new QuestionViewHolder(view);
            case R.layout.survey_step_item_answer:
                return new AnswerViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof QuestionViewHolder) {
            ((QuestionViewHolder) holder).questionView.setText(question);
        } else if (holder instanceof AnswerViewHolder) {
            ((AnswerViewHolder) holder).setAnswerIndex(position - getAnswerPosition(0));
        }
    }

    private class QuestionViewHolder extends RecyclerView.ViewHolder {
        TextView questionView;

        QuestionViewHolder(View view) {
            super(view);
            questionView = (TextView) view.findViewById(R.id.question);
        }
    }

    private class AnswerViewHolder extends RecyclerView.ViewHolder {
        Survey.Answer bindedAnswer;
        TextView titleView;
        String bindedAnswerId;

        AnswerViewHolder(View view) {
            super(view);
            titleView = (TextView) view.findViewById(R.id.answer_title);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null) {
                        if (surveyType == null) {
                            // Ensure backwards compatibility with an old type of survey without nodes.
                            callback.onAnswerClick(bindedAnswerId);
                        } else if (surveyType.equals(Survey.SURVEY_TYPE_LIST)) {
                            callback.onAnswerClick(bindedAnswerId);
                        } else if (surveyType.equals(Survey.SURVEY_TYPE_TREE)) {
                            callback.onAnswerClick(bindedAnswer, childrenId);
                        }
                    }
                }
            });
        }

        void setAnswerIndex(int index) {
            this.bindedAnswer = answers.get(index);
            this.bindedAnswerId = answers.get(index).id;
            titleView.setText(answers.get(index).title);
            if (bindedAnswerId.equals(selectedAnswerId)) {
                ViewUtils.setTextAppearance(titleView, R.style.ParticipationSteps_TextAppearance_Label_Active);
                titleView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_step_check_active, 0);
            } else {
                ViewUtils.setTextAppearance(titleView, R.style.ParticipationSteps_TextAppearance_Label_Inactive);
                titleView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_step_check_inactive, 0);
            }
        }
    }

    public interface Callback {
        void onAnswerClick(String answerId);

        void onAnswerClick(Survey.Answer answer, String childrenId);
    }
}
