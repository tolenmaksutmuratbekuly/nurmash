package com.nurmash.nurmash.ui.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.widget.LinearLayout;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.MobappCompatible;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.main.MainPresenter;
import com.nurmash.nurmash.mvp.main.MainView;
import com.nurmash.nurmash.mvp.profile.CallbackBackPressed;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.prizes.PrizesTabFragment;
import com.nurmash.nurmash.ui.profile.ProfileDetailFragment;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView {
    private MainPresenter presenter;
    private CallbackBackPressed mCallbacks;
    private DataLayer dataLayer;
    @Bind(R.id.ll_main_parent) LinearLayout ll_main_parent;
    @Bind(R.id.pager) ViewPager viewPager;
    @Bind(R.id.tabs) CommonTabLayout tabLayout;
    public static boolean isProfilePhotoUpdated;
    public static boolean needToOpenNewPromotions;
    ProfileDetailFragment fragmentProfileDetail;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, MainActivity.class);
    }

    public static Intent newIntentAsRoot(Context packageContext) {
        return newIntent(packageContext).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // Presenter
        dataLayer = DataLayer.getInstance(this);
        presenter = new MainPresenter(dataLayer);
        presenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, ll_main_parent)));
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        presenter.setErrorHandler(null);
        presenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataLayer != null) {
            dataLayer.pushRegistrationHelper().registerForNotifications();
            dataLayer.notificationsHelper().clearAllPromoNotifications();
            dataLayer.notificationsHelper().clearAllDisqualNotifications();
            dataLayer.notificationsHelper().clearAllUnBannedUserNotifications();
            dataLayer.notificationsHelper().clearAllVerifyUserNotifications();
        }
        if (isProfilePhotoUpdated) {
            FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
            if (fTrans != null) {
                fTrans.detach(fragmentProfileDetail);
                fTrans.attach(fragmentProfileDetail);
                fTrans.commit();
            }
            isProfilePhotoUpdated = false;
        }

        if (needToOpenNewPromotions) {
            viewPager.setCurrentItem(0, false);
            needToOpenNewPromotions = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (fragmentProfileDetail != null) {
            fragmentProfileDetail.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void configureViewPager() {
        ArrayList<CustomTabEntity> tabEntities = new ArrayList<>(3);
        tabEntities.add(new TabEntity(getString(R.string.label_promotions_tab).toUpperCase(),
                R.drawable.ic_tab_promotions_active,
                R.drawable.ic_tab_promotions_inactive));
//        tabEntities.add(new TabEntity(getString(R.string.label_checkin_tab),
//                R.drawable.ic_tab_checkin_active,
//                R.drawable.ic_tab_checkin_inactive));
        tabEntities.add(new TabEntity(getString(R.string.label_prizes_tab).toUpperCase(),
                R.drawable.ic_tab_prizes_active,
                R.drawable.ic_tab_prizes_inactive));
        tabEntities.add(new TabEntity(getString(R.string.label_profile_tab).toUpperCase(),
                R.drawable.ic_tab_profile_active,
                R.drawable.ic_tab_profile_inactive));
        tabLayout.setTabData(tabEntities);
        tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        fragmentProfileDetail = ProfileDetailFragment.newMyProfileInstance();
        FragmentArrayAdapter pagerAdapter = new FragmentArrayAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(PromotionsTabFragment.newInstance());
//        pagerAdapter.addFragment(CheckinTabFragment.newInstance());
        pagerAdapter.addFragment(PrizesTabFragment.newInstance());
        pagerAdapter.addFragment(fragmentProfileDetail);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.setCurrentTab(position);
            }
        });
        viewPager.setCurrentItem(0);

        if (fragmentProfileDetail != null) {
            mCallbacks = fragmentProfileDetail;
        }

        fragmentProfileDetail.setCallback(new ProfileDetailFragment.Callback() {
            @Override
            public void participateClick() {
                if (viewPager != null) {
                    viewPager.setCurrentItem(0);
                }
            }
        });
    }

    static class TabEntity implements CustomTabEntity {
        public String title;
        private int selectedIcon;
        private int unSelectedIcon;

        private TabEntity(String title, int selectedIcon, int unSelectedIcon) {
            this.title = title;
            this.selectedIcon = selectedIcon;
            this.unSelectedIcon = unSelectedIcon;
        }

        @Override
        public String getTabTitle() {
            return title;
        }

        @Override
        public int getTabSelectedIcon() {
            return selectedIcon;
        }

        @Override
        public int getTabUnselectedIcon() {
            return unSelectedIcon;
        }
    }

    @Override
    public void onBackPressed() {
        if (mCallbacks != null) {
            if (!mCallbacks.isPhotoOpenOnBackPressed()) {
                finish();
            }
        }
    }

    private void openPlayStoreDirectly() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // MainView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onMobCompatibilityLoaded(final MobappCompatible mobappCompatible) {
        configureViewPager();
        if (mobappCompatible.isRequestedAppCompatible()) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(getString(R.string.label_version_single_title))
                    .setCancelable(false)
                    .setMessage(getString(R.string.label_version_single_text))
                    .setPositiveButton(getString(R.string.label_version_single_action), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            openPlayStoreDirectly();
                        }
                    }).show();
        } else if (mobappCompatible.requested_app != null && mobappCompatible.requested_app.size() > 0) {
            if (!mobappCompatible.requested_app.get(0).on_store &&
                    ((mobappCompatible.on_store_app == null || mobappCompatible.on_store_app.size() == 0)
                            || (mobappCompatible.on_store_app != null && mobappCompatible.on_store_app.size() > 0
                            && (!dataLayer.authPreferences().getAppAvailabilityVersion().equals(mobappCompatible.getVersion()))))) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(getString(R.string.label_version_multiple_title))
                        .setCancelable(false)
                        .setMessage(getString(R.string.label_version_multiple_text))
                        .setPositiveButton(getString(R.string.label_version_multiple_action_update),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dataLayer.authPreferences().setAppAvailability(mobappCompatible.getVersion());
                                        dialog.cancel();
                                        openPlayStoreDirectly();
                                    }
                                })
                        .setNegativeButton(getString(R.string.label_version_multiple_action_later), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
            }
        }
    }
}
