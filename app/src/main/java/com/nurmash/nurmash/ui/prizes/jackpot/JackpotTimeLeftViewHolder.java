package com.nurmash.nurmash.ui.prizes.jackpot;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nurmash.nurmash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class JackpotTimeLeftViewHolder {
    @Bind(R.id.message) TextView messageView;
    @Bind(R.id.loaded_data) ViewGroup loadedDataContainer;
    @Bind(R.id.jackpot_amount) TextView jackpotAmountView;
    @Bind(R.id.time_left_label) TextView timeLeftLabelView;
    @Bind(R.id.time_left) TextView timeLeftView;

    public JackpotTimeLeftViewHolder(View view) {
        ButterKnife.bind(this, view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    public void showJackpotTitle(String title) {
        messageView.setText(title);
    }

    public void showJackpotMoneyList(String jackpotAmountStr) {
        jackpotAmountView.append(jackpotAmountStr);
        loadedDataContainer.setVisibility(jackpotAmountStr != null ? View.VISIBLE : View.GONE);
    }

    public void showJackpotTimeLeft(String timeLeftStr) {
        if (timeLeftStr != null) {
            timeLeftView.setText(timeLeftStr);
            timeLeftView.setVisibility(View.VISIBLE);
            timeLeftLabelView.setVisibility(View.VISIBLE);
        } else {
            timeLeftView.setVisibility(View.GONE);
            timeLeftLabelView.setVisibility(View.GONE);
        }
    }
}
