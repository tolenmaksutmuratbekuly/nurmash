package com.nurmash.nurmash.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.social.facebook.FacebookProvider;
import com.nurmash.nurmash.data.social.google.GoogleAuthProvider;
import com.nurmash.nurmash.data.social.vk.VKProvider;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.login.LoginPresenter;
import com.nurmash.nurmash.mvp.login.LoginView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.PromptDialogFragment;
import com.nurmash.nurmash.ui.intro.IntroActivity;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.registration.RegistrationActivity;
import com.nurmash.nurmash.util.ToastUtils;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {
    public static final String ACTION_LOGOUT = "ACTION_LOGOUT";
    public static final String ACTION_HANDLE_EXPIRED_AUTH_TOKEN_ERROR = "ACTION_HANDLE_EXPIRED_AUTH_TOKEN_ERROR";
    public static final String ACTION_HANDLE_USER_BANNED_ERROR = "ACTION_HANDLE_USER_BANNED_ERROR";
    public static final String ACTION_LOGIN_FACEBOOK = "ACTION_LOGIN_FACEBOOK";
    public static final String ACTION_LOGIN_GOOGLE = "ACTION_LOGIN_GOOGLE";
    public static final String ACTION_LOGIN_VK = "ACTION_LOGIN_VK";
    public static final String ACTION_SKIP_INTRO = "ACTION_SKIP_INTRO";

    LoginPresenter loginPresenter;
    FacebookProvider facebookProvider;
    GoogleAuthProvider googleAuthProvider;
    VKProvider vkProvider;
    @Bind(R.id.content_view) ViewGroup contentView;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, LoginActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public static Intent newLogoutIntent(Context packageContext) {
        return newIntent(packageContext).setAction(ACTION_LOGOUT);
    }

    public static Intent newIntentForAuthTokenExpiredError(Context packageContext) {
        return newIntent(packageContext).setAction(ACTION_HANDLE_EXPIRED_AUTH_TOKEN_ERROR);
    }

    public static Intent newIntentForUserBannedError(Context packageContext) {
        return newIntent(packageContext).setAction(ACTION_HANDLE_USER_BANNED_ERROR);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        // Providers
        facebookProvider = new FacebookProvider(this);
        googleAuthProvider = new GoogleAuthProvider(this);
        vkProvider = new VKProvider(this);
        // Presenter
        configurePresenter();
    }

    private void configurePresenter() {
        loginPresenter = new LoginPresenter(DataLayer.getInstance(this), facebookProvider, googleAuthProvider,
                vkProvider);
        loginPresenter.setErrorHandler(createErrorHandler());
        loginPresenter.attachView(this);

        String action = getIntent().getAction();
        if (ACTION_LOGOUT.equals(action)) {
            loginPresenter.onLogoutIntentAction();
        } else if (ACTION_HANDLE_EXPIRED_AUTH_TOKEN_ERROR.equals(action)) {
            loginPresenter.onAuthTokenExpiredIntentAction();
        } else if (ACTION_HANDLE_USER_BANNED_ERROR.equals(action)) {
            loginPresenter.onUserBannedErrorIntentAction();
        } else if (ACTION_SKIP_INTRO.equals(action)) {
            loginPresenter.onSkipIntroIntentAction();
        } else if (ACTION_LOGIN_FACEBOOK.equals(action)) {
            loginPresenter.onFacebookLoginIntentAction();
        } else if (ACTION_LOGIN_GOOGLE.equals(action)) {
            loginPresenter.onGoogleLoginIntentAction();
        } else if (ACTION_LOGIN_VK.equals(action)) {
            loginPresenter.onVKLoginIntentAction();
        } else {
            loginPresenter.onAppLaunchIntentAction();
        }
    }

    private ErrorHandler createErrorHandler() {
        RequestErrorHandler requestErrorHandler = new RequestErrorHandler();
        requestErrorHandler.setCallbacks(new DefaultRequestErrorCallbacks(this) {
            @Override
            public void onAuthTokenInvalidated() {
                Toast.makeText(LoginActivity.this, R.string.error_message_generic_request, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUserBannedError() {
                showUserBannedError();
            }
        });
        return requestErrorHandler;
    }

    @Override
    protected void onDestroy() {
        googleAuthProvider.disconnect();
        loginPresenter.setErrorHandler(null);
        loginPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (facebookProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (googleAuthProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (vkProvider.onActivityResult(requestCode, resultCode, data)) return;
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.button_facebook)
    void onFacebookButtonClick() {
        loginPresenter.onFacebookButtonClick();
    }

    @OnClick(R.id.button_google)
    void onGoogleButtonClick() {
        loginPresenter.onGoogleButtonClick();
    }

    @OnClick(R.id.button_vk)
    void onVKButtonClick() {
        loginPresenter.onVKButtonClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // LoginView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openRegistrationScreen(User user) {
        Intent intent = RegistrationActivity.newIntent(this);
        if (user != null) {
            intent.putExtra("user", Parcels.wrap(user));
        }
        startActivity(intent);
    }

    @Override
    public void openIntroScreen() {
        startActivity(IntroActivity.newIntent(this));
    }

    @Override
    public void openMainScreen() {
        startActivity(MainActivity.newIntent(this));
    }

    @Override
    public void onFacebookSdkError() {
        Toast.makeText(this, R.string.error_message_login_facebook, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmailPermissionNotGranted() {
        Toast.makeText(this, R.string.error_message_login_email_permission, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGoogleApiConnectingError() {
        Toast.makeText(this, R.string.error_message_login_google_connecting, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGoogleApiNotConnectedError() {
        Toast.makeText(this, R.string.error_message_login_google_not_connected, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGoogleSdkError() {
        Toast.makeText(this, R.string.error_message_login_google_sdk_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVKSdkError() {
        Toast.makeText(this, R.string.error_message_login_vk_sdk_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTokenExpiredError() {
        ToastUtils.centeredToast(this, R.string.error_message_token_invalid, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showUserBannedError() {
        PromptDialogFragment.newInstance(R.string.error_message_user_banned, false)
                .show(getSupportFragmentManager(), "UserBannedMessageDialog");
    }
}
