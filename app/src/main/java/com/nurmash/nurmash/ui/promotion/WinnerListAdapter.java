package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.Promotion.Winner;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersSimpleAdapter;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WinnerListAdapter extends BaseAdapter implements StickyGridHeadersSimpleAdapter {
    private static final int MAIN_WINNERS_HEADER_ID = 0;
    private static final int FIRST_EACH_WINNERS_HEADER_ID = 1;
    private static final int RANDOM_EACH_WINNERS_HEADER_ID = 2;

    private final Object picassoTag;
    private final NumberFormat moneyAmountFormat;

    private int itemLayoutId;
    private Callback callback;
    private Promotion promo;
    private Promotion.Prizes prizes;
    private Promotion.Winners winners;
    private String currencyCode;
    private List<Winner> mainWinners = Collections.emptyList();
    private List<Winner> firstEachWinners = Collections.emptyList();
    private List<Winner> randomEachWinners = Collections.emptyList();

    public WinnerListAdapter(Object picassoTag, NumberFormat moneyAmountFormat) {
        this.picassoTag = picassoTag;
        this.moneyAmountFormat = moneyAmountFormat;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setPromo(Promotion promo) {
        this.promo = promo;
        this.prizes = promo.prizes;
        this.winners = promo.winners;
        if (this.promo != null) {
            if (promo.getCurrencyCode() != null) {
                currencyCode = " " + promo.getCurrencyCode();
            }
        }

        mainWinners = new ArrayList<>(3);
        if (winners.place_1 != null) {
            mainWinners.add(winners.place_1);
        }
        if (winners.place_2 != null) {
            mainWinners.add(winners.place_2);
        }
        if (winners.place_3 != null) {
            mainWinners.add(winners.place_3);
        }
        firstEachWinners = Arrays.asList(winners.first_each);
        randomEachWinners = Arrays.asList(winners.random_each);
        itemLayoutId = promo.isPhotoSharingPromo() ? R.layout.winner_list_photo_item : R.layout.winner_list_user_item;
        notifyDataSetChanged();
    }

    @Override
    public long getHeaderId(int position) {
        final int mainItems = mainWinners.size();
        final int firstEachItems = firstEachWinners.size();
        final int randomEachItems = randomEachWinners.size();
        if (position < mainItems) {
            return MAIN_WINNERS_HEADER_ID;
        } else if (position - mainItems < firstEachItems) {
            return FIRST_EACH_WINNERS_HEADER_ID;
        } else if (position - mainItems - firstEachItems < randomEachItems) {
            return RANDOM_EACH_WINNERS_HEADER_ID;
        } else {
            return -1;
        }
    }

    private String formatPrize(Money prize) {
        return moneyAmountFormat.format(prize.getAmount());
    }

    private String getHeaderTitle(Context context, int headerId) {
        switch (headerId) {
            case MAIN_WINNERS_HEADER_ID:
                return context.getString(R.string.winner_list_main_winners_label);
            case FIRST_EACH_WINNERS_HEADER_ID:
                return context.getString(R.string.winner_list_first_each_winners_label, prizes.getFirstPoolSize(),
                        formatPrize(prizes.first_each) + " " + currencyCode);
            case RANDOM_EACH_WINNERS_HEADER_ID:
                return context.getString(R.string.winner_list_random_each_winners_label, prizes.getRandomPoolSize(),
                        formatPrize(prizes.random_each) + " " + currencyCode);
            default:
                return null;
        }
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.winner_list_header_item, parent, false);
            convertView.setTag(new HeaderViewHolder(convertView));
        }
        HeaderViewHolder viewHolder = (HeaderViewHolder) convertView.getTag();
        viewHolder.bindText(getHeaderTitle(parent.getContext(), (int) getHeaderId(position)));
        return convertView;
    }

    @Override
    public int getCount() {
        return mainWinners.size() + firstEachWinners.size() + randomEachWinners.size();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).id;
    }

    @Override
    public Winner getItem(final int position) {
        final int mainItems = mainWinners.size();
        final int firstEachItems = firstEachWinners.size();
        final int randomEachItems = randomEachWinners.size();
        if (position < mainItems) {
            return mainWinners.get(position);
        } else if (position - mainItems < firstEachItems) {
            return firstEachWinners.get(position - mainItems);
        } else if (position - mainItems - firstEachItems < randomEachItems) {
            return randomEachWinners.get(position - mainItems - firstEachItems);
        } else {
            return null;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(itemLayoutId, parent, false);
            convertView.setTag(new WinnerItemViewHolder(convertView));
        }
        WinnerItemViewHolder viewHolder = (WinnerItemViewHolder) convertView.getTag();
        viewHolder.bindWinner(getItem(position));
        return convertView;
    }

    static class HeaderViewHolder {
        @Bind(R.id.text) TextView textView;

        HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bindText(String text) {
            textView.setText(text);
        }
    }

    class WinnerItemViewHolder {
        Context context;
        Winner bindedWinner;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.title) TextView titleView;
        @Bind(R.id.amount) TextView amountView;
        @Nullable
        @Bind(R.id.display_name)
        TextView nameView;

        WinnerItemViewHolder(View view) {
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindWinner(Winner winner) {
            bindedWinner = winner;
            bindPrize(winner);
            if (promo.isPhotoSharingPromo()) {
                bindPhoto(winner == null ? null : winner.photo);
            } else {
                bindUser(winner == null ? null : winner.user);
            }
        }

        void bindPrize(Winner winner) {
            if (titleView == null) {
                return;
            }
            if (winner == winners.place_1) {
                setTitle(R.string.label_main_prize);
                setAmount(prizes.place_1);
            } else if (winner == winners.place_2) {
                if (promo.isPhotoSharingPromo()) {
                    setTitle(R.string.label_community_prize);
                    setAmount(prizes.place_2);
                } else {
                    setTitle(R.string.label_second_place_prize);
                    setAmount(prizes.place_2);
                }
            } else if (winner == winners.place_3) {
                if (promo.isPhotoSharingPromo()) {
                    setTitle(R.string.label_company_prize);
                    setAmount(prizes.place_3);
                } else {
                    setTitle(R.string.label_third_place_prize);
                    setAmount(prizes.place_3);
                }
            } else {
                setTitle(0);
                setAmount(null);
            }
        }

        void bindUser(User user) {
            if (user == null) {
                setName(null);
                photoView.setImageDrawable(null);
            } else {
                setName(user.getDisplayName());
                Picasso.with(context).load(PhotoUrl.l(user.photo))
                        .fit()
                        .centerCrop()
                        .transform(Transformations.CIRCLE_NO_BORDER)
                        .tag(picassoTag)
                        .into(photoView);
            }
        }

        void bindPhoto(String photoHash) {
            if (photoHash == null || photoHash.isEmpty()) {
                photoView.setImageResource(R.drawable.bg_placeholder_gradient);
            } else {
                Picasso.with(context).load(PhotoUrl.xl(photoHash))
                        .placeholder(R.drawable.bg_placeholder_gradient)
                        .fit()
                        .centerCrop()
                        .tag(picassoTag)
                        .into(photoView);
            }
        }

        void setName(String name) {
            if (nameView != null) {
                nameView.setText(name);
            }
        }

        void setTitle(@StringRes int titleRes) {
            if (titleRes == 0) {
                titleView.setText(null);
                titleView.setVisibility(View.GONE);
            } else {
                titleView.setText(titleRes);
                titleView.setVisibility(View.VISIBLE);
            }
        }

        void setAmount(Money amount) {
            if (amount == null) {
                amountView.setText(null);
                amountView.setVisibility(View.GONE);
            } else {
                amountView.setText(amount.toString(moneyAmountFormat) + " " + currencyCode);
                amountView.setVisibility(View.VISIBLE);
            }
        }

        @OnClick(R.id.item_view)
        void onItemClick() {
            if (callback != null && bindedWinner != null) {
                callback.onWinnerClick(bindedWinner);
            }
        }
    }

    public interface Callback {
        void onWinnerClick(Winner winner);
    }
}
