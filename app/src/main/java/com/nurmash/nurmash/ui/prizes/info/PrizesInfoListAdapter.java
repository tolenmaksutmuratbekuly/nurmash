package com.nurmash.nurmash.ui.prizes.info;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PrizesInfoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<PrizesInfoItem> items = new ArrayList<>();

    public void addItem(@DrawableRes int logoRes, @StringRes int titleRes, @StringRes int textRes) {
        items.add(new PrizesInfoItem(logoRes, titleRes, textRes));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prizes_info_list_item, parent, false);
        return new PrizesInfoItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PrizesInfoItemViewHolder) holder).bind(items.get(position));
    }

    static class PrizesInfoItem {
        @DrawableRes int logoRes;
        @StringRes int titleRes;
        @StringRes int textRes;

        PrizesInfoItem(@DrawableRes int logoRes, @StringRes int titleRes, @StringRes int textRes) {
            this.logoRes = logoRes;
            this.titleRes = titleRes;
            this.textRes = textRes;
        }
    }

    static class PrizesInfoItemViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.logo) ImageView logoView;
        @Bind(R.id.title) TextView titleView;
        @Bind(R.id.text) TextView textView;

        PrizesInfoItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bind(PrizesInfoItem item) {
            logoView.setImageResource(item.logoRes);
            titleView.setText(item.titleRes);
            textView.setText(item.textRes);
        }
    }
}
