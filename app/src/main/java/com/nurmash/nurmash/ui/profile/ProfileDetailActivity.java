package com.nurmash.nurmash.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.profile.CallbackBackPressed;
import com.nurmash.nurmash.ui.BaseActivity;

import org.parceler.Parcels;

public class ProfileDetailActivity extends BaseActivity {
    private CallbackBackPressed mCallbacks;

    private static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, ProfileDetailActivity.class);
    }

    public static Intent newIntent(Context packageContext, long userId) {
        return newIntent(packageContext).putExtra("userId", userId);
    }

    public static Intent newIntent(Context packageContext, User user) {
        return newIntent(packageContext).putExtra("user", Parcels.wrap(user));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail);
        ProfileDetailFragment fragment = ProfileDetailFragment.newInstance(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit();

        if (fragment instanceof CallbackBackPressed)
            mCallbacks = (CallbackBackPressed) fragment;

    }

    @Override
    public void onBackPressed() {
        if (mCallbacks != null)
            mCallbacks.isPhotoOpenOnBackPressed();
    }
}
