package com.nurmash.nurmash.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.nurmash.nurmash.R;

public class PromptDialogFragment extends DialogFragment {
    public static PromptDialogFragment newInstance(@StringRes int messageRes, boolean isCancelable) {
        PromptDialogFragment fragment = new PromptDialogFragment();
        Bundle args = new Bundle();
        args.putInt("messageRes", messageRes);
        args.putBoolean("isCancelable", isCancelable);
        fragment.setArguments(args);
        return fragment;
    }

    public static PromptDialogFragment newInstance(@StringRes int messageRes) {
        return newInstance(messageRes, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        @StringRes int messageRes = getArguments().getInt("messageRes");
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setMessage(messageRes)
                .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Fragment target = getTargetFragment();
                        if (target != null) {
                            target.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                        }
                    }
                });
        boolean isCancelable = getArguments().getBoolean("isCancelable", false);
        if (isCancelable) {
            builder.setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Fragment target = getTargetFragment();
                    if (target != null) {
                        target.onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
                    }
                }
            });
        }
        return builder.create();
    }
}
