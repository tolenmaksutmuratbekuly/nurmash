package com.nurmash.nurmash.ui.prizes;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.model.json.PrizeItemPojo;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.ui.prizes.claimprize.PrizeClaimActivity;
import com.nurmash.nurmash.ui.prizes.claimprize.PrizeStatusActivity;
import com.nurmash.nurmash.ui.prizes.discount.DiscountActivity;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.listgridviews.EndlessScrollListenerGridListViews;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.nurmash.nurmash.ui.prizes.PrizesTabFragment.CLAIM_PRIZE_REQUEST_CODE;

public class PrizeDataVerificationHolder {
    private Activity activity;
    private Callback callback;
    private ListAdapterPrizes prizesListAdapter;
    @Bind(R.id.lv_prizes) ListView lv_prizes;
    private LinearLayout ll_not_verified, ll_won_balance;
    private Button btn_pass_verification;

    public PrizeDataVerificationHolder(Activity context, View view) {
        ButterKnife.bind(this, view);
        this.activity = context;
        configureListView();
    }

    private void configureListView() {
        LayoutInflater inflater = activity.getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.lv_header_verification, lv_prizes, false);
        ll_not_verified = (LinearLayout) header.findViewById(R.id.ll_not_verified);
        ll_won_balance = (LinearLayout) header.findViewById(R.id.ll_won_balance);
        btn_pass_verification = (Button) header.findViewById(R.id.btn_pass_verification);

        lv_prizes.addHeaderView(header, null, false);
        prizesListAdapter = new ListAdapterPrizes(activity);
        prizesListAdapter.setCallback(new ListAdapterPrizes.Callback() {
            @Override
            public void onPrizeItemClick(PrizeItemPojo prizeItem) {
                if (prizeItem.itemType == PrizeItemPojo.ITEM_PROMO_PRIZE) {
                    //                if (prizeItem.status == 0) {
//                    Intent intent = PrizeClaimActivity.newIntent(activity, prizeItem);
//                    activity.startActivityForResult(intent, CLAIM_PRIZE_REQUEST_CODE);
//                } else {
//                    activity.startActivityForResult(PrizeStatusActivity.newIntent(activity, prizeItem), CLAIM_PRIZE_REQUEST_CODE);
//                }
                } else if (prizeItem.itemType == PrizeItemPojo.ITEM_PARTNER_PROGRAM) {
//                    if (partnerBalance.last_claim != null) {
//                        activity.startActivityForResult(PrizeStatusActivity.newIntent(activity, partnerBalance), CLAIM_PRIZE_REQUEST_CODE);
//                    } else {
//                        activity.startActivityForResult(PrizeClaimActivity.newIntent(activity, partnerBalance), CLAIM_PRIZE_REQUEST_CODE);
//                    }
                } else if (prizeItem.itemType == PrizeItemPojo.ITEM_DISCOUNT) {
                    activity.startActivity(DiscountActivity.newIntent(activity));
                }
            }
        });

        lv_prizes.setAdapter(prizesListAdapter);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void addPrize(PrizeItemPojo prizeItem) {
        prizesListAdapter.addPrize(prizeItem);
    }

    public void profileVerificationStatus(final ProfileVerifyStatus profileVerifyStatus) {
        ll_won_balance.setVisibility(profileVerifyStatus.isUserVerified() ? View.VISIBLE : View.GONE);
        ll_not_verified.setVisibility(!profileVerifyStatus.isUserVerified() ? View.VISIBLE : View.GONE);

        prizesListAdapter.setArrowRightVisibility(profileVerifyStatus.isUserVerified());
        btn_pass_verification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onPassVerificationClick(profileVerifyStatus);
                }
            }
        });
    }

    public interface Callback {
        void onPassVerificationClick(ProfileVerifyStatus profileVerifyStatus);
    }
}

