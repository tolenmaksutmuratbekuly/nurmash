package com.nurmash.nurmash.ui.prizes.claimprize;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrizeClaimListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<PrizeClaimItem> items = new ArrayList<>();
    private Callback callback;

    public void addItem(@DrawableRes int logoRes, @StringRes int titleRes) {
        items.add(new PrizeClaimItem(logoRes, titleRes));
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prize_claim_list_item, parent, false);
        return new PrizeClaimItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PrizeClaimItemViewHolder) holder).bind(items.get(position));
    }

    static class PrizeClaimItem {
        @DrawableRes int logoRes;
        @StringRes int titleRes;

        PrizeClaimItem(@DrawableRes int logoRes, @StringRes int titleRes) {
            this.logoRes = logoRes;
            this.titleRes = titleRes;
        }
    }

    public class PrizeClaimItemViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_icon) ImageView logoView;
        @Bind(R.id.tv_title) TextView titleView;
        PrizeClaimItem bindedClaimPrize;

        PrizeClaimItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bind(PrizeClaimItem claimPrize) {
            bindedClaimPrize = claimPrize;
            logoView.setImageResource(claimPrize.logoRes);
            titleView.setText(claimPrize.titleRes);
        }

        @OnClick(R.id.ll_parent_view)
        void onPrizeItemClick() {
            if (callback != null && bindedClaimPrize != null) {
                callback.onClaimPrizeItemClick(bindedClaimPrize);
            }
        }
    }

    public interface Callback {
        void onClaimPrizeItemClick(PrizeClaimItem item);
    }
}
