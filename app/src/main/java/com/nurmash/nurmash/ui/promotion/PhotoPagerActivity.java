package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.util.support.ArrayPagerAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoPagerActivity extends BaseActivity {
    PhotoPagerAdapter photoPagerAdapter;
    @Bind(R.id.pager) ViewPager photoPager;

    public static Intent newIntent(Context packageContext, List<String> photoUrls, int initialPosition) {
        Intent intent = new Intent(packageContext, PhotoPagerActivity.class);
        intent.putExtra("photoUrls", Parcels.wrap(photoUrls));
        intent.putExtra("initialPosition", initialPosition);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_pager);
        ButterKnife.bind(this);
        // Set up pager
        List<String> photoUrls = Parcels.unwrap(getIntent().getParcelableExtra("photoUrls"));
        int initialPosition = getIntent().getIntExtra("initialPosition", 0);
        configureViewPager(photoUrls, initialPosition);
    }

    private void configureViewPager(List<String> photoUrls, int initialPosition) {
        photoPagerAdapter = new PhotoPagerAdapter(photoUrls);
        photoPager.setAdapter(photoPagerAdapter);
        photoPager.setCurrentItem(initialPosition);
        photoPager.setPageMargin(photoPager.getPaddingLeft());
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            Picasso.with(this).cancelTag(this);
        }
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoPagerAdapter implementation
    ///////////////////////////////////////////////////////////////////////////

    class PhotoPagerAdapter extends ArrayPagerAdapter<String> {
        public PhotoPagerAdapter(Collection<String> initialItems) {
            super(initialItems);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(container.getContext()).inflate(R.layout.photo_pager_item, container, false);
            container.addView(view);
            PhotoItemViewHolder viewHolder = new PhotoItemViewHolder(view);
            viewHolder.bindPhoto(getItem(position));
            return viewHolder;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((PhotoItemViewHolder) object).view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            PhotoItemViewHolder viewHolder = (PhotoItemViewHolder) object;
            container.removeView(viewHolder.view);
            viewHolder.destroy();
        }

        class PhotoItemViewHolder {
            Context context;
            View view;
            @Bind(R.id.progress_bar) ProgressBar progressBar;
            @Bind(R.id.photo) ImageView photoView;

            PhotoItemViewHolder(View view) {
                this.context = view.getContext();
                this.view = view;
                ButterKnife.bind(this, view);
            }

            void destroy() {
                Picasso.with(context).cancelRequest(photoView);
            }

            void bindPhoto(final String photoUrl) {
                progressBar.setVisibility(View.VISIBLE);
                Picasso.with(context).load(photoUrl)
                        .tag(context)
                        .into(photoView, new Callback.EmptyCallback() {
                            @Override
                            public void onSuccess() {
                                if (progressBar != null) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        });
            }

            @OnClick(R.id.item_view)
            void onClick() {
                finish();
            }
        }
    }
}
