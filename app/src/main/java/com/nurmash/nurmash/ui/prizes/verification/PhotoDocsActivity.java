package com.nurmash.nurmash.ui.prizes.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocFrgmCallbacks;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocsFrgmCallbacks;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocsPresenter;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocsView;
import com.nurmash.nurmash.ui.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PhotoDocsActivity extends BaseActivity implements PhotoDocsView, PhotoDocFrgmCallbacks, PhotoDocsFrgmCallbacks {
    private PhotoDocsPresenter photoDocsPresenter;
    private FragmentManager fragmentManager;
    private PhotoDocsFrgm photoDocsFrgm;
    private PhotoDocIDCardFrgm photoDocIDCardFrgm;
    private PhotoDocPassportFrgm photoDocPassportFrgm;
    @Bind(R.id.ll_parent_content) LinearLayout ll_parent_content;
    @Bind(R.id.photo_upload_progress) CircularProgressView photoUploadProgressView;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, PhotoDocsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_documents);
        ButterKnife.bind(this);

        // Presenter
        photoDocsPresenter = new PhotoDocsPresenter();
        photoDocsPresenter.attachView(this);

        configureFragments();
    }

    private void configureFragments() {
        fragmentManager = getSupportFragmentManager();

        FragmentTransaction frTrans = getSupportFragmentManager().beginTransaction();
        photoDocsFrgm = PhotoDocsFrgm.newInstance();
        photoDocsFrgm.setCallbacks(this);

        photoDocIDCardFrgm = PhotoDocIDCardFrgm.newInstance();
        photoDocIDCardFrgm.setCallbacks(this);
        photoDocPassportFrgm = PhotoDocPassportFrgm.newInstance();
        photoDocPassportFrgm.setCallbacks(this);

        frTrans.add(R.id.frgm_document_container, photoDocsFrgm, "docs");
        frTrans.add(R.id.frgm_document_container, photoDocIDCardFrgm, "doc_idcard");
        frTrans.add(R.id.frgm_document_container, photoDocPassportFrgm, "doc_passport");

        frTrans.hide(photoDocIDCardFrgm);
        frTrans.hide(photoDocPassportFrgm);

        frTrans.commit();
    }

    private void onBackPagePagination() {
        FragmentTransaction frgmTransaction = fragmentManager.beginTransaction();
        frgmTransaction.show(photoDocsFrgm);
        frgmTransaction.hide(photoDocPassportFrgm);
        frgmTransaction.hide(photoDocIDCardFrgm);
        frgmTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        photoDocsPresenter.onBackEventClick();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Default Back button handling
            case android.R.id.home:
                photoDocsPresenter.onBackEventClick();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        photoDocsPresenter.detachView();
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoDocsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onIDCardButtonClick() {
        FragmentTransaction frgmTransaction = fragmentManager.beginTransaction();
        frgmTransaction.hide(photoDocsFrgm);
        frgmTransaction.hide(photoDocPassportFrgm);
        frgmTransaction.show(photoDocIDCardFrgm);
        frgmTransaction.commit();
    }

    @Override
    public void onPassportButtonClick() {
        FragmentTransaction frgmTransaction = fragmentManager.beginTransaction();
        frgmTransaction.hide(photoDocsFrgm);
        frgmTransaction.show(photoDocPassportFrgm);
        frgmTransaction.hide(photoDocIDCardFrgm);
        frgmTransaction.commit();
    }

    @Override
    public void onBackEventClick() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("docs");
        if (currentFragment != null && currentFragment.isVisible()) {
            finish();
        } else {
            onBackPagePagination();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoDocFrgmCallbacks implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onIDCardClick() {
        photoDocsPresenter.onIDCardClick();
    }

    @Override
    public void onPassportClick() {
        photoDocsPresenter.onPassportClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoDocsFrgmCallbacks implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onPhotoUploadFailed() {
        Snackbar snackbar = Snackbar.make(ll_parent_content, R.string.error_message_upload_photo_failed, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment passportFrgm = getSupportFragmentManager().findFragmentByTag("doc_passport");
                Fragment idcardFrgm = getSupportFragmentManager().findFragmentByTag("doc_idcard");
                if (passportFrgm != null && passportFrgm.isVisible()) {
                    if (photoDocPassportFrgm != null) {
                        photoDocPassportFrgm.retryPhotoUpload();
                    }
                } else if (idcardFrgm != null && idcardFrgm.isVisible()) {
                    if (photoDocIDCardFrgm != null) {
                        photoDocIDCardFrgm.retryPhotoUpload();
                    }
                }
            }
        });
        snackbar.show();
    }

    @Override
    public void showPhotoUploadIndicator(boolean show) {
        photoUploadProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPhotoUploadProgress(final int progress) {
        photoUploadProgressView.setProgress(progress);
    }
}
