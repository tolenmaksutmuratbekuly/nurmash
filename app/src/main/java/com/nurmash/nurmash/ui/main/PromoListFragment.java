package com.nurmash.nurmash.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.main.PromoListPresenter;
import com.nurmash.nurmash.mvp.main.PromoListView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.ListPlaceholderViewHolder;
import com.nurmash.nurmash.ui.photo.CommentListAdapter;
import com.nurmash.nurmash.ui.promotion.PastPromoDetailActivity;
import com.nurmash.nurmash.ui.promotion.PromoDetailActivity;
import com.nurmash.nurmash.util.ApiUtils;
import com.nurmash.nurmash.util.annotation.PromoFilter;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import it.sephiroth.android.library.widget.HListView;

public class PromoListFragment extends Fragment implements PromoListView {
    PromoListPresenter promoListPresenter;
    PromoListAdapter promoListAdapter;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list_placeholder_stub) ViewStub listPlaceholderStub;
    ListPlaceholderViewHolder listPlaceholderViewHolder;
    EndlessScrollListener endlessScrollListener;
    private String filter;
    private Callback callback;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    public static PromoListFragment newInstance(@PromoFilter String filter) {
        PromoListFragment instance = new PromoListFragment();
        Bundle args = new Bundle();
        args.putString("filter", filter);
        instance.setArguments(args);
        return instance;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filter = getArguments().getString("filter");
        promoListPresenter = new PromoListPresenter(DataLayer.getInstance(getContext()), filter);
        promoListAdapter = new PromoListAdapter();
        promoListAdapter.setCallback(new PromoListAdapter.Callback() {
            @Override
            public void onPromotionClick(Promotion promotion) {
                promoListPresenter.onPromoClick(promotion);
            }

            @Override
            public void initDailyPromoHorizontalList(HListView hlv_daily_promo) {
                if (hlv_daily_promo.getAdapter() == null) {
                    DailyPromoAdapter dailyPromoAdapter = new DailyPromoAdapter(getActivity());
                    dailyPromoAdapter.setCallback(new DailyPromoAdapter.Callback() {
                        @Override
                        public void onUserClick(User user) {

                        }
                    });

                    hlv_daily_promo.setAdapter(dailyPromoAdapter);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_promo_list, container, false);
        ButterKnife.bind(this, rootView);
        configureRecyclerView();
        promoListPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), recyclerView)));
        promoListPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        promoListPresenter.setErrorHandler(null);
        promoListPresenter.detachView();
        endlessScrollListener = null;
        recyclerView.clearOnScrollListeners();
        recyclerView.setAdapter(null);
        swipeRefreshLayout.setOnRefreshListener(null);
        listPlaceholderViewHolder = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        promoListPresenter.onDestroy();
        super.onDestroy();
    }

    private void configureRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(promoListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                promoListPresenter.loadMoreItems();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                promoListPresenter.refreshItems();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    private void configureListPlaceholder() {
        listPlaceholderViewHolder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                promoListPresenter.refreshItems();
            }
        });
        listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    public interface Callback {
        void showLoadingIndicator(boolean show);

        void addPromoListItems(List<Promotion> promotions);
    }

    private void addHotPromoListItems(List<Promotion> promotions) {
        if (callback != null) {
            if (filter.equals(ApiUtils.FILTER_NEW_PROMOTIONS)) {
                if (promotions.size() >= 5) {
                    callback.addPromoListItems(promotions.subList(0, 5));
                } else {
                    callback.addPromoListItems(promotions);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PromoListView methods
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        ((PromoListAdapter) recyclerView.getAdapter()).setLoading(show);
        if (callback != null) {
            if (filter.equals(ApiUtils.FILTER_NEW_PROMOTIONS)) {
                callback.showLoadingIndicator(show);
            }
        }
    }

    @Override
    public void showRefreshingIndicator(boolean show) {
        swipeRefreshLayout.setRefreshing(show);
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setRefreshing(show);
        }
    }

    @Override
    public void setPromoListItems(List<Promotion> promotions) {
        ((PromoListAdapter) recyclerView.getAdapter()).setPromotions(promotions);
        recyclerView.post(needMoreItemsChecker);
        addHotPromoListItems(promotions);
    }

    @Override
    public void addPromoListItems(List<Promotion> promotions) {
        ((PromoListAdapter) recyclerView.getAdapter()).addPromotions(promotions);
        recyclerView.post(needMoreItemsChecker);
        addHotPromoListItems(promotions);
    }

    @Override
    public void setSoonPromoListItems(List<Promotion> promotions) {
        ((PromoListAdapter) recyclerView.getAdapter()).setSoonPromotions(promotions);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addSoonPromoListItems(List<Promotion> promotions) {
        ((PromoListAdapter) recyclerView.getAdapter()).addSoonPromotions(promotions);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void showListPlaceholder(@StringRes int textRes) {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListPlaceholderViewHolder(listPlaceholderStub.inflate());
            configureListPlaceholder();
        }
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideListPlaceholder() {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void openPromoDetailActivity(Promotion promo) {
        startActivity(PromoDetailActivity.newIntent(getActivity(), promo));
    }

    @Override
    public void openPastPromoDetailActivity(Promotion promo) {
        startActivity(PastPromoDetailActivity.newIntent(getActivity(), promo));
    }
}