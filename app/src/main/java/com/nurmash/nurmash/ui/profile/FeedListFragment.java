package com.nurmash.nurmash.ui.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.FeedItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.profile.FeedListPresenter;
import com.nurmash.nurmash.mvp.profile.FeedListView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.ListPlaceholderViewHolder;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class FeedListFragment extends Fragment implements FeedListView {
    FeedListPresenter feedListPresenter;
    FeedListAdapter feedListAdapter;
    EndlessScrollListener endlessScrollListener;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list_placeholder_stub) ViewStub listPlaceholderStub;
    ListPlaceholderViewHolder listPlaceholderViewHolder;
    private Callback callback;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    public static FeedListFragment newInstance() {
        return new FeedListFragment();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        feedListPresenter = new FeedListPresenter(DataLayer.getInstance(getContext()));
        feedListAdapter = new FeedListAdapter();
        feedListAdapter.setCallback(new FeedListAdapter.Callback() {
            @Override
            public void onFeedItemClick(FeedItem feedItem) {
                feedListPresenter.onItemClick(feedItem);
            }

            @Override
            public void onUserClick(User user) {
                feedListPresenter.onUserClick(user);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_feed_list, container, false);
        ButterKnife.bind(this, rootView);
        configureRecyclerView();
        // Presenter
        feedListPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), recyclerView)));
        feedListPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        feedListPresenter.onResume();
    }

    @Override
    public void onDestroyView() {
        feedListPresenter.setErrorHandler(null);
        feedListPresenter.detachView();
        recyclerView.setAdapter(null);
        recyclerView.clearOnScrollListeners();
        endlessScrollListener = null;
        listPlaceholderViewHolder = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void configureRecyclerView() {
        recyclerView.setAdapter(feedListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                feedListPresenter.onRequestMoreItems();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                feedListPresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    private ListPlaceholderViewHolder getListPlaceholderViewHolder() {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListPlaceholderViewHolder(listPlaceholderStub.inflate());
            listPlaceholderViewHolder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    feedListPresenter.onRefreshAction();
                }
            });
            listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
        }
        return listPlaceholderViewHolder;
    }

    ///////////////////////////////////////////////////////////////////////////
    // FeedListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showFeedLoadingIndicator(boolean show) {
        feedListAdapter.setLoading(show);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
            if (listPlaceholderViewHolder != null) {
                listPlaceholderViewHolder.setRefreshing(false);
            }
        }
    }

    @Override
    public void setFeedItems(List<FeedItem> items) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        feedListAdapter.setItems(items);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addFeedItems(List<FeedItem> items) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }

        feedListAdapter.addItems(items);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void showFeedListPlaceholder(@StringRes int textRes, @StringRes int btnRes) {
        ListPlaceholderViewHolder listPlaceholderViewHolder = getListPlaceholderViewHolder();
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setButtonText(btnRes);
        listPlaceholderViewHolder.setButtonCallback(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.participateClick();
                }
            }
        });
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void openPhotoDetailActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(getContext(), competitorId));
    }

    @Override
    public void openUserProfileActivity(User user) {
        startActivity(ProfileDetailActivity.newIntent(getContext(), user));
    }

    @Override
    public void finishActivity() {
        getActivity().finish();
    }

    public interface Callback {
        void participateClick();
    }
}
