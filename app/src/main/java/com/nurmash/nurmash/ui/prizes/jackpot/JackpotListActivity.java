package com.nurmash.nurmash.ui.prizes.jackpot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.JackpotList;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.mvp.prizes.jackpot.JackpotListPresenter;
import com.nurmash.nurmash.mvp.prizes.jackpot.JackpotListView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.FormatUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class JackpotListActivity extends BaseActivity implements JackpotListView, StringsProvider {
    JackpotListPresenter jackpotPresenter;
    @Bind(R.id.fl_parent) FrameLayout fl_parent;
    @Bind(R.id.loading_indicator) ViewGroup loading_indicator;
    @Bind(R.id.jackpot_time_left) View jackpot_time_left;
    @Bind(R.id.jackpot_winner) View jackpot_winner;

    JackpotTimeLeftViewHolder jackpotTimeLeftViewHolder;
    JackpotWinnerViewHolder jackpotWinnerHolder;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, JackpotListActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jackpot_list);
        ButterKnife.bind(this);

        // Presenter
        jackpotPresenter = new JackpotListPresenter(DataLayer.getInstance(this), FormatUtils.getMoneyAmountDisplayFormat());
        jackpotPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, fl_parent)));
        jackpotPresenter.setStringsProvider(this);
        jackpotPresenter.attachView(this);

        configureTimeLeft();
        configureWinner();
    }

    @Override
    protected void onDestroy() {
        jackpotPresenter.detachView();
        jackpotTimeLeftViewHolder = null;
        jackpotWinnerHolder = null;
        super.onDestroy();
    }

    private void configureTimeLeft() {
        jackpotTimeLeftViewHolder = new JackpotTimeLeftViewHolder(findViewById(R.id.jackpot_time_left));
    }

    private void configureWinner() {
        jackpotWinnerHolder = new JackpotWinnerViewHolder(findViewById(R.id.jackpot_winner), this);
        jackpotWinnerHolder.setCallback(new JackpotWinnerViewHolder.Callback() {
            @Override
            public void onCardJackpotClick() {

            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // JackpotListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void viewHoldersState(boolean display_winners) {
        jackpot_time_left.setVisibility(display_winners ? View.GONE : View.VISIBLE);
        jackpot_winner.setVisibility(display_winners ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        loading_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showJackpotTitle(String title) {
        jackpotTimeLeftViewHolder.showJackpotTitle(title);
    }

    @Override
    public void showJackpotMoneyList(String jackpotAmountStr) {
        jackpotTimeLeftViewHolder.showJackpotMoneyList(jackpotAmountStr);
    }

    @Override
    public void showJackpotTimeLeft(String timeLeftStr) {
        jackpotTimeLeftViewHolder.showJackpotTimeLeft(timeLeftStr);
    }

    @Override
    public void setWinners(List<JackpotList.Winners> winners) {
        jackpotWinnerHolder.setWinners(winners);
    }
}
