package com.nurmash.nurmash.ui.prizes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.PrizeItemPojo;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListAdapterPrizes extends BaseAdapter {
    private static final int ITEM_LAYOUT = R.layout.lv_item_prizes;
    private Context context;
    private boolean showRightArrow;
    private List<PrizeItemPojo> prizes = new ArrayList<>(6);
    private Callback callback;

    public ListAdapterPrizes(Context context) {
        this.context = context;
        this.prizes = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            this.prizes.add(null);
        }
    }

    public void setArrowRightVisibility(boolean showRightArrow) {
        this.showRightArrow = showRightArrow;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    public void addPrize(PrizeItemPojo prizeItem) {
        this.prizes.set(prizeItem.itemType, prizeItem);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return prizes.size();
    }

    @Override
    public Object getItem(int position) {
        return prizes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(ITEM_LAYOUT, parent, false);
            convertView.setTag(new PrizeViewHolder(convertView));
        }

        PrizeViewHolder viewHolder = (PrizeViewHolder) convertView.getTag();
        viewHolder.bindPrize((PrizeItemPojo) getItem(position));

        return convertView;
    }

    class PrizeViewHolder extends RecyclerView.ViewHolder {
        Context context;
        PrizeItemPojo bindedPrize;
        @Bind(R.id.img_promo) ImageView img_promo;
        @Bind(R.id.tv_promo_title) TextView tv_promo_title;
        @Bind(R.id.img_arrow_right) ImageView img_arrow_right;

        public PrizeViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindPrize(PrizeItemPojo prize) {
            bindedPrize = prize;
            if (prize == null) {
                img_promo.setImageDrawable(null);
                tv_promo_title.setText(null);
                img_arrow_right.setImageDrawable(null);
                return;
            }
            img_promo.setImageResource(prize.imgResource);
            tv_promo_title.setText(prize.title);
            img_arrow_right.setVisibility(showRightArrow ? View.VISIBLE : View.GONE);
        }

        @OnClick(R.id.ll_view)
        void onPrizeItemClick() {
            if (callback != null && bindedPrize != null) {
                if (showRightArrow) {
                    callback.onPrizeItemClick(bindedPrize);
                }
            }
        }
    }

    public interface Callback {
        void onPrizeItemClick(PrizeItemPojo prizeItem);
    }
}
