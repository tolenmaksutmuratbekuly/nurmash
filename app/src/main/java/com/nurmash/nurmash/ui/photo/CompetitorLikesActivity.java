package com.nurmash.nurmash.ui.photo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStub;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.LikesItem;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.photo.CompetitorLikesPresenter;
import com.nurmash.nurmash.mvp.photo.CompetitorLikesView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.profile.ProfileDetailActivity;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CompetitorLikesActivity extends BaseActivity implements CompetitorLikesView {
    public static Intent newIntent(Context packageContext, long competitorId) {
        Intent intent = new Intent(packageContext, CompetitorLikesActivity.class);
        intent.putExtra("competitorId", competitorId);
        return intent;
    }

    CompetitorLikesPresenter competitorLikesPresenter;
    CompetitorLikesListAdapter likesListAdapter;
    EndlessScrollListener endlessScrollListener;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list_placeholder_stub) ViewStub listPlaceholderStub;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competitor_likes);
        ButterKnife.bind(this);

        // Presenter
        Intent intent = getIntent();
        long competitorId = 0;
        if (intent.hasExtra("competitorId")) {
            competitorId = getIntent().getExtras().getLong("competitorId");
        }

        competitorLikesPresenter = new CompetitorLikesPresenter(DataLayer.getInstance(this), competitorId);
        likesListAdapter = new CompetitorLikesListAdapter();
        likesListAdapter.setCallback(new CompetitorLikesListAdapter.Callback() {
            @Override
            public void onUserClick(User user) {
                competitorLikesPresenter.onUserClick(user);
            }
        });

        configureRecyclerView();

        // Presenter
        competitorLikesPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, recyclerView)));
        competitorLikesPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        competitorLikesPresenter.setErrorHandler(null);
        competitorLikesPresenter.detachView();
        recyclerView.setAdapter(null);
        recyclerView.clearOnScrollListeners();
        endlessScrollListener = null;
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    private void configureRecyclerView() {
        recyclerView.setAdapter(likesListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                competitorLikesPresenter.onRequestMoreItems();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                competitorLikesPresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    ///////////////////////////////////////////////////////////////////////////
    // CompetitorLikesView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        likesListAdapter.setLoading(show);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void setLikeItems(List<LikesItem> items) {
        likesListAdapter.setItems(items);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addLikeItems(List<LikesItem> items) {
        likesListAdapter.addItems(items);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void openUserProfileActivity(User user) {
        startActivity(ProfileDetailActivity.newIntent(this, user));
    }
}
