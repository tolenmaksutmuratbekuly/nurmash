package com.nurmash.nurmash.ui.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.login.LoginActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntroLoginFragment extends Fragment {
    public static IntroLoginFragment newInstance() {
        IntroLoginFragment fragment = new IntroLoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_intro_login, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @OnClick(R.id.button_facebook)
    void onFacebookClick() {
        startActivity(LoginActivity.newIntent(getActivity()).setAction(LoginActivity.ACTION_LOGIN_FACEBOOK));
    }

    @OnClick(R.id.button_vk)
    void onVKClick() {
        startActivity(LoginActivity.newIntent(getActivity()).setAction(LoginActivity.ACTION_LOGIN_VK));
    }

    @OnClick(R.id.button_google)
    void onGoogleClick() {
        startActivity(LoginActivity.newIntent(getActivity()).setAction(LoginActivity.ACTION_LOGIN_GOOGLE));
    }
}
