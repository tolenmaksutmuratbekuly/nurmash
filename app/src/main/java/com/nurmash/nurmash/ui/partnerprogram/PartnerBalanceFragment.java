package com.nurmash.nurmash.ui.partnerprogram;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.partnerprogram.PartnerBalancePresenter;
import com.nurmash.nurmash.mvp.partnerprogram.PartnerBalanceView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.FormatUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PartnerBalanceFragment extends Fragment implements PartnerBalanceView {
    PartnerBalancePresenter partnerBalancePresenter;
    @Bind(R.id.fl_parent) FrameLayout fl_parent;
    @Bind(R.id.loading_indicator) ViewGroup loading_indicator;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.tv_drawal_money) TextView tv_drawal_money;
    @Bind(R.id.tv_reward_money) TextView tv_reward_money;
    @Bind(R.id.tv_balance_description) TextView tv_balance_description;
    @Bind(R.id.btn_cash_withdraw) Button btn_cash_withdraw;

    public static PartnerBalanceFragment newInstance() {
        return new PartnerBalanceFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        partnerBalancePresenter = new PartnerBalancePresenter(DataLayer.getInstance(getContext()));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_partner_balance, container, false);
        ButterKnife.bind(this, rootView);

        DataLayer dataLayer = DataLayer.getInstance(getContext());
        if (dataLayer.authPreferences().hasShownBalanceDescription()) {
            tv_balance_description.setVisibility(View.GONE);
        } else {
            tv_balance_description.setVisibility(View.VISIBLE);
            dataLayer.authPreferences().setShownBalanceDescription(true);
        }

        // Presenter
        partnerBalancePresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), fl_parent)));
        partnerBalancePresenter.attachView(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                partnerBalancePresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        partnerBalancePresenter.setErrorHandler(null);
        partnerBalancePresenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @OnClick(R.id.btn_cash_withdraw)
    void cashWithdrawal() {
//        startActivity(PrizeDataActivity.newIntent(getContext(), isZeroBalance));
    }

    @OnClick(R.id.ll_payments_history)
    void paymentsHistory() {
        startActivity(PaymentsHistoryActivity.newIntent(getActivity()));
    }

    ///////////////////////////////////////////////////////////////////////////
    // PartnerBalanceView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        loading_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onBalanceLoaded(PartnerBalance partnerBalance) {
        if (partnerBalance != null && partnerBalance.bonuses != null) {
            tv_drawal_money.setText(partnerBalance.bonuses.available.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + partnerBalance.getCurrencyCode());
            tv_reward_money.setText(partnerBalance.bonuses.invited.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + partnerBalance.getCurrencyCode());
        }
    }

    @Override
    public void isZeroBalanceActive(boolean isZeroBalance) {
        btn_cash_withdraw.setEnabled(!isZeroBalance);
    }
}
