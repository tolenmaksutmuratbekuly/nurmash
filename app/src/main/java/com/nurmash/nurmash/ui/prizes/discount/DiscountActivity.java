package com.nurmash.nurmash.ui.prizes.discount;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DiscountActivity extends BaseActivity {
    private PagerAdapter pagerAdapter;
    @Bind(R.id.tabs) TabLayout tabLayout;
    @Bind(R.id.pager) ViewPager viewPager;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, DiscountActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);
        ButterKnife.bind(this);
        pagerAdapter = createPagerAdapter();
        viewPager.setAdapter(pagerAdapter);
        configureTabLayout();
    }

    @Override
    protected void onDestroy() {
        tabLayout.setupWithViewPager(null);
        viewPager.setAdapter(null);
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    private void configureTabLayout() {
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);
    }

    private PagerAdapter createPagerAdapter() {
        FragmentArrayAdapter pagerAdapter = new FragmentArrayAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(DiscountsFragment.newInstance(true), getString(R.string.label_prize_discount_current));
        pagerAdapter.addFragment(DiscountsFragment.newInstance(false), getString(R.string.label_prize_discount_used));

        return pagerAdapter;
    }
}
