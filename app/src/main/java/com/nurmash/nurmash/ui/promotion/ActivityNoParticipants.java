package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityNoParticipants extends BaseActivity {
    public static final int STATUS_NO_PARTICIPANTS = 1;
    private int status;
    @Bind(R.id.img_status) ImageView img_status;
    @Bind(R.id.tv_status_title) TextView tv_status_title;
    @Bind(R.id.tv_status_description) TextView tv_status_description;
    @Bind(R.id.btn_participate) Button btn_participate;

    public static Intent newIntent(Context packageContext, int status) {
        Intent intent = new Intent(packageContext, ActivityNoParticipants.class);
        intent.putExtra("status", status);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noparticipants);
        ButterKnife.bind(this);
        status = getIntent().getIntExtra("status", 0);

        if (status == STATUS_NO_PARTICIPANTS) {
            img_status.setImageResource(R.drawable.promo_noparticipants);
            tv_status_title.setText(getString(R.string.promo_detail_no_participants_title));
            tv_status_description.setText(getString(R.string.promo_detail_no_participants_label));
            btn_participate.setText(getString(R.string.action_participate));
        }
    }

    @OnClick(R.id.btn_participate)
    void onClickBack() {
        finish();
    }
}

