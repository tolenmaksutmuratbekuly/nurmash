package com.nurmash.nurmash.ui.prizes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.PluralsRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PrizeItemPojo;
import com.nurmash.nurmash.model.json.ProfileVerifyStatus;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;
import com.nurmash.nurmash.mvp.prizes.PrizesPresenter;
import com.nurmash.nurmash.mvp.prizes.PrizesView;
import com.nurmash.nurmash.ui.prizes.info.PrizesInfoActivity;
import com.nurmash.nurmash.ui.prizes.verification.DocsVerifiyResultActivity;
import com.nurmash.nurmash.ui.prizes.verification.PhotoDocsActivity;
import com.nurmash.nurmash.util.SnackbarUtils;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PrizesTabFragment extends Fragment implements StringsProvider, PrizesView {
    public static final int CLAIM_PRIZE_REQUEST_CODE = 2;

    PrizeDataVerificationHolder viewHolderVerification;
    PrizesPresenter prizesPresenter;
    View rootView;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.fl_parent) FrameLayout fl_parent;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;

    public static PrizesTabFragment newInstance() {
        PrizesTabFragment fragment = new PrizesTabFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prizesPresenter = new PrizesPresenter(DataLayer.getInstance(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_prizes_tab, container, false);
        ButterKnife.bind(this, rootView);
        configureToolbar();
        configureVerification();

        prizesPresenter.setStringsProvider(this);
        prizesPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        prizesPresenter.onResume();
    }

    @Override
    public void onDestroyView() {
        prizesPresenter.setStringsProvider(null);
        prizesPresenter.detachView();
        rootView = null;
        viewHolderVerification = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        prizesPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CLAIM_PRIZE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    prizesPresenter.onClaimPrizeSuccess();
                }
                break;
        }
    }

    private void configureToolbar() {
        toolbar.inflateMenu(R.menu.menu_prizes);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.info:
                        prizesPresenter.onInfoClick();
                        return true;
                }
                return false;
            }
        });
    }

    private void configureVerification() {
        viewHolderVerification = new PrizeDataVerificationHolder(getActivity(), fl_parent);
        viewHolderVerification.setCallback(new PrizeDataVerificationHolder.Callback() {
            @Override
            public void onPassVerificationClick(ProfileVerifyStatus profileVerifyStatus) {
                if (profileVerifyStatus.needToShowUploadButtons()) {
                    // Previously document is NOT sent, request on verification (Need to open page "Photo upload page")
                    startActivity(PhotoDocsActivity.newIntent(getActivity()));
                } else if (profileVerifyStatus.isPendingRequest()) {
                    // Previously document sent, request is pending. (Need to open page with "Pending screen")
                    startActivity(DocsVerifiyResultActivity.newIntent(getActivity(),
                            profileVerifyStatus.isPendingRequest()));
                } else if (profileVerifyStatus.isPhotoQualityBad() || profileVerifyStatus.isRejected()) {
                    startActivity(DocsVerifiyResultActivity.newIntent(getActivity(),
                            profileVerifyStatus.isPendingRequest(), profileVerifyStatus.status));
                } else {
                    final Snackbar snackbar = SnackbarUtils.multilineSnackbar(fl_parent, R.string.error_message_generic_request, Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction(R.string.action_dismiss, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackbar.dismiss();
                        }
                    });
                    snackbar.show();
                }
            }
        });

        addPrize(new PrizeItemPojo(
                getActivity().getString(R.string.label_prize_item_discount),
                R.drawable.prize_item_discount, 0, PrizeItemPojo.ITEM_DISCOUNT
        ));

        addPrize(new PrizeItemPojo(
                getActivity().getString(R.string.label_prize_item_prizes),
                R.drawable.prize_item_prizes, 0, PrizeItemPojo.ITEM_PRIZES
        ));
        addPrize(new PrizeItemPojo(
                getActivity().getString(R.string.label_prize_item_jackpot),
                R.drawable.prize_item_jackpot, 0, PrizeItemPojo.ITEM_JACKPOT
        ));
        addPrize(new PrizeItemPojo(
                getActivity().getString(R.string.label_prize_item_withdraw_history),
                R.drawable.prize_item_history, 0, PrizeItemPojo.ITEM_PAYMENT_HISTORY
        ));
    }

    ///////////////////////////////////////////////////////////////////////////
    // StringsProvider implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getQuantityString(@PluralsRes int resId, int quantity, Object... args) {
        return getResources().getQuantityString(resId, quantity, args);
    }

    ///////////////////////////////////////////////////////////////////////////
    // PrizesView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void addPrize(PrizeItemPojo wonPrizes) {
        viewHolderVerification.addPrize(wonPrizes);
    }

    @Override
    public void profileVerificationStatus(ProfileVerifyStatus profileVerifyStatus) {
        viewHolderVerification.profileVerificationStatus(profileVerifyStatus);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPrizeInfoLoadingFailed() {
        final Snackbar snackbar = Snackbar.make(rootView, R.string.error_message_generic_request, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prizesPresenter.onRetryClick();
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override
    public void onInviteBonusLoadingFailed() {
        final Snackbar snackbar = Snackbar.make(rootView, R.string.error_message_generic_request, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prizesPresenter.onInviteBonusRetry();
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override
    public void openPrizesInfoActivity() {
        startActivity(PrizesInfoActivity.newPrizesInfoIntent(getContext()));
    }

    @Override
    public void showClaimPrizeSuccessMessage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(rootView, R.string.info_message_claim_prize_success, Snackbar.LENGTH_LONG).show();
            }
        }, 500);
    }

//    @Override
//    public void openJackpotActivity() {
//        startActivity(JackpotListActivity.newIntent(getContext()));
//    }
}
