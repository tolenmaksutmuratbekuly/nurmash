package com.nurmash.nurmash.ui.prizes.withdraw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.model.withdraw.WireTransferForm;
import com.nurmash.nurmash.mvp.prizes.withdraw.WireTransferFormPresenter;
import com.nurmash.nurmash.mvp.prizes.withdraw.WireTransferFormView;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;

import static com.nurmash.nurmash.util.ContextUtils.hideSoftKeyboard;

public class WireTransferFormFragment extends WithdrawFormFragment<WireTransferForm> implements WireTransferFormView {
    final WireTransferFormPresenter wireTransferFormPresenter;
    @Bind(R.id.form_view) ViewGroup formView;
    @Bind(R.id.first_name_input) MaterialEditText firstNameInput;
    @Bind(R.id.last_name_input) MaterialEditText lastNameInput;
    @Bind(R.id.country_input) MaterialEditText countryInput;
    @Bind(R.id.region_input) MaterialEditText regionInput;
    @Bind(R.id.city_input) MaterialEditText cityInput;
    @Bind(R.id.address_1_input) MaterialEditText addressLine1Input;
    @Bind(R.id.address_2_input) MaterialEditText addressLine2Input;
    @Bind(R.id.zip_code_input) MaterialEditText zipCodeInput;
    @Bind(R.id.bank_name_input) MaterialEditText bankNameInput;
    @Bind(R.id.bank_swift_input) MaterialEditText bankSwiftInput;
    @Bind(R.id.bank_account_number_input) MaterialEditText bankAccountNumberInput;

    public static WireTransferFormFragment newInstance() {
        WireTransferFormFragment fragment = new WireTransferFormFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public WireTransferFormFragment() {
        wireTransferFormPresenter = new WireTransferFormPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wire_transfer_withdraw_form, container, false);
        ButterKnife.bind(this, rootView);
        wireTransferFormPresenter.attachView(this, DataLayer.getInstance(getActivity()));
        return rootView;
    }

    @Override
    public void onDestroyView() {
        wireTransferFormPresenter.detachView();
        hideSoftKeyboard(formView);
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @OnEditorAction(R.id.bank_account_number_input)
    boolean onEmailInputAction(int action) {
        if (action == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyboard(formView);
            formView.requestFocus();
            return true;
        }
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Abstract methods implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public WireTransferForm getForm() {
        return wireTransferFormPresenter.getForm(
                firstNameInput.getText().toString(),
                lastNameInput.getText().toString(),
                countryInput.getText().toString(),
                regionInput.getText().toString(),
                cityInput.getText().toString(),
                addressLine1Input.getText().toString(),
                addressLine2Input.getText().toString(),
                zipCodeInput.getText().toString(),
                bankNameInput.getText().toString(),
                bankSwiftInput.getText().toString(),
                bankAccountNumberInput.getText().toString()
        );
    }

    @Override
    public void onFormValidationError(ValidationError error) {
        wireTransferFormPresenter.onValidationError(error);
    }

    ///////////////////////////////////////////////////////////////////////////
    // WireTransferFormView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showFirstName(String firstName) {
        firstNameInput.setText(firstName);
    }

    @Override
    public void showLastName(String lastName) {
        lastNameInput.setText(lastName);
    }

    @Override
    public void showCountry(String country) {
        countryInput.setText(country);
    }

    @Override
    public void showRegion(String region) {
        regionInput.setText(region);
    }

    @Override
    public void showCity(String city) {
        cityInput.setText(city);
    }

    @Override
    public void showAddressLine1(String addressLine1) {
        addressLine1Input.setText(addressLine1);
    }

    @Override
    public void showAddressLine2(String addressLine2) {
        addressLine2Input.setText(addressLine2);
    }

    @Override
    public void showZipCode(String zipCode) {
        zipCodeInput.setText(zipCode);
    }

    @Override
    public void showBankName(String bankName) {
        bankNameInput.setText(bankName);
    }

    @Override
    public void showBankSwift(String bankSwift) {
        bankSwiftInput.setText(bankSwift);
    }

    @Override
    public void showBankAccountNumber(String bankAccountNumber) {
        bankAccountNumberInput.setText(bankAccountNumber);
    }

    @Override
    public void showFirstNameError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(firstNameInput, errorRes, scrollTo);
    }

    @Override
    public void showLastNameError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(lastNameInput, errorRes, scrollTo);
    }

    @Override
    public void showAddressError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(addressLine1Input, errorRes, scrollTo);
    }

    @Override
    public void showZipCodeError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(zipCodeInput, errorRes, scrollTo);
    }

    @Override
    public void showCityError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(cityInput, errorRes, scrollTo);
    }

    @Override
    public void showRegionError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(regionInput, errorRes, scrollTo);
    }

    @Override
    public void showCountryError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(countryInput, errorRes, scrollTo);
    }

    @Override
    public void showBankNameError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(bankNameInput, errorRes, scrollTo);
    }

    @Override
    public void showBankSwiftError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(bankSwiftInput, errorRes, scrollTo);
    }

    @Override
    public void showBankAccountNumberError(@StringRes int errorRes, boolean scrollTo) {
        showInputError(bankAccountNumberInput, errorRes, scrollTo);
    }

    @Override
    public void showLowAmountError(@StringRes int errorRes) {
        Toast.makeText(getContext(), errorRes, Toast.LENGTH_LONG).show();
    }
}
