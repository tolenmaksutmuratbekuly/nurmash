package com.nurmash.nurmash.ui;

import android.os.Bundle;
import android.support.annotation.PluralsRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.MenuItem;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.mvp.helpers.FilesProvider;
import com.nurmash.nurmash.mvp.helpers.StringsProvider;

public class BaseActivity extends AppCompatActivity implements StringsProvider, FilesProvider {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureActionBar(getSupportActionBar());
    }

    private void configureActionBar(ActionBar actionBar) {
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    protected int getStatusBarHeight() {
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return getResources().getDimensionPixelSize(resourceId);
        } else {
            // It seems 25 dp is the default value.
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());
        }
    }

    protected int getActionBarSizeFromAttr() {
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true);
        return TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Default Back button handling
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ///////////////////////////////////////////////////////////////////////////
    // StringsProvider implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getQuantityString(@PluralsRes int resId, int quantity, Object... args) {
        return getResources().getQuantityString(resId, quantity, args);
    }
}
