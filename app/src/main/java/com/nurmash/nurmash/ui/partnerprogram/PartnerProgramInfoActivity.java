package com.nurmash.nurmash.ui.partnerprogram;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;

import butterknife.ButterKnife;

public class PartnerProgramInfoActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, PartnerProgramInfoActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner_program_info);
        ButterKnife.bind(this);
    }
}
