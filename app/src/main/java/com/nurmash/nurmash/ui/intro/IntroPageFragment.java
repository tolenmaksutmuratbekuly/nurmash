package com.nurmash.nurmash.ui.intro;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class IntroPageFragment extends Fragment {
    @StringRes int titleRes;
    @StringRes int textRes;
    @DrawableRes int imageRes;
    @Bind(R.id.image) ImageView imageView;
    @Bind(R.id.title) TextView titleView;
    @Bind(R.id.text) TextView textView;

    public static IntroPageFragment newInstance(@StringRes int titleRes,
                                                @StringRes int textRes,
                                                @DrawableRes int imageRes) {
        IntroPageFragment fragment = new IntroPageFragment();
        Bundle args = new Bundle();
        args.putInt("titleRes", titleRes);
        args.putInt("textRes", textRes);
        args.putInt("imageRes", imageRes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titleRes = getArguments().getInt("titleRes");
        textRes = getArguments().getInt("textRes");
        imageRes = getArguments().getInt("imageRes");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_intro_page, container, false);
        ButterKnife.bind(this, rootView);
        imageView.setImageResource(imageRes);
        titleView.setText(titleRes);
        textView.setText(textRes);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }
}
