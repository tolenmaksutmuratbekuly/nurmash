package com.nurmash.nurmash.ui.participate;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.mvp.participate.PhotoStepPresenter;
import com.nurmash.nurmash.mvp.participate.PhotoStepView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.util.CameraUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoStepActivity extends BaseActivity implements PhotoStepView {
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CROP_REQUEST_CODE = 2;

    PhotoStepPresenter photoStepPresenter;
    @Bind(R.id.content_view) ViewGroup contentView;

    public static Intent newIntent(Context packageContext, long promoId) {
        Intent intent = new Intent(packageContext, PhotoStepActivity.class);
        intent.putExtra("promoId", promoId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_step);
        ButterKnife.bind(this);
        // Presenter
        long promoId = getIntent().getLongExtra("promoId", 0);
        photoStepPresenter = new PhotoStepPresenter(promoId);
        photoStepPresenter.restoreInstanceState(savedInstanceState);
        photoStepPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        photoStepPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        photoStepPresenter.saveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                photoStepPresenter.onCameraResult(resultCode == RESULT_OK);
                break;
            case GALLERY_REQUEST_CODE:
                photoStepPresenter.onGalleryResult(resultCode == RESULT_OK ? data.getData() : null);
                break;
            case CROP_REQUEST_CODE:
                photoStepPresenter.onCropResult(resultCode == RESULT_OK);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.button_camera)
    public void onCameraButtonClick() {
        photoStepPresenter.onCameraOptionSelected();
    }

    @OnClick(R.id.button_gallery)
    public void onGalleryButtonClick() {
        photoStepPresenter.onGalleryOptionSelected();
    }

    // PhotoStepView implementation

    @Override
    public boolean openCameraApp(Uri tempPhotoUri) {
        Intent intent = CameraUtils.newCameraIntent(tempPhotoUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void openGalleryApp() {
        startActivityForResult(CameraUtils.newGalleryIntent(), GALLERY_REQUEST_CODE);
    }

    @Override
    public boolean openCropApp(Uri inFileUri, Uri outFileUri) {
        Intent intent = CameraUtils.newCropIntent(inFileUri, outFileUri, true);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, CROP_REQUEST_CODE);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void openShareStepActivity(long promoId, Uri photoFileUri) {
        startActivity(ShareStepActivity.newIntent(this, promoId, photoFileUri));
    }

    @Override
    public void showTempStorageError() {
        Snackbar.make(contentView, R.string.error_message_storage_create_temp_file, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCameraAppError() {
        Snackbar.make(contentView, R.string.error_message_camera_not_available, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCropAppError() {
        Snackbar.make(contentView, R.string.error_message_crop_not_available, Snackbar.LENGTH_LONG).show();
    }
}
