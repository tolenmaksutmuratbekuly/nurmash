package com.nurmash.nurmash.ui.partnerprogram;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.ui.BaseActivity;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PartnerProgramIntroActivity extends BaseActivity {
    @Bind(R.id.view_intro1) ViewGroup view_intro1;
    @Bind(R.id.view_intro2) ViewGroup view_intro2;
    private boolean isQRIntroPage;
    private User user;

    public static Intent newIntent(Context packageContext, boolean isQRIntroPage, User user) {
        Intent intent = new Intent(packageContext, PartnerProgramIntroActivity.class);
        intent.putExtra("isQRIntroPage", isQRIntroPage);
        intent.putExtra("user", Parcels.wrap(user));
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner_program_intro);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra("isQRIntroPage") && intent.hasExtra("user")) {
            isQRIntroPage = intent.getBooleanExtra("isQRIntroPage", false);
            user = Parcels.unwrap(getIntent().getParcelableExtra("user"));

            if (isQRIntroPage) {
                view_intro2.setVisibility(View.VISIBLE);
                view_intro1.setVisibility(View.GONE);
            } else {
                view_intro2.setVisibility(View.GONE);
                view_intro1.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @OnClick(R.id.btn_partnerprogram_intro)
    void partnerProgramNextClick() {
        startActivity(PartnerProgramActivity.newIntent(this, user));
        finish();
    }

    @OnClick(R.id.btn_nextintro_qr)
    void qrNextClick() {
        startActivity(QRScanerActivity.newIntent(this, user));
    }
}

