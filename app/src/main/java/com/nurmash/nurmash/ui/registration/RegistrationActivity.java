package com.nurmash.nurmash.ui.registration;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Gender;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.registration.RegistrationPresenter;
import com.nurmash.nurmash.mvp.registration.RegistrationView;
import com.nurmash.nurmash.ui.AndroidPhotoPicker;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DatePickerDialogFragment;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.ui.settings.LocationPickerActivity;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.parceler.Parcels;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.ContextUtils.dp2pixels;
import static com.nurmash.nurmash.util.ContextUtils.hideSoftKeyboard;
import static com.nurmash.nurmash.util.SnackbarUtils.multilineSnackbar;
import static com.nurmash.nurmash.util.ViewUtils.getRelativeTopInAncestor;
import static com.nurmash.nurmash.util.ViewUtils.moveEditorCursorToEnd;
import static com.nurmash.nurmash.util.ViewUtils.setTextIfDiffers;
import static com.squareup.picasso.MemoryPolicy.NO_CACHE;

public class RegistrationActivity extends BaseActivity implements RegistrationView, DatePickerDialogFragment.Callback {
    private static final int LOCATION_PICKER_REQUEST_CODE = 0;
    private static final int PHOTO_PICKER_BASE_REQUEST_CODE = 1;

    RegistrationPresenter registrationPresenter;
    AndroidPhotoPicker photoPicker;
    private User user;
    private boolean isGeneralInfoCompleted;
    @Bind(R.id.scroll_view) ScrollView scrollView;
    @Bind(R.id.content_view) ViewGroup contentView;
    @Bind(R.id.ll_general_content) ViewGroup ll_general_content;
    @Bind(R.id.ll_location_content) ViewGroup ll_location_content;
    @Bind(R.id.loading_progress_overlay) ViewGroup loadingProgressOverlay;
    @Bind(R.id.translucent_progress_overlay) ViewGroup translucentProgressOverlay;
    // Photo
    @Bind(R.id.profile_photo) ImageView photoView;
    @Bind(R.id.profile_photo_dark_overlay) View photoDarkOverlay;
    @Bind(R.id.profile_photo_progress_overlay) ViewGroup photoProgressOverlay;
    @Bind(R.id.profile_photo_upload_progress) CircularProgressView photoUploadProgressView;
    // Input fields
    @Bind(R.id.first_name_input) MaterialEditText firstNameInput;
    @Bind(R.id.last_name_input) MaterialEditText lastNameInput;
    @Bind(R.id.email_input) MaterialEditText emailInput;
    @Bind(R.id.birthdate_input) MaterialEditText birthdateInput;
    @Bind(R.id.gender_radio_group) RadioGroup genderRadioGroup;
    @Bind(R.id.country_input) MaterialEditText countryInput;
    @Bind(R.id.region_input) MaterialEditText regionInput;
    @Bind(R.id.city_input) MaterialEditText cityInput;
    @Bind(R.id.tv_privacy_policy) TextView tv_privacy_policy;
    @Bind(R.id.img_checkbox) ImageView img_checkbox;
    // Buttons
    @Bind(R.id.btn_nextstep) Button btnNextstep;
    @Bind(R.id.btn_save) Button btnSave;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, RegistrationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        if (getIntent().getParcelableExtra("user") != null) {
            user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
        }

        configurePrivacyPolicyTextView();
        configureGenderRadioGroup();
        // PhotoPicker
        photoPicker = new AndroidPhotoPicker(this, PHOTO_PICKER_BASE_REQUEST_CODE);
        photoPicker.onRestoreInstanceState(savedInstanceState);
        // Presenter
        registrationPresenter = new RegistrationPresenter(DataLayer.getInstance(this), user);
        registrationPresenter.onRestoreInstanceState(savedInstanceState);
        registrationPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, scrollView)));
        registrationPresenter.setPhotoPicker(photoPicker);
        registrationPresenter.setFilesProvider(this);
        registrationPresenter.attachView(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        photoPicker.onSaveInstanceState(outState);
        registrationPresenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        registrationPresenter.detachView();
        registrationPresenter.setErrorHandler(null);
        registrationPresenter.setPhotoPicker(null);
        registrationPresenter.setFilesProvider(null);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackControl();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onBackControl();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (photoPicker.onActivityResult(requestCode, resultCode, data, false)) return;
        switch (requestCode) {
            case LOCATION_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Country country = Parcels.unwrap(data.getParcelableExtra("country"));
                    if (country != null) {
                        registrationPresenter.setCountry(country);
                    }
                    Region region = Parcels.unwrap(data.getParcelableExtra("region"));
                    if (region != null) {
                        registrationPresenter.setRegion(region);
                    }
                    City city = Parcels.unwrap(data.getParcelableExtra("city"));
                    if (city != null) {
                        registrationPresenter.setCity(city);
                    }
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onBackControl() {
        if (isGeneralInfoCompleted) {
            ll_general_content.setVisibility(View.VISIBLE);
            ll_location_content.setVisibility(View.GONE);
            nextStepButtonEnable(true);
            isGeneralInfoCompleted = false;
        } else {
            registrationPresenter.onBackButtonClick();
        }
    }

    private void configurePrivacyPolicyTextView() {
        String dec = getString(R.string.label_registration_privacy_policy) + "<a href='" + getString(R.string.link_privacy_policy) + "'>" +
                getString(R.string.label_registration_privacy_policy_click) + "</a>";

        CharSequence sequence = Html.fromHtml(dec);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        UnderlineSpan[] underlines = strBuilder.getSpans(0, 10, UnderlineSpan.class);
        for (UnderlineSpan span : underlines) {
            int start = strBuilder.getSpanStart(span);
            int end = strBuilder.getSpanEnd(span);
            int flags = strBuilder.getSpanFlags(span);
            ClickableSpan browserLauncher = new ClickableSpan() {
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_privacy_policy)));
                    startActivity(intent);
                }
            };
            strBuilder.setSpan(browserLauncher, start, end, flags);
        }

        tv_privacy_policy.setText(strBuilder);
        tv_privacy_policy.setLinksClickable(true);
        tv_privacy_policy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnFocusChange({R.id.first_name_input, R.id.last_name_input, R.id.email_input})
    void onInputFieldFocusChange(EditText editText, boolean focused) {
        if (focused) {
            moveEditorCursorToEnd(editText);
        } else {
            // Input field lost it's focus. We'll notify the presenter about the new value entered by the user,
            // so that it can run the validation check on it.
            switch (editText.getId()) {
                case R.id.first_name_input:
                    registrationPresenter.setFirstName(editText.getText().toString());
                    break;
                case R.id.last_name_input:
                    registrationPresenter.setLastName(editText.getText().toString());
                    break;
                case R.id.email_input:
                    registrationPresenter.setEmail(editText.getText().toString());
                    break;
            }
        }
    }

    @OnEditorAction(R.id.email_input)
    boolean onEmailInputAction(int action) {
        if (action == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyboard(emailInput);
            clearFocusFromAllViews();
            return true;
        }
        return false;
    }

    @OnClick(R.id.profile_photo)
    void onPhotoClick() {
        registrationPresenter.onPhotoClick();
    }

    @OnClick(R.id.birthdate_input_overlay)
    void onBirthdateFieldClick() {
        registrationPresenter.onBirthdateFieldClick();
    }

    @OnClick(R.id.country_input_overlay)
    void onCountryFieldClick() {
        registrationPresenter.onCountryFieldClick();
    }

    @OnClick(R.id.region_input_overlay)
    void onRegionFieldClick() {
        registrationPresenter.onRegionFieldClick();
    }

    @OnClick(R.id.city_input_overlay)
    void onCityFieldClick() {
        registrationPresenter.onCityFieldClick();
    }

    @OnClick(R.id.img_checkbox)
    void onCheckBox(ImageView img_checkbox) {
        if (img_checkbox.getTag().toString().equals("default")) {
            img_checkbox.setTag("active");
            img_checkbox.setImageResource(R.drawable.check_active);
        } else if (img_checkbox.getTag().toString().equals("active")) {
            img_checkbox.setTag("default");
            img_checkbox.setImageResource(R.drawable.check_default);
        }
    }

    @OnClick(R.id.btn_nextstep)
    void onNextstepButtonClick() {
        clearFocusFromAllViews();
        hideSoftKeyboard(contentView);
        registrationPresenter.setFirstName(firstNameInput.getText().toString());
        registrationPresenter.setLastName(lastNameInput.getText().toString());
        registrationPresenter.setEmail(emailInput.getText().toString());
        registrationPresenter.checkGeneralInfo();
    }

    @OnClick(R.id.btn_save)
    void onSaveButtonClick() {
        if (img_checkbox.getTag().toString().equals("active")) {
            registrationPresenter.saveUser();
        } else {
            registrationPresenter.showPrivacyDialog();
        }
    }

    /**
     * Focuses on {@link #contentView} to clear focus from all other input fields.
     */
    private void clearFocusFromAllViews() {
        contentView.requestFocus();
    }

    private void smoothScrollTo(final View view, final int adjustOffset) {
        scrollView.smoothScrollTo(0, getRelativeTopInAncestor(scrollView, view) - adjustOffset);
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes, boolean scroll) {
        inputField.setError(getString(errorRes));
        if (scroll) {
            smoothScrollTo(inputField, dp2pixels(this, 20));
        }
    }

    private void showSelectedPhotoInternal(String url) {
        if (url != null) {
            Picasso.with(this).load(isEmpty(url) ? null : url)
                    .memoryPolicy(NO_CACHE)
                    .fit()
                    .centerCrop()
                    .transform(Transformations.CIRCLE_NO_BORDER)
                    .placeholder(R.drawable.ic_profile_photo_select)
                    .into(photoView);

        }
    }

    private void configureGenderRadioGroup() {
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                registrationPresenter.setGender(radioButtonIdToGender(checkedId));
            }
        });
    }

    private static int genderToRadioButtonId(Gender gender) {
        if (gender == Gender.FEMALE) {
            return R.id.female_radio_button;
        } else if (gender == Gender.MALE) {
            return R.id.male_radio_button;
        } else {
            return -1;
        }
    }

    private static Gender radioButtonIdToGender(int id) {
        switch (id) {
            case R.id.female_radio_button:
                return Gender.FEMALE;
            case R.id.male_radio_button:
                return Gender.MALE;
            default:
                return null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // DatePickerDialogFragment.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onDatePicked(Date date) {
        registrationPresenter.setBirthdate(date);
    }

    ///////////////////////////////////////////////////////////////////////////
    // RegistrationView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showProfileLoadingIndicator(boolean show) {
        loadingProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showBirthdatePickerView(Date currentBirthdate) {
        DatePickerDialogFragment.newInstance(currentBirthdate).show(getFragmentManager(), "BirthdatePickerDialog");
    }

    @Override
    public void showCountryPickerView() {
        Intent intent = LocationPickerActivity.newCountryPickerIntent(this);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void showRegionPickerView(long countryId) {
        Intent intent = LocationPickerActivity.newRegionPickerIntent(this, countryId);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void showCityPickerView(long countryId, long regionId) {
        Intent intent = LocationPickerActivity.newCityPickerIntent(this, countryId, regionId);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void showCityPickerViewWithoutRegion(long countryId) {
        Intent intent = LocationPickerActivity.newNoRegionCityPickerIntent(this, countryId);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void setSaveButtonTitle(@StringRes int titleRes) {
        btnSave.setText(titleRes);
    }

    @Override
    public void setDisplayName(String displayName) {
    }

    @Override
    public void setFirstName(String firstName) {
        setTextIfDiffers(firstNameInput, firstName);
    }

    @Override
    public void setLastName(String lastName) {
        setTextIfDiffers(lastNameInput, lastName);
    }

    @Override
    public void setEmail(String email) {
        setTextIfDiffers(emailInput, email);
    }

    @Override
    public void setBirthdate(String birthdate) {
        setTextIfDiffers(birthdateInput, birthdate);
    }

    @Override
    public void setGender(Gender gender) {
        genderRadioGroup.check(genderToRadioButtonId(gender));
    }

    @Override
    public void setCountryTitle(String country) {
        setTextIfDiffers(countryInput, country);
    }

    @Override
    public void setRegionTitle(String region) {
        setTextIfDiffers(regionInput, region);
    }

    @Override
    public void setCityTitle(String city) {
        setTextIfDiffers(cityInput, city);
    }

    @Override
    public void showProfileSavingIndicator(boolean show) {
        translucentProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showFirstNameError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(firstNameInput, errorRes, scrollToView);
    }

    @Override
    public void showLastNameError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(lastNameInput, errorRes, scrollToView);
    }

    @Override
    public void showEmailError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(emailInput, errorRes, scrollToView);
    }

    @Override
    public void showBirthdateError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(birthdateInput, errorRes, scrollToView);
    }

    @Override
    public void showGenderError(@StringRes int errorRes, boolean scrollToView) {
        if (scrollToView) {
            Toast.makeText(this, errorRes, Toast.LENGTH_SHORT).show();
            smoothScrollTo(genderRadioGroup, dp2pixels(this, 20));
        }
    }

    @Override
    public void showCountryError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(countryInput, errorRes, scrollToView);
    }

    @Override
    public void showRegionError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(regionInput, errorRes, scrollToView);
    }

    @Override
    public void showCityError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(cityInput, errorRes, scrollToView);
    }

    @Override
    public void showPhotoError(@StringRes int errorRes, boolean scrollToView) {
        if (scrollToView) {
            Toast.makeText(this, errorRes, Toast.LENGTH_SHORT).show();
            smoothScrollTo(photoView, dp2pixels(this, 20));
        }
    }

    @Override
    public void showSelectedPhoto(Uri photoFileUri) {
        showSelectedPhotoInternal(photoFileUri == null ? null : photoFileUri.toString());
    }

    @Override
    public void showSelectedPhoto(String photoHash, boolean isPhotoHash) {
        showSelectedPhotoInternal(isPhotoHash ? PhotoUrl.xl(photoHash) : photoHash);
    }

    @Override
    public void showPhotoUploadOverlay(boolean show) {
        // TODO: 24/01/2016 Rethink naming of: showPhotoUploadOverlay, showPhotoUploadingIndicator, photoDarkOverlay, photoProgressOverlay
        photoDarkOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPhotoUploadingIndicator(boolean show) {
        photoProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setPhotoUploadProgress(int progress) {
        photoUploadProgressView.setProgress(progress);
    }

    @Override
    public void showPhotoUploadError() {
        Snackbar snackbar = multilineSnackbar(scrollView, R.string.photo_upload_failed, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrationPresenter.onPhotoUploadRetryClick();
            }
        });
        snackbar.show();
    }

    @Override
    public void showTempPhotoStorageError() {
        Snackbar.make(scrollView, R.string.error_message_storage_create_temp_file, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCropActivityNotAvailable() {
        Snackbar.make(scrollView, R.string.error_message_crop_not_available, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(@StringRes int errorRes) {
        Toast.makeText(this, errorRes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openMainScreen() {
        startActivity(MainActivity.newIntent(this));
    }

    @Override
    public void nextStepButtonEnable(boolean isEnabled) {
        btnNextstep.setEnabled(isEnabled);
    }

    @Override
    public void generalInfoCompleted(boolean isGeneralInfoCompleted) {
        this.isGeneralInfoCompleted = isGeneralInfoCompleted;
        ll_general_content.setVisibility(View.GONE);
        ll_location_content.setVisibility(View.VISIBLE);
    }

    @Override
    public void downloadSocialNetworkAvatar(Target target, String photoLink) {
        photoView.setTag(target);
        Picasso.with(this).load(photoLink)
                .into(target);
    }

    @Override
    public void showPrivacyDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.label_attention));
        builder.setMessage(getString(R.string.error_message_privacy_policy));

        builder.setPositiveButton(getString(R.string.label_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton(getString(R.string.label_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
