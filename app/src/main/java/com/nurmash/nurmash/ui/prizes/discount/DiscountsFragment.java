package com.nurmash.nurmash.ui.prizes.discount;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.prizes.discount.CurrentDiscountsPresenter;
import com.nurmash.nurmash.mvp.prizes.discount.CurrentDiscountsView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.ListPlaceholderViewHolder;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.ui.prizes.PromoPrizeListAdapter;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DiscountsFragment extends Fragment implements CurrentDiscountsView {
    CurrentDiscountsPresenter currentDiscountsPresenter;
    PromoPrizeListAdapter promoPrizeListAdapter;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list_placeholder_stub) ViewStub listPlaceholderStub;
    EndlessScrollListener endlessScrollListener;
    ListPlaceholderViewHolder listPlaceholderViewHolder;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    public static DiscountsFragment newInstance(boolean isCurrent) {
        DiscountsFragment fragment = new DiscountsFragment();
        Bundle args = new Bundle();
        args.putBoolean("isCurrent", isCurrent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataLayer dataLayer = DataLayer.getInstance(getContext());
        currentDiscountsPresenter = new CurrentDiscountsPresenter(dataLayer);
        promoPrizeListAdapter = new PromoPrizeListAdapter();
        promoPrizeListAdapter.setCallback(new PromoPrizeListAdapter.Callback() {
            @Override
            public void onPrizeItemClick(PrizeItem prizeItem, int competitorIndex) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_discounts, container, false);
        ButterKnife.bind(this, rootView);
        configureRecyclerView();
        // Presenter
        currentDiscountsPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), recyclerView)));
        currentDiscountsPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Picasso.with(getContext()).cancelTag(promoPrizeListAdapter.picassoTag);
        currentDiscountsPresenter.setErrorHandler(null);
        currentDiscountsPresenter.detachView();
        recyclerView.clearOnScrollListeners();
        recyclerView.setAdapter(null);
        endlessScrollListener = null;
        listPlaceholderViewHolder = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void configureRecyclerView() {
        recyclerView.setAdapter(promoPrizeListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                currentDiscountsPresenter.onRequestMore();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentDiscountsPresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    private ListPlaceholderViewHolder getListPlaceholderViewHolder() {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListPlaceholderViewHolder(listPlaceholderStub.inflate());
            listPlaceholderViewHolder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    currentDiscountsPresenter.onRefreshAction();
                }
            });
            listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
        }
        return listPlaceholderViewHolder;
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoListView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showPhotosLoadingIndicator(boolean show) {
        promoPrizeListAdapter.setLoading(show);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
            if (listPlaceholderViewHolder != null) {
                listPlaceholderViewHolder.setRefreshing(false);
            }
        }
    }

    @Override
    public void setPrizeItems(List<PrizeItem> prizeItems) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        promoPrizeListAdapter.setPrizeItems(prizeItems);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addPrizeItems(List<PrizeItem> prizeItems) {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        promoPrizeListAdapter.addPrizeItems(prizeItems);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void showPhotoListPlaceholder(@StringRes int textRes, @StringRes int btnRes) {
        ListPlaceholderViewHolder listPlaceholderViewHolder = getListPlaceholderViewHolder();
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setButtonText(btnRes);
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void openPhotoDetailActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(getContext(), competitorId));
    }

    @Override
    public void finishActivity() {
        getActivity().finish();
    }
}
