package com.nurmash.nurmash.ui.prizes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.util.recyclerview.SimpleViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PromoPrizeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int PROMO_PRIZE_ITEM_LAYOUT_ID = R.layout.lv_promo_prize;
    private static final int LOADING_INDICATOR_LAYOUT_ID = R.layout.photo_list_loading_item;

    // WARNING: Don't forget to call Picasso.cancelTag
    public final Object picassoTag;

    private boolean loading;
    private List<PrizeItem> prizeItems = new ArrayList<>();
    private Callback callback;

    public PromoPrizeListAdapter() {
        picassoTag = this;
    }

    public void setLoading(boolean loading) {
        if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(prizeItems.size());
        } else if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(prizeItems.size());
        }
    }

    public void setPrizeItems(List<PrizeItem> newPrizeItem) {
        if (newPrizeItem == null || newPrizeItem.isEmpty()) {
            prizeItems = new ArrayList<>();
        } else {
            prizeItems = new ArrayList<>(newPrizeItem);
        }
        notifyDataSetChanged();
    }

    public void addPrizeItems(List<PrizeItem> newPrizes) {
        if (newPrizes == null || newPrizes.isEmpty()) {
            return;
        }
        int positionStart = prizeItems.size();
        int itemCount = newPrizes.size();
        prizeItems.addAll(newPrizes);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private int getLoadingIndicatorPosition() {
        return prizeItems.size();
    }

    private int getCompetitorIndex(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 10;//prizeItems.size() + (loading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getLoadingIndicatorPosition()) {
            return LOADING_INDICATOR_LAYOUT_ID;
        } else {
            return PROMO_PRIZE_ITEM_LAYOUT_ID;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case PROMO_PRIZE_ITEM_LAYOUT_ID:
                return new ViewHolder(view);
            case LOADING_INDICATOR_LAYOUT_ID:
                return new SimpleViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        if (holder instanceof ViewHolder) {
//            ((ViewHolder) holder).bindPrize(prizeItems.get(getCompetitorIndex(position)), position);
//        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        Context context;
        PrizeItem bindedPrize;
        int mPosition;

        ViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindPrize(PrizeItem prizeItem, int position) {
            bindedPrize = prizeItem;
            mPosition = position;

        }

        @OnClick(R.id.item_view)
        void onItemClick() {
            if (callback != null) callback.onPrizeItemClick(bindedPrize, mPosition);
        }
    }

    public interface Callback {
        void onPrizeItemClick(PrizeItem prizeItem, int competitorIndex);
    }
}
