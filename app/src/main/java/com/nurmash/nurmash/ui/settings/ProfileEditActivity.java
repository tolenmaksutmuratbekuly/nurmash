package com.nurmash.nurmash.ui.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.social.facebook.FacebookProvider;
import com.nurmash.nurmash.data.social.google.GoogleAuthProvider;
import com.nurmash.nurmash.data.social.vk.VKProvider;
import com.nurmash.nurmash.model.Gender;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.settings.ProfileEditPresenter;
import com.nurmash.nurmash.mvp.settings.ProfileEditView;
import com.nurmash.nurmash.ui.AndroidPhotoPicker;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DatePickerDialogFragment;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.login.LoginActivity;
import com.nurmash.nurmash.ui.main.MainActivity;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.ContextUtils.dp2pixels;
import static com.nurmash.nurmash.util.ContextUtils.hideSoftKeyboard;
import static com.nurmash.nurmash.util.SnackbarUtils.multilineSnackbar;
import static com.nurmash.nurmash.util.ViewUtils.getRelativeTopInAncestor;
import static com.nurmash.nurmash.util.ViewUtils.moveEditorCursorToEnd;
import static com.nurmash.nurmash.util.ViewUtils.setTextIfDiffers;
import static com.squareup.picasso.MemoryPolicy.NO_CACHE;

public class ProfileEditActivity extends BaseActivity implements ProfileEditView, DatePickerDialogFragment.Callback {
    private static final int LOCATION_PICKER_REQUEST_CODE = 0;
    private static final int PHOTO_PICKER_BASE_REQUEST_CODE = 1;

    ProfileEditPresenter profileEditPresenter;
    AndroidPhotoPicker photoPicker;
    FacebookProvider facebookProvider;
    VKProvider vkProvider;
    GoogleAuthProvider googleAuthProvider;
    @Bind(R.id.scroll_view) ScrollView scrollView;
    @Bind(R.id.content_view) ViewGroup contentView;
    @Bind(R.id.loading_progress_overlay) ViewGroup loadingProgressOverlay;
    @Bind(R.id.translucent_progress_overlay) ViewGroup translucentProgressOverlay;
    // Photo
    @Bind(R.id.profile_photo) ImageView photoView;
    @Bind(R.id.profile_photo_dark_overlay) View photoDarkOverlay;
    @Bind(R.id.profile_photo_progress_overlay) ViewGroup photoProgressOverlay;
    @Bind(R.id.profile_photo_upload_progress) CircularProgressView photoUploadProgressView;
    // Input fields
    @Bind(R.id.display_name_input) MaterialEditText displayNameInput;
    @Bind(R.id.first_name_input) MaterialEditText firstNameInput;
    @Bind(R.id.last_name_input) MaterialEditText lastNameInput;
    @Bind(R.id.native_first_name_input) MaterialEditText nativeFirstNameInput;
    @Bind(R.id.native_last_name_input) MaterialEditText nativeLastNameInput;
    @Bind(R.id.email_input) MaterialEditText emailInput;
    @Bind(R.id.birthdate_input) MaterialEditText birthdateInput;
    @Bind(R.id.gender_radio_group) RadioGroup genderRadioGroup;
    @Bind(R.id.country_input) MaterialEditText countryInput;
    @Bind(R.id.region_input) MaterialEditText regionInput;
    @Bind(R.id.city_input) MaterialEditText cityInput;
    // Social buttons
    @Bind(R.id.button_link_facebook) ImageView linkFacebookButton;
    @Bind(R.id.button_link_google) ImageView linkGoogleButton;
    @Bind(R.id.button_link_vk) ImageView linkVKButton;
    // Save button
    @Bind(R.id.button_save) Button saveButton;

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, ProfileEditActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.bind(this);
        configureGenderRadioGroup();
        // Social providers
        facebookProvider = new FacebookProvider(this);
        vkProvider = new VKProvider(this);
        googleAuthProvider = new GoogleAuthProvider(this);
        // PhotoPicker
        photoPicker = new AndroidPhotoPicker(this, PHOTO_PICKER_BASE_REQUEST_CODE);
        photoPicker.onRestoreInstanceState(savedInstanceState);
        // Presenter
        profileEditPresenter = new ProfileEditPresenter(DataLayer.getInstance(this), facebookProvider,
                googleAuthProvider, vkProvider);
        profileEditPresenter.onRestoreInstanceState(savedInstanceState);
        profileEditPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, scrollView)));
        profileEditPresenter.setPhotoPicker(photoPicker);
        profileEditPresenter.attachView(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        photoPicker.onSaveInstanceState(outState);
        profileEditPresenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        profileEditPresenter.detachView();
        profileEditPresenter.setErrorHandler(null);
        profileEditPresenter.setPhotoPicker(null);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (profileEditPresenter.isLogoutButtonEnabled()) {
            getMenuInflater().inflate(R.menu.menu_profile_edit, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                profileEditPresenter.onBackButtonClick();
                return true;
            case R.id.logout:
                profileEditPresenter.onLogoutButtonClick();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (photoPicker.onActivityResult(requestCode, resultCode, data, false)) return;
        if (facebookProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (googleAuthProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (vkProvider.onActivityResult(requestCode, resultCode, data)) return;

        switch (requestCode) {
            case LOCATION_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Country country = Parcels.unwrap(data.getParcelableExtra("country"));
                    if (country != null) {
                        profileEditPresenter.setCountry(country);
                    }
                    Region region = Parcels.unwrap(data.getParcelableExtra("region"));
                    if (region != null) {
                        profileEditPresenter.setRegion(region);
                    }
                    City city = Parcels.unwrap(data.getParcelableExtra("city"));
                    if (city != null) {
                        profileEditPresenter.setCity(city);
                    }
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnFocusChange({R.id.first_name_input, R.id.last_name_input, R.id.native_first_name_input,
            R.id.native_last_name_input, R.id.display_name_input, R.id.email_input})
    void onInputFieldFocusChange(EditText editText, boolean focused) {
        if (focused) {
            moveEditorCursorToEnd(editText);
            if (editText.getId() == R.id.first_name_input || editText.getId() == R.id.last_name_input) {
                profileEditPresenter.checkVerifyStatus();
            }
        } else {
            // Input field lost it's focus. We'll notify the presenter about the new value entered by the user,
            // so that it can run the validation check on it.
            switch (editText.getId()) {
                case R.id.first_name_input:
                    profileEditPresenter.setFirstName(editText.getText().toString());
                    break;
                case R.id.last_name_input:
                    profileEditPresenter.setLastName(editText.getText().toString());
                    break;
                case R.id.native_first_name_input:
                    profileEditPresenter.setNativeFirstName(editText.getText().toString());
                    break;
                case R.id.native_last_name_input:
                    profileEditPresenter.setNativeLastName(editText.getText().toString());
                    break;
                case R.id.display_name_input:
                    profileEditPresenter.setDisplayName(editText.getText().toString());
                    break;
                case R.id.email_input:
                    profileEditPresenter.setEmail(editText.getText().toString());
                    break;
            }
        }
    }

    @OnEditorAction(R.id.email_input)
    boolean onEmailInputAction(int action) {
        if (action == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyboard(emailInput);
            clearFocusFromAllViews();
            return true;
        }
        return false;
    }

    @OnClick(R.id.profile_photo)
    void onPhotoClick() {
        profileEditPresenter.onPhotoClick();
    }

    @OnClick(R.id.birthdate_input_overlay)
    void onBirthdateFieldClick() {
        profileEditPresenter.onBirthdateFieldClick();
    }

    @OnClick(R.id.country_input_overlay)
    void onCountryFieldClick() {
        profileEditPresenter.onCountryFieldClick();
    }

    @OnClick(R.id.region_input_overlay)
    void onRegionFieldClick() {
        profileEditPresenter.onRegionFieldClick();
    }

    @OnClick(R.id.city_input_overlay)
    void onCityFieldClick() {
        profileEditPresenter.onCityFieldClick();
    }

    @OnClick(R.id.button_link_facebook)
    void onFacebookLinkClick() {
        profileEditPresenter.onFacebookLinkClick();
    }

    @OnClick(R.id.button_link_google)
    void onGoogleLinkClick() {
        profileEditPresenter.onGoogleLinkClick();
    }

    @OnClick(R.id.button_link_vk)
    void onVKLinkClick() {
        profileEditPresenter.onVKLinkClick();
    }

    @OnClick(R.id.button_save)
    void onSaveButtonClick() {
        clearFocusFromAllViews();
        hideSoftKeyboard(contentView);
        profileEditPresenter.setDisplayName(displayNameInput.getText().toString());
        profileEditPresenter.setFirstName(firstNameInput.getText().toString());
        profileEditPresenter.setLastName(lastNameInput.getText().toString());
        profileEditPresenter.setNativeFirstName(nativeFirstNameInput.getText().toString());
        profileEditPresenter.setNativeLastName(nativeLastNameInput.getText().toString());
        profileEditPresenter.setEmail(emailInput.getText().toString());
        profileEditPresenter.saveProfile();
    }

    /**
     * Focuses on {@link #contentView} to clear focus from all other input fields.
     */
    private void clearFocusFromAllViews() {
        contentView.requestFocus();
    }

    private void smoothScrollTo(final View view, final int adjustOffset) {
        scrollView.smoothScrollTo(0, getRelativeTopInAncestor(scrollView, view) - adjustOffset);
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes, boolean scroll) {
        inputField.setError(getString(errorRes));
        if (scroll) {
            smoothScrollTo(inputField, dp2pixels(this, 20));
        }
    }

    private void showSelectedPhotoInternal(String url) {
        Picasso.with(this).load(isEmpty(url) ? null : url)
                .memoryPolicy(NO_CACHE)
                .fit()
                .centerCrop()
                .transform(Transformations.CIRCLE_NO_BORDER)
                .placeholder(R.drawable.ic_profile_photo_select)
                .into(photoView);
    }

    private void configureGenderRadioGroup() {
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                profileEditPresenter.setGender(radioButtonIdToGender(checkedId));
            }
        });
    }

    private static int genderToRadioButtonId(Gender gender) {
        if (gender == Gender.FEMALE) {
            return R.id.female_radio_button;
        } else if (gender == Gender.MALE) {
            return R.id.male_radio_button;
        } else {
            return -1;
        }
    }

    private static Gender radioButtonIdToGender(int id) {
        switch (id) {
            case R.id.female_radio_button:
                return Gender.FEMALE;
            case R.id.male_radio_button:
                return Gender.MALE;
            default:
                return null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // DatePickerDialogFragment.Callback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onDatePicked(Date date) {
        profileEditPresenter.setBirthdate(date);
    }

    ///////////////////////////////////////////////////////////////////////////
    // ProfileEditView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showProfileLoadingIndicator(boolean show) {
        loadingProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showBirthdatePickerView(Date currentBirthdate) {
        DatePickerDialogFragment.newInstance(currentBirthdate).show(getFragmentManager(), "BirthdatePickerDialog");
    }

    @Override
    public void showCountryPickerView() {
        Intent intent = LocationPickerActivity.newCountryPickerIntent(this);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void showRegionPickerView(long countryId) {
        Intent intent = LocationPickerActivity.newRegionPickerIntent(this, countryId);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void showCityPickerView(long countryId, long regionId) {
        Intent intent = LocationPickerActivity.newCityPickerIntent(this, countryId, regionId);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void showCityPickerViewWithoutRegion(long countryId) {
        Intent intent = LocationPickerActivity.newNoRegionCityPickerIntent(this, countryId);
        startActivityForResult(intent, LOCATION_PICKER_REQUEST_CODE);
    }

    @Override
    public void setSaveButtonTitle(@StringRes int titleRes) {
        saveButton.setText(titleRes);
    }

    @Override
    public void setDisplayName(String displayName) {
        setTextIfDiffers(displayNameInput, displayName);
    }

    @Override
    public void setFirstName(String firstName) {
        setTextIfDiffers(firstNameInput, firstName);
    }

    @Override
    public void setLastName(String lastName) {
        setTextIfDiffers(lastNameInput, lastName);
    }

    @Override
    public void setNativeFirstName(String nativeFirstName) {
        setTextIfDiffers(nativeFirstNameInput, nativeFirstName);
    }

    @Override
    public void setNativeLastName(String nativeLastName) {
        setTextIfDiffers(nativeLastNameInput, nativeLastName);
    }

    @Override
    public void setEmail(String email) {
        setTextIfDiffers(emailInput, email);
    }

    @Override
    public void setBirthdate(String birthdate) {
        setTextIfDiffers(birthdateInput, birthdate);
    }

    @Override
    public void setGender(Gender gender) {
        genderRadioGroup.check(genderToRadioButtonId(gender));
    }

    @Override
    public void setCountryTitle(String country) {
        setTextIfDiffers(countryInput, country);
    }

    @Override
    public void setRegionTitle(String region) {
        setTextIfDiffers(regionInput, region);
    }

    @Override
    public void setCityTitle(String city) {
        setTextIfDiffers(cityInput, city);
    }

    @Override
    public void showProfileSavingIndicator(boolean show) {
        translucentProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateProfileData() {
        MainActivity.isProfilePhotoUpdated = true;
    }

    @Override
    public void showDisplayNameError(int errorRes, boolean scrollToView) {
        showInputFieldError(displayNameInput, errorRes, scrollToView);
    }

    @Override
    public void showFirstNameError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(firstNameInput, errorRes, scrollToView);
    }

    @Override
    public void showLastNameError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(lastNameInput, errorRes, scrollToView);
    }

    @Override
    public void showNativeFirstNameError(int errorRes, boolean scrollToView) {
        showInputFieldError(nativeFirstNameInput, errorRes, scrollToView);
    }

    @Override
    public void showNativeLastNameError(int errorRes, boolean scrollToView) {
        showInputFieldError(nativeLastNameInput, errorRes, scrollToView);
    }

    @Override
    public void showEmailError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(emailInput, errorRes, scrollToView);
    }

    @Override
    public void showBirthdateError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(birthdateInput, errorRes, scrollToView);
    }

    @Override
    public void showGenderError(@StringRes int errorRes, boolean scrollToView) {
        if (scrollToView) {
            Toast.makeText(this, errorRes, Toast.LENGTH_SHORT).show();
            smoothScrollTo(genderRadioGroup, dp2pixels(this, 20));
        }
    }

    @Override
    public void showCountryError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(countryInput, errorRes, scrollToView);
    }

    @Override
    public void showRegionError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(regionInput, errorRes, scrollToView);
    }

    @Override
    public void showCityError(@StringRes int errorRes, boolean scrollToView) {
        showInputFieldError(cityInput, errorRes, scrollToView);
    }

    @Override
    public void showPhotoError(@StringRes int errorRes, boolean scrollToView) {
        if (scrollToView) {
            Toast.makeText(this, errorRes, Toast.LENGTH_SHORT).show();
            smoothScrollTo(photoView, dp2pixels(this, 20));
        }
    }

    @Override
    public void showSelectedPhoto(Uri photoFileUri) {
        showSelectedPhotoInternal(photoFileUri == null ? null : photoFileUri.toString());
    }

    @Override
    public void showSelectedPhoto(String photoHash) {
        showSelectedPhotoInternal(PhotoUrl.xl(photoHash));
    }

    @Override
    public void showPhotoUploadOverlay(boolean show) {
        // TODO: 24/01/2016 Rethink naming of: showPhotoUploadOverlay, showPhotoUploadingIndicator, photoDarkOverlay, photoProgressOverlay
        photoDarkOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPhotoUploadingIndicator(boolean show) {
        photoProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setPhotoUploadProgress(int progress) {
        photoUploadProgressView.setProgress(progress);
    }

    @Override
    public void showPhotoUploadError() {
        Snackbar snackbar = multilineSnackbar(scrollView, R.string.photo_upload_failed, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileEditPresenter.onPhotoUploadRetryClick();
            }
        });
        snackbar.show();
    }

    @Override
    public void showTempPhotoStorageError() {
        Snackbar.make(scrollView, R.string.error_message_storage_create_temp_file, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCropActivityNotAvailable() {
        Snackbar.make(scrollView, R.string.error_message_crop_not_available, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openMainScreen() {
        startActivity(MainActivity.newIntent(this));
    }

    @Override
    public void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.label_settings_wantto_exit_title));
        builder.setMessage(getString(R.string.label_settings_wantto_exit));
        builder.setPositiveButton(getString(R.string.label_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(LoginActivity.newLogoutIntent(getApplicationContext()));
            }
        });

        builder.setNegativeButton(getString(R.string.label_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void setFacebookLinkActive(boolean active) {
        linkFacebookButton.setImageResource(active ? R.drawable.ic_button_link_facebook_active : R.drawable.ic_button_link_facebook_inactive);
    }

    @Override
    public void setGoogleLinkActive(boolean active) {
        linkGoogleButton.setImageResource(active ? R.drawable.ic_button_link_google_active : R.drawable.ic_button_link_google_inactive);
    }

    @Override
    public void setVKLinkActive(boolean active) {
        linkVKButton.setImageResource(active ? R.drawable.ic_button_link_vk_active : R.drawable.ic_button_link_vk_inactive);
    }

    @Override
    public void showLinkProgressIndicator(boolean show) {
        translucentProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onFacebookSdkError() {
        Toast.makeText(this, R.string.error_message_login_facebook, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmailPermissionNotGranted() {
        Toast.makeText(this, R.string.error_message_login_email_permission, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGoogleApiConnectingError() {
        Toast.makeText(this, R.string.error_message_login_google_connecting, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGoogleApiNotConnectedError() {
        Toast.makeText(this, R.string.error_message_login_google_not_connected, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGoogleSdkError() {
        Toast.makeText(this, R.string.error_message_login_google_sdk_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVKSdkError() {
        Toast.makeText(this, R.string.error_message_login_vk_sdk_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAccountAlreadyLinkedError() {
        Toast.makeText(this, R.string.error_message_cannot_link_existing_user, Toast.LENGTH_SHORT).show();
    }

    public void needToVerifyAgainMessage(final Date birthdate) {
        new AlertDialog.Builder(ProfileEditActivity.this)
                .setTitle(getString(R.string.hint_verification_changes_info))
                .setCancelable(false)
                .setMessage(getString(R.string.hint_verification_changes))
                .setPositiveButton(getString(R.string.label_okay),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (birthdate != null) {
                                    DatePickerDialogFragment.newInstance(birthdate).show(getFragmentManager(), "BirthdatePickerDialog");
                                }
                            }
                        }).show();

    }
}
