package com.nurmash.nurmash.ui.partnerprogram;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.partnerprogram.QRScanerPresenter;
import com.nurmash.nurmash.mvp.partnerprogram.QRScanerView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.livotov.labs.android.camview.ScannerLiveView;
import eu.livotov.labs.android.camview.scanner.decoder.zxing.ZXDecoder;

public class QRScanerActivity extends BaseActivity implements QRScanerView {
    QRScanerPresenter presenter;
    private ScannerLiveView camera;
    private User user;
    @Bind(R.id.fl_parent) FrameLayout fl_parent;
    @Bind(R.id.loading_indicator) ViewGroup loading_indicator;

    public static Intent newIntent(Context packageContext, User user) {
        Intent intent = new Intent(packageContext, QRScanerActivity.class);
        intent.putExtra("user", Parcels.wrap(user));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscaner);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("user")) {
            user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
        }
        // Presenter
        presenter = new QRScanerPresenter(DataLayer.getInstance(this), user);
        presenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, fl_parent)));
        presenter.attachView(this);

        configureQRScanerView();
    }

    private void configureQRScanerView() {
        ZXDecoder decoder = new ZXDecoder();
        decoder.setScanAreaPercent(0.5);
        camera = (ScannerLiveView) findViewById(R.id.camview);
        camera.setDecoder(decoder);
        camera.startScanner();
        camera.setHudImageResource(R.drawable.qr_square);
        camera.setScannerViewEventListener(new ScannerLiveView.ScannerViewEventListener() {
            @Override
            public void onScannerStarted(ScannerLiveView scanner) {

            }

            @Override
            public void onScannerStopped(ScannerLiveView scanner) {

            }

            @Override
            public void onScannerError(Throwable err) {
                showQRCodeError(getString(R.string.label_invalid_qrcode));
            }

            @Override
            public void onCodeScanned(String scannedUserId) {
                presenter.inviteUser(scannedUserId);
            }
        });
    }

    @Override
    protected void onDestroy() {
        presenter.setErrorHandler(null);
        presenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // QRScanerView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        loading_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSuccessUserInvited() {
        AlertDialog.Builder builder = new AlertDialog.Builder(QRScanerActivity.this);
        builder.setTitle(getString(R.string.label_qr_scansuccess));
        builder.setMessage(getString(R.string.label_qr_scansuccess_description))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.label_okay), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(PartnerProgramActivity.newIntent(QRScanerActivity.this, user));
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void canNotInviteYourself() {
        showQRCodeError(getString(R.string.label_cant_invite_yourself));
    }

    @Override
    public void invalidQRCodeError() {
        showQRCodeError(getString(R.string.label_invalid_qrcode));
    }

    @Override
    public void invitedAlreadyError() {
        showQRCodeError(getString(R.string.label_have_already_invited));
    }

    private void showQRCodeError(String errorLabel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(QRScanerActivity.this);
        builder.setMessage(errorLabel)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.label_okay), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}