package com.nurmash.nurmash.ui.prizes.jackpot;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.JackpotList;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class JackpotWinnerViewHolder {
    private Callback callback;
    private Context context;
    @Bind(R.id.tv_congrats_year) TextView tv_congrats_year;
    @Bind(R.id.tv_jackpot_sum) TextView tv_jackpot_sum;
    @Bind(R.id.img_winner_photo) ImageView img_winner_photo;
    @Bind(R.id.tv_winner_name) TextView tv_winner_name;
    private final NumberFormat moneyAmountFormat;

    public JackpotWinnerViewHolder(View view, Context context) {
        ButterKnife.bind(this, view);
        this.context = context;
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onCardJackpotClick();
                }
            }
        });
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setWinners(List<JackpotList.Winners> winners) {
        if (winners.size() > 0) {
            JackpotList.Winners winnerItem = winners.get(0);
            if (winnerItem != null) {
                tv_congrats_year.setText(context.getString(R.string.label_jackpot_congratulations, winnerItem.atyear));
                tv_jackpot_sum.setText(winnerItem.balance.toString(moneyAmountFormat) + " " + winnerItem.getCurrencyCode());
                tv_winner_name.setText(winnerItem.user.display_name);
                Picasso.with(context)
                        .load(PhotoUrl.l(winnerItem.user.photo))
                        .fit()
                        .transform(Transformations.CIRCLE_GRAY_BORDER_2DP)
                        .into(img_winner_photo);
            }
        }
    }

    public interface Callback {
        void onCardJackpotClick();
    }
}