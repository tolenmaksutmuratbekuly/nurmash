package com.nurmash.nurmash.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.util.FormatUtils;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HotPromoPageFragment extends Fragment {
    @Bind(R.id.photo) ImageView photo;
    @Bind(R.id.title) TextView title;
    @Bind(R.id.dates) TextView dates;
    @Bind(R.id.participants_number) TextView participants_number;
    @Bind(R.id.tv_prize) TextView tv_prize;
    Promotion promo;
    private NumberFormat moneyAmountFormat;
    private Callback callback;

    public static HotPromoPageFragment newInstance(Promotion promo) {
        HotPromoPageFragment fragment = new HotPromoPageFragment();
        Bundle args = new Bundle();
        args.putParcelable("promo", Parcels.wrap(promo));
        fragment.setArguments(args);
        return fragment;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        promo = Parcels.unwrap(getArguments().getParcelable("promo"));
        this.moneyAmountFormat = FormatUtils.getMoneyAmountDisplayFormat();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hotpromo_slider, container, false);
        ButterKnife.bind(this, rootView);
        bindPromotion();
        return rootView;
    }

    public void bindPromotion() {
        if (promo == null) return;
        Picasso.with(getActivity())
                .load(PhotoUrl.grxl(promo.photo))
                .fit()
                .centerCrop()
                .into(photo);
        title.setText(promo.title);
        if (promo.deadline_type != null) {
            if (promo.deadline_type.equals(Promotion.DEADLINE_TYPE_PARTICIPANT)) {
                participants_number.setText(getActivity().getString(R.string.participants_number_format, promo.participants_amount + " / " + promo.participants_boundary));
                if (promo.start_date != null) {
                    dates.setText(FormatUtils.formatDisplayDate(promo.start_date));
                } else {
                    dates.setText(null);
                }
            } else if (promo.deadline_type.equals(Promotion.DEADLINE_TYPE_DATE)) {
                participants_number.setText(getActivity().getString(R.string.participants_number_format, promo.participants_amount));
                if (promo.start_date != null && promo.stop_date != null) {
                    dates.setText(String.format(Locale.US, "%s - %s", FormatUtils.formatDisplayDate(promo.start_date), FormatUtils.formatDisplayDate(promo.stop_date)));
                } else {
                    dates.setText(null);
                }
            } else {
                participants_number.setText(null);
            }
        }

        if (promo.prizes != null) {
            String totalPrizeSum = promo.prizes.getTotalPrize().toString(moneyAmountFormat);
            if (promo.getCurrencyCode() != null) {
                totalPrizeSum += " " + promo.getCurrencyCode();
            }
            tv_prize.setText(totalPrizeSum);
        }
    }

    @OnClick(R.id.content_view)
    void onCardClick() {
        if (callback != null && promo != null) {
            callback.onPromotionClick(promo);
        }
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    public interface Callback {
        void onPromotionClick(Promotion promotion);
    }
}
