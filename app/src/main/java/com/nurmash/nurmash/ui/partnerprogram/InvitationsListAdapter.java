package com.nurmash.nurmash.ui.partnerprogram;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.InviteList;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.nurmash.nurmash.util.recyclerview.SimpleViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InvitationsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int FEED_ITEM_LAYOUT_ID = R.layout.lv_partner_invitations_list_item;
    private static final int LOADING_ITEM_LAYOUT_ID = R.layout.profile_feed_list_loading_item;

    private boolean loading;
    private List<InviteList> items = new ArrayList<>();
    private boolean isStatusVisible;
    private long myUserId;

    public InvitationsListAdapter(boolean isStatusVisible) {
        this.isStatusVisible = isStatusVisible;
    }

    public void setLoading(boolean loading) {
        if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(items.size());
        } else if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(items.size());
        }
    }

    public void clearAllItems() {
        items.clear();
    }

    public void setItems(List<InviteList> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            items = new ArrayList<>();
        } else {
            items = new ArrayList<>(newItems);
        }
        notifyDataSetChanged();
    }

    public void addItems(List<InviteList> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            return;
        }
        int positionStart = items.size();
        int itemCount = newItems.size();
        items.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setMyUserId(long myUserId) {
        this.myUserId = myUserId;
    }

    private int getLoadingIndicatorPosition() {
        return items.size();
    }

    private int getItemIndex(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size() + (loading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getLoadingIndicatorPosition()) {
            return LOADING_ITEM_LAYOUT_ID;
        } else {
            return FEED_ITEM_LAYOUT_ID;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case FEED_ITEM_LAYOUT_ID:
                return new FeedItemViewHolder(view);
            case LOADING_ITEM_LAYOUT_ID:
                return new SimpleViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FeedItemViewHolder) {
            ((FeedItemViewHolder) holder).bindFeedItem(items.get(getItemIndex(position)));
        }
    }

    class FeedItemViewHolder extends RecyclerView.ViewHolder {
        Context context;
        InviteList bindedInvitation;
        @Bind(R.id.img_status) ImageView img_status;
        @Bind(R.id.tv_username) TextView tv_username;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_drawal_money) TextView tv_drawal_money;
        @Bind(R.id.img_user_photo) ImageView img_user_photo;

        public FeedItemViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindFeedItem(InviteList item) {
            bindedInvitation = item;
            if (item == null) {
                return;
            }

            if (isStatusVisible) {
                img_status.setVisibility(View.VISIBLE);
                img_status.setImageResource(item.is_fulfilled == 1 ? R.drawable.ic_partner_check_active : R.drawable.ic_partner_check);
            } else {
                img_status.setVisibility(View.GONE);
            }

            if (item.referral_user != null) {
                if (item.referral_user.display_name != null) {
                    if (myUserId == item.referral_user.id) {
                        tv_username.setText(context.getString(R.string.label_personal_bonus));
                    } else {
                        tv_username.setText(item.referral_user.display_name);
                    }
                }
                if (item.referral_user.photo != null && !item.referral_user.photo.isEmpty()) {
                    Picasso.with(context).load(PhotoUrl.l(item.referral_user.photo))
                            .fit()
                            .transform(Transformations.CIRCLE_NO_BORDER)
                            .into(img_user_photo);
                }
            }
            if (item.invitation_datetime != null) {
                tv_date.setText(FormatUtils.formatDisplayDate(item.invitation_datetime));
            }
            if (item.bonus != null) {
                tv_drawal_money.setText(item.bonus.toString(FormatUtils.getMoneyAmountDisplayFormat()) + " " + item.getCurrencyCode());
            }
        }
    }
}
