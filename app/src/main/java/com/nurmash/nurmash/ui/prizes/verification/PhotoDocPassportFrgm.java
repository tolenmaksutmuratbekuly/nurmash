package com.nurmash.nurmash.ui.prizes.verification;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocPassportPresenter;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocPassportView;
import com.nurmash.nurmash.mvp.prizes.verification.PhotoDocsFrgmCallbacks;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.CameraUtils;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoDocPassportFrgm extends Fragment implements PhotoDocPassportView {
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CROP_REQUEST_CODE = 2;
    private PhotoDocPassportPresenter presenter;
    private PhotoDocsFrgmCallbacks callbacks;
    @Bind(R.id.ll_passport_content) LinearLayout ll_passport_content;
    @Bind(R.id.img_photo_passport) ImageView img_photo_passport;
    @Bind(R.id.check_passport) ImageView check_passport;

    public static PhotoDocPassportFrgm newInstance() {
        return new PhotoDocPassportFrgm();
    }

    public void setCallbacks(PhotoDocsFrgmCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataLayer dataLayer = DataLayer.getInstance(getContext());
        presenter = new PhotoDocPassportPresenter(dataLayer);
        presenter.restoreInstanceState(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.saveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_document_passport, viewGroup, false);
        ButterKnife.bind(this, rootView);

        // Presenter
        presenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), ll_passport_content)));
        presenter.attachView(this);
        return rootView;
    }

    @OnClick(R.id.img_passport_gallery)
    void onPassportGalleryClick() {
        presenter.onGalleryOptionSelected();
    }

    @OnClick(R.id.img_passport_camera)
    void onPassportCameraClick() {
        presenter.onCameraOptionSelected();
    }

    @OnClick(R.id.btn_send_passport)
    void onSendDocumentClick() {
        presenter.sendDocumentClick();
    }

    public void retryPhotoUpload() {
        presenter.retryPhotoUpload();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                presenter.onCameraResult(resultCode == getActivity().RESULT_OK);
                break;
            case GALLERY_REQUEST_CODE:
                presenter.onGalleryResult(resultCode == getActivity().RESULT_OK ? data.getData() : null);
                break;
            case CROP_REQUEST_CODE:
                presenter.onCropResult(resultCode == getActivity().RESULT_OK);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoDocPassportView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean openCameraApp(Uri tempPhotoUri) {
        Intent intent = CameraUtils.newCameraIntent(tempPhotoUri);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void openGalleryApp() {
        startActivityForResult(CameraUtils.newGalleryIntent(), GALLERY_REQUEST_CODE);
    }

    @Override
    public boolean openCropApp(Uri inFileUri, Uri outFileUri) {
        Intent intent = CameraUtils.newCropIntent(inFileUri, outFileUri, false);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, CROP_REQUEST_CODE);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void showTempStorageError() {
        Snackbar.make(ll_passport_content, R.string.error_message_storage_create_temp_file, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCameraAppError() {
        Snackbar.make(ll_passport_content, R.string.error_message_camera_not_available, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCropAppError() {
        Snackbar.make(ll_passport_content, R.string.error_message_crop_not_available, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPhotoNotSelectedError() {
        Snackbar.make(ll_passport_content, R.string.error_message_select_photo, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onPhotoSuccessUploaded() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgm_document_container, new DocsVerifyResultPendingFrgm());
        ft.commit();
    }

    @Override
    public void onPhotoUploadFailed() {
        callbacks.onPhotoUploadFailed();
    }

    @Override
    public void showPhotoUploadIndicator(boolean show) {
        callbacks.showPhotoUploadIndicator(show);
    }

    @Override
    public void onPhotoUploadProgress(@IntRange(from = 0, to = 100) int progress) {
        callbacks.onPhotoUploadProgress(progress);
    }

    @Override
    public void showPhotoPreview(final String photoUrl) {
        check_passport.setImageResource(R.drawable.ic_doc_check_active);
        img_photo_passport.post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getActivity()).load(photoUrl)
                        .fit()
                        .transform(Transformations.ROUND_RECT_2DP)
                        .into(img_photo_passport);
            }
        });
    }
}
