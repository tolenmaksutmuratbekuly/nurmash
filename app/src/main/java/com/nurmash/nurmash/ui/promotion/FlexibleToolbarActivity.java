package com.nurmash.nurmash.ui.promotion;

import android.graphics.Color;
import android.support.annotation.FloatRange;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.ui.BaseActivity;

public class FlexibleToolbarActivity extends BaseActivity implements ObservableScrollViewCallbacks {
    private Toolbar mToolbar;
    private View mView_toolbar_below;
    private ObservableScrollView mScrollView;
    private View mImageContainer;
    private View mOverlayView;
    private ViewGroup mToolbarContentView;
    private TextView mTitleView;
    private ShadowLayerHolder mTitleShadowLayer;
    private TextView mSubtitleView;
    private ShadowLayerHolder mSubtitleShadowLayer;
    private int mFlexibleRange;
    private int mPrimaryColor;
    private int mTransparentColor;
    private int mDarkColor;

    protected void initFlexibleToolbar(ObservableScrollView scrollView) {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mView_toolbar_below = findViewById(R.id.mView_toolbar_below);
        mToolbarContentView = (ViewGroup) findViewById(R.id.view_toolbar_content);
        mTitleView = (TextView) findViewById(R.id.toolbar_title);
        mTitleShadowLayer = new ShadowLayerHolder(mTitleView);
        mSubtitleView = (TextView) findViewById(R.id.toolbar_subtitle);
        mSubtitleShadowLayer = new ShadowLayerHolder(mSubtitleView);
        mImageContainer = findViewById(R.id.toolbar_image_container);
        mOverlayView = findViewById(R.id.toolbar_image_overlay);
        mFlexibleRange = getResources().getDimensionPixelSize(R.dimen.flexible_toolbar_max_height) - getActionBarSizeFromAttr();
        mPrimaryColor = ContextCompat.getColor(this, R.color.white);
        mTransparentColor = ContextCompat.getColor(this, android.R.color.transparent);
        mDarkColor = ContextCompat.getColor(this, R.color.actionbar_title);
        mScrollView = scrollView;
        mScrollView.setScrollViewCallbacks(this);
        ScrollUtils.addOnGlobalLayoutListener(mToolbarContentView, new Runnable() {
            @Override
            public void run() {
                updateFlexibleSpace(mScrollView.getCurrentScrollY());
            }
        });
    }

    private void updateFlexibleSpace(int scrollY) {
        int adjustedScrollY = (int) ScrollUtils.getFloat(scrollY, 0, mFlexibleRange);
        // Translate image view
        mImageContainer.setTranslationY(-((float) adjustedScrollY / 2));
        // Update overlay view
        mOverlayView.setTranslationY(-adjustedScrollY);
        mOverlayView.setAlpha((float) adjustedScrollY / mFlexibleRange);
        // Make toolbar opaque if overlay is fully collapsed
        mToolbar.setBackgroundColor(scrollY >= mFlexibleRange ? mPrimaryColor : mTransparentColor);
        mTitleView.setTextColor(scrollY >= mFlexibleRange ? mDarkColor : mPrimaryColor);
        mSubtitleView.setTextColor(scrollY >= mFlexibleRange ? mDarkColor : mPrimaryColor);
        mToolbar.setNavigationIcon(scrollY >= mFlexibleRange ? R.drawable.ic_arrow_back_black : R.drawable.ic_arrow_back_white);
        mView_toolbar_below.setVisibility(scrollY >= mFlexibleRange ? View.VISIBLE : View.GONE);
        // Scale toolbar content by at most 10% when it's collapsed
        float scale = ((float) adjustedScrollY / mFlexibleRange) / 10;
        mToolbarContentView.setPivotX(0);
        mToolbarContentView.setScaleX(1 + scale);
        mToolbarContentView.setScaleY(1 + scale);
        // Translate title view
        float inverseScale = (float) (mFlexibleRange - adjustedScrollY) / mFlexibleRange;
        mToolbarContentView.setTranslationX(-mToolbarContentView.getLeft() * inverseScale);
        mToolbarContentView.setTranslationY(mFlexibleRange - adjustedScrollY);
        // Adjust shadow transparency: no shadow when fully collapsed, and full shadow when open
        mTitleShadowLayer.applyWithAlpha(mTitleView, inverseScale);
        mSubtitleShadowLayer.applyWithAlpha(mSubtitleView, inverseScale);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        updateFlexibleSpace(scrollY);
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    private static class ShadowLayerHolder {
        private final int mShadowColorAlpha;
        private final int mShadowColorRed;
        private final int mShadowColorGreen;
        private final int mShadowColorBlue;
        private final float mShadowRadius;
        private final float mShadowDx;
        private final float mShadowDy;

        ShadowLayerHolder(TextView textView) {
            int shadowColor = textView.getShadowColor();
            mShadowColorAlpha = Color.alpha(shadowColor);
            mShadowColorRed = Color.red(shadowColor);
            mShadowColorGreen = Color.green(shadowColor);
            mShadowColorBlue = Color.blue(shadowColor);
            mShadowRadius = textView.getShadowRadius();
            mShadowDx = textView.getShadowDx();
            mShadowDy = textView.getShadowDy();
        }

        void applyWithAlpha(TextView textView, @FloatRange(from = 0.0, to = 1.0) float alphaFactor) {
            int shadowColor = Color.argb(Math.round(alphaFactor * mShadowColorAlpha), mShadowColorRed, mShadowColorGreen, mShadowColorBlue);
            textView.setShadowLayer(mShadowRadius, mShadowDx, mShadowDy, shadowColor);
        }
    }
}
