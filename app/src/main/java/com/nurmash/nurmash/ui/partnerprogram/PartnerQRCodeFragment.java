package com.nurmash.nurmash.ui.partnerprogram;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.InviteAvailable;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.partnerprogram.PartnerQRCodePresenter;
import com.nurmash.nurmash.mvp.partnerprogram.PartnerQRCodeView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;

import org.parceler.Parcels;

import java.util.Hashtable;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PartnerQRCodeFragment extends Fragment implements PartnerQRCodeView {
    @Bind(R.id.fl_parent) FrameLayout fl_parent;
    @Bind(R.id.loading_indicator) ViewGroup loading_indicator;
    @Bind(R.id.tv_qrdescription) TextView tv_qrdescription;
    @Bind(R.id.tv_userid) TextView tv_userid;
    @Bind(R.id.img_qrcode) ImageView img_qrcode;
    private User user;
    private PartnerQRCodePresenter presenter;

    public static PartnerQRCodeFragment newInstance(User user) {
        PartnerQRCodeFragment fragment = new PartnerQRCodeFragment();
        Bundle args = new Bundle();
        args.putParcelable("user", Parcels.wrap(user));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("user")) {
            user = Parcels.unwrap(getArguments().getParcelable("user"));
        }

        presenter = new PartnerQRCodePresenter(DataLayer.getInstance(getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_partner_qrcode, container, false);
        ButterKnife.bind(this, rootView);

        DataLayer dataLayer = DataLayer.getInstance(getContext());
        if (dataLayer.authPreferences().hasShownQRDescription()) {
            tv_qrdescription.setVisibility(View.GONE);
        } else {
            tv_qrdescription.setVisibility(View.VISIBLE);
            dataLayer.authPreferences().setShownQRDescription(true);
        }

        if (user != null) {
            tv_userid.setText(user.id + "");
            ViewGroup.LayoutParams lp = img_qrcode.getLayoutParams();
            Bitmap bitmap = generateQRCodeEncodeAsBitmap(user.id + "", lp.width, lp.height, 0, Color.BLACK, Color.WHITE);
            if (bitmap != null)
                img_qrcode.setImageBitmap(bitmap);

        }

        // Presenter
        presenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(getContext(), fl_parent)));
        presenter.attachView(this);
        return rootView;
    }

    public static Bitmap generateQRCodeEncodeAsBitmap(String qrData, int w, int h, int margin, @ColorInt int foreground, @ColorInt int background) {
        if (TextUtils.isEmpty(qrData)) {
            return null;
        }

        // set encode hint
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, margin);

        Writer writer = new QRCodeWriter();
        BitMatrix matrix = null;
        try {
            matrix = writer.encode(qrData, BarcodeFormat.QR_CODE, w, h, hints);
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }

        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                boolean b = matrix.get(x, y);
                pixels[y * width + x] = b ? foreground : background;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

        return bitmap;
    }

    @Override
    public void onDestroyView() {
        presenter.setErrorHandler(null);
        presenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PartnerQRCodeView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        loading_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void inviteAvailableLoaded(InviteAvailable inviteAvailable) {
        if (!inviteAvailable.can_invite) {
            tv_qrdescription.setText(getString(R.string.label_partner_program_notavailable));
            tv_qrdescription.setVisibility(View.VISIBLE);
            tv_userid.setVisibility(View.GONE);
            img_qrcode.setVisibility(View.GONE);
        }
    }
}

