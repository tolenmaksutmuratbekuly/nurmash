package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.nurmash.nurmash.BuildConfig;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.config.DebugConfig;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.AppData;
import com.nurmash.nurmash.model.ParticipationData;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.ErrorHandler;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.helpers.ClipboardHelper;
import com.nurmash.nurmash.mvp.promotion.PromoDetailPresenter;
import com.nurmash.nurmash.mvp.promotion.PromoDetailView;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.participate.ShareStepActivity;
import com.nurmash.nurmash.ui.participate.StepsSummaryActivity;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.util.DebugUtils;
import com.nurmash.nurmash.util.FormatUtils;
import com.nurmash.nurmash.util.android.AndroidClipboardHelper;
import com.nurmash.nurmash.util.picasso.RoundedRectTransformation;
import com.nurmash.nurmash.util.recyclerview.WrapContentLinearLayoutManager;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.Arrays;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.text.TextUtils.isEmpty;
import static com.nurmash.nurmash.util.ContextUtils.dp2pixels;
import static com.nurmash.nurmash.util.ContextUtils.getRawAsString;
import static com.squareup.picasso.MemoryPolicy.NO_CACHE;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class PromoDetailActivity extends FlexibleToolbarActivity implements PromoDetailView {
    PromoDetailPresenter promoDetailPresenter;
    SamplePhotosAdapter samplePhotosAdapter;
    @Bind(R.id.toolbar_image) ImageView promotionPhotoView;
    @Bind(R.id.toolbar_title) TextView promoTitleView;
    @Bind(R.id.toolbar_subtitle) TextView datesView;
    @Bind(R.id.scroll_view) ScrollView scrollView;
    @Bind(R.id.promotion_detail_view) ViewGroup promotionDetailView;
    @Bind(R.id.tv_total_prize_sum) TextView tv_total_prize_sum;
    @Bind(R.id.tv_participants) TextView tv_participants;
    @Bind(R.id.tv_starts_timer) TextView tv_starts_timer;
    @Bind(R.id.tv_participants_sum) TextView tv_participants_sum;
    @Bind(R.id.img_participants_arrow) ImageView img_participants_arrow;

    @Bind(R.id.pb_new_promo_type) ProgressBar pb_new_promo_type;
    @Bind(R.id.description) TextView promotionDescriptionView;
    @Bind(R.id.section_download_app) ViewGroup downloadAppSection;
    @Bind(R.id.app_name) TextView appNameView;
    @Bind(R.id.app_icon) ImageView appIconView;
    @Bind(R.id.sample_photos_section) ViewGroup samplePhotosSectionView;
    @Bind(R.id.sample_photos_view) RecyclerView samplePhotosView;
    @Bind(R.id.rules_web_view) WebView rulesWebView;
    @Bind(R.id.button_next) Button nextButton;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressOverlay;
    @Bind(R.id.ll_notavailable) LinearLayout ll_notavailable;
    String promoPhotoHash;

    private boolean isPromotionStarted = true;

    public static Intent newIntent(Context packageContext, Promotion promotion) {
        Intent intent = new Intent(packageContext, PromoDetailActivity.class);
        intent.putExtra("promotion", Parcels.wrap(promotion));
        return intent;
    }

    public static Intent newIntent(Context packageContext, long promoId) {
        Intent intent = new Intent(packageContext, PromoDetailActivity.class);
        intent.putExtra("promoId", promoId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_detail);
        ButterKnife.bind(this);

        // Action bar
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        configureActionBar(getSupportActionBar());
        configureSamplePhotosView();
        configureRulesWebView();
        // Flexible toolbar
        initFlexibleToolbar((ObservableScrollView) scrollView);

        // Presenter
        if (getIntent().hasExtra("promotion")) {
            Promotion promotion = Parcels.unwrap(getIntent().getParcelableExtra("promotion"));
            promoDetailPresenter = new PromoDetailPresenter(DataLayer.getInstance(this), promotion);
        } else {
            long promoId = getIntent().getLongExtra("promoId", 0);
            promoDetailPresenter = new PromoDetailPresenter(DataLayer.getInstance(this), promoId);
        }
        promoDetailPresenter.setStringsProvider(this);
        promoDetailPresenter.attachView(this);
        promoDetailPresenter.setErrorHandler(createErrorHandler());
    }

    @Override
    protected void onResume() {
        super.onResume();
        promoDetailPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        promoDetailPresenter.setErrorHandler(null);
        promoDetailPresenter.detachView();
        super.onDestroy();
    }

    private ErrorHandler createErrorHandler() {
        RequestErrorHandler requestErrorHandler = new RequestErrorHandler();
        requestErrorHandler.setCallbacks(new DefaultRequestErrorCallbacks(this, scrollView) {
            @Override
            public void onNetworkError() {
                super.onNetworkError();
                promotionDetailView.setVisibility(View.GONE);
            }

            @Override
            public void onRequestFailed() {
                super.onRequestFailed();
                promotionDetailView.setVisibility(View.GONE);
            }

            @Override
            public void onInternalServerError() {
                super.onInternalServerError();
                promotionDetailView.setVisibility(View.GONE);
            }

            @Override
            public void onPromoHasEndedError() {
                super.onPromoHasEndedError();
                promotionDetailView.setVisibility(View.GONE);
            }

            @Override
            public void onPromoNotFoundError() {
                super.onPromoNotFoundError();
                promotionDetailView.setVisibility(View.GONE);
            }
        });
        return requestErrorHandler;
    }

    private void configureActionBar(ActionBar actionBar) {
        if (actionBar != null) {
            actionBar.setTitle(null);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureSamplePhotosView() {
        samplePhotosAdapter = new SamplePhotosAdapter();
        samplePhotosView.setAdapter(samplePhotosAdapter);
        samplePhotosView.setLayoutManager(new WrapContentLinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    private void configureRulesWebView() {
        // TODO: 3/28/16 Load rules html resource asynchronously.
        // ContextUtils.getRawAsString() might be blocking the UI thread, which might be the reason of slow activity startup.
        String rulesHtml = getRawAsString(this, R.raw.official_rules);
        rulesWebView.loadDataWithBaseURL(null, rulesHtml, "text/html", "utf-8", null);
        // Disable web view scrolling.
        rulesWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    startActivity(OfficialRulesActivity.newIntent(PromoDetailActivity.this));
                }
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
    }

    @OnClick(R.id.ll_participants)
    public void onParticipantsClick() {
        if (isPromotionStarted) {
            promoDetailPresenter.onCompetitorsPreviewClick();
        }
    }

    @OnClick(R.id.ll_total_prize)
    public void onTotalPrizeClick() {
        promoDetailPresenter.onTotalPrizeClick();
    }

    @OnClick(R.id.button_install)
    public void onInstallAppClick() {
        promoDetailPresenter.onInstallAppClick();
    }

    @OnClick(R.id.rules_section)
    public void onRulesSectionClick() {
        startActivity(OfficialRulesActivity.newIntent(this));
    }

    @OnClick(R.id.button_next)
    public void onNextClick() {
        promoDetailPresenter.onNextClick();
    }

    @OnLongClick(R.id.button_next)
    public boolean onNextLongClick() {
        if (!BuildConfig.DEBUG && !DebugConfig.DEV_BUILD) {
            return false;
        }
        final ParticipationData data = promoDetailPresenter.getParticipationData();
        if (data != null) {
            final ClipboardHelper clipboard = new AndroidClipboardHelper(this);
            final DataLayer dataLayer = DataLayer.getInstance(this);
            final long userId = dataLayer.authPreferences().getUserId();
            dataLayer.pushRegistrationHelper()
                    .getGcmToken()
                    .subscribeOn(Schedulers.io())
                    .observeOn(mainThread())
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String token) {
                            if (DebugUtils.copyParticipationDataToClipboard(clipboard, userId, token, data)) {
                                Toast.makeText(PromoDetailActivity.this, "Copied GCM data to clipboard.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable e) {
                            Timber.e(new SubscriberErrorWrapper(e), null);
                        }
                    });
            return true;
        } else {
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PromoDetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPromotionDataLoaded(final Promotion promotion) {
        if (promotion == null) return;
        // Don't load reload photo if already loaded.
        if (promoPhotoHash == null) {
            promoPhotoHash = promotion.photo;
            if (promoPhotoHash != null && !promoPhotoHash.isEmpty()) {
                Picasso.with(this).load(PhotoUrl.grxl(promoPhotoHash))
                        .memoryPolicy(NO_CACHE)
                        .into(promotionPhotoView);
            }
        }
        promoTitleView.setText(promotion.title);
        datesView.setText(String.format(Locale.US, "%s - %s", FormatUtils.formatDisplayDate(promotion.start_date),
                FormatUtils.formatDisplayDate(promotion.stop_date)));

        if (promotion.deadline_type != null) {
            if (promotion.deadline_type.equals(Promotion.DEADLINE_TYPE_PARTICIPANT)) {
                pb_new_promo_type.setVisibility(View.VISIBLE);
                Spannable participantsBoundaryStr = new SpannableString(promotion.participants_amount + "" + getString(R.string.label_promo_detail_of));
                participantsBoundaryStr.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.participants_span)), 0, participantsBoundaryStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_participants_sum.setText(participantsBoundaryStr);

                Spannable participantsAmountStr = new SpannableString(promotion.participants_boundary != null ? promotion.participants_boundary + "" : "");
                participantsAmountStr.setSpan(new ForegroundColorSpan(Color.BLACK), 0, participantsAmountStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_participants_sum.append(participantsAmountStr);
                pb_new_promo_type.setMax(promotion.participants_boundary != null ? promotion.participants_boundary : 0);
                pb_new_promo_type.post(new Runnable() {
                    public void run() {
                        pb_new_promo_type.setProgress(promotion.participants_amount);
                    }
                });

                if (promotion.start_date != null) {
                    datesView.setText(FormatUtils.formatDisplayDate(promotion.start_date));
                } else {
                    datesView.setText(null);
                }
            } else if (promotion.deadline_type.equals(Promotion.DEADLINE_TYPE_DATE)) {
                pb_new_promo_type.setVisibility(View.GONE);
                tv_participants_sum.setText(promotion.participants_amount + "");
                if (promotion.start_date != null && promotion.stop_date != null) {
                    datesView.setText(String.format(Locale.US, "%s - %s", FormatUtils.formatDisplayDate(promotion.start_date), FormatUtils.formatDisplayDate(promotion.stop_date)));
                } else {
                    datesView.setText(null);
                }
            } else {
                tv_participants_sum.setText(null);
            }
        }

        if (promotion.prizes != null) {
            String totalPrizeSum = promotion.prizes.getTotalPrize().toString(FormatUtils.getMoneyAmountDisplayFormat());
            if (promotion.getCurrencyCode() != null) {
                totalPrizeSum += " " + promotion.getCurrencyCode();
            }
            tv_total_prize_sum.setText(totalPrizeSum);
        }
        if (promotion.description != null) {
            promotionDescriptionView.setText(Html.fromHtml(promotion.description.trim().replace("\n", "<br>")));
        }
        if (promotion.sample_photos != null && promotion.sample_photos.length != 0) {
            samplePhotosSectionView.setVisibility(View.VISIBLE);
            samplePhotosAdapter.setPhotoHashes(Arrays.asList(promotion.sample_photos));
        } else {
            samplePhotosSectionView.setVisibility(View.GONE);
        }

        if (!(promotion.can_participate)) {
            ll_notavailable.setVisibility(View.VISIBLE);
        } else {
            ll_notavailable.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLinkedAppDataLoaded(AppData appData) {
        if (appData != null) {
            downloadAppSection.setVisibility(View.VISIBLE);
            if (isEmpty(appData.description)) {
                appNameView.setText(appData.title);
            } else {
                appNameView.setText(String.format("%s - %s", appData.title, appData.description));
            }
            if (!isEmpty(appData.imageUrl)) {
                Picasso.with(this).load(appData.imageUrl)
                        .fit()
                        .centerCrop()
                        .transform(new RoundedRectTransformation(dp2pixels(this, 4)))
                        .into(appIconView);
            }
        } else {
            downloadAppSection.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNextButton(boolean show) {
        nextButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNextButtonText(String text) {
        nextButton.setText(text);
    }

    @Override
    public void setNextButtonCallback(View.OnClickListener callback) {
        nextButton.setEnabled(callback != null);
        nextButton.setOnClickListener(callback);
    }

    @Override
    public void openGooglePlayLink(String appLink) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(appLink));
        startActivity(intent);
    }

    @Override
    public void openStepsSummaryActivity(long promoId) {
        startActivity(StepsSummaryActivity.newIntent(this, promoId));
    }

    @Override
    public void openCompetitorListActivity(Promotion promo) {
        startActivity(CompetitorListActivity.newIntent(this, promo));
    }

    @Override
    public void openTotalPrizeInfoActivity(Promotion promo) {
        startActivity(TotalPrizeInfoActivity.newIntent(this, promo));
    }

    @Override
    public void openShareStepActivity(long promoId) {
        startActivity(ShareStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openPhotoDetailActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(this, competitorId));
    }

    @Override
    public void openPastPromoDetailActivity(Promotion promo) {
        startActivity(PastPromoDetailActivity.newIntent(this, promo));
    }

    @Override
    public void setNextButtonEnabled(boolean can_participate) {
        nextButton.setEnabled(can_participate);
    }

    @Override
    public void openNoParticipantsActivity() {
        startActivity(ActivityNoParticipants.newIntent(this, ActivityNoParticipants.STATUS_NO_PARTICIPANTS));
    }

    @Override
    public void timerStateBeforeStart(String timerStr, boolean isPromotionStarted) {
        this.isPromotionStarted = isPromotionStarted;
        tv_participants.setText(isPromotionStarted ? getString(R.string.label_promo_detail_participants) : getString(R.string.info_text_promo_starts_in));
        tv_starts_timer.setVisibility(isPromotionStarted ? View.GONE : View.VISIBLE);
        img_participants_arrow.setVisibility(isPromotionStarted ? View.VISIBLE : View.INVISIBLE);
        tv_participants_sum.setVisibility(isPromotionStarted ? View.VISIBLE : View.GONE);
        if (!isPromotionStarted) {
            tv_starts_timer.setText(timerStr);
        }
    }
}