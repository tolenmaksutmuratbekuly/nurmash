package com.nurmash.nurmash.ui.prizes;

import android.view.View;

import com.nurmash.nurmash.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrizeDataNoBalanceHolder {
    private Callback callback;
    public PrizeDataNoBalanceHolder(View view) {
        ButterKnife.bind(this, view);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @OnClick(R.id.btn_backto_promotions)
    void onBackToPromotions() {
        if (callback != null) {
            callback.onBackToPromotionsClick();
        }
    }

    public interface Callback {
        void onBackToPromotionsClick();
    }
}

