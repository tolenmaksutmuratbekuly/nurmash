package com.nurmash.nurmash.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoListAdapterGV extends BaseAdapter {
    private static final int USER_ITEM_LAYOUT_ID = R.layout.user_list_item;
    private static final int PHOTO_ITEM_LAYOUT_ID = R.layout.photo_list_item;

    // WARNING: Don't forget to call Picasso.cancelTag
    public final Object picassoTag;

    private final int itemLayoutId;
    private boolean photoList;
    private boolean isShowLikeCount;
    private List<Competitor> competitors = new ArrayList<>();
    private Callback callback;
    private Context context;

    public PhotoListAdapterGV(boolean photoList, boolean isShowLikeCount, Context context) {
        picassoTag = this;
        this.photoList = photoList;
        this.isShowLikeCount = isShowLikeCount;
        this.context = context;
        if (photoList) {
            itemLayoutId = PHOTO_ITEM_LAYOUT_ID;
        } else {
            itemLayoutId = USER_ITEM_LAYOUT_ID;
        }
    }

    public void refreshItem() {
        notifyDataSetChanged();
    }

    public void setCompetitors(List<Competitor> newCompetitors) {
        if (newCompetitors == null || newCompetitors.isEmpty()) {
            competitors = new ArrayList<>();
        } else {
            competitors = new ArrayList<>(newCompetitors);
        }
        notifyDataSetChanged();
    }

    public void addCompetitors(List<Competitor> newCompetitors) {
        if (newCompetitors == null || newCompetitors.isEmpty()) {
            return;
        }
        competitors.addAll(newCompetitors);
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private int getCompetitorIndex(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return competitors.size();
    }

    @Override
    public Object getItem(int position) {
        return competitors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(itemLayoutId, parent, false);
            convertView.setTag(photoList ? new PhotoViewHolder(convertView) : new UserViewHolder(convertView));
        }

        if (photoList) {
            PhotoViewHolder viewHolder = (PhotoViewHolder) convertView.getTag();
            viewHolder.bindCompetitor(competitors.get(getCompetitorIndex(position)), position);
        } else {
            UserViewHolder viewHolder = (UserViewHolder) convertView.getTag();
            viewHolder.bindCompetitor(competitors.get(getCompetitorIndex(position)), position);
        }
        return convertView;
    }

    class UserViewHolder {
        Context context;
        Competitor bindedCompetitor;
        int mPosition;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.display_name) TextView nameView;

        UserViewHolder(View view) {
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindCompetitor(Competitor competitor, int position) {
            bindedCompetitor = competitor;
            mPosition = position;
            User user = competitor == null ? null : competitor.user;
            if (user != null) {
                Picasso.with(context).load(PhotoUrl.l(user.photo))
                        .fit()
                        .centerCrop()
                        .transform(Transformations.CIRCLE_NO_BORDER)
                        .tag(picassoTag)
                        .into(photoView);
                nameView.setText(user.getDisplayName());
            } else {
                photoView.setImageResource(R.drawable.bg_placeholder_gradient);
                nameView.setText(null);
            }
        }

        @OnClick(R.id.item_view)
        void onItemClick() {
            if (callback != null) callback.onCompetitorClick(bindedCompetitor, mPosition);
        }
    }

    class PhotoViewHolder {
        Context context;
        Competitor bindedCompetitor;
        int mPosition;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.like_count) TextView likeCountView;
        @Bind(R.id.like_count_gradient_background) View like_count_gradient_background;

        PhotoViewHolder(View view) {
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindCompetitor(Competitor competitor, int position) {
            bindedCompetitor = competitor;
            mPosition = position;
            if (competitor != null && competitor.isDisqualified()) {
                photoView.setBackgroundColor(Color.WHITE);
                photoView.setImageResource(R.drawable.ic_photo_disqual_placeholder);
            } else if (competitor != null && competitor.getPhotoHash() != null && !competitor.getPhotoHash().isEmpty()) {
                photoView.setBackgroundColor(Color.TRANSPARENT);
                Picasso.with(context).load(PhotoUrl.xl(competitor.getPhotoHash()))
                        .placeholder(R.drawable.bg_placeholder_gradient)
                        .fit()
                        .centerCrop()
                        .tag(picassoTag)
                        .into(photoView);

                // Like count
                if (!isShowLikeCount || competitor.like_count == 0) {
                    likeCountView.setVisibility(View.GONE);
                    like_count_gradient_background.setVisibility(View.GONE);
                } else {
                    like_count_gradient_background.setVisibility(View.VISIBLE);
                    likeCountView.setVisibility(View.VISIBLE);
                    likeCountView.setText(competitor.like_count + "");
                    likeCountView.setCompoundDrawablesWithIntrinsicBounds(competitor.is_liked ? R.drawable.ic_photo_likes_count_active_white
                            : R.drawable.ic_photo_likes_count_inactive, 0, 0, 0);
                }
            } else {
                photoView.setBackgroundColor(Color.TRANSPARENT);
                photoView.setImageResource(R.drawable.bg_placeholder_gradient);
            }
        }

        @OnClick(R.id.photo)
        void onPhotoClick() {
            if (callback != null) callback.onCompetitorClick(bindedCompetitor, mPosition);
        }
    }

    public interface Callback {
        void onCompetitorClick(Competitor competitor, int competitorIndex);
    }
}
