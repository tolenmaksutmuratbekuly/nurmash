package com.nurmash.nurmash.ui.partnerprogram;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.Money;
import com.nurmash.nurmash.model.json.InviteList;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.partnerprogram.PaymentsHistoryPresenter;
import com.nurmash.nurmash.mvp.partnerprogram.PaymentsHistoryView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.recyclerview.EndlessScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PaymentsHistoryActivity extends BaseActivity implements PaymentsHistoryView {
    PaymentsHistoryPresenter invitationsPresenter;
    InvitationsListAdapter invitationsListAdapter;
    EndlessScrollListener endlessScrollListener;
    private List<InviteList> items = new ArrayList<>();
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.tv_total_amount) TextView tv_total_amount;

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, PaymentsHistoryActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments_history);
        ButterKnife.bind(this);

        // Presenter
        invitationsPresenter = new PaymentsHistoryPresenter(DataLayer.getInstance(this));
        configureRecyclerView();

        invitationsPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, recyclerView)));
        invitationsPresenter.attachView(this);

    }

    private void configureRecyclerView() {
        invitationsListAdapter = new InvitationsListAdapter(false);
        recyclerView.setAdapter(invitationsListAdapter);
        endlessScrollListener = new EndlessScrollListener(recyclerView) {
            @Override
            public void onRequestMoreItems() {
                invitationsPresenter.onRequestMoreItems();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                invitationsPresenter.onRefreshAction();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_1, R.color.refresh_progress_2);
    }

    @Override
    protected void onDestroy() {
        invitationsPresenter.detachView();
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PaymentsHistoryView implementation
    ///////////////////////////////////////////////////////////////////////////
    @Override
    public void showLoadingIndicator(boolean show) {
        invitationsListAdapter.setLoading(show);
        if (!show) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void setInvitationItems(List<InviteList> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            items = new ArrayList<>();
        } else {
            items = new ArrayList<>(newItems);
        }

        invitationsListAdapter.setItems(newItems);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void addInvitationItems(List<InviteList> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            return;
        }
        items.addAll(newItems);
        invitationsListAdapter.addItems(newItems);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.post(needMoreItemsChecker);
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void setTotalWithDrawnSum(String totalWithDrawnSum) {
        tv_total_amount.setText(totalWithDrawnSum);
    }
}
