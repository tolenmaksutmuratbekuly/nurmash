package com.nurmash.nurmash.ui.participate;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.social.facebook.FacebookProvider;
import com.nurmash.nurmash.data.social.google.GoogleShareProvider;
import com.nurmash.nurmash.data.social.twitter.TwitterProvider;
import com.nurmash.nurmash.data.social.vk.VKProvider;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.participate.ShareStepPresenter;
import com.nurmash.nurmash.mvp.participate.ShareStepView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;
import com.nurmash.nurmash.util.ViewUtils;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class ShareStepActivity extends BaseActivity implements ShareStepView {
    ShareStepPresenter shareStepPresenter;
    FacebookProvider facebookProvider;
    TwitterProvider twitterProvider;
    GoogleShareProvider googleShareProvider;
    VKProvider vkProvider;
    @Bind(R.id.content_view) ViewGroup contentView;
    @Bind(R.id.progress_bar_overlay) ViewGroup loadingProgressOverlay;
    @Bind(R.id.photo) ImageView photoView;
    @Bind(R.id.photo_upload_progress) CircularProgressView photoUploadProgressView;
    @Bind(R.id.comment) TextView commentView;
    @Bind(R.id.commit_progress_overlay) ViewGroup commitProgressOverlay;
    @Bind(R.id.button_twitter) TextView twitterButton;
    @Bind(R.id.button_facebook) TextView facebookButton;
    @Bind(R.id.button_vk) TextView vkButton;
    @Bind(R.id.button_google) TextView googleButton;
    @Bind(R.id.button_next) Button nextButton;

    public static Intent newIntent(Context packageContext, long promoId) {
        return newIntent(packageContext, promoId, null);
    }

    public static Intent newIntent(Context packageContext, long promoId, Uri photoFileUri) {
        Intent intent = new Intent(packageContext, ShareStepActivity.class);
        intent.putExtra("promoId", promoId);
        intent.putExtra("photoFileUri", photoFileUri);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_step);
        ButterKnife.bind(this);
        // Share providers
        facebookProvider = new FacebookProvider(this);
        twitterProvider = new TwitterProvider(this);
        googleShareProvider = new GoogleShareProvider(this);
        vkProvider = new VKProvider(this);
        // Presenter
        long promoId = getIntent().getLongExtra("promoId", 0);
        Uri photoFileUri = getIntent().getParcelableExtra("photoFileUri");
        shareStepPresenter = new ShareStepPresenter(DataLayer.getInstance(this), promoId, photoFileUri,
                facebookProvider, twitterProvider, googleShareProvider, vkProvider);
        shareStepPresenter.restoreInstanceState(savedInstanceState);
        shareStepPresenter.attachView(this);
        shareStepPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, contentView)));
        shareStepPresenter.setFilesProvider(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        shareStepPresenter.reloadParticipationData();
    }

    @Override
    protected void onDestroy() {
        shareStepPresenter.setFilesProvider(null);
        shareStepPresenter.setErrorHandler(null);
        shareStepPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        shareStepPresenter.saveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (twitterProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (facebookProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (vkProvider.onActivityResult(requestCode, resultCode, data)) return;
        if (googleShareProvider.onActivityResult(requestCode, resultCode, data)) return;
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnEditorAction(R.id.comment)
    boolean onCommentFieldAction(TextView textView, int actionId, KeyEvent event) {
        if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(textView.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            return true;
        }
        return false;
    }

    @OnClick(R.id.button_twitter)
    void onTwitterClick() {
        shareStepPresenter.onTwitterShareClick();
    }

    @OnClick(R.id.button_facebook)
    void onFacebookClick() {
        shareStepPresenter.onFacebookShareClick();
    }

    @OnClick(R.id.button_vk)
    void onVKClick() {
        shareStepPresenter.onVKShareClick();
    }

    @OnClick(R.id.button_google)
    void onGoogleClick() {
        shareStepPresenter.onGoogleShareClick();
    }

    @OnClick(R.id.button_next)
    void onNextStepClick() {
        shareStepPresenter.onNextStepClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // ShareStepView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showPhotoPreview(final String photoUrl) {
        photoView.post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(ShareStepActivity.this).load(photoUrl)
                        .fit()
                        .transform(Transformations.ROUND_RECT_2DP)
                        .into(photoView);
            }
        });
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        loadingProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPhotoUploadIndicator(boolean show) {
        photoUploadProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPhotoUploadProgress(final int progress) {
        photoUploadProgressView.setProgress(progress);
    }

    @Override
    public void showPhotoNotUploadedYetMessage() {
        showToastAtTop(R.string.error_message_upload_photo_not_finished_yet, Toast.LENGTH_LONG);
    }

    @Override
    public void onPhotoUploadFailed() {
        Snackbar snackbar = Snackbar.make(contentView, R.string.error_message_upload_photo_failed, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareStepPresenter.retryPhotoUpload();
            }
        });
        snackbar.show();
    }

    @Override
    public String getComment() {
        return commentView.getText().toString();
    }

    @Override
    public void showCommitProgressIndicator(boolean show) {
        commitProgressOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setTwitterShareCompleted() {
        setShareCompleted(twitterButton, R.drawable.ic_share_twitter_active);
    }

    @Override
    public void setFacebookShareCompleted() {
        setShareCompleted(facebookButton, R.drawable.ic_share_facebook_active);
    }

    @Override
    public void setVKShareCompleted() {
        setShareCompleted(vkButton, R.drawable.ic_share_vk_active);
    }

    @Override
    public void setGoogleShareCompleted() {
        setShareCompleted(googleButton, R.drawable.ic_share_google_active);
    }

    @Override
    public void onFacebookShareFailed() {
        showToastAtTop(R.string.error_message_share_facebook_general, Toast.LENGTH_SHORT);
    }

    @Override
    public void onTwitterAppNotAvailable() {
        showToastAtTop(R.string.error_message_share_twitter_app_not_found, Toast.LENGTH_SHORT);
    }

    @Override
    public void onGooglePlusAppNotAvailable() {
        showToastAtTop(R.string.error_message_share_google_plus_app_not_found, Toast.LENGTH_SHORT);
    }

    @Override
    public void openSurveyStepActivity(long promoId) {
        startActivity(SurveyStepActivity.newIntent(this, promoId));
    }

    @Override
    public void openMyPhotoActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(this, competitorId));
    }

    private void setShareCompleted(TextView textView, @DrawableRes int activeDrawable) {
        textView.setCompoundDrawablesWithIntrinsicBounds(activeDrawable, 0, 0, 0);
        ViewUtils.setTextAppearance(textView, R.style.ParticipationSteps_TextAppearance_Label_Active_Big);
        nextButton.setEnabled(true);
    }

    private void showToastAtTop(@StringRes int resId, int duration) {
        Toast toast = Toast.makeText(this, resId, duration);
        toast.setGravity(Gravity.TOP, 0, getActionBarSizeFromAttr() * 3 / 2);
        toast.show();
    }
}
