package com.nurmash.nurmash.ui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;

import com.nurmash.nurmash.mvp.helpers.photo.PhotoPicker;
import com.nurmash.nurmash.mvp.helpers.photo.PhotoPickerCallback;
import com.nurmash.nurmash.util.android.TempFilesManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AndroidPhotoPicker implements PhotoPicker {
    private final String CAMERA_FILE_KEY = "CAMERA_FILE_OUT_URI";
    private final String CROP_FILE_KEY = "CROP_FILE_OUT_URI";
    private final int PICK_PHOTO_REQUEST_CODE_OFFSET = 0;
    private final int CROP_PHOTO_REQUEST_CODE_OFFSET = 1;

    private final int baseRequestCode;
    private final Activity activity;
    private final TempFilesManager filesManager;
    private PhotoPickerCallback callback;
    private Uri cameraOutFileUri;
    private Uri cropOutFileUri;

    public AndroidPhotoPicker(Activity activity, int baseRequestCode) {
        this.baseRequestCode = baseRequestCode;
        this.activity = activity;
        this.filesManager = new TempFilesManager(activity, "image", ".jpg");
    }

    public void onSaveInstanceState(Bundle outState) {
        if (outState == null) {
            return;
        }
        if (cameraOutFileUri != null) {
            outState.putParcelable(CAMERA_FILE_KEY, cameraOutFileUri);
        }
        if (cropOutFileUri != null) {
            outState.putParcelable(CROP_FILE_KEY, cropOutFileUri);
        }
    }

    public void onRestoreInstanceState(Bundle savedState) {
        if (savedState != null && savedState.containsKey(CAMERA_FILE_KEY)) {
            cameraOutFileUri = savedState.getParcelable(CAMERA_FILE_KEY);
        }
        if (savedState != null && savedState.containsKey(CROP_FILE_KEY)) {
            cropOutFileUri = savedState.getParcelable(CROP_FILE_KEY);
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data,  boolean isProfileBg) {
        switch (requestCode - baseRequestCode) {
            case PICK_PHOTO_REQUEST_CODE_OFFSET:
                if (resultCode == Activity.RESULT_OK) {
                    if (isResultFromCamera(data)) {
                        startCropActivity(cameraOutFileUri, isProfileBg);
                    } else if (data.getData() != null) {
                        startCropActivity(data.getData(), isProfileBg);
                    } else {
                        onCancel();
                    }
                } else {
                    onCancel();
                }
                return true;
            case CROP_PHOTO_REQUEST_CODE_OFFSET:
                if (resultCode == Activity.RESULT_OK) {
                    onCropSuccess();
                } else {
                    onCancel();
                }
                return true;
        }
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    // PhotoPicker implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setPhotoPickerCallback(PhotoPickerCallback callback) {
        this.callback = callback;
    }

    @Override
    public void startCroppedPhotoPicker() {
        startPickActivity();
    }

    @Override
    public void disposeOfPhotoFile(Uri imageUri) {
        // Check that this is a temporary file before deleting it, because it might be a file from gallery
        // or other external source.
        if (filesManager.isTempFileUri(imageUri)) {
            filesManager.deleteTempFile(imageUri);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Private implementation
    ///////////////////////////////////////////////////////////////////////////

    private void onCropSuccess() {
        if (cameraOutFileUri != null) {
            filesManager.deleteTempFile(cameraOutFileUri);
        }
        if (cropOutFileUri == null) {
            throw new NullPointerException("cropOutFileUri == null");
        }
        if (callback != null) {
            callback.onCroppedPhotoPicked(cropOutFileUri);
        }
    }

    private void onCancel() {
        deleteTempFiles();
        if (callback != null) {
            callback.onPhotoPickCanceled();
        }
    }

    private void onError(Throwable e) {
        deleteTempFiles();
        if (callback != null) {
            callback.onPhotoPickError(e);
        }
    }

    private void startPickActivity() {
        if (cameraOutFileUri != null) {
            filesManager.deleteTempFile(cameraOutFileUri);
            cameraOutFileUri = null;
        }
        try {
            cameraOutFileUri = filesManager.createTempFile();
        } catch (IOException ignored) {
        }
        Intent chooserIntent = createChooserIntent(cameraOutFileUri);
        activity.startActivityForResult(chooserIntent, baseRequestCode + PICK_PHOTO_REQUEST_CODE_OFFSET);
    }

    private void startCropActivity(Uri sourceImageUri,  boolean isProfileBg) {
        if (sourceImageUri == null) {
            return;
        }
        if (cropOutFileUri != null) {
            filesManager.deleteTempFile(cropOutFileUri);
            cropOutFileUri = null;
        }
        try {
            cropOutFileUri = filesManager.createTempFile();
            Intent cropIntent = createCropIntent(sourceImageUri, cropOutFileUri, isProfileBg);
            activity.startActivityForResult(cropIntent, baseRequestCode + CROP_PHOTO_REQUEST_CODE_OFFSET);
        } catch (IOException | ActivityNotFoundException e) {
            onError(e);
        }
    }

    private void deleteTempFiles() {
        if (cameraOutFileUri != null) {
            filesManager.deleteTempFile(cameraOutFileUri);
        }
        if (cropOutFileUri != null) {
            filesManager.deleteTempFile(cropOutFileUri);
        }
    }

    private static boolean isResultFromCamera(Intent data) {
        return data == null || data.getData() == null || MediaStore.ACTION_IMAGE_CAPTURE.equals(data.getAction());
    }

    //////////////////////////////////////////////////////////////////////////
    // Intents helper methods
    ///////////////////////////////////////////////////////////////////////////

    private Intent createChooserIntent(Uri cameraOutFileUri) {
        Intent chooserIntent = Intent.createChooser(createGalleryIntent(), null);
        List<Intent> cameraIntents = createCameraIntentList(cameraOutFileUri);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
        return chooserIntent;
    }

    private Intent createGalleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        return galleryIntent;
    }

    private List<Intent> createCameraIntentList(Uri cameraOutFileUri) {
        if (cameraOutFileUri == null) {
            return Collections.emptyList();
        }
        List<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = activity.getPackageManager();
        for (ResolveInfo res : packageManager.queryIntentActivities(captureIntent, 0)) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraOutFileUri);
            cameraIntents.add(intent);
        }
        return cameraIntents;
    }

    private Intent createCropIntent(Uri inputFileUri, Uri outputFileUri, boolean isProfileBg) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(inputFileUri, "image/*");
        cropIntent.putExtra("crop", "true");
        if (!isProfileBg) {
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);
        }

        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        return cropIntent;
    }
}
