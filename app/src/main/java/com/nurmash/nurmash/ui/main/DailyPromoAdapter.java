package com.nurmash.nurmash.ui.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DailyPromoAdapter extends BaseAdapter {
    private static final int COMMENT_ITEM_LAYOUT = R.layout.lv_daily_promo;
    private Context context;

    private List<Promotion> comments = new ArrayList<>();
    private Callback callback;

    public DailyPromoAdapter(Context context) {
        this.context = context;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setDailyPromo(List<Promotion> newComments) {
        comments = new ArrayList<>(newComments);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return 10;//comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(COMMENT_ITEM_LAYOUT, parent, false);
//            convertView.setTag(new com.nurmash.nurmash.ui.photo.CommentListAdapter.CommentViewHolder(convertView));
        }

//        com.nurmash.nurmash.ui.photo.CommentListAdapter.CommentViewHolder viewHolder = (com.nurmash.nurmash.ui.photo.CommentListAdapter.CommentViewHolder) convertView.getTag();
//        if (position == getOrigCommentPosition()) {
//            viewHolder.bindComment(origComment);
//        } else {
//            viewHolder.bindComment(comments.get(getCommentIndex(position)));
//        }
        return convertView;
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        Context context;
        Promotion bindedPromotion;
        @Bind(R.id.user_photo) ImageView userPhotoView;
        @Bind(R.id.user_name) TextView userNameView;
        @Bind(R.id.comment_body) TextView commentBodyView;
        @Bind(R.id.timestamp) TextView timestampView;

        public CommentViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindComment(Promotion promotion) {
            bindedPromotion = promotion;
//            if (promotion == null) {
//                userPhotoView.setImageDrawable(null);
//                userNameView.setText(null);
//                commentBodyView.setText(null);
//                timestampView.setText(null);
//                return;
//            }
//            User user = promotion.user;
//            if (user != null) {
//                if (!TextUtils.isEmpty(user.photo)) {
//                    Picasso.with(context).load(PhotoUrl.l(user.photo))
//                            .fit()
//                            .transform(Transformations.CIRCLE_NO_BORDER)
//                            .into(userPhotoView);
//                } else {
//                    userPhotoView.setImageDrawable(null);
//                }
//                userNameView.setText(user.getDisplayName());
//            } else {
//                userPhotoView.setImageDrawable(null);
//                userNameView.setText(null);
//            }
//            commentBodyView.setText(comment.body);
//            if (comment.created_at != null) {
//                timestampView.setText(new PrettyTime().format(comment.created_at));
//            } else {
//                timestampView.setText(null);
//            }
        }
    }

    public interface Callback {
        void onUserClick(User user);
    }
}
