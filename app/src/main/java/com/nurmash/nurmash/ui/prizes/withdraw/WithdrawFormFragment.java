package com.nurmash.nurmash.ui.prizes.withdraw;

import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import com.nurmash.nurmash.model.withdraw.ValidationError;
import com.nurmash.nurmash.model.withdraw.WithdrawForm;
import com.rengwuxian.materialedittext.MaterialEditText;

import static com.nurmash.nurmash.util.ViewUtils.scrollToView;

public abstract class WithdrawFormFragment<T extends WithdrawForm> extends Fragment {
    public abstract T getForm();

    public abstract void onFormValidationError(ValidationError e);

    protected void showInputError(MaterialEditText inputField, @StringRes int errorRes, boolean scrollTo) {
        inputField.setError(getString(errorRes));
        if (scrollTo) {
            scrollToView(inputField);
        }
    }
}
