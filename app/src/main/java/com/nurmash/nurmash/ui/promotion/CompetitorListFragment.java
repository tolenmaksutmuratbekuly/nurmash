package com.nurmash.nurmash.ui.promotion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.promotion.CompetitorListFragmentCallbacks;
import com.nurmash.nurmash.ui.PhotoListAdapterGV;
import com.nurmash.nurmash.util.listgridviews.EndlessScrollListenerGridListViews;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

///////////////////////////////////////////////////////////////////////////
// CompetitorListFragment implementation
///////////////////////////////////////////////////////////////////////////
public class CompetitorListFragment extends Fragment {
    public static CompetitorListFragment newInstance(Promotion promo) {
        CompetitorListFragment fragment = new CompetitorListFragment();
        Bundle args = new Bundle();
        args.putParcelable("promo", Parcels.wrap(promo));
        fragment.setArguments(args);
        return fragment;
    }

    private CompetitorListFragmentCallbacks callbacks;
    PhotoListAdapterGV photoListAdapter;
    Promotion promo;
    @Bind(R.id.competitors_view) GridView competitorsView;
    @Bind(R.id.toolbar) Toolbar toolbar;

    public void setCallbacks(CompetitorListFragmentCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        promo = Parcels.unwrap(getArguments().getParcelable("promo"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_competitor_list, container, false);
        ButterKnife.bind(this, rootView);
        configureGridView(promo);
        configureToolbar();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        Picasso.with(getActivity()).cancelTag(photoListAdapter.picassoTag);
        ButterKnife.unbind(getActivity());
        super.onDestroyView();
    }

    private void configureGridView(Promotion promo) {
        photoListAdapter = new PhotoListAdapterGV(promo.isPhotoSharingPromo(), true, getActivity());
        photoListAdapter.setCallback(new PhotoListAdapterGV.Callback() {
            @Override
            public void onCompetitorClick(Competitor competitor, int position) {
                callbacks.onCompetitorClick(competitor, position);
            }
        });
        competitorsView.setAdapter(photoListAdapter);
        competitorsView.setOnScrollListener(new EndlessScrollListenerGridListViews(false) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                callbacks.loadMoreItems();
                return true;
            }
        });
    }

    private void configureToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    public void setCompetitors(List<Competitor> competitors) {
        photoListAdapter.setCompetitors(competitors);
    }

    public void addCompetitors(List<Competitor> competitors) {
        photoListAdapter.addCompetitors(competitors);
    }

    public void scrollToPosition(final int competitorIndex) {
        competitorsView.requestFocusFromTouch();
        competitorsView.setSelection(competitorIndex);
    }

    public void refreshItem() {
        photoListAdapter.refreshItem();
    }
}