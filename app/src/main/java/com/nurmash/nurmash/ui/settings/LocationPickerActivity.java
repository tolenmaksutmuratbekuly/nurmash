package com.nurmash.nurmash.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.geo.City;
import com.nurmash.nurmash.model.json.geo.Country;
import com.nurmash.nurmash.model.json.geo.Region;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.settings.location.CityPickerPresenter;
import com.nurmash.nurmash.mvp.settings.location.CountryPickerPresenter;
import com.nurmash.nurmash.mvp.settings.location.LocationPickerPresenter;
import com.nurmash.nurmash.mvp.settings.location.LocationPickerView;
import com.nurmash.nurmash.mvp.settings.location.RegionPickerPresenter;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LocationPickerActivity extends BaseActivity implements LocationPickerView, SearchView.OnQueryTextListener {
    private static final String ACTION_PICK_COUNTRY = "ACTION_PICK_COUNTRY";
    private static final String ACTION_PICK_REGION = "ACTION_PICK_REGION";
    private static final String ACTION_PICK_CITY = "ACTION_PICK_CITY";
    private static final String ACTION_PICK_CITY_WITHOUT_REGION = "ACTION_PICK_CITY_WITHOUT_REGION";

    LocationPickerPresenter locationPickerPresenter;
    ArrayAdapter<TitleItem> itemsAdapter;
    @Bind(R.id.list_view) ListView listView;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;

    public static Intent newCountryPickerIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, LocationPickerActivity.class);
        intent.setAction(ACTION_PICK_COUNTRY);
        return intent;
    }

    public static Intent newRegionPickerIntent(Context packageContext, long countryId) {
        Intent intent = new Intent(packageContext, LocationPickerActivity.class);
        intent.setAction(ACTION_PICK_REGION);
        intent.putExtra("countryId", countryId);
        return intent;
    }

    public static Intent newCityPickerIntent(Context packageContext, long countryId, long regionId) {
        Intent intent = new Intent(packageContext, LocationPickerActivity.class);
        intent.setAction(ACTION_PICK_CITY);
        intent.putExtra("countryId", countryId);
        intent.putExtra("regionId", regionId);
        return intent;
    }

    public static Intent newNoRegionCityPickerIntent(Context packageContext, long countryId) {
        Intent intent = new Intent(packageContext, LocationPickerActivity.class);
        intent.setAction(ACTION_PICK_CITY_WITHOUT_REGION);
        intent.putExtra("countryId", countryId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_picker);
        ButterKnife.bind(this);
        configureTitle();
        configureListView();
        configurePresenter();
    }

    @Override
    protected void onDestroy() {
        locationPickerPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_location_picker, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setTextColor(ContextCompat.getColor(this, R.color.black));
        return true;
    }

    private void configureTitle() {
        switch (getIntent().getAction()) {
            case ACTION_PICK_COUNTRY:
                setTitle(R.string.label_activity_location_picker_country);
                break;
            case ACTION_PICK_REGION:
                setTitle(R.string.label_activity_location_picker_region);
                break;
            case ACTION_PICK_CITY:
            case ACTION_PICK_CITY_WITHOUT_REGION:
                setTitle(R.string.label_activity_location_picker_city);
                break;
        }
    }

    private void configureListView() {
        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(itemsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TitleItem item = (TitleItem) listView.getItemAtPosition(position);
                locationPickerPresenter.onItemSelected(item.index);
            }
        });
    }

    private void configurePresenter() {
        Intent intent = getIntent();
        long countryId = intent.getLongExtra("countryId", -1);
        long regionId = intent.getLongExtra("regionId", -1);
        DataLayer dataLayer = DataLayer.getInstance(this);
        switch (intent.getAction()) {
            case ACTION_PICK_COUNTRY:
                locationPickerPresenter = new CountryPickerPresenter(dataLayer);
                break;
            case ACTION_PICK_REGION:
                if (countryId == -1) {
                    throw new IllegalStateException("Country id was not provided for Region picker.");
                }
                locationPickerPresenter = new RegionPickerPresenter(dataLayer, countryId);
                break;
            case ACTION_PICK_CITY:
                if (countryId == -1 || regionId == -1) {
                    throw new IllegalStateException("Country and region ids were not fully provided for City picker: " +
                            "countryId = " + countryId + ", regionId = " + regionId);
                }
                locationPickerPresenter = new CityPickerPresenter(dataLayer, countryId, regionId);
                break;
            case ACTION_PICK_CITY_WITHOUT_REGION:
                if (countryId == -1) {
                    throw new IllegalStateException("Country id was not provided for 'City w/o Region' picker.");
                }
                locationPickerPresenter = new CityPickerPresenter(dataLayer, countryId);
                break;
            default:
                throw new IllegalStateException("Unsupported intent action.");
        }
        locationPickerPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, listView)));
        locationPickerPresenter.attachView(this);
    }

    ///////////////////////////////////////////////////////////////////////////
    // SearchView.OnQueryTextListener implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        itemsAdapter.getFilter().filter(newText);
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////
    // LocationPickerView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setItemTitles(List<String> titles) {
        itemsAdapter.setNotifyOnChange(false);
        itemsAdapter.clear();
        for (int i = 0; i < titles.size(); ++i) {
            itemsAdapter.add(new TitleItem(i, titles.get(i)));
        }
        itemsAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishWithResult(Object result) {
        Intent data = new Intent();
        if (result instanceof Country) {
            data.putExtra("country", Parcels.wrap(result));
        } else if (result instanceof Region) {
            data.putExtra("region", Parcels.wrap(result));
        } else if (result instanceof City) {
            data.putExtra("city", Parcels.wrap(result));
        } else {
            throw new IllegalArgumentException("Unsupported result object type.");
        }
        setResult(RESULT_OK, data);
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // TitleItem
    ///////////////////////////////////////////////////////////////////////////

    private static class TitleItem {
        int index;
        String title;

        TitleItem(int index, String title) {
            this.index = index;
            this.title = title;
        }

        @Override
        public String toString() {
            return title;
        }
    }
}
