package com.nurmash.nurmash.ui.checkin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.mvp.checkin.CheckinPresenter;
import com.nurmash.nurmash.mvp.checkin.CheckinView;
import com.nurmash.nurmash.ui.settings.ProfileEditActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CheckinTabFragment extends Fragment implements CheckinView {
    CheckinPresenter checkinPresenter;
    View rootView;
    @Bind(R.id.toolbar) Toolbar toolbar;

    public static CheckinTabFragment newInstance() {
        CheckinTabFragment fragment = new CheckinTabFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkinPresenter = new CheckinPresenter(DataLayer.getInstance(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_checkin_tab, container, false);
        ButterKnife.bind(this, rootView);
        configureToolbar();
        checkinPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkinPresenter.onResume();
    }

    @Override
    public void onDestroyView() {
        checkinPresenter.detachView();
        rootView = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        checkinPresenter.onDestroy();
        super.onDestroy();
    }

    private void configureToolbar() {
        toolbar.inflateMenu(R.menu.menu_prizes);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.info:

                        return true;
                }
                return false;
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // CheckinView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
//        userBalanceViewHolder.setLoading(show);
    }

    @Override
    public void openProfileEditActivity() {
        startActivity(ProfileEditActivity.newIntent(getContext()));
    }
}
