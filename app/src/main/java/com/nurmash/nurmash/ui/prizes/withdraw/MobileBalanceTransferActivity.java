package com.nurmash.nurmash.ui.prizes.withdraw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.PartnerBalance;
import com.nurmash.nurmash.model.json.PrizeItem;
import com.nurmash.nurmash.model.withdraw.OperatorCode;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.prizes.withdraw.MobileBalanceTransferPresenter;
import com.nurmash.nurmash.mvp.prizes.withdraw.MobileBalanceTransferView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;

public class MobileBalanceTransferActivity extends BaseActivity implements MobileBalanceTransferView {
    MobileBalanceTransferPresenter presenter;
    MobileOperatorAdapter mobileOperatorAdapter;
    PrizeItem prizeItem;
    PartnerBalance partnerBalance;
    @Bind(R.id.tv_amount_of_prize) TextView tv_amount_of_prize;
    @Bind(R.id.tv_tax) TextView tv_tax;
    @Bind(R.id.tv_comission) TextView tv_comission;
    @Bind(R.id.tv_amount_crediting) TextView tv_amount_crediting;
    @Bind(R.id.mobile_operator_dropdown) Spinner mobileOperatorDropdown;
    @Bind(R.id.phone_country_code_input) EditText phoneCountryCodeInput;
    @Bind(R.id.phone_number_input) EditText phoneNumberInput;
    @Bind(R.id.phone_country_code_input_confirm) EditText phone_country_code_input_confirm;
    @Bind(R.id.phone_number_input_confirm) EditText phone_number_input_confirm;

    @Bind(R.id.img_carrier_phone) ImageView img_carrier_phone;
    @Bind(R.id.img_phone_checkbox) ImageView img_phone_checkbox;
    @Bind(R.id.img_phone_confirm_checkbox) ImageView img_phone_confirm_checkbox;
    @Bind(R.id.img_save_phone) ImageView img_save_phone;

    @Bind(R.id.claim_button) Button claimButton;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;

    public static Intent newIntent(Context packageContext, PrizeItem prizeItem) {
        Intent intent = new Intent(packageContext, MobileBalanceTransferActivity.class);
        intent.putExtra("prizeItem", Parcels.wrap(prizeItem));
        return intent;
    }

    public static Intent newIntent(Context packageContext, PartnerBalance partnerBalance) {
        Intent intent = new Intent(packageContext, MobileBalanceTransferActivity.class);
        intent.putExtra("partnerBalance", Parcels.wrap(partnerBalance));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_balance_transfer);
        ButterKnife.bind(this);
        mobileOperatorAdapter = new MobileOperatorAdapter();
        mobileOperatorDropdown.setAdapter(mobileOperatorAdapter);
        mobileOperatorDropdown.setSelection(mobileOperatorAdapter.getPlaceholderPosition());

        Intent intent = getIntent();
        if (intent.hasExtra("prizeItem")) {
            prizeItem = Parcels.unwrap(getIntent().getParcelableExtra("prizeItem"));
            presenter = new MobileBalanceTransferPresenter(DataLayer.getInstance(this), prizeItem);
        } else if (intent.hasExtra("partnerBalance")) {
            partnerBalance = Parcels.unwrap(getIntent().getParcelableExtra("partnerBalance"));
            presenter = new MobileBalanceTransferPresenter(DataLayer.getInstance(this), partnerBalance);
        }

        if (presenter != null) {
            presenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this)));
            presenter.attachView(this);
        }
    }

    @OnItemSelected(R.id.mobile_operator_dropdown)
    void onCarrierSpinnerSelected() {
        presenter.setOperatorCodeErrorHandle(mobileOperatorAdapter.getOperatorCode(mobileOperatorDropdown.getSelectedItemPosition()));
    }

    @OnTextChanged({R.id.phone_number_input, R.id.phone_number_input_confirm})
    void onPhoneNumberChanged() {
        presenter.setPhoneNumberErrorHandle(phoneCountryCodeInput.getText().toString() + phoneNumberInput.getText().toString(),
                phone_country_code_input_confirm.getText().toString() + phone_number_input_confirm.getText().toString());
    }

    @OnClick(R.id.img_save_phone)
    void onSavePhoneClick(ImageView img) {
        if (img.getTag().toString().equals("default")) {
            img.setTag("active");
            img.setImageResource(R.drawable.check_active);
            presenter.savePhoneNumber(true, phoneNumberInput.getText().toString());
        } else if (img.getTag().toString().equals("active")) {
            img.setTag("default");
            img.setImageResource(R.drawable.check_default);
            presenter.savePhoneNumber(false, phoneNumberInput.getText().toString());
        }
    }

    @OnClick(R.id.claim_button)
    void onClaimPrizeClick() {
        presenter.setOperatorCode(mobileOperatorAdapter.getOperatorCode(mobileOperatorDropdown.getSelectedItemPosition()));
        presenter.setPhoneNumber(phoneCountryCodeInput.getText().toString() + phoneNumberInput.getText().toString(),
                phone_country_code_input_confirm.getText().toString() + phone_number_input_confirm.getText().toString());
        presenter.onClaimPrizeClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // MobileBalanceTransferView implementation
    ///////////////////////////////////////////////////////////////////////////
    @Override
    public void showAmountPrize(String amountOfPrize) {
        tv_amount_of_prize.setText(amountOfPrize);
    }

    @Override
    public void showTaxAmount(String taxAmount) {
        tv_tax.setText(taxAmount);
    }

    @Override
    public void showComission(String comissionAmount) {
        tv_comission.setText(comissionAmount);
    }

    @Override
    public void showAmountCrediting(String creditingAmount) {
        tv_amount_crediting.setText(creditingAmount);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showOperatorNotSelectedErrorToast() {
        Toast.makeText(this, R.string.error_message_mobile_operator_not_selected, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInvalidPhoneNumberErrorToast() {
        Toast.makeText(this, R.string.error_message_invalid_phone_number, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInvalidPhoneNumbersAraNotSameErrorToast() {
        Toast.makeText(this, R.string.error_message_invalid_phone_number_not_same, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishWithSuccess() {
        Toast.makeText(this, R.string.info_message_claim_prize_success, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void carrierCheckState(boolean hasError) {
        img_carrier_phone.setImageResource(hasError ? R.drawable.check_default : R.drawable.check_active);
    }

    @Override
    public void phoneNumberCheckState(boolean hasError) {
        img_phone_checkbox.setImageResource(hasError ? R.drawable.check_default : R.drawable.check_active);
    }

    @Override
    public void phoneNumberConfirmationCheckState(boolean hasError) {
        img_phone_confirm_checkbox.setImageResource(hasError ? R.drawable.check_default : R.drawable.check_active);
    }

    @Override
    public void setLocalPhoneNumber(String localPhoneNumber) {
        img_save_phone.setTag(localPhoneNumber.length() > 0 ? "active" : "default");
        img_save_phone.setImageResource(localPhoneNumber.length() > 0 ? R.drawable.check_active : R.drawable.check_default);
        phoneNumberInput.setText(localPhoneNumber);
        phone_number_input_confirm.setText(localPhoneNumber);
    }

    ///////////////////////////////////////////////////////////////////////////
    // MobileOperatorAdapter
    ///////////////////////////////////////////////////////////////////////////

    class MobileOperatorItem {
        @OperatorCode String code;
        @StringRes int titleRes;
        @DrawableRes int iconRes;

        MobileOperatorItem(@OperatorCode String code, @StringRes int titleRes, @DrawableRes int iconRes) {
            this.code = code;
            this.titleRes = titleRes;
            this.iconRes = iconRes;
        }
    }

    class MobileOperatorAdapter extends BaseAdapter {
        final MobileOperatorItem[] items = new MobileOperatorItem[]{
                new MobileOperatorItem(OperatorCode.ACTIV, R.string.label_mobile_operator_activ, R.drawable.ic_mobile_operator_activ),
                new MobileOperatorItem(OperatorCode.KCELL, R.string.label_mobile_operator_kcell, R.drawable.ic_mobile_operator_kcell),
                new MobileOperatorItem(OperatorCode.BEELINE, R.string.label_mobile_operator_beeline, R.drawable.ic_mobile_operator_beeline),
                new MobileOperatorItem(OperatorCode.TELE2, R.string.label_mobile_operator_tele2, R.drawable.ic_mobile_operator_tele2),
                new MobileOperatorItem(OperatorCode.ALTEL, R.string.label_mobile_operator_altel, R.drawable.ic_mobile_operator_altel),
        };

        public int getPlaceholderPosition() {
            return items.length;
        }

        @OperatorCode
        public String getOperatorCode(int position) {
            return position != getPlaceholderPosition() ? items[position].code : null;
        }

        @Override
        public int getCount() {
            return items.length;
        }

        @Override
        public MobileOperatorItem getItem(int position) {
            return position != getPlaceholderPosition() ? items[position] : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(MobileBalanceTransferActivity.this);
                convertView = inflater.inflate(R.layout.mobile_operator_dropdown_item, parent, false);
            }
            TextView textView = (TextView) convertView;
            if (position == getPlaceholderPosition()) {
                textView.setText(R.string.label_mobile_operator_dropdown_placeholder);
                textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            } else {
                MobileOperatorItem item = items[position];
                textView.setText(item.titleRes);
                textView.setCompoundDrawablesWithIntrinsicBounds(item.iconRes, 0, 0, 0);
            }
            return convertView;
        }
    }
}
