package com.nurmash.nurmash.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.main.HotPromoSlidePresenter;
import com.nurmash.nurmash.mvp.main.HotPromoSlideView;
import com.nurmash.nurmash.ui.promotion.PromoDetailActivity;
import com.nurmash.nurmash.util.ApiUtils;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import butterknife.ButterKnife;

import static butterknife.ButterKnife.findById;

public class PromotionsTabFragment extends Fragment implements HotPromoSlideView, AppBarLayout.OnOffsetChangedListener {
    private HotPromoSlidePresenter hotPromoSlidePresenter;
    private PagerAdapter pagerAdapter;
    private PagerAdapter hotPromoPagerAdapter;
    private ViewGroup loading_indicator;

    public static PromotionsTabFragment newInstance() {
        PromotionsTabFragment fragment = new PromotionsTabFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pagerAdapter = createPagerAdapter(getChildFragmentManager());
        hotPromoSlidePresenter = new HotPromoSlidePresenter();
        hotPromoPagerAdapter = new FragmentArrayAdapter(getChildFragmentManager());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_promotions_tab, container, false);
        loading_indicator = findById(rootView, R.id.loading_indicator);
        ViewPager viewPager = findById(rootView, R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        ViewPager vp_hot_pager = findById(rootView, R.id.vp_hot_pager);

        CirclePageIndicator mIndicator = findById(rootView, R.id.vpindicator_hot_pager);
        mIndicator.setRadius(3 * getResources().getDisplayMetrics().density);
        vp_hot_pager.setAdapter(hotPromoPagerAdapter);
        mIndicator.setViewPager(vp_hot_pager);
        viewPager.bringToFront();

        TabLayout tabs = findById(rootView, R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        AppBarLayout appbar_layout = findById(rootView, R.id.appbar_layout);
        appbar_layout.addOnOffsetChangedListener(this);
        hotPromoSlidePresenter.attachView(this);
        return rootView;
    }

    private PagerAdapter createPagerAdapter(FragmentManager fragmentManager) {
        PromoListFragment fragment = PromoListFragment.newInstance(ApiUtils.FILTER_NEW_PROMOTIONS);
        fragment.setCallback(new PromoListFragment.Callback() {
            @Override
            public void showLoadingIndicator(boolean show) {
                loading_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
            }

            @Override
            public void addPromoListItems(List<Promotion> promotions) {
                ((FragmentArrayAdapter)hotPromoPagerAdapter).clearFragments();
                for (int i = 0; i < promotions.size(); i++) {
                    HotPromoPageFragment hotPromoPage = HotPromoPageFragment.newInstance(promotions.get(i));
                    hotPromoPage.setCallback(new HotPromoPageFragment.Callback() {
                        @Override
                        public void onPromotionClick(Promotion promotion) {
                            hotPromoSlidePresenter.onPromoClick(promotion);
                        }
                    });
                    ((FragmentArrayAdapter)hotPromoPagerAdapter).addFragment(hotPromoPage);
                }
                hotPromoPagerAdapter.notifyDataSetChanged();
            }
        });

        FragmentArrayAdapter pagerAdapter = new FragmentArrayAdapter(fragmentManager);
        pagerAdapter.addFragment(fragment, getString(R.string.label_new_tab));
        pagerAdapter.addFragment(PromoListFragment.newInstance(ApiUtils.FILTER_MY_PROMOTIONS), getString(R.string.label_participating_tab));
        pagerAdapter.addFragment(PromoListFragment.newInstance(ApiUtils.FILTER_PAST_PROMOTIONS), getString(R.string.label_completed_tab));
        return pagerAdapter;
    }

    @Override
    public void onDestroyView() {
        hotPromoSlidePresenter.detachView();
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        enableDisableSwipeRefresh(verticalOffset);
    }

    private void enableDisableSwipeRefresh(int verticalOffset) {
        FragmentArrayAdapter pager = (FragmentArrayAdapter) pagerAdapter;
        for (int i = 0; i < pager.getCount(); i++) {
            if (((PromoListFragment) pager.getItem(i)).swipeRefreshLayout != null) {
                ((PromoListFragment) pager.getItem(i)).swipeRefreshLayout.setEnabled(verticalOffset == 0);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // HotPromoSlideView methods
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void openPromoDetailActivity(Promotion promo) {
        startActivity(PromoDetailActivity.newIntent(getActivity(), promo));
    }
}
