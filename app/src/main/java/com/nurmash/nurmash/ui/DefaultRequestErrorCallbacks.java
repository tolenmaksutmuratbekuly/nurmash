package com.nurmash.nurmash.ui;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.mvp.error.RequestErrorCallbacks;
import com.nurmash.nurmash.ui.login.LoginActivity;
import com.nurmash.nurmash.util.Validate;

import static com.nurmash.nurmash.util.SnackbarUtils.multilineSnackbar;

public class DefaultRequestErrorCallbacks implements RequestErrorCallbacks {
    private final Context context;
    private final View snackbarParentView;

    public DefaultRequestErrorCallbacks(Context context) {
        this(context, null);
    }

    public DefaultRequestErrorCallbacks(Context context, View snackbarParentView) {
        this.context = context;
        this.snackbarParentView = snackbarParentView;
    }

    @Override
    public void onAuthTokenInvalidated() {
        Validate.runningOnUiThread();
        context.startActivity(LoginActivity.newIntentForAuthTokenExpiredError(context));
    }

    @Override
    public void onUserBannedError() {
        Validate.runningOnUiThread();
        context.startActivity(LoginActivity.newIntentForUserBannedError(context));
    }

    @Override
    public void onNetworkError() {
        displayErrorMessage(R.string.error_message_generic_network);
    }

    @Override
    public void onRequestFailed() {
        displayErrorMessage(R.string.error_message_generic_request);
    }

    @Override
    public void onInternalServerError() {
        displayErrorMessage(R.string.error_message_server_internal);
    }

    @Override
    public void onPromoHasEndedError() {
        displayErrorMessage(R.string.error_message_promo_has_ended);
    }

    @Override
    public void onPromoNotFoundError() {
        displayErrorMessage(R.string.error_message_promo_not_found);
    }

    private void displayErrorMessage(@StringRes int resId) {
        Validate.runningOnUiThread();
        if (snackbarParentView != null) {
            final Snackbar snackbar = multilineSnackbar(snackbarParentView, resId, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(R.string.action_dismiss, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        } else {
            Toast.makeText(context, resId, Toast.LENGTH_LONG).show();
        }
    }
}
