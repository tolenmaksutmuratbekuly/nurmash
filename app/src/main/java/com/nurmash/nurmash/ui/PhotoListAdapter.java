package com.nurmash.nurmash.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.nurmash.nurmash.util.recyclerview.SimpleViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int USER_ITEM_LAYOUT_ID = R.layout.user_list_item;
    private static final int PHOTO_ITEM_LAYOUT_ID = R.layout.photo_list_item;
    private static final int LOADING_INDICATOR_LAYOUT_ID = R.layout.photo_list_loading_item;

    // WARNING: Don't forget to call Picasso.cancelTag
    public final Object picassoTag;

    private final int itemLayoutId;
    private boolean loading;
    private boolean isShowLikeCount;
    private List<Competitor> competitors = new ArrayList<>();
    private Callback callback;

    public PhotoListAdapter(boolean photoList, boolean isShowLikeCount) {
        picassoTag = this;
        this.isShowLikeCount = isShowLikeCount;
        if (photoList) {
            itemLayoutId = PHOTO_ITEM_LAYOUT_ID;
        } else {
            itemLayoutId = USER_ITEM_LAYOUT_ID;
        }
    }

    public void setLoading(boolean loading) {
        if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(competitors.size());
        } else if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(competitors.size());
        }
    }

    public void setCompetitors(List<Competitor> newCompetitors) {
        if (newCompetitors == null || newCompetitors.isEmpty()) {
            competitors = new ArrayList<>();
        } else {
            competitors = new ArrayList<>(newCompetitors);
        }
        notifyDataSetChanged();
    }

    public void addCompetitors(List<Competitor> newCompetitors) {
        if (newCompetitors == null || newCompetitors.isEmpty()) {
            return;
        }
        int positionStart = competitors.size();
        int itemCount = newCompetitors.size();
        competitors.addAll(newCompetitors);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private int getLoadingIndicatorPosition() {
        return competitors.size();
    }

    private int getCompetitorIndex(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return competitors.size() + (loading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getLoadingIndicatorPosition()) {
            return LOADING_INDICATOR_LAYOUT_ID;
        } else {
            return itemLayoutId;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case USER_ITEM_LAYOUT_ID:
                return new UserViewHolder(view);
            case PHOTO_ITEM_LAYOUT_ID:
                return new PhotoViewHolder(view);
            case LOADING_INDICATOR_LAYOUT_ID:
                return new SimpleViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PhotoViewHolder) {
            ((PhotoViewHolder) holder).bindCompetitor(competitors.get(getCompetitorIndex(position)), position);
        } else if (holder instanceof UserViewHolder) {
            ((UserViewHolder) holder).bindCompetitor(competitors.get(getCompetitorIndex(position)), position);
        }
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        Context context;
        Competitor bindedCompetitor;
        int mPosition;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.display_name) TextView nameView;

        UserViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindCompetitor(Competitor competitor, int position) {
            bindedCompetitor = competitor;
            mPosition = position;
            User user = competitor == null ? null : competitor.user;
            if (user != null) {
                Picasso.with(context).load(PhotoUrl.l(user.photo))
                        .fit()
                        .centerCrop()
                        .transform(Transformations.CIRCLE_NO_BORDER)
                        .tag(picassoTag)
                        .into(photoView);
                nameView.setText(user.getDisplayName());
            } else {
                photoView.setImageResource(R.drawable.bg_placeholder_gradient);
                nameView.setText(null);
            }
        }

        @OnClick(R.id.item_view)
        void onItemClick() {
            if (callback != null) callback.onCompetitorClick(bindedCompetitor, mPosition);
        }
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {
        Context context;
        Competitor bindedCompetitor;
        int mPosition;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.like_count) TextView likeCountView;

        PhotoViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindCompetitor(Competitor competitor, int position) {
            bindedCompetitor = competitor;
            mPosition = position;
            if (competitor != null && competitor.isDisqualified()) {
                photoView.setBackgroundColor(Color.WHITE);
                photoView.setImageResource(R.drawable.ic_photo_disqual_placeholder);
                likeCountView.setVisibility(View.GONE);
            } else if (competitor != null && competitor.getPhotoHash() != null && !competitor.getPhotoHash().isEmpty()) {
                photoView.setBackgroundColor(Color.TRANSPARENT);
                Picasso.with(context).load(PhotoUrl.xl(competitor.getPhotoHash()))
                        .placeholder(R.drawable.bg_placeholder_gradient)
                        .fit()
                        .centerCrop()
                        .tag(picassoTag)
                        .into(photoView);

                // Like count
                if (!isShowLikeCount || competitor.like_count == 0) {
                    likeCountView.setVisibility(View.GONE);
                } else {
                    likeCountView.setVisibility(View.VISIBLE);
                    likeCountView.setText(competitor.like_count + "");
                    likeCountView.setCompoundDrawablesWithIntrinsicBounds(competitor.is_liked ? R.drawable.ic_photo_likes_count_active_white
                            : R.drawable.ic_photo_likes_count_inactive, 0, 0, 0);
                }
            } else {
                photoView.setBackgroundColor(Color.TRANSPARENT);
                photoView.setImageResource(R.drawable.bg_placeholder_gradient);
            }
        }

        @OnClick(R.id.photo)
        void onPhotoClick() {
            if (callback != null) callback.onCompetitorClick(bindedCompetitor, mPosition);
        }
    }

    public interface Callback {
        void onCompetitorClick(Competitor competitor, int competitorIndex);
    }
}
