package com.nurmash.nurmash.ui.participate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Survey;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.participate.SurveyStepPresenter;
import com.nurmash.nurmash.mvp.participate.SurveyStepView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.ui.photo.PhotoDetailActivity;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.text.TextUtils.isEmpty;

public class SurveyStepActivity extends BaseActivity implements SurveyStepView {
    SurveyStepPresenter surveyStepPresenter;
    SurveyStepContentAdapter contentAdapter;
    @Bind(R.id.content_view) ViewGroup contentView;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;
    @Bind(R.id.commit_progress_bar_overlay) ViewGroup commitProgressBarOverlay;
    @Bind(R.id.button_next) Button nextButton;

    public static Intent newIntent(Context packageContext, long promoId) {
        Intent intent = new Intent(packageContext, SurveyStepActivity.class);
        intent.putExtra("promoId", promoId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_step);
        ButterKnife.bind(this);

        nextButton.setVisibility(View.GONE);
        nextButton.setEnabled(false);
        configureRecyclerView();

        // Presenter
        long promoId = getIntent().getLongExtra("promoId", 0);
        surveyStepPresenter = new SurveyStepPresenter(DataLayer.getInstance(this), promoId);
        surveyStepPresenter.attachView(this);
        surveyStepPresenter.setErrorHandler(new RequestErrorHandler(new DefaultRequestErrorCallbacks(this, contentView)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        surveyStepPresenter.reloadParticipationData();
    }

    @Override
    protected void onDestroy() {
        surveyStepPresenter.setErrorHandler(null);
        surveyStepPresenter.detachView();
        super.onDestroy();
    }

    private void configureRecyclerView() {
        contentAdapter = new SurveyStepContentAdapter();
        contentAdapter.setCallback(new SurveyStepContentAdapter.Callback() {
            @Override
            public void onAnswerClick(String answerId) {
                surveyStepPresenter.onAnswerClick(answerId);
            }

            @Override
            public void onAnswerClick(Survey.Answer answer, String childrenId) {
                surveyStepPresenter.onAnswerClick(answer, childrenId);
            }
        });
        recyclerView.setAdapter(contentAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    @OnClick(R.id.button_next)
    public void onNextClick() {
        surveyStepPresenter.onNextClick();
    }

    // SurveyStepView implementation
    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNode(Survey.Node node) {
        ArrayList<Survey.Answer> answerList = new ArrayList<>();
        for (int i = 0; i < node.answers.length; i++) {
            if (!isEmpty(node.answers[i].id) && !isEmpty(node.answers[i].title)) {
                answerList.add(node.answers[i]);
            }
        }
        contentAdapter.setQuestion(node.question, "first_tree_question_id",answerList);
    }

    @Override
    public void setChildren(Survey.Children[] children) {
        ArrayList<Survey.Answer> answerList = new ArrayList<>();
        for (int i = 0; i < children[0].answers.length; i++) {
            if (!isEmpty(children[0].answers[i].id) && !isEmpty(children[0].answers[i].title)) {
                answerList.add(children[0].answers[i]);
            }
        }
        contentAdapter.setQuestion(children[0].question, children[0].id, answerList);
    }

    @Override
    public void setSelectedAnswerId(String answerId) {
        contentAdapter.setSelectedAnswerId(answerId);
    }

    @Override
    public void showNextButtonState(boolean show) {
        nextButton.setVisibility(show ? View.VISIBLE : View.GONE);
        nextButton.setEnabled(show);
    }

    @Override
    public void setSurveyType(String type) {
        contentAdapter.setSurveyType(type);
    }

    @Override
    public void showCommitLoadingIndicator(boolean show) {
        commitProgressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openMyPhotoActivity(long competitorId) {
        startActivity(PhotoDetailActivity.newIntent(this, competitorId));
    }
}
