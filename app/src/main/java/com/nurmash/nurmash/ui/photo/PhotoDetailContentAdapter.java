package com.nurmash.nurmash.ui.photo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.model.PhotoUrl;
import com.nurmash.nurmash.model.json.Comment;
import com.nurmash.nurmash.model.json.Competitor;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.model.json.User;
import com.nurmash.nurmash.util.picasso.Transformations;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class PhotoDetailContentAdapter extends BaseAdapter implements StickyListHeadersAdapter {
    private static final int PHOTO_ITEM_LAYOUT_ID = R.layout.photo_detail_list_item;
    private static final int PHOTO_ITEM_HEADER_LAYOUT_ID = R.layout.photo_detail_list_item_header;
    private List<Competitor> competitors = Collections.emptyList();
    long authUserId;
    private Callbacks callbacks;
    private final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    final long DOUBLE_CLICK_TIME = 300;
    private long lastClickTime = 0;
    private Context context;

    public PhotoDetailContentAdapter(Context context) {
        this.context = context;
    }

    public void setAuthUserId(long authUserId) {
        this.authUserId = authUserId;
    }

    public void setCompetitors(List<Competitor> newCompetitors) {
        if (newCompetitors == null || newCompetitors.isEmpty()) {
            competitors = new ArrayList<>();
        } else {
            competitors = new ArrayList<>(newCompetitors);
        }
        notifyDataSetChanged();
    }

    public void addCompetitors(List<Competitor> newCompetitors) {
        if (newCompetitors == null || newCompetitors.isEmpty()) {
            return;
        }
        competitors.addAll(newCompetitors);
        notifyDataSetChanged();

    }

    public void refreshItem() {
        notifyDataSetChanged();
    }

    public void setCallbacks(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    private boolean hasComments(Competitor competitor) {
        return competitor != null && (competitor.hasDescription() || competitor.hasComments());
    }

    @Override
    public int getCount() {
        return competitors.size();
    }

    @Override
    public Object getItem(int position) {
        return competitors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int getCompetitorIndex(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(PHOTO_ITEM_LAYOUT_ID, parent, false);
            convertView.setTag(new DetailViewHolder(convertView));
        }

        DetailViewHolder viewHolder = (DetailViewHolder) convertView.getTag();
        viewHolder.bindCompetitor(competitors.get(getCompetitorIndex(position)), position);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return position;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(PHOTO_ITEM_HEADER_LAYOUT_ID, parent, false);
            convertView.setTag(new HeaderDetailViewHolder(convertView));
        }

        HeaderDetailViewHolder viewHolder = (HeaderDetailViewHolder) convertView.getTag();
        viewHolder.bindCompetitor(competitors.get(getCompetitorIndex(position)), position);
        return convertView;
    }

    private void bindCommentBody(TextView textView, User user, String body, ClickableSpan clickableSpan, long competitor_id) {
        String name = user != null ? user.getDisplayName() : null;
        if (name == null || name.length() == 0) {
            if (body == null || body.length() == 0) {
                Timber.w(new Exception("Both user's name and comment body are empty."),
                        "competitor: %d", competitor_id);
                return;
            }
            name = "NO_NAME";
        }
        Context context = textView.getContext();
        SpannableString spannable = new SpannableString(name + " " + body);
        TextAppearanceSpan nameAppearanceSpan = new TextAppearanceSpan(context,
                R.style.Photo_Comment_UserName_TextAppearance);
        spannable.setSpan(clickableSpan, 0, name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(nameAppearanceSpan, 0, name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannable);
    }

    private boolean isMyPhoto(Competitor competitor) {
        return competitor != null && competitor.getUserId() == this.authUserId;
    }

    class DetailViewHolder extends RecyclerView.ViewHolder {
        Competitor bindedCompetitor;
        int competitorPosition;
        boolean canLike;
        boolean canComment;
        boolean canReport;
        Context context;
        @Bind(R.id.promo_title) TextView promoTitleView;
        @Bind(R.id.photo) ImageView photoView;
        @Bind(R.id.view_like) View view_like;
        @Bind(R.id.img_like_photo) ImageView img_like_photo;
        @Bind(R.id.photo_loading_indicator) ProgressBar photoLoadingIndicator;
        @Bind(R.id.disqual_placeholder) TextView disqualPlaceholderView;
        @Bind(R.id.like_count) TextView likeCountView;
        @Bind(R.id.like_count_gradient_background) View likeCountBgView;
        @Bind(R.id.description) TextView descriptionView;
        @Bind(R.id.like_button) View likeButton;
        @Bind(R.id.like_button_text) TextView likeButtonText;
        @Bind(R.id.comment_button) View commentButton;
        @Bind(R.id.options_button) View optionsButton;
        @Bind(R.id.promo_ended_info) View promoEndedInfoView;
        @Bind(R.id.more_comments) TextView moreCommentsView;
        @Bind(R.id.ll_comments_portion) LinearLayout ll_comments_portion;

        class SpanClick extends ClickableSpan {
            private User user;

            public SpanClick(User user) {
                this.user = user;
            }

            @Override
            public void onClick(View widget) {
                handleUserClick(user);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        }

        public DetailViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
            // Make ClickableSpans clickable
            descriptionView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        void bindCompetitor(Competitor newCompetitor, int position) {
            Competitor oldCompetitor = bindedCompetitor;
            bindedCompetitor = newCompetitor;
            competitorPosition = position;

            // Promo photo banner
            Promotion newPromo = newCompetitor == null ? null : newCompetitor.promo;
            int promoState = newPromo == null ? Promotion.STATE_UNKNOWN : newPromo.getState(System.currentTimeMillis());
            boolean isPromoActive = Promotion.isActiveState(promoState);
            canLike = newCompetitor != null && newCompetitor.canLike() && isPromoActive;
            canComment = newCompetitor != null && newCompetitor.canComment() && isPromoActive;
            canReport = newCompetitor != null && newCompetitor.canReport() && !isMyPhoto(newCompetitor) && isPromoActive;

            // Promo ended indicator
            promoEndedInfoView.setVisibility(isPromoActive ? GONE : VISIBLE);

            // Promo title
            String promoTitle = newPromo == null ? null : newPromo.title;
            promoTitleView.setText(promoTitle);

            // Action buttons
            if (newCompetitor != null && newPromo != null) {
                // Like button
                likeButton.setVisibility(VISIBLE);
                likeButton.setEnabled(canLike);
                if (newCompetitor.is_liked) {
                    likeButtonText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_like_button_active, 0, 0, 0);
                } else {
                    likeButtonText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_like_button_inactive, 0, 0, 0);
                }
                // Comment button
                commentButton.setVisibility(VISIBLE);
                commentButton.setEnabled(canComment);
                // Options button
                optionsButton.setVisibility(VISIBLE);
            } else {
                likeButton.setVisibility(GONE);
                commentButton.setVisibility(GONE);
                optionsButton.setVisibility(GONE);
            }

            // Photo
            updateCompetitorPhotoEntry(newCompetitor, oldCompetitor);

            Resources res = context.getResources();
            // Like count
            if (newCompetitor != null) {
                likeCountView.setText(res.getQuantityString(R.plurals.like_count, newCompetitor.like_count, newCompetitor.like_count));
                if (newCompetitor.is_liked) {
                    likeCountView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_likes_count_active, 0, 0, 0);
                } else {
                    likeCountView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_likes_count_inactive, 0, 0, 0);
                }
            } else {
                likeCountView.setText(null);
                likeCountView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            // Photo description
            String description = newCompetitor == null ? null : newCompetitor.getPhotoDescription();
            if (description != null && description.length() != 0) {
                descriptionView.setVisibility(VISIBLE);
                bindCommentBody(descriptionView, newCompetitor.user, description, new SpanClick(bindedCompetitor != null ? bindedCompetitor.user : null), newCompetitor.id);
            } else {
                descriptionView.setVisibility(GONE);
            }

            // See all comments
            if (hasComments(newCompetitor)) {
                moreCommentsView.setVisibility(View.VISIBLE);
                int count = newCompetitor.getCommentCountIncludingDescription();
                moreCommentsView.setText(context.getString(R.string.action_photo_comments_see_all, count));
                bindComments(newCompetitor);

            } else
                moreCommentsView.setVisibility(View.GONE);
        }

        void updateCompetitorPhotoEntry(Competitor newCompetitor, Competitor oldCompetitor) {
            if (newCompetitor != null && newCompetitor.isDisqualified()) {
                photoView.setVisibility(INVISIBLE);
                photoLoadingIndicator.setVisibility(GONE);
                likeCountView.setVisibility(GONE);
                likeCountBgView.setVisibility(GONE);
                disqualPlaceholderView.setVisibility(VISIBLE);
            } else if (newCompetitor != null && newCompetitor.hasPhoto()) {
                String oldPhotoHash = oldCompetitor == null ? null : oldCompetitor.getPhotoHash();
                String newPhotoHash = newCompetitor.getPhotoHash();
                // Check if we previously loaded the same photo, so that we don't try to reload it again.
                if (!newPhotoHash.equals(oldPhotoHash) && !newPhotoHash.isEmpty()) {
                    photoView.setVisibility(VISIBLE);
                    photoLoadingIndicator.setVisibility(VISIBLE);
                    disqualPlaceholderView.setVisibility(GONE);
                    Picasso picasso = Picasso.with(context);
                    picasso.cancelRequest(photoView);
                    picasso.load(PhotoUrl.xxl(newPhotoHash))
                            .tag(context)
                            .fit()
                            .centerCrop()
                            .into(photoView, new Callback() {
                                @Override
                                public void onSuccess() {
                                    photoLoadingIndicator.setVisibility(GONE);
                                }

                                @Override
                                public void onError() {
                                    photoLoadingIndicator.setVisibility(GONE);
                                }
                            });
                }
            } else {
                photoView.setVisibility(INVISIBLE);
                photoLoadingIndicator.setVisibility(GONE);
                likeCountView.setVisibility(GONE);
                likeCountBgView.setVisibility(GONE);
                disqualPlaceholderView.setVisibility(GONE);
            }
        }

        void bindComments(Competitor competitor) {
            List<Comment> comments;
            if (competitor.last_comments != null) {
                comments = new ArrayList<>(Arrays.asList(competitor.last_comments));
                Collections.reverse(comments);
            } else {
                comments = Collections.emptyList();
            }

            ll_comments_portion.removeAllViews();
            for (Comment comment : comments) {
                if (comment != null) {
                    View child = ((Activity) context).getLayoutInflater().inflate(R.layout.photo_detail_comment_item, ll_comments_portion, false);
                    ll_comments_portion.addView(child);
                    TextView textView = (TextView) child;
                    // Make ClickableSpans clickable
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    bindCommentBody(textView, comment.user, comment.body, new SpanClick(comment.user), competitor.id);
                }
            }
        }

        void likePhotoAnimate() {
            view_like.setVisibility(View.VISIBLE);
            img_like_photo.setVisibility(View.VISIBLE);

            view_like.setScaleY(0.1f);
            view_like.setScaleX(0.1f);
            view_like.setAlpha(1f);
            img_like_photo.setScaleY(0.1f);
            img_like_photo.setScaleX(0.1f);

            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator bgScaleYAnim = getObjectAnimator(view_like, "scaleY", 0.1f, 1f, 200, true, 0);
            ObjectAnimator bgScaleXAnim = getObjectAnimator(view_like, "scaleX", 0.1f, 1f, 200, true, 0);
            ObjectAnimator bgAlphaAnim = getObjectAnimator(view_like, "alpha", 1f, 0f, 200, true, 150);

            ObjectAnimator imgScaleUpYAnim = getObjectAnimator(img_like_photo, "scaleY", 0.1f, 1f, 300, true, 0);
            ObjectAnimator imgScaleUpXAnim = getObjectAnimator(img_like_photo, "scaleX", 0.1f, 1f, 300, true, 0);
            ObjectAnimator imgScaleDownYAnim = getObjectAnimator(img_like_photo, "scaleY", 1f, 0f, 300, false, 0);
            ObjectAnimator imgScaleDownXAnim = getObjectAnimator(img_like_photo, "scaleX", 1f, 0f, 300, false, 0);

            animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
            animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view_like.setVisibility(View.GONE);
                    img_like_photo.setVisibility(View.GONE);
                }
            });
            animatorSet.start();
        }

        ObjectAnimator getObjectAnimator(View view, String coordinate, float animStart, float animEnd, int duration, boolean isDeccelerate, int startDelay) {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, coordinate, animStart, animEnd);
            objectAnimator.setDuration(duration);
            objectAnimator.setInterpolator(isDeccelerate ? DECCELERATE_INTERPOLATOR : ACCELERATE_INTERPOLATOR);
            if (startDelay != 0)
                objectAnimator.setStartDelay(startDelay);

            return objectAnimator;

        }

        @OnClick(R.id.photo)
        void onLikeAnimaionClick() {
            long clickTime = System.currentTimeMillis();
            if (clickTime - lastClickTime < DOUBLE_CLICK_TIME) {
                if (callbacks != null) {
                    likePhotoAnimate();
                    callbacks.onLikeButtonClick(bindedCompetitor, competitorPosition);
                }
            }

            lastClickTime = clickTime;
        }

        @OnClick(R.id.like_button)
        void onLikeButtonClick() {
            if (callbacks != null) {
                callbacks.onLikeButtonClick(bindedCompetitor, competitorPosition);
            }
        }

        @OnClick(R.id.like_count)
        void onCompetitorLikes() {
            if (callbacks != null) {
                if (bindedCompetitor.like_count > 0) {
                    callbacks.onCompetitorLikesButtonClick(bindedCompetitor);
                }
            }
        }

        @OnClick(R.id.comment_button)
        void onCommentButtonClick() {
            if (callbacks != null) {
                callbacks.onCommentButtonClick(bindedCompetitor);
            }
        }

        @OnClick(R.id.options_button)
        void onOptionsClick() {
            final PopupMenu popupMenu = new PopupMenu(context, optionsButton);
            popupMenu.inflate(R.menu.photo_detail_more_options_popup);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    popupMenu.dismiss();
                    switch (item.getItemId()) {
                        case R.id.report:
                            if (callbacks != null) {
                                callbacks.onReportButtonClick(bindedCompetitor, competitorPosition);
                            }
                            return true;
                        case R.id.open_promo:
                            if (callbacks != null) {
                                callbacks.onOpenPromoButtonClick(bindedCompetitor);
                            }
                            return true;
                    }
                    return false;
                }
            });
            // Hide/show report menu item if necessary.
            MenuItem reportItem = popupMenu.getMenu().findItem(R.id.report);
            if (reportItem != null) {
                reportItem.setVisible(canReport);
            }
            popupMenu.show();
        }

        @OnClick(R.id.more_comments)
        void onMoreCommentsClick() {
            if (callbacks != null) {
                callbacks.onMoreCommentsButtonClick(bindedCompetitor);
            }
        }
    }

    class HeaderDetailViewHolder extends RecyclerView.ViewHolder {
        Competitor bindedCompetitor;
        int competitorPosition;
        Context context;
        @Bind(R.id.timestamp) TextView timestampView;
        @Bind(R.id.user_photo) ImageView userPhotoView;
        @Bind(R.id.user_display_name) TextView userDisplayNameView;
        PrettyTime prettyTime = new PrettyTime();

        public HeaderDetailViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindCompetitor(Competitor newCompetitor, int position) {
            Competitor oldCompetitor = bindedCompetitor;
            bindedCompetitor = newCompetitor;
            competitorPosition = position;

            // Timestamp
            if (newCompetitor != null && newCompetitor.completed_at != null) {
                timestampView.setText(prettyTime.format(newCompetitor.completed_at));
            } else {
                timestampView.setText(null);
            }

            // User avatar
            updateUserAvatar(newCompetitor, oldCompetitor);

            // User name
            if (newCompetitor != null && newCompetitor.user != null) {
                userDisplayNameView.setText(newCompetitor.user.getDisplayName());

            } else {
                userDisplayNameView.setText(null);
            }
        }

        void updateUserAvatar(Competitor newCompetitor, Competitor oldCompetitor) {
            String newPhotoHash = newCompetitor == null ? null : newCompetitor.getUserPhotoHash();
            if (newPhotoHash != null && newPhotoHash.length() != 0) {
                String oldPhotoHash = oldCompetitor == null ? null : oldCompetitor.getUserPhotoHash();
                // Check if we previously loaded the same photo, so that we don't try to reload it again.
                if (!newPhotoHash.equals(oldPhotoHash) && !newPhotoHash.isEmpty()) {
                    userPhotoView.setVisibility(VISIBLE);
                    Picasso.with(context).load(PhotoUrl.l(newPhotoHash))
                            .fit()
                            .transform(Transformations.CIRCLE_NO_BORDER)
                            .into(userPhotoView);
                }
            } else {
                userPhotoView.setVisibility(GONE);
            }
        }

        @OnClick({R.id.user_photo, R.id.user_display_name})
        void onUserClick() {
            if (bindedCompetitor != null) {
                handleUserClick(bindedCompetitor.user);
            }
        }
    }

    void handleUserClick(User user) {
        if (callbacks != null && user != null) {
            callbacks.onUserClick(user);
        }
    }

    public interface Callbacks {
        void onUserClick(User user);

        void onLikeButtonClick(Competitor competitor, int competitorPosition);

        void onCommentButtonClick(Competitor competitor);

        void onCompetitorLikesButtonClick(Competitor competitor);

        void onReportButtonClick(Competitor competitor, int competitorPosition);

        void onOpenPromoButtonClick(Competitor competitor);

        void onMoreCommentsButtonClick(Competitor competitor);
    }
}
