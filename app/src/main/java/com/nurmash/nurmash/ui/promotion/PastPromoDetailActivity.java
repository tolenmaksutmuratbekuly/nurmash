package com.nurmash.nurmash.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.json.Promotion;
import com.nurmash.nurmash.mvp.error.RequestErrorHandler;
import com.nurmash.nurmash.mvp.promotion.PastPromoDetailPresenter;
import com.nurmash.nurmash.mvp.promotion.PastPromoDetailView;
import com.nurmash.nurmash.ui.BaseActivity;
import com.nurmash.nurmash.ui.DefaultRequestErrorCallbacks;
import com.nurmash.nurmash.util.support.FragmentArrayAdapter;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.squareup.picasso.MemoryPolicy.NO_CACHE;

public class PastPromoDetailActivity extends BaseActivity implements PastPromoDetailView {
    PastPromoDetailPresenter pastPromoDetailPresenter;
    Promotion promo;
    @Bind(R.id.photo) ImageView photoView;
    @Bind(R.id.title) TextView titleView;
    @Bind(R.id.pager) ViewPager viewPager;
    @Bind(R.id.progress_bar_overlay) ViewGroup progressBarOverlay;

    public static Intent newIntent(Context packageContext, Promotion promo) {
        Intent intent = new Intent(packageContext, PastPromoDetailActivity.class);
        intent.putExtra("promo", Parcels.wrap(promo));
        return intent;
    }

    public static Intent newIntent(Context packageContext, long promoId, boolean isPhotoSharingPromo) {
        Intent intent = new Intent(packageContext, PastPromoDetailActivity.class);
        intent.putExtra("promoId", promoId);
        intent.putExtra("isPhotoSharingPromo", isPhotoSharingPromo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_promo_detail);
        ButterKnife.bind(this);
        configureActionBar();
        if (getIntent().hasExtra("promo")) {
            promo = Parcels.unwrap(getIntent().getParcelableExtra("promo"));
            pastPromoDetailPresenter = new PastPromoDetailPresenter(DataLayer.getInstance(this), promo);
            configureViewPager(promo.id, promo.isPhotoSharingPromo());
            configureTabLayout();
        } else {
            long promoId = getIntent().getLongExtra("promoId", 0);
            boolean isPhotoSharingPromo = getIntent().getBooleanExtra("isPhotoSharingPromo", false);
            pastPromoDetailPresenter = new PastPromoDetailPresenter(DataLayer.getInstance(this), promoId);
            configureViewPager(promoId, isPhotoSharingPromo);
            configureTabLayout();
        }
        pastPromoDetailPresenter.setErrorHandler(new RequestErrorHandler(
                new DefaultRequestErrorCallbacks(this, viewPager)));
        pastPromoDetailPresenter.attachView(this);
    }

    private void configureActionBar() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    private void configureViewPager(long promoId, boolean isPhotoSharingPromo) {
        FragmentArrayAdapter adapter = new FragmentArrayAdapter(getSupportFragmentManager());
        adapter.addFragment(WinnerListFragment.newInstance(promoId), getString(R.string.winners_tab_label));
        adapter.addFragment(ParticipantListFragment.newInstance(promoId, isPhotoSharingPromo, promo), getString(R.string.participants_tab_label));
        viewPager.setAdapter(adapter);
    }

    private void configureTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        pastPromoDetailPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        pastPromoDetailPresenter.setErrorHandler(null);
        pastPromoDetailPresenter.detachView();
        super.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PastPromoDetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBarOverlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showTitle(String title) {
        titleView.setText(title);
    }

    @Override
    public void showPhoto(String photoUrl) {
        Picasso.with(this).load(photoUrl)
                .memoryPolicy(NO_CACHE)
                .fit()
                .centerCrop()
                .into(photoView);
    }
}
