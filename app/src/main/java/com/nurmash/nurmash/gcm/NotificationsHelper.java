package com.nurmash.nurmash.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.NonNull;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.push.PushMessage;
import com.nurmash.nurmash.model.push.disqual.DisqualNotificationBuilder;
import com.nurmash.nurmash.model.push.disqual.DisqualPushMessage;
import com.nurmash.nurmash.model.push.photo.CommentAnswerNotificationBuilder;
import com.nurmash.nurmash.model.push.photo.CommentAnswerPushMessage;
import com.nurmash.nurmash.model.push.photo.PhotoCommentNotificationBuilder;
import com.nurmash.nurmash.model.push.photo.PhotoCommentPushMessage;
import com.nurmash.nurmash.model.push.photo.PhotoLikeNotificationBuilder;
import com.nurmash.nurmash.model.push.photo.PhotoLikePushMessage;
import com.nurmash.nurmash.model.push.promo.PromoNotificationBuilder;
import com.nurmash.nurmash.model.push.promo.PromoPushMessage;
import com.nurmash.nurmash.model.push.unbanned.UnBannedUserNotificationBuilder;
import com.nurmash.nurmash.model.push.unbanned.UnBannedUserPushMessage;
import com.nurmash.nurmash.model.push.verify.UserVerifyNotificationBuilder;
import com.nurmash.nurmash.model.push.verify.UserVerifyPushMessage;
import com.nurmash.nurmash.model.push.winnings.WinningsNotificationBuilder;
import com.nurmash.nurmash.model.push.winnings.WinningsPushMessage;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import timber.log.Timber;

public class NotificationsHelper {
    private static final int ID_PROMO = 0;
    private static final int ID_WINNINGS = 1;
    private static final int ID_DISQUAL = 2;
    private static final int ID_PHOTO_LIKE = 3;
    private static final int ID_PHOTO_COMMENT = 4;
    private static final int ID_COMMENT_ANSWER = 5;
    private static final int ID_UNBANNED_USER = 6;
    private static final int ID_VERIFY_USER = 7;

    private final Context context;
    private final DataLayer dataLayer;

    // TODO: 2/29/16 Implement persistent storage for pending push messages, so that we don't lose them when app restarts.
    private final Set<PromoPushMessage> promoPushMessages = new LinkedHashSet<>();
    private final Set<WinningsPushMessage> winningsPushMessages = new LinkedHashSet<>();
    private final Set<DisqualPushMessage> disqualPushMessages = new LinkedHashSet<>();
    private final Set<PhotoLikePushMessage> photoLikePushMessages = new LinkedHashSet<>();
    private final Set<PhotoCommentPushMessage> photoCommentPushMessages = new LinkedHashSet<>();
    private final Set<CommentAnswerPushMessage> commentAnswerPushMessages = new LinkedHashSet<>();
    private final Set<UnBannedUserPushMessage> unBannedUserPushMessages = new LinkedHashSet<>();
    private final Set<UserVerifyPushMessage> verifyUserPushMessages = new LinkedHashSet<>();

    private final PromoNotificationBuilder promoNotificationBuilder;
    private final WinningsNotificationBuilder winningsNotificationBuilder;
    private final DisqualNotificationBuilder disqualNotificationBuilder;
    private final PhotoLikeNotificationBuilder photoLikeNotificationBuilder;
    private final PhotoCommentNotificationBuilder photoCommentNotificationBuilder;
    private final CommentAnswerNotificationBuilder commentAnswerNotificationBuilder;
    private final UnBannedUserNotificationBuilder unBannedUserNotificationBuilder;
    private final UserVerifyNotificationBuilder verifyUserNotificationBuilder;

    public NotificationsHelper(Context context, DataLayer dataLayer) {
        this.context = context.getApplicationContext();
        this.dataLayer = dataLayer;
        this.promoNotificationBuilder = new PromoNotificationBuilder(context, ID_PROMO);
        this.winningsNotificationBuilder = new WinningsNotificationBuilder(context, ID_WINNINGS);
        this.disqualNotificationBuilder = new DisqualNotificationBuilder(context, ID_DISQUAL);
        this.photoLikeNotificationBuilder = new PhotoLikeNotificationBuilder(context, ID_PHOTO_LIKE);
        this.photoCommentNotificationBuilder = new PhotoCommentNotificationBuilder(context, ID_PHOTO_COMMENT);
        this.commentAnswerNotificationBuilder = new CommentAnswerNotificationBuilder(context, ID_COMMENT_ANSWER);
        this.unBannedUserNotificationBuilder = new UnBannedUserNotificationBuilder(context, ID_UNBANNED_USER);
        this.verifyUserNotificationBuilder = new UserVerifyNotificationBuilder(context, ID_VERIFY_USER);
    }

    public void clearAllNotifications() {
        promoPushMessages.clear();
        winningsPushMessages.clear();
        disqualPushMessages.clear();
        photoLikePushMessages.clear();
        photoCommentPushMessages.clear();
        commentAnswerPushMessages.clear();
        unBannedUserPushMessages.clear();
        verifyUserPushMessages.clear();
        NotificationManager notificationManager = getNotificationManager();
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }

    public void handlePushMessage(PushMessage msg) {
        if (!dataLayer.authPreferences().isAuthenticated()) {
            return;
        }
        if (msg == null || msg.user_id != dataLayer.authPreferences().getUserId()) {
            return;
        }

        if (msg instanceof PromoPushMessage) {
            handlePromoPushMessage((PromoPushMessage) msg);
        } else if (msg instanceof WinningsPushMessage) {
            handleWinningsPushMessage((WinningsPushMessage) msg);
        } else if (msg instanceof DisqualPushMessage) {
            handleDisqualPushMessage((DisqualPushMessage) msg);
        } else if (msg instanceof PhotoLikePushMessage) {
            handlePhotoLikePushMessage((PhotoLikePushMessage) msg);
        } else if (msg instanceof PhotoCommentPushMessage) {
            handlePhotoCommentPushMessage((PhotoCommentPushMessage) msg);
        } else if (msg instanceof CommentAnswerPushMessage) {
            handleCommentAnswerPushMessage((CommentAnswerPushMessage) msg);
        } else if (msg instanceof UnBannedUserPushMessage) {
            handleUnBannedUserPushMessage((UnBannedUserPushMessage) msg);
        } else if (msg instanceof UserVerifyPushMessage) {
            handleVerifyUserPushMessage((UserVerifyPushMessage) msg);
        } else {
            throw new IllegalArgumentException("Unsupported PushMessage instance: " + msg.getClass().getName());
        }
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private synchronized void updateNotification(int id, Notification notification) {
        NotificationManager notificationManager = getNotificationManager();
        if (notificationManager == null) {
            return;
        }

        if (notification == null) {
            notificationManager.cancel(id);
        } else {
            try {
                notificationManager.notify(id, notification);
            } catch (RuntimeException e) {
                Timber.e(e, null);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // New Promotions notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearPromoNotification(long promoId) {
        synchronized (promoPushMessages) {
            for (PromoPushMessage msg : promoPushMessages) {
                if (msg.contest_id == promoId) {
                    promoPushMessages.remove(msg);
                    updateNotification(ID_PROMO, promoNotificationBuilder.build(promoPushMessages));
                    return;
                }
            }
        }
    }

    public void clearAllPromoNotifications() {
        synchronized (promoPushMessages) {
            promoPushMessages.clear();
            updateNotification(ID_PROMO, null);
        }
    }

    private void handlePromoPushMessage(@NonNull PromoPushMessage msg) {
        synchronized (promoPushMessages) {
            promoPushMessages.add(msg);
            updateNotification(ID_PROMO, promoNotificationBuilder.build(promoPushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Winnings notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearWinningsNotification(long promoId) {
        synchronized (winningsPushMessages) {
            for (WinningsPushMessage msg : winningsPushMessages) {
                if (msg.contest_id == promoId) {
                    winningsPushMessages.remove(msg);
                    updateNotification(ID_WINNINGS, winningsNotificationBuilder.build(winningsPushMessages));
                    return;
                }
            }
        }
    }

    public void clearAllWinningsNotifications() {
        synchronized (winningsPushMessages) {
            winningsPushMessages.clear();
            updateNotification(ID_WINNINGS, null);
        }
    }

    private void handleWinningsPushMessage(@NonNull WinningsPushMessage msg) {
        synchronized (winningsPushMessages) {
            winningsPushMessages.add(msg);
            updateNotification(ID_WINNINGS, winningsNotificationBuilder.build(winningsPushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Disqualification notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearAllDisqualNotifications() {
        synchronized (disqualPushMessages) {
            disqualPushMessages.clear();
            updateNotification(ID_DISQUAL, null);
        }
    }

    public void clearDisqualNotification(long promoId) {
        synchronized (disqualPushMessages) {
            for (DisqualPushMessage msg : disqualPushMessages) {
                if (msg.contest_id == promoId) {
                    disqualPushMessages.remove(msg);
                    updateNotification(ID_DISQUAL, disqualNotificationBuilder.build(disqualPushMessages));
                    return;
                }
            }
        }
    }

    private void handleDisqualPushMessage(@NonNull DisqualPushMessage msg) {
        synchronized (disqualPushMessages) {
            disqualPushMessages.add(msg);
            updateNotification(ID_DISQUAL, disqualNotificationBuilder.build(disqualPushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Photo Like notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearAllPhotoLikeNotifications() {
        synchronized (photoLikePushMessages) {
            photoLikePushMessages.clear();
            updateNotification(ID_PHOTO_LIKE, null);
        }
    }

    public void clearPhotoLikeNotifications(long competitorId) {
        synchronized (photoLikePushMessages) {
            Iterator<PhotoLikePushMessage> iterator = photoLikePushMessages.iterator();
            while (iterator.hasNext()) {
                PhotoLikePushMessage msg = iterator.next();
                if (msg.competitor_id == competitorId) {
                    iterator.remove();
                }
            }
            updateNotification(ID_PHOTO_LIKE, photoLikeNotificationBuilder.build(photoLikePushMessages));
        }
    }

    private void handlePhotoLikePushMessage(@NonNull PhotoLikePushMessage msg) {
        synchronized (photoLikePushMessages) {
            photoLikePushMessages.add(msg);
            updateNotification(ID_PHOTO_LIKE, photoLikeNotificationBuilder.build(photoLikePushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Photo Comment notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearAllPhotoCommentNotifications() {
        synchronized (photoCommentPushMessages) {
            photoCommentPushMessages.clear();
            updateNotification(ID_PHOTO_COMMENT, null);
        }
    }

    public void clearPhotoCommentNotifications(long competitorId) {
        synchronized (photoCommentPushMessages) {
            Iterator<PhotoCommentPushMessage> iterator = photoCommentPushMessages.iterator();
            while (iterator.hasNext()) {
                PhotoCommentPushMessage msg = iterator.next();
                if (msg.competitor_id == competitorId) {
                    iterator.remove();
                }
            }
            updateNotification(ID_PHOTO_COMMENT, photoCommentNotificationBuilder.build(photoCommentPushMessages));
        }
    }

    private void handlePhotoCommentPushMessage(@NonNull PhotoCommentPushMessage msg) {
        synchronized (photoCommentPushMessages) {
            photoCommentPushMessages.add(msg);
            updateNotification(ID_PHOTO_COMMENT, photoCommentNotificationBuilder.build(photoCommentPushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Comment Answer notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearAllCommentAnswerNotifications() {
        synchronized (commentAnswerPushMessages) {
            commentAnswerPushMessages.clear();
            updateNotification(ID_COMMENT_ANSWER, null);
        }
    }

    public void clearCommentAnswerNotifications(long competitorId) {
        synchronized (commentAnswerPushMessages) {
            Iterator<CommentAnswerPushMessage> iterator = commentAnswerPushMessages.iterator();
            while (iterator.hasNext()) {
                CommentAnswerPushMessage msg = iterator.next();
                if (msg.competitor_id == competitorId) {
                    iterator.remove();
                }
            }
            updateNotification(ID_COMMENT_ANSWER, commentAnswerNotificationBuilder.build(commentAnswerPushMessages));
        }
    }

    private void handleCommentAnswerPushMessage(@NonNull CommentAnswerPushMessage msg) {
        synchronized (commentAnswerPushMessages) {
            commentAnswerPushMessages.add(msg);
            updateNotification(ID_COMMENT_ANSWER, commentAnswerNotificationBuilder.build(commentAnswerPushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Banned & Unbanned user notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearAllUnBannedUserNotifications() {
        synchronized (unBannedUserPushMessages) {
            unBannedUserPushMessages.clear();
            updateNotification(ID_UNBANNED_USER, null);
        }
    }

    private void handleUnBannedUserPushMessage(@NonNull UnBannedUserPushMessage msg) {
        synchronized (unBannedUserPushMessages) {
            unBannedUserPushMessages.add(msg);
            updateNotification(ID_UNBANNED_USER, unBannedUserNotificationBuilder.build(unBannedUserPushMessages));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Verify user notifications
    ///////////////////////////////////////////////////////////////////////////

    public void clearAllVerifyUserNotifications() {
        synchronized (verifyUserPushMessages) {
            verifyUserPushMessages.clear();
            updateNotification(ID_VERIFY_USER, null);
        }
    }

    private void handleVerifyUserPushMessage(@NonNull UserVerifyPushMessage msg) {
        synchronized (verifyUserPushMessages) {
            verifyUserPushMessages.add(msg);
            updateNotification(ID_VERIFY_USER, verifyUserNotificationBuilder.build(verifyUserPushMessages));
        }
    }
}
