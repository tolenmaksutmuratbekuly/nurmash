package com.nurmash.nurmash.gcm;

import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.nurmash.nurmash.R;
import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.data.preferences.AuthPreferences;
import com.nurmash.nurmash.data.preferences.PushPreferences;
import com.nurmash.nurmash.model.exception.GooglePlayServicesNotAvailableException;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.nurmash.nurmash.util.rx.RxUtils.isActive;

public class PushRegistrationHelper {
    private static final int RETRY_COUNT = 5;

    private final Context context;
    private final DataLayer dataLayer;
    private final AuthPreferences authPreferences;
    private final PushPreferences pushPreferences;
    private Subscription sendTokenSubscription;

    public PushRegistrationHelper(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        this.authPreferences = dataLayer.authPreferences();
        this.pushPreferences = dataLayer.pushPreferences();
    }

    public void cancelOperationsInProgress() {
        cancelSendingToken();
    }

    public void updatePushToken() {
        if (pushPreferences.isPushNotificationsEnabled()) {
            pushPreferences.setPushTokenSentToServer(false);
            sendToken();
        }
    }

    public void registerForNotifications() {
        if (pushPreferences.isPushNotificationsEnabled() && !pushPreferences.isPushTokenSentToServer()) {
            sendToken();
        }
    }

    private synchronized void sendToken() {
        if (isActive(sendTokenSubscription)) {
            sendTokenSubscription.unsubscribe();
        }
        if (!authPreferences.isAuthenticated()) {
            return;
        }
        sendTokenSubscription = getGcmToken()
                .flatMap(new Func1<String, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(String token) {
                        return dataLayer.sendPushToken(token);
                    }
                })
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer attempt, Throwable throwable) {
                        // TODO: 3/26/16 Implement Exponential Backoff as suggested by Google.
                        return attempt < RETRY_COUNT && !(throwable instanceof GooglePlayServicesNotAvailableException);
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onNext(Void x) {
                        pushPreferences.setPushTokenSentToServer(true);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(new SubscriberErrorWrapper(e), null);
                    }
                });
    }

    private synchronized void cancelSendingToken() {
        if (sendTokenSubscription != null) {
            sendTokenSubscription.unsubscribe();
            sendTokenSubscription = null;
        }
    }

    public Observable<String> getGcmToken() {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
                if (result != ConnectionResult.SUCCESS) {
                    throw new GooglePlayServicesNotAvailableException(result);
                }
                InstanceID instanceID = InstanceID.getInstance(context);
                return instanceID.getToken(context.getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            }
        });
    }
}
