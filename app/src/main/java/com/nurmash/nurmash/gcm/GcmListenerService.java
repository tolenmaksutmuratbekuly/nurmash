package com.nurmash.nurmash.gcm;

import android.os.Bundle;

import com.nurmash.nurmash.data.DataLayer;
import com.nurmash.nurmash.model.push.PushMessage;

public class GcmListenerService extends com.google.android.gms.gcm.GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        DataLayer.getInstance(this).notificationsHelper().handlePushMessage(PushMessage.fromBundle(data));
    }
}
