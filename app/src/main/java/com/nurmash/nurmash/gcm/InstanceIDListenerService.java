package com.nurmash.nurmash.gcm;

import com.nurmash.nurmash.data.DataLayer;

public class InstanceIDListenerService extends com.google.android.gms.iid.InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        DataLayer.getInstance(this).pushRegistrationHelper().updatePushToken();
    }
}
