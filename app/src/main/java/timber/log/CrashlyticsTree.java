package timber.log;

import android.content.ActivityNotFoundException;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.nurmash.nurmash.data.social.facebook.FacebookProviderException;
import com.nurmash.nurmash.data.social.google.GoogleProviderException;
import com.nurmash.nurmash.data.social.twitter.TwitterProviderException;
import com.nurmash.nurmash.data.social.vk.VKProviderException;
import com.nurmash.nurmash.model.exception.ApiError;
import com.nurmash.nurmash.model.exception.UserCancelledRequestException;
import com.nurmash.nurmash.util.rx.SubscriberErrorWrapper;
import com.vk.sdk.api.VKError;

import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit.RetrofitError;

import static com.nurmash.nurmash.data.social.facebook.FacebookProviderException.ErrorCode.FACEBOOK_SDK_ERROR;

/**
 * This class is inside {@code timber.log} package because we want to be able to access some package protected methods,
 * such as {@link #getTag()}.
 */
public class CrashlyticsTree extends Timber.DebugTree {
    @Override
    protected boolean isLoggable(int priority) {
        return priority >= Log.WARN;
    }

    @SuppressWarnings({"RedundantIfStatement", "SimplifiableIfStatement"})
    protected boolean isLoggable(Throwable t) {
        if (t == null) {
            // Returns true to be able to log plain messages without a throwable.
            return true;
        } else if (t instanceof RetrofitError || t instanceof SubscriberErrorWrapper) {
            // Recursively detect if the cause is interesting enough to log it.
            return isLoggable(t.getCause());
        } else if (t instanceof FacebookProviderException) {
            return ((FacebookProviderException) t).errorCode == FACEBOOK_SDK_ERROR;
        } else if (t instanceof GoogleProviderException) {
            GoogleProviderException.ErrorCode errorCode = ((GoogleProviderException) t).errorCode;
            switch (errorCode) {
                case GOOGLE_SDK_ERROR:
                    // ActivityNotFoundException just means that Google+ app is not available on the device, so we
                    // don't really need to log this to Crashlytics. Otherwise, we want to know what kind of SDK error
                    // happened.
                    return !(t.getCause() instanceof ActivityNotFoundException);
                case GOOGLE_API_NOT_CONNECTED:
                    // Log this exception to see the reason why Google API Client failed to connect.
                    return true;
                default:
                    return false;
            }
        } else if (t instanceof VKProviderException) {
            return ((VKProviderException) t).getErrorCode() != VKError.VK_CANCELED;
        } else if (t instanceof SocketTimeoutException || t instanceof UnknownHostException) {
            // This happens pretty often because of bad connectivity issues, that's why we are not logging it.
            return false;
        } else if (t instanceof TwitterProviderException) {
            return ((TwitterProviderException) t).errorCode == TwitterProviderException.ErrorCode.FAILED;
        } else if (t instanceof UserCancelledRequestException || t instanceof InterruptedIOException) {
            // Not logging these exceptions because they usually happen when user somehow cancel the request.
            return false;
        } else if (t instanceof ApiError) {
            return ((ApiError) t).shouldReport();
        } else {
            // This exception might need a fix.
            return true;
        }
    }

    /**
     * A custom version of {@link timber.log.Timber.Tree#prepareLog(int, Throwable, String, Object...)} which doesn't
     * append stack trace to message string, because we use {@link Crashlytics#logException(Throwable)} to log the stack
     * trace to Crashlytics.
     */
    private void prepareLog(int priority, Throwable t, String message, Object... args) {
        if (!isLoggable(priority) || !isLoggable(t)) return;

        if (message == null || message.length() == 0) {
            if (t == null) return;
        } else {
            if (args.length > 0) message = String.format(message, args);
        }
        log(priority, getTag(), message, t);
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (!isLoggable(priority) || !isLoggable(t)) return;

        if (message != null) {
            Crashlytics.log(priority, tag, message);
        }
        if (t != null) {
            Crashlytics.logException(t);
        }
    }

    // Override every public method to use custom prepareLog which doesn't append throwable's stack trace to message.

    @Override
    public void v(String message, Object... args) {
        prepareLog(Log.VERBOSE, null, message, args);
    }

    @Override
    public void v(Throwable t, String message, Object... args) {
        prepareLog(Log.VERBOSE, t, message, args);
    }

    @Override
    public void d(String message, Object... args) {
        prepareLog(Log.DEBUG, null, message, args);
    }

    @Override
    public void d(Throwable t, String message, Object... args) {
        prepareLog(Log.DEBUG, t, message, args);
    }

    @Override
    public void i(String message, Object... args) {
        prepareLog(Log.INFO, null, message, args);
    }

    @Override
    public void i(Throwable t, String message, Object... args) {
        prepareLog(Log.INFO, t, message, args);
    }

    @Override
    public void w(String message, Object... args) {
        prepareLog(Log.WARN, null, message, args);
    }

    @Override
    public void w(Throwable t, String message, Object... args) {
        prepareLog(Log.WARN, t, message, args);
    }

    @Override
    public void e(String message, Object... args) {
        prepareLog(Log.ERROR, null, message, args);
    }

    @Override
    public void e(Throwable t, String message, Object... args) {
        prepareLog(Log.ERROR, t, message, args);
    }

    @Override
    public void wtf(String message, Object... args) {
        prepareLog(Log.ASSERT, null, message, args);
    }

    @Override
    public void wtf(Throwable t, String message, Object... args) {
        prepareLog(Log.ASSERT, t, message, args);
    }

    @Override
    public void log(int priority, String message, Object... args) {
        prepareLog(priority, null, message, args);
    }

    @Override
    public void log(int priority, Throwable t, String message, Object... args) {
        prepareLog(priority, t, message, args);
    }
}
